# 一.Java基础

## 1.Java运行环境

### <1>.Java程序的运行

Java是一门半编译型,半解释性语言.先通过javac编译程序把源文件进行编译,编译后生成的.class文件由字节码组成的平台无关,是面向JVM的文件,最后启动Java虚拟机运行.class文件,此时JVM会将字节码转换成平台能够理解的形式来运行

### <2>.JDK,JRE,JVM的区别

JRE:Java运行时的环境,包含了JVM,java基础类库.是使用Java语言编写程序运行时所需的环境

JDK:Java开发工具包,提供给Java程序员使用,包含了JRE,同时还包含了编译器javac与自带的调试工具Jconsole,jstack等

JVM:Java虚拟机,是运行Java字节码的虚拟机,通过编译.java文件为.class文件得到字节码文件,

.class文件包含JVM可以理解的字节码

## 2.基本数据类型

### <1>基本数据类型的种类及大小

整数类型：byte，1字节，8位，最大存储数据量是255，存放的数据范围是-128~127之间。

整数类型：short，2字节，16位，最大数据存储量是65536，数据范围是-32768~32767之间。

整数类型：int，4字节，32位，最大数据存储容量是2的32次方减1，数据范围是负的2的31次方到正的2的31次方减1。

整数类型：long，8字节，64位，最大数据存储容量是2的64次方减1，数据范围为负的2的63次方到正的2的63次方减1。

浮点类型：float，4字节，32位，数据范围在3.4e-45~1.4e38，直接赋值时必须在数字后加上f或F。

浮点类型：double，8字节，64位，数据范围在4.9e-324~1.8e308，赋值时可以加d或D也可以不加。

字符型：char，2字节，16位，存储Unicode码，用单引号赋值。

布尔型：boolean，只有true和false两个取值

## 3.方法

### <1>重载和重写的区别

方法的重写和重载都是实现多态的方式，而重载是编译时的多态性，重写是运行是的多态性。

重载发生在一个类中，同名的方法如果有不同的参数列表，则视为重载；重写发生在父类和子类之间，重写要求子类被重写的方法与父类被重写的方法有相同的返回类型，比父类的方法更好访问，不能比父类的方法声明更多的异常，而重载对于返回类型和异常没用太多的要求

重载无法不能根据返回类型来判断，如下：报错

```java
   public static void max(int a,int b){
        return;
    }
    public static int max(int a,int b){
        return Math.max(a,b);
    }
```



## 4.引用

### <1>对引用的简单理解

String s = "Hello World"; // 这里的 s 的类型是 String 类型的引用；该引用指向了一个

String 类型的对象。

int[] a = { 1, 2, 3, 4, 5 }; // 这里的 a 的类型是 int[] 类型的引用；该引用指向了一个

元素类型是 int 的数组类型对象。

int[] a = null; // a 不指向任何的对象

1.只有引用指向对象；没有对象指向引用，也没有引用指向引用，更没有对象指向对象。

2.对象可以被多个引用指向。

3.操作引用，其实操作的是引用指向的对象。



### <2>引用和内存泄漏

1.内存泄漏相关概念：

内存溢出：没有足够的内存提供申请者使用

内存泄漏：内存泄漏是指程序中已经动态分配的堆内存由于某种原因程序未释放或无法释放，造成了系统内存的浪费，导致程序运行速度减慢甚至崩溃等严重后果，内存泄漏的堆积终将会导致内存溢出.

2.强，弱，软，虚引用：

**强引用：**就是我们最常见的普通对象的引用，只要还有强引用指向一个对象，就能表明还有对象还"活着"，垃圾回收器就不会回收这种对象

**弱引用：**垃圾回收器一旦发现了只具有弱引用的对象，不管当前内存空间是否足够，都会回收它的内存.

****

## 5.类和对象

### <1>.类的创建过程



### <2>.抽象类和接口

**抽象类：**

抽象类：使用abstract修饰的类  ，抽象类拥有和普通类一样的定义，知识它不能被实例化

抽象方法：被abstract修饰的方法，只声明不实现，由子类实现。

例如：

```java
public abstract int A();
```

一个类中含有抽象方法（被abstract修饰），那么这个类一定是抽象类，但是一个抽象类中可以没有抽象方法。



**接口：**

接口是一个抽象类型，是抽象方法的集合。

接口中不能定构造器，不能有静态方法

接口中的方法默认是public abstrac

所有的成员变量一定是public final static



一个类只可以继承一个类，但是可以实现多个接口



### <3>.谈谈对抽象的理解

  首先，抽象我觉得是为了更快，更有质量解决问题。抽象可以让我们大脑有组织性的去记忆东西。例如：苹果，香蕉，饼干，哈密瓜，辣条，薯片，梨 我们可以通过分类去记，这样记忆就容易的多

  对于Java中抽象的理解：

Java中的抽象有两种，抽象类和接口，是Java面对对象设计的两个基础机制

接口是对行为的抽象，是抽象方法的集合，利用接口可以达到API实现分离的目的，它不包含任何非常量的变量，同时没有方法的实现

而抽象类是不能实例化的类，其主要目的是代码重用，除了不能实例化，其他和普通类没有多大的区别。



## 6.面对对象编程



## 7.String

### <1>.对字符串引用，字符串常量池的理解

**理解字符串不可变：**

字符串是一种不可变的对象，他的内容不可改变，String类的内部实现也是基于char[]来实现的，但是String类并没有提供set方法之类的来修饰内部的字符数组

**字符串常量池：**

jdk1.7中已经把字符串常量池从永久代中剥离出来，存放在堆空间中。

直接赋值：只会开辟一块堆内存空间，并且该字符串对象可以自动保存在对象池中以供下次使用。
构造方法：会开辟两块堆内存空间，不会自动保存在对象池中，可以使用intern()方法手工入池。
注意：一旦一个字符串常量，被存储到常量池当中后，只有一份，如果后续还有相同的字符串要存放的时候，首先需要看当前常量池中是否有该字符串常量

### <2>String，StringBuilder，StringBuffer

**首先来回顾下String类的特点：**

任何的字符串常量都是String对象，而且String的常量一旦声明不可改变，如果改变对象内容，改变的是其引用的指向而已。

通常来讲String的操作比较简单，但是由于String的不可更改特性，为了方便字符串的修改，提供StringBuﬀer和StringBuilder类。

StringBuﬀer 和 StringBuilder 大部分功能是相同的，但是StringBuffer采用同步处理，方法上都加了Synchronized，线程安全，但StringBuilder没有，线程不安全，但是单线程中StringBuilder效率更高

在String中使用"+"来进行字符串连接，但是这个操作在StringBuﬀer类中需要更改为append()方法

转换方法：
String变为StringBuﬀer:利用StringBuﬀer的构造方法或append()方法StringBuﬀer变为String:调用toString()方法。

StringBuilder和StringBuffer提供了字符串反转，字符串删除指定范围数据和插入数据等

String的equals的比较是比较内容，StringBuffer，StringBuilder的equals和==都比较的是身份



### <3>== 和equals的区别

equals和==最大的区别是：一个是方法，一个是运算符

==:如果比较的是基本数据类型（不含包装类），则比较数值是否相等。如果比较的是引用数据类型，则比较的是对象的地址是否相等。

equlas：用来比较方法的两个对象的内容是否相等（这里注意，如果比较内容，首先需要对equals方法进行重写，如果不重写，比较的仍然是地址）

Object类中equals源码：

```java
  public boolean equals(Object obj) {
        return (this == obj);
    }
```



## 8.异常

### <1>.异常体系

![](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210630233451815.png)

Throwable有两个子类：Error（错误）和Exception（异常）

Error：是程序中无法处理的错误，表示运行应用程序中出现了严重的错误。此类错误一般表示代码运行时JVM出现问题。

Exception（异常）：程序本身可以捕获并且可以处理的异常。而Exception又分为两类，编译时异常和运行时异常

运行时异常(不受检异常)：RuntimeException类极其子类表示JVM在运行期间可能出现的错误。比如说试图使用空值对象的引用（NullPointerException）、数组下标越界（ArrayIndexOutBoundException）。此类异常属于不可查异常，一般是由程序逻辑错误引起的，在程序中可以选择捕获处理，也可以不处理。

2、编译时异常(受检异常)：Exception中除RuntimeException极其子类之外的异常。如果程序中出现此类异常，比如说IOException，必须对该异常进行处理，否则编译不通过。在程序中，通常不会自定义该类异常，而是直接使用系统提供的异常类。

### <2>常见的运行时异常（RuntimeException）

1.空指针异常：NullPointerException 

2.类型强制转换异常：ClassCastException 

3.下标越界异常：IndexOutOfBoundsException 

4.数组下标越界异常：ArrayIndexOutOfBoundsException

5.方法未找到异常：NoSuchMethodException

6.SQL语句执行异常：SQLException 

7.类找不到异常：ClassNotFoundException 

# 二.数据结构

## 1.堆

### <1>向下调整

​				![image-20210714170858071](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210714170858071.png)

>这里以大根堆为例：
>
>思路：
>
>1.判断数组长度，如果只有一个元素，则不需要调整
>
>2.判断索引为index的结点是否是叶子结点，即判断这个完全二叉树是否有左孩子。如果没有，直接返回(左孩子的索引=index * 2 + 1)
>
>3.找到索引为index结点的最小左孩子
>
>4.判断索引为index的值是否小于等于最小的孩子,如果大于或等于，直接返回
>
>5.如果小于，则最小的孩子和index交换
>
>6.将最小孩子的索引minIndex视为index，循环回第2步

```java
   /**
     * 小根堆的向下调整
     * @param arr 待调整的数组
     * @param index 待调整的数组索引
     * @param size  数组的长度
     */
    public static void adjustDown(Integer[] arr,int index,int size){
        //1.数组长度小于1，不需要调整
        if(size < 2){
            return;
        }
        while (true) {
            //2.判断索引为in dex的结点是否是叶子结点(左孩子的索引index * 2 + 1)
            int leftIndex=index *2 + 1;
            if(leftIndex >= size){
                return;
            }
            //3.找到最小的孩子
            int minIndex=leftIndex;
            int rightIndex=leftIndex+1;//右孩子
            if(rightIndex < size && arr[rightIndex] < arr[leftIndex]){
                minIndex=rightIndex;
            }
            //4.判断：如果index位置的值比最小的孩子还要小或相等，直接返回
            if(arr[index] <= arr[minIndex]){
                return;
            }
            //5.如果index我i之的值要大，则与最小孩子的值交换
            int t=arr[index];
            arr[index]=arr[minIndex];
            arr[minIndex]=t;
            //5.将minIndex视为index，循环回去
            index=minIndex;
        }
    }
```

### <2>向上调整

>以小根堆为例：
>
>思路：
>
>1.判断数组个数，小于2，直接返回
>
>2.判断索引位置是不是根结点，如果是，直接返回
>
>3.比较index和parentIndex的值，如果array[index]>=array[parentIndex],调整结束
>
>4.否则交换
>
>5.将parentIndex视为index，继续循环

```java
public static void adjustUp(Integer[]array,int size,int index){
    //1.判断数组个数，小于2，直接返回
    if(size < 2){
        return;
    }
    while (true) {
        //2.判断索引位置是不是根结点，如果是，直接返回
        int parentIndex= (index-1)/2;//index父亲结点的索引
        if(parentIndex < 0){
            return;
        }
        //3.比较index和parentIndex的值，如果array[index]>=array[parentIndex],调整结束
        if(array[index] >= array[parentIndex]){
            return;
        }
        //4.否则交换
        int t=array[index];
        array[index]=array[parentIndex];
        array[parentIndex]=t;

        //5.将parentIndex视为index，继续循环
        index=parentIndex;
    }
}
```

### <3>建堆

>思路：
>
>1.判断：如果数组中只有一个元素，直接返回
>
>2.找到完全二叉树的最后一个结点
>
>3.找到完全二叉树的最后一个父亲结点
>
>4.从最后一个父节点开始，一直到根结点，都进行向下调整 [lastParentIndex,0]

```java
/**
 * 建堆
 * @param array
 * @param size
 */
public static void buildHeap(int[] array,int size){
    //1.如果数组只有一个元素，直接返回
    if(size < 2){
        return;
    }
    //2.找到最后一个结点
    int lastIndex=size-1;
    //3.找到最后一个父结点
    int lastParentIndex=(lastIndex - 1)/2;
    //4.从最后一个父节点开始，一直到根结点，都进行向下调整 [lastParentIndex,0]
    for (int i = lastParentIndex; i >=0 ; i--) {
        adjustDown(array,i,size);
    }
}
```

>时间复杂度：
>
>最好时O（n），最坏时O（n*logn）

## 2.优先级队列（利用堆）

1.添加元素：

>1.如果数组为空，直接添加
>
>2.添加到数组的最后一个元素，进行一次向上调整

2.删除元素

>  * 1.判断数组元素个数是否小于2
>     * 2.将数组的第一个元素array[0]取出
>     * 3.将最后一个元素array[size-1]放到第一个数的位置
>     * 4.对第一个数进行一次向下调整

```java
public class MyPriorityQueue {
    private Integer array[];
    private int size;

    public MyPriorityQueue() {
        array=new Integer[100];//这里为了简单期间，不考虑扩容的情况
    }

    //添加元素
    public void addElement(Integer element){
        //1.如果数组为空，直接添加
        if(size == 0){
            array[0]=element;
            size++;
            return;
        }
        //2. 添加到数组的最后一个元素，进行一次向上调整
        array[size]=element;
        size++;
        adjustUp(array,size,size-1);
    }

    /**
     * 删除队头元素，并返回
     * 1.判断数组元素个数是否小于2
     * 2.将数组的第一个元素array[0]取出
     * 3.将最后一个元素array[size-1]放到第一个数的位置
     * 4.对第一个数进行一次向下调整
     * @return
     */
    public Integer remove(){
        //1.判断队列中元素个数是否小于2
        if(size == 0){
            throw new RuntimeException("队列已经为空");
        }
        if(size == 1){
            return array[0];
        }
        //2.将数组的第一个元素array[0]取出
        int temp=array[0];
        //3.将最后一个元素array[size-1]放到第一个数的位置
        array[0]=array[size-1];
        size--;
        //4.对第一个数进行一次向下调整
        adjustDown(array,0,size);
        //5.返回temp
        return temp;
    }
    }
```



## 3.Hash表

>元素类型，使用Integer
>使用拉链法解决冲突

```java
 class Node {
    public Integer key;
    Node next;

public Node(Integer key) {
    this.key = key;
}
```

```java
public class MyHashTable {
//1.数组（存储链表的头节点）
private Node[] array=new Node[11];

//2.维护hash表中元素的个数
private int size;

//true:key之前不在hash表中
//false:key之前在hash表中
public boolean insert(Integer key) {
    //1.把对象转成int类型，调用类的hashCode（）方法
    int hashValue = key.hashCode();
    //2.把hashValue转成合法的下标
    int index = hashValue % array.length;
    //3.遍历index下标的链表，看看key在不在链表中
    Node current = array[index];
    while (current != null) {
        if (key.equals(current.key)) {
            return false;
        }
        current = current.next;
    }
    //4.把key装入结点中，并插入到对应的链表中
    //头插
    Node node = new Node(key);
    node.next = array[index];
    array[index] = node;
    //5.维护元素个数
    size++;

    return true;
}

public boolean contains(Integer key){
    int hashValue=key.hashCode();
    int index=hashValue%array.length;
    Node current=array[index];
    while (current!=null){
        if(current.key.equals(key)){
            return true;
        }
        current=current.next;
    }
    return false;
}

//true代表key在哈希表中，且删除成功
//false代表key不在哈希表中，删除失败
public boolean remove(Integer key){
    int hashValue=key.hashCode();
    int index=hashValue%array.length;
    Node current=array[index];
    Node parent=null;
    while (current!=null){
        if(current.key.equals(key)){
            break;
        }
        parent=current;
        current=current.next;
    }
    if(current==null){
        //不存在key
        return false;
    }else if(parent==null){
        //current是头节点
        array[index]=array[index].next;
        size--;
        return true;
    }else {
        parent.next=current.next;
        size--;
        return true;
    }
}
}
```
## 4.平衡搜索树

### <1>AVL树

>AVL树：二叉平衡搜索树
>
>相对于普通搜索树的优势：由于平衡，不会退化成单支树的情况，所以可以保持稳定的时间复杂度O(logn)

​	**结点的结构：**

```java
//1.使用泛型,且带Value
private static class Node1<K,V>{
    Node1<K,V> left;
    Node1<K,V> right;
    K key;
    V value;
    int bf;//平衡因子
    Node1<K,V> parent;
}
//2.不使用泛型，不带value
private static class Node2{
    Node2 left;
    Node2 right;
    int value;
    int bf;//平衡因子
    Node2 parent; 
}
```



**AVL树的插入操作：**

插入前满足AVL树 ----->插入中可能破坏AVL树的结构----->插入完成后需满足AVL树的结构

[https://www.cnblogs.com/skywang12345/p/3577479.html](https://www.cnblogs.com/skywang12345/p/3577479.html)

### <2>红黑树

原理和算法：

[https://www.cnblogs.com/skywang12345/p/3245399.html]()

Java实现：

[https://www.cnblogs.com/skywang12345/p/3624343.html#a3]()



# 三.算法

## 1.七大基于比较的排序算法

### <1>选择排序

>算法思想： 对于数组array
>
>index记录无序区间开始的位置:  有序区间[0,index)  无序区间[index,array.length - 1]
>
>1.记录无序区间的开始位置,即最小元素插入位置
>
>2.遍历数组，依次从无序区间找到最小的数的下标索引
>
>3.交换index和minIndex下标的值
>
>4.无序区间往后移动一位

```java
public static void selectSort(int[] array){
    //1.判断数组
    if(array == null || array.length == 0){
        return;
    }
    //2.记录无序区间的开始位置,即最小元素插入位置
    int index=0;
    //3.遍历数组，依次从无序区间找到最小的数的下标索引
    for (int i = 0; i < array.length ; i++) { //第一层循环：记录数组中数据应插入的位置
        int minIndex=index;//记录最小值的下标
        for (int j = index; j < array.length ; j++) {//第二层循环：从无序区间的开始位置index开始找，找无序区间最小数的下标
            if(array[minIndex] > array[j]){
                minIndex=j;
            }
        }
        //4.交换index和minIndex下标的值
        int temp=array[index];
        array[index]=array[minIndex];
        array[minIndex]=temp;
        //5.无序区间往后移动一位
        index++;
    }
}
```

### <2>插入排序

>算法思想：对于数组array
>
>index记录无序区间开始的位置  有序区间：[0,index)  无序区间：[index,array.length)
>
>遍历数组，将遍历到的元素依次插入到有序区间中去

```java
public static void insertSort(int[] array){
    //1.数组判断
    if(array == null || array.length == 0){
        return;
    }
    //2.记录无序区间开始的位置  有序区间：[0,index)  无序区间：[index,array.length)
    int index=1;
    for (int i = 1; i < array.length ; i++) {//第一层循环，排序的个数
        index = i;
        for (int j = index; j > 0 ; j--) { //第二层循环，和有序区间做比较
            //当前j索引值小于前一个，交换
            if(array[j] < array[j - 1]){
                int t=array[j];
                array[j]=array[j-1];
                array[j-1]=t;
            }else {
                break;
            }
        }
    }
}
```

### <3>冒泡排序

>算法思想:对于数组array
>
>index记录无序区间结束的位置 有序区间(index,array.length),无序区间[0,index]
>
>从索引0开始，向右依次比较，较大的数放在数组后面

```java
public static void bubblingSort(int[] array){
    //1.判断数组是否合法
    if(array == null || array.length == 0){
        return;
    }
    int index=array.length - 1; // 记录无序区间结束的位置 有序区间(index,array.length),无序区间[0,index]
    for (int i = 0; i < array.length ; i++) { // 第一层循环，元素个数
        for (int j = 0; j < index; j++) {//第二层循环，将无序区间中的最大值移动到有序区间的开始的前一个位置
            //如果前一个值大于后一个，交换
            if(array[j] > array[j+1]){
                int t=array[j];
                array[j]=array[j+1];
                array[j+1] = t;
            }
        }
        //有序区间位置向前移动一个
        index--;
    }
}
```

### <4>堆排序

>算法思想：对于数组array
>
>建大堆,将堆的根结点放入数组后面 ，与无序区间的最后一个数交换，在对根结点进行依次向下调整
>
>index记录无序区间结束的位置 i 无序区间[0,index]    有序区(index,array.length - 1)

```java
 /**
  * 建大堆,将堆的根结点放入数组后面，index记录无序区间结束的位置 index=array.length - 1
  * 无序区间[0,index]
  * 有序区间(index,array.length - 1)
  * @param array
  */
public static void heapSort(int[] array){
    //1.判断数组的合法性
    if(array == null || array.length == 0){
        return;
    }
    //2.建大堆
    BuildHeap(array,array.length);
    //3.将堆顶array[0]的元素放到无序区间的末尾
    for (int i = 0; i < array.length ; i++) { //元素的个数
        swap(array,0,array.length-1-i);//将堆顶元素放到无序区间的末尾，并将末尾的数放回堆顶
        adjust(array,0,array.length-1-i);//对堆顶元素进行依次向下调整
    }
}

//建大根堆
 private static void BuildHeap(int[] array, int length) {
    //1.如果长度小于2，不用建
     if(length <= 2){
         return;
     }
    //2.找到最后一个结点元素
     int lastNode=length-1;
     //3.找到最后一个父亲结点
     int lastParentNode=(lastNode - 1) / 2;
     //4.从最后一个父结点开始，一直到根结点，都进行依次向下调整
     for (int i = lastParentNode; i >=0 ; i--) {
         adjust(array,i,length);
     }
 }

 //大根堆的向下调整
 private static void adjust(int[] array, int index, int size) {
    //1.对数据进行判断  长度小于2，无序调整
     if(size < 2){
         return;
     }
     while (true) {
         //2.如果这个结点是叶子结点  (堆是一个完全二叉树，如果不是叶子结点，一定有左孩子)
         int leftChild=index * 2 + 1;
         if(leftChild >= size){
             return;
         }
         //3.找到index的最大孩子
         int maxIndex=leftChild;
         int rightChild=leftChild+1;
         if(rightChild < size && array[rightChild] > array[leftChild]){
             maxIndex=rightChild;
         }
         //4.如果index的值大于等于最大的孩子，调整结束
         if(array[index] >= array[maxIndex]){
             return;
         }
         //5.否则交换
         swap(array,index,maxIndex);
         //6.将maxIndex视为index，循环回去
         index=maxIndex;
     }

 }

 private static void swap(int[] array, int i, int j) {
    int t=array[i];
    array[i]=array[j];
    array[j]=t;
 }
```

### <5>希尔排序

>算法思想：带间隔的插入排序
>
>1.将数组分为gap组
>
>2.认为每组的第一个数是有序的，所以从数组下标为gap的地方开始进行分组插排
>
>3.每次进行完分组插排，gap减半，直到为1(到1时，还要进行依次插排)，然后结束	

```java
public static void shellSort(int[] array){
    //1.数组合法性判断
    if(array == null || array.length == 0){
        return;
    }
    //2.分为gap组
    int gap=array.length/2;
    //3.每次组数减少一半，直到1
    while (true){
        //4.分组插排
        insertSortWithGap(array,gap);
        if(gap == 1){
            return;
        }
        gap=gap/2;
    }
}

//分组插排
private static void insertSortWithGap(int[] array, int gap) {
    for (int i = gap; i < array.length ; i++) {//表示需要进行插排的个数
        //1.刚开始时，认为每组的第一个数是有序的，所以从gap索引开始
        int key=array[i];
        for (int j = i; j >= gap ; j=j-gap) {//从第i个数开始
            if(key < array[j-gap]){
                swap(array,j,j-gap);
            }else {
                break;
            }
        }
    }
}

private static void swap(int[] array, int j, int i) {
    int t=array[j];
    array[j]=array[i];
    array[i]=t;
}
```

### <6>快速排序

>算法思想：
>
>先定一个基准值，比他小的数放左边，比他大的数放右边，然后再以左右区间为新数组递归
>
>1.选择第一个数为基准，比他小的在左边，比他大的在右边
>
>2.设立左指针和右指针，右指针先动，遇到第一个比key小的值停下，再动左指针，遇到第一个比key大的停下，直到left>=right
>
>3.选择左侧为基准时，一定让右指针先动，这样当left=right时，交换基准值和right，right一定比基准值小，去了数组开头
>
>4.交换基准值和左右指针相遇的位置
>
>5.递归左右区间

```java
public static void quickSort(int[] array){
    //1.数组合法性判断
    if(array == null || array.length == 0){
        return;
    }
    //2.快排
    _quickSort(array,0,array.length-1);
}

private static void _quickSort(int[] array, int lowIndex, int highIndex) {
    //1.递归结束条件(待排序数组长度小于2)
   if(highIndex-lowIndex+1 < 2){
       return;
   }
    //2.选择第一个数为基准，比他小的在左边，比他大的在右边
    int key=array[lowIndex];
    //3.设立左指针和右指针，右指针先动，遇到第一个比key小的值停下，再动左指针，遇到第一个比key大的停下，直到left>=right
    int leftIndex=lowIndex+1;
    int rightIndex=highIndex;
    while (leftIndex<rightIndex){
        //4.选择左侧为基准时，一定让右指针先动，这样当left=right时，交换基准值和right，right一定比基准值小，去了数组开头
       while (true){
           if(array[rightIndex] < key){
               break;
           }else {
               rightIndex--;
           }
       }
       while (leftIndex < rightIndex){
           if(array[leftIndex] > key){
               break;
           }else {
               leftIndex++;
           }
       }
        swap(array,leftIndex,rightIndex);
    }
    //5.交换基准值和左右指针相遇的位置
    swap(array,lowIndex,rightIndex);
    //6.递归左右区间
    _quickSort(array,lowIndex,rightIndex-1);
    _quickSort(array,rightIndex+1,highIndex);
}

private static void swap(int[] array, int leftIndex, int rightIndex) {
    int t=array[leftIndex];
    array[leftIndex]=array[rightIndex];
    array[rightIndex]=t;
}
```

### <7>归并排序(二路归并)

>算法思想：
>
>将数组每次等分两份，递归，直到数组长度小于2，然后两个等分数组有序，合并两个有序数组
>
>1.递归结束条件，当数组元素个数小于2时
>
>2.找到数组中间索引
>
>3.递归，分为两个数组
>
>4.合并两个有序数组:第一个数组[start,middle)  第二个数组[middle,end)
>
>5.将合并后的数组复制到array的相应位置

```java
public static void mergeSort(int[] array){
    //1.判断数组下标合法性
    if(array == null || array.length == 0){
        return;
    }
    //2.分成左右两边两个数组，进行二路归并
    _mergeSort(array,0,array.length);
}

private static void _mergeSort(int[] array, int start, int end) {
    //1.递归结束条件，当数组元素个数小于2时
    if(end-start < 2){
        return;
    }
    //2.找到数组中间索引
    int middle=(end+start)/2;
    //3.递归，分为两个数组
    _mergeSort(array,start,middle);
    _mergeSort(array,middle,end);
    //4.合并两个有序数组
    mergeTwoSortedArray(array,start,middle,end);
}

private static void mergeTwoSortedArray(int[] array, int start, int middle, int end) {
    //1.第一个数组[start,middle)  第二个数组[middle,end)
    //2.从start到end排序后的数组
    int[] temp=new int[end-start];
    int newIndex=0;
    //3.合并两个有序数组
   int oneIndex=start;
   int twoIndex=middle;
   while (oneIndex < middle && twoIndex <end){
       if(array[oneIndex] < array[twoIndex]){
           temp[newIndex]=array[oneIndex];
           oneIndex++;
           newIndex++;
       }else {
           temp[newIndex]=array[twoIndex];
           twoIndex++;
           newIndex++;
       }
   }
   if(oneIndex < middle){
       for (int i = oneIndex; i <middle ; i++) {
           temp[newIndex]=array[i];
           newIndex++;
       }
   }
   if(twoIndex <end){
       for (int i = twoIndex; i <end ; i++) {
           temp[newIndex]=array[i];
           newIndex++;
       }
   }
    //4.将temp复制到array的相应位置
    for (int i = 0; i < temp.length ; i++) {
        array[i+start]=temp[i];
    }
}
```

### <8>性能总结

![img](https://img-blog.csdnimg.cn/2021022122450480.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20210221224518574.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

## 2.海量数据的查询，排序

哈希表 VS 平衡搜索树：

哈希表简单，速度快，更加容易实现线程安全

平衡搜索树，它维护的key是有序的，不会出现极端情况（哈希极度冲突的情况）

**海量数据查找：**

### <1>位图

例如：40亿（内存放不下）无符号不同整数，查找一个数n（很频繁查不同的n）

**前提：**

>1.无符号整数(>=0)，是一个合法的数组下标
>
>2.不重复->如果直接把数看成下标，绝对不会冲突

40亿个数，如果直接用int存，内存存不下，可以缩小每个数据占用的空间，进而使得整个集合占用的空间减少。

而仅仅表示数n在不在这40亿的集合内，只有一个二元信息，在OR不在，所以可以使用bit的0，1来表示

**伪代码：**

>bit table[]=new bit[40亿中最大的数];
>
>直接把每个数看作下标，一次遍历，把数据放入哈希表中
>
>0表示存在，1表示不存在
>
>每次查找只需找到对应哈希表的位置查看是0还是1

但是主流的编程语言都不支持bit[]数组，就需要我们自己实现，如图：

![image-20210715103244996](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210715103244996.png)

我们可以定义一个byte[]数组，每个byte是八个bit位，如上图，每个bit表示一个数是否存在.然后利用位运算，可以改变某一个二进制位对应的状态.

**例如:将22放入集合中**

>22/8=2  22%8=6  所以应改变第2个Byte位的第6个bit位为1

改变bit位的方法(利用位运算):

>1.byte x：只修改x的第3个bit位为1，其他bit不变   如：
>
>7  6  5  4  3  2  1  0  存储数据的位置
>
>_  _  _  _  _  _  _  _这些数据原本的二进制位
>
>0  0  0  0  1  0  0  0要修改的位置与1或，其他与0或，这样只会改变一个位置为1
>
>所以位运算为：x | (1 << y)

>2.byte x :只修改x的第3个bit位为0，其他bit不变 如：
>
>7  6  5  4  3  2  1  0 存储数据的位置
>
>_  _  _  _  _  _  _  _这些数据原本的二进制位
>
>1  1  1  1  0  1  1  1要修改的位置与0与，其他与1与，这样只会改变一个位置为0
>
>而：1 1 1 1 0 1 1 1是~(1<<3)
>
>所以位运算：x & (~ ( 1 << y ) )



**位图的实现：**Java8中的实现BitSet，这里使用伪代码

>```java
>byte[] table=new byte[...];
>
>public void put(int n){
>    int index=n/8;
>    int bit=n%8;
>    table[index] = (byte) (table[index] | (1 << bit));
>}
>
>public void remove(int n){
>    int index=n/8;
>    int bit=n%8;
>    table[index] = (byte) (table[index] & (~(1 << bit)));
>}
>
>public boolean contains(int n){
>    int index=n/8;
>    int bit=n%8;
>    return (table[index] & (1 << bit)) != 0 ;
>}
>```

**位图的限制：**	

1.只适合元素是否存在的问题

2.对集合中的元素有要求（是数字且大于等于0，数字尽可能连续）

### <2>布隆过滤器

布隆过滤器是由布隆（Burton Howard Bloom）在1970年提出的 一种紧凑型的、比较巧妙的概率型数据结

构，特点是高效地插入和查询，可以用来告诉你 “某样东西一定不存在或者可能存在”，它是用多个哈希函

数，将一个数据映射到位图结构中。此种方式不仅可以提升查询效率，也可以节省大量的内存空间。

**限制：**

1.只适合元素是否存在的问题

2.只能保证概率的正确性

3.对元素没有要求了

​	

**举例一个应用的场景:**

>当我们在使用新闻客户端看新闻，刷抖音时，它会不断的给我们推荐新的内容，每次推荐都需要去重，去除那些已经看过的内容。
>
>元素：新闻，视频
>
>集合：已经给你推送过的新闻，视频
>
>由于不允许重复推送，所以，每次需要检查每个新闻是否已经被推送过-->检查元素是否在集合中存在了



**布隆过滤器的内部结构:**

![image-20210715133048552](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210715133048552.png)

![image-20210715132746239](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210715132746239.png)

1.把一个元素放入到该集合中：

>1.把元素，分别经过n个哈希函数进行计算，并转化为位图的合法的bit "下标"
>
>2.把所有"下标"处的位图位置改为1

2.判断给定的元素是否在该集合中

>1.把元素，分别经过n个哈希函数进行计算，并转化为位图的合法位置
>
>2.如果所有位置中，有0，则标识元素一定不在集合中
>
>3.如果所有位置，都是1，则大概率，元素在集合中
>
>4.但也有可能，这个元素不在集合中，但是标志位因为其他元素都改变为了1(这种情况称为假阳性)

3.删除元素

>不能再布隆过滤器中删除元素，删除元素改变了位图中下标的状态，可能对其他数据产生影响
>如果真要删除，可以使用计数方式删除



**设计多个hash函数的原因：**

>1.由于元素不限，所有，必然有着hash冲突的情况。(不同的元素，映射到了相同的下标出)
>
>2.在这个情况下，hash函数个数太少，假阳性概率就会特别高，所有通过添加多个hash函数，使得假阳性概率降低



### <3>Hash分组思想

例如：给一个超过100G大小的log fifile, log中存着IP地址, 设计算法找到出现次数最多的IP地址？

利用hash，对100个G的文件进行分组，然后先在每一组中找出现次数最多的数字，再将各个组出现最多的数字进行比较，最后找到出现次数最多的数字



### <4>Topk问题

例如：在5TB的文件中，找出最大的十个数

>思路：建立一个小根堆，维护十个数据，将5TB文件中的数据依次和堆顶元素比较，如果比堆顶元素大，插入，调整堆。直到最后剩下的十个数据就是最大的十个

**形象化记忆：**

![img](https://img-blog.csdnimg.cn/20210217233248672.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

### <5>海量数据排序

海量数据排序(内存放不下,要借助硬盘)---->多路归并排序

![image-20210716161825849](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210716161825849.png)

>思路：将海量数据文件分成多份，没份可以达到内存允许，进行普通的排序方法，将每份放入一个文件中，然后按打擂台的方式，每个文件排除代表，较小的放入最终的结构文件中，直到排完
>
>步骤：
>
>1.先将数据平均分成n份
>
>2.分别对每份数据进行排序(例如快速，归并等排序算法)
>
>3.至此，得到了n个分别有序的数据
>
>4.借助内存，进行n个有序数据文件的合并
>
><1>将每份文件中最小的整数放入内存中(实践中，可以放入不止一个)
>
><2>将其中最小的数选出来，尾插到最后的有序文件中

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210221232953218.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

### <6>对海量数据的处理总结

**思路：**

>查询：
>
>1.分文件--->Hash切割，分组的思想
>
>2.利用压缩信息(位图，布隆过滤器)
>
>3.Topk问题
>
>排序：
>
>多路归并排序



# 四.Java集合框架

**集合框架总览：**

![image-20210702002526843](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702002526843.png)

分为两大类：Collection和Map
Collection 接口存储一组不唯一，无序的对象
List 接口存储一组不唯一，有序（索引顺序）的对象
Set 接口存储一组唯一，无序的对象
Map 接口存储一组键值对象，提供 key 到 value 的映射，Key 唯一 无序，value 不唯一 无序

## 1.Collection集合

### <1>.Collection集合的遍历（这里以ArrayList为例）

```java
package org.example.data_structure.collection.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        //方法一，使用for循环（这种方法Set集合不适用）
        for (int i = 0; i <list.size() ; i++) {
            System.out.println(list.get(i));
        }

        //方法二：增强for循环
        for(Integer elem:list){
            System.out.println(elem);
        }

        //方法三：迭代器实现遍历（第一种和第二种的底层也是使用迭代器实现
        Iterator<Integer> integerIterator=list.iterator();
        while (integerIterator.hasNext()){
            Integer elem=integerIterator.next();
            System.out.println(elem);
        }
        //方法四：Lambda表达式+流式编程
        list.forEach((elem)->System.out.println(elem));
        //或者
        list.forEach(System.out::println);
    }
}
```

### <2>List

List 集合的主要实现类有 ArrayList 和 LinkedList，分别是数据结构中顺序表和链表的 实现。另外还包括栈和队列的实现类：Deque 和 Queue。
特点：有序，不唯一

#### (1)ArrayList

在内存中分配连续的空间，实现了长度可变的数组
• 优点：遍历元素和随机访问元素的效率比较高
• 缺点：添加和删除需大量移动元素效率低，按照内容查询效率低

ArrayList的部分源码分析：
ArrayList 底层就是一个长度可以动态增长的 Object 数组

```java
  public class ArrayList<E> extends AbstractList<E> implements
                List<E>, RandomAccess, Cloneable, Serializable {
            private static final int DEFAULT_CAPACITY = 10;
            private static final Object[] EMPTY_ELEMENTDATA = {};
            private static final Object[]
                    DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
            transient Object[] elementData;
            private int size;
        }
```

JDK1.7中，使用无参数构造方法创建ArrayList对象时，默认底层数组长度是10。
JDK1.8中，使用无参数构造方法创建ArrayList对象时，默认底层数组长度是0；

jdk1.8中，当第一次添加元素时候，如果容量不足则需要扩容

•容量不足时进行扩容，默认扩容50%。如果扩容50%还不足容纳新增元素，就扩容为能容纳新增元素的最小数量。
源码如下：

```java
private void grow(int minCapacity) {
    int oldCapacity = elementData.length;
    int newCapacity = oldCapacity + (oldCapacity >> 1);
    if (newCapacity - minCapacity < 0)
        newCapacity = minCapacity;
    if (newCapacity - MAX_ARRAY_SIZE > 0)
        newCapacity = hugeCapacity(minCapacity); 
    elementData = Arrays.copyOf(elementData, newCapacity);
}
```

使用移位操作，将容量扩充百分之50，如果还不够，则扩充为最小需要的容量,然后使用Arrays.copyof扩充



#### (2)LinkedList

采用双向链表存储方式。
缺点：遍历和随机访问元素效率低下
优点：插入、删除元素效率比较高

![image-20210702003352829](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702003352829.png)

LinkedList的源码分析：
LinkedList的底层是一个双向链表，其中有两个成员变量分别指向链表的头结点和尾结点

```java
public class LinkedList<E>
    extends AbstractSequentialList<E>
    implements List<E>, Deque<E>, Cloneable, java.io.Serializable{
    transient int size = 0;//节点的数量
    transient Node<E> first; //指向第一个节点
    transient Node<E> last; //指向最后一个节点
    public LinkedList() {  }
 }
```

![image-20210702003505645](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702003505645.png)

```java
private static class Node<E> {
    E item;//存储节点的数据
    Node<E> next;//指向后一个节点
    Node<E> prev; //指向前一个节点
    Node(Node<E> prev, E element, Node<E> next) {
        this.item = element;
        this.next = next;
        this.prev = prev;
    }
}
```

LinkedList也实现了Queue接口，Deque接口，可以用作队列和栈使用

### <3>.Set

特点：无序，唯一（不重复）

#### （1）.HashSet

采用Hashtable哈希表存储结构
优点：添加速度快 查询速度快 删除速度快
缺点：无序

![image-20210702003648106](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702003648106.png)

#### （2）.LinkedHashSet

也是使用Hash表存储结构，加上链表来维护元素添加顺序

![image-20210702003714503](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702003714503.png)

#### （3）.TreeSet

采用二叉树（红黑树）的存储结构
优点：有序 查询速度比List快（按照内容查询）
缺点：查询速度没有HashSet快

![image-20210702003856349](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702003856349.png)

## 2.Hash表

简单来说
Hash表可以按照内容查找，不进行比较，而是通过计算得到地址，实现类似数组按照索引查询的高效率O（1）

Hash表的结构：主结构：顺序表，每个顺序表的节点在单独引出一个链表

![image-20210702004025317](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702004025317.png)

Hash是如何添加数据的？
1)计算哈希 码(调用hashCode(),结果是一个int值，整数的哈希码取自身即可)
2)计算在哈希表中的存储位置 y=k(x)=x%11（哈希函数）
x:哈希码 k(x) 函数y：在哈希表中的存储位置
3)存入哈希表
情况1：一次添加成功
情况2：多次添加成功（出现了冲突，调用equals()和对应链表的元素进行比较，比较到最后，结果都是false，创建新节点，存储数据，并加入链表末尾）
情况3：不添加（出现了冲突，调用equals()和对应链表的元素进行比较， 经过一次或者多次比较后，结果是true，表明重复，不添加）
结论1：哈希表添加数据快（3步即可，不考虑冲突）
结论2：唯一、无序

hashCode和equals到底有什么神奇的作用？
hashCode():计算哈希码，是一个整数，根据哈希码可以计算出数据在哈希表中的存储位置
equals()：添加时出现了冲突，需要通过equals进行比较，判断是否相同；查询时也需要使用equals进行比较，判断是否相同

hashCode的获取： 尽量让不同的对象的Hash值不同

装填因子： 装填因子=表中的记录数/哈希表的长度 装填因子越大，发生冲突几率越大，0.5为最佳



## 3.Map

**Map 特点：**存储的键值对映射关系，根据 key 可以找到 value

Map的遍历:

```java
   //map的遍历
        //1. 获取所有的key
        Set<String> set=map.keySet();
        for(String key : set){
            System.out.println(key+":"+map.get(key));
        }

        //2.使用entrySet
        Set<Map.Entry<String,String>> entrySet=map.entrySet();
        Iterator<Map.Entry<String,String>> iterator=entrySet.iterator();
        while (iterator.hasNext()){
            Map.Entry<String,String> it=iterator.next();
            System.out.println(it.getKey()+":"+it.getValue());
        }
```

结构图和上述Set类似

• HashMap
• 采用 Hashtable 哈希表存储结构（神奇的结构）
• 优点：添加速度快 查询速度快 删除速度快
• 缺点：key 无序

• LinkedHashMap
• 采用哈希表存储结构，同时使用链表维护次序
• key 有序（添加顺序）

• TreeMap
• 采用二叉树（红黑树）的存储结构
• 优点：key 有序 查询速度比 List 快（按照内容查询）
• 缺点：查询速度没有 HashMap 快

## 4.内部比较器和外部比较器

### <1>.内部比较器（实现了Comparable接口）

外部比较器可以让类实现Comparable接口，重写compareTo方法但是他的判断条件有限，只能有一个
例如：以下学生类按照name进行排序(升序）

```java
public class Student implements Comparable<Student>{
    private String name;
    private String sex;
    private int age;
    private double score;

    @Override
    public int compareTo(Student other) {
        return this.name.compareTo(other.name);
        //return this.age - other.age;
    }
    }
```

### <2>.外部比较器(实现了Comparator接口）

需要一个新的类，实现Comparator接口，重写compare方法，可以指定多个排序条件
还是以上述Student类为例，建立一个新的类

```java
import java.util.Comparator;

public class StuScoreNameDescComparator implements Comparator<Student> {
    @Override
    public int compare(Student stu1, Student stu2) {
        if(stu1.getScore()>stu2.getScore()){
            return 1;
        }else if(stu1.getScore()<stu2.getScore()){
            return -1;
        }else{
           // return 0;
            return -stu1.getName().compareTo(stu2.getName());
        }
        //return  (int)(stu1.getScore()-stu2.getScore()); // 98.5 -98 =0.5 --->0
    }
}
```

## 5.Map和Set的源码分析

Set的底层是使用Map实现的

### <1>.HashMap和HashSet

#### (1).HashMap

HashMap的源码：

JDK1.7 及其之前，HashMap 底层是一个 table 数组+链表实现的哈希表存储结构，而jdk1.8采用table数组+链表/红黑树如图1.7的存储结构：

![image-20210702004503760](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702004503760.png)

jdk1.8的存储结构：
当链表的存储数据个数大于等于 8 的时候，不再采用链 表存储，而采用红黑树存储结构。这么做主要是查询的时间复杂度上，链表为 O(n)， 而红黑树一直是 O(logn)。如果冲突多，并且超过 8，采用红黑树来提高效率，而当结点减少，小于等于6时，会重新变回链表结构
![image-20210702004524047](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702004524047.png)

链表中每一个结点Entry，结构如下：

```java
static class Entry<K, V> implements Map.Entry<K, V> {
    final K key; //key
    V value;//value
    Entry<K, V> next; //指向下一个节点的指针
    int hash;//哈希码
}
```

HsahMap主要成员：

```java
  public class HashMap<K, V> implements Map<K, V> {
            //哈希表主数组的默认长度
            static final int DEFAULT_INITIAL_CAPACITY = 16;
            //默认的装填因子
            static final float DEFAULT_LOAD_FACTOR = 0.75f;
            //主数组的引用
            transient Entry<K, V>[] table;
            int threshold;//界限值  阈值
            final float loadFactor;//装填因子
            public HashMap() {
                this(DEFAULT_INITIAL_CAPACITY, DEFAULT_LOAD_FACTOR);
            }
            public HashMap(int initialCapacity, float loadFactor) {
                this.loadFactor = loadFactor;//0.75
                threshold = (int) Math.min(capacity * loadFactor,
                        MAXIMUM_CAPACITY + 1);//16*0.75=12
                table = new Entry[capacity];
    
            }
        }
```

调用put方法添加键值对。哈希表三步添加数据原理的具体实现；是计算key的哈希码，和value无关

1.第一步计算哈希码时，不仅调用了key的hashCode()，还进行了更复杂处理，目的是尽量保证不同的key尽量得到不同的哈希码
2.第二步根据哈希码计算存储位置时，使用了位运算提高效率。同时也要求主数组长度必须是2的幂）
3.第三步添加Entry时添加到链表末尾（jdk1.8)，jdk1.7是添加到头部
4.如果索引处为空，则直接插入到对应的数组中，否则，判断是否是红黑树，若是，则红黑树插入，否则遍历链表，若长度不小于8，则将链表转为红黑树，转成功之后 再插入。如果插入的key为null，则直接插入到hash表数组索引为0的位置
5.第三步添加Entry是发现了相同的key已经存在，就使用新的value替代旧的value，并且返回旧的value

put部分源码：

```java
public class HashMap {
    public V put(K key, V value) {
       //如果key是null，特殊处理
        if (key == null) return putForNullKey(value);
        //1.计算key的哈希码hash 
        int hash = hash(key);
        //2.将哈希码代入函数，计算出存储位置  y= x%16；
        int i = indexFor(hash, table.length);
        //如果已经存在链表，判断是否存在该key，需要用到equals()
        for (Entry<K,V> e = table[i]; e != null; e = e.next) {
            Object k;
            //如找到了,使用新value覆盖旧的value，返回旧value
        if (e.hash == hash && ((k = e.key) == key || key.equals(k))) { 
                V oldValue = e.value;// the United States
                e.value = value;//America
                e.recordAccess(this);
                return oldValue;
            }
        }
        //添加一个结点
        addEntry(hash, key, value, i);
        return null;
    }
final int hash(Object k) {
    int h = 0;
    h ^= k.hashCode();
    h ^= (h >>> 20) ^ (h >>> 12);
    return h ^ (h >>> 7) ^ (h >>> 4);
}
static int indexFor(int h, int length) {
//作用就相当于y = x%16,采用了位运算，效率更高
    return h & (length-1);
 }
}
```

添加元素时如达到了阈值（默认为16*0.75=12），需扩容，每次扩容为原来主数组容量的2倍：

```java
void addEntry(int hash, K key, V value, int bucketIndex) {
    //如果达到了门槛值，就扩容，容量为原来容量的2位 16---32
    if ((size >= threshold) && (null != table[bucketIndex])) {
        resize(2 * table.length);
        hash = (null != key) ? hash(key) : 0;
        bucketIndex = indexFor(hash, table.length);
    }
    //添加节点
    createEntry(hash, key, value, bucketIndex);
}
```

#### (2).HashSet

•HashSet的底层使用的是HashMap，所以底层结构也是哈希表
•HashSet的元素到HashMap中做key，value统一是同一个Object() —>这个Object用处不大，指向同一个Object而不是每次添加都new，节省空间

HashSet的部分源码:

```java
public class HashSet<E> implements Set<E> {
    private transient HashMap<E, Object> map;
    private static final Object PRESENT = new Object();
    public HashSet() {
        map = new HashMap<>();
    }
    public boolean add(E e) {
return map.put(e,  new Object()) == null;
        return map.put(e, PRESENT) == null;
    }
    public int size() {
        return map.size();
    }
    public Iterator<E> iterator() {
        return map.keySet().iterator();
    }
}
```

### <2>TreeMap和TreeSet

#### (1).TreeMap

TreeMap源码分析：
基本特征：二叉树、二叉查找树、二叉平衡树、红黑树

![image-20210702004930476](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702004930476.png)

每个结点的结构：

![image-20210702004944124](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702004944124.png)

```java
static final class Entry<K,V> implements Map.Entry<K,V> {
    K key;
    V value;
    Entry<K,V> left;
    Entry<K,V> right;
    Entry<K,V> parent;
    boolean color = BLACK;
}
```

TreeMap主要的成员变量

```java
public class TreeMap<K, V> implements NavigableMap<K, V> {
    private final Comparator<? super K> comparator;//外部比较器
    private transient Entry<K, V> root = null; //红黑树根节点的引用
    private transient int size = 0;//红黑树中节点的个数
    public TreeMap() {
        comparator = null;//没有指定外部比较器
    }
    public TreeMap(Comparator<? super K> comparator) {
        this.comparator = comparator;//指定外部比较器
    }
}
```

TreeMap的put原理
•从根节点开始比较
•添加过程就是构造二叉平衡树的过程，会自动平衡
•平衡离不开比较：外部比较器优先，然后是内部比较器。如果两个比较器都没有，就抛出异常(因此key无法为null）

添加的部分源码：

```java
public V put(K key, V value) {
    Entry<K,V> t = root;
    //如果是添加第一个节点，就这么处理
    if (t == null) {
        //即使是添加第一个节点，也要使用比较器
        compare(key, key); // type (and possibly null) check
        //创建根节点  //此时只有一个节点
        size = 1;
        return null;
    }
    //如果是添加非第一个节点，就这么处理
    int cmp;
    Entry<K,V> parent; 
    Comparator<? super K> cpr = comparator;
    //如果外部比较器存在，就使用外部比较器
    if (cpr != null) {
        do {
            parent = t;
            cmp = cpr.compare(key, t.key);
            if (cmp < 0)
                t = t.left;//在左子树中查找
           else if (cmp > 0)                
                t = t.right; //在右子树查找
            else
               //找到了对应的key，使用新的value覆盖旧的value                 
                return t.setValue(value);
        } while (t != null);
    }
    else {
        //如果外部比较器没有，就使用内部比较器
       ....
    }
    //找到了要添加的位置，创建一个新的节点，加入到树中
    Entry<K,V> e = new Entry<>(key, value, parent);
    if (cmp < 0)  
        parent.left = e;
    else
        parent.right = e;       
    size++;
    return null;
}
        root = new Entry<>(key, value, null);  
```

#### (2).TreeSet

TreeSet的底层使用的是TreeMap，所以底层结构也是红黑树
TreeSet的元素e是作为TreeMap的key存在的，value统一为同一个 Object()
部分源码：

```java
  public class TreeSet<E> implements NavigableSet<E> {
//底层的TreeMap的引用
    private transient NavigableMap<E, Object> m; 
    private static final Object PRESENT = new Object();
    public TreeSet() {
//创建TreeSet对象就是创建一个TreeMap对象
        this(new TreeMap<E, Object>()); 
    }
TreeSet(NavigableMap<E, Object> m) {
        this.m = m;
    }
    public boolean add(E e) {
        return m.put(e, PRESENT) == null;
    }
public int size() {
        return m.size();
    }
}
```

## 6.集合的其他内容

### <1>.迭代器

#### (1)Iterator迭代器

Iterator专门为遍历集合而生，集合并没有提供专门的遍历的方法

Iterator实际上迭代器设计模式的实现

Iterator的常用方法
boolean hasNext(): 判断是否存在另一个可访问的元素
Object next(): 返回要访问的下一个元素
void remove(): 删除上次访问返回的对象。

哪些集合可以使用Iterator遍历
层次1：Collection、List、Set可以、Map不可以
层次2：提供iterator()方法的就可以将元素交给Iterator;
层次3：实现Iterable接口的集合类都可以使用迭代器遍历(最核心）

for-each循环和Iterator的联系
for-each循环(遍历集合)时，底层使用的是Iterator
凡是可以使用for-each循环(遍历的集合)，肯定也可以使用Iterator进行遍历
for-each循环和Iterator的区别
for-each还能遍历数组，Iterator只能遍历集合
使用for-each遍历集合时不能删除元素，会抛出异常ConcurrentModificationException使用Iterator遍历合时能删除元素

Iterator是一个接口，它的实现类在哪里？
在相应的集合实现类中 ,比如在ArrayList中存在一个内部了Itr implements Iterator

为什么Iterator不设计成一个类，而是一个接口
不同的集合类，底层结构不同，迭代的方式不同，所以提供一个接口，让相应的实现类来实现

示例：

```java
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        Iterator<Integer> integerIterator=list.iterator();
        while (integerIterator.hasNext()){
            Integer elem=integerIterator.next();
            System.out.println(elem);
        }
        }
```

#### (4).ListIterator

ListIterator和Iterator的关系
public interface ListIterator extends Iterator
都可以遍历List

ListIterator和Iterator的区别

使用范围不同
Iterator可以应用于更多的集合，Set、List和这些集合的子类型。
ListIterator只能用于List及其子类型。

遍历顺序不同
Iterator只能顺序向后遍历; ListIterator还可以逆序向前遍历
Iterator可以在遍历的过程中remove();ListIterator可以在遍历的过程中remove()、add()、set()
ListIterator可以定位当前的索引位置，nextIndex()和previousIndex()可以实现。Iterator没有此功能。

示例：

```java
public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList();
        //添加元素
        list.add(80);
        list.add(90);
        list.add(80);
        list.add(70);

        ListIterator<Integer> lit = list.listIterator();
        //从前向后遍历
        while(lit.hasNext()){
           int elem =  lit.next();
            System.out.println(elem+"  "+lit.previousIndex()+"  "+lit.nextIndex());
        }
        System.out.println("================");
        //从后向前遍历
        while(lit.hasPrevious()){
           int elem  =  lit.previous();
            System.out.println(elem+"  "+lit.previousIndex()+"  "+lit.nextIndex());
        }
    }
```

### <2>Collections集合类

这个集合类无法创建，唯一的构造方式是私有的，里面的方法直接通过类名调用

```java
  public static void main(String[] args) {
        //给集合快速赋值
        List<Integer> list = new ArrayList<>();
        Collections.addAll(list,50,40,70,60,80,34,100,76);
        System.out.println(list);
        //排序
        Collections.sort(list);
        System.out.println(list);
        //查找元素(元素必须有序)
        int index = Collections.binarySearch(list,80);
        System.out.println(index);

        //最大值最小值
        System.out.println(Collections.max(list));
        System.out.println(Collections.min(list));

        //填充集合
        //Collections.fill(list,100);
        System.out.println(list);
        //复制集合
        List<Integer> list2 = new ArrayList<>();
        Collections.addAll(list2,0,0,0,0,0,0,0,0,0,0,0,0);
        Collections.copy(list2,list);//目的dest集合的size>=源集合src的size
        System.out.println(list);
        System.out.println(list2);

        //同步集合！！！
        StringBuffer buffer;//线程同步  synchronized  上锁
        StringBuilder builder;//线程不同步

        ArrayList list3;//线程不安全 没有上锁  多线程操作会有安全问题
        //进去是一个不安全的集合
        //返回的是一个安全的集合
        List list4 = Collections.synchronizedList(list);
        //List <Integer> list4 = new Collections.SynchronizedRandomAccessList<>(list);
        list4.add(90);
        
        Set<Integer> set=new HashSet<>();
        Set<Integer> set1=Collections.synchronizedSet(set);
    }
```

### <3>.旧的集合类（Vector和Hashtbale)

Vector
实现原理和ArrayList相同，功能相同，都是长度可变的数组结构，很多情况下可以互用
两者的主要区别如下
Vector是早期JDK接口，ArrayList是替代Vector的新接口
Vector线程安全，效率低下；ArrayList重速度轻安全，线程非安全
长度需增长时，Vector默认增长一倍，ArrayList增长50%

Hashtable类
实现原理和HashMap相同，功能相同，底层都是哈希表结构，查询速度快，很多情况下可互用
两者的主要区别如下
Hashtable是早期JDK提供，HashMap是新版JDK提供
Hashtable继承Dictionary类，HashMap实现Map接口
Hashtable线程安全，HashMap线程非安全
Hashtable不允许null值，HashMap允许null值

### <4>.新一代的并发集合类（JUC包）

#### (1)ConcurrentHashMap(多线程那里详细介绍)

![image-20210702005523421](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702005523421.png)

结点结构：

```java
 static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;
        final K key;
        volatile V val;
        volatile Node<K,V> next;
        }
```

1.底层的数据结构和HashMap jdk1.8版本一样，都是基于数组+链表+红黑树
2.支持多线程的并发操作，实现原理：CAS+Synchronized保证并发更新
3.put方法存储元素时，通过key的hashCode计算出相应数组的索引，如果没有Node，CAS自旋直到插入成功；如果存在Node，则使用synchronized 锁住改Node元素（链表或者红黑树）
4.键，值迭代器为弱一致性迭代器，创建迭代器之后，可以对元素进行更新
5.读操作没有加锁，value是volatile修饰的，保证了可见性，所以是安全的
6.读写分离可以提高效率：多线程多不同的Node/Segment的插入/删除操作是可以并发，并行的，多同一个Node/Segment的作用是互斥的。读操作都是无锁操作，可以并发，并行执行

#### (2)CopyOnWriteArrayList

CopyOnWriteArrayList ：CopyOnWrite+Lock锁
对于set()、add()、remove()等方法使用ReentrantLock的lock和unlock来加锁和解锁。读操作不需要加锁（之前集合安全类，即使读操作也要加锁，保证数据的实时一致）。

![image-20210702005626204](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702005626204.png)

当我们往一个容器添加元素的时候，不直接往当前容器添加，而是先将当前容器进行Copy，复制出一个新的容器，然后新的容器里添加元素，

添加完元素之后，再将原容器的引用指向新的容器。这样做的好处是我们可以对CopyOnWrite容器进行并发的读，而不需要加锁，因为当前容器不会添加任何元素。所以CopyOnWrite容器也是一种读写分离的思想，读和写不同的容器。

CopyOnWrite容器只能保证数据的最终一致性，不能保证数据实时一致性。所以如果你希望写入的的数据，马上能读到，请不要使用CopyOnWrite容器。

部分源码：

```java
public boolean add(E e) {
    final ReentrantLock lock = this.lock;
    lock.lock();
    try {
        Object[] elements = getArray();
        int len = elements.length;
// 复制出新数组
        Object[] newElements = Arrays.copyOf(elements, len + 1);
// 把新元素添加到新数组里
        newElements[len] = e;
// 把原数组引用指向新数组
        setArray(newElements);
        return true;
    } finally {
        lock.unlock();
    }
}
final void setArray(Object[] a) {
    array = a;
}
public E get(int index) {
    return get(getArray(), index);
}
```

#### (3)CopyOnWriteArraySet

CopyOnWriteArraySet：CopyOnWrite+Lock锁
它是线程安全的无序的集合，可以将它理解成线程安全的HashSet
底层是由CopyOnWriteArrayList 实现，思想大致相同



### 7.Java集合框架面试题

# 五.Java8的新特性

## 1.接口中的默认方法修饰为普通方法

在 JDK 1.8 开始 支持使用 static 和 default 修饰 可以写方法体，不需要子类重写。 

方法： 

普通方法 :可以有方法体 

抽象方法 :没有方法体需要子类实现 重写

```java
public interface DefaultInterface {
    void get();

    /**
     * 接口中可以使用default static来修饰方法，则可以写方法体
     */
    default void eat(){
        System.out.println("eat");
    }

    static void run(){
        System.out.println("run");
    }
}
```

## 2.函数式接口

使用 Lambda 表达式 依赖于函数接口 

1. 在接口中只能够允许有一个抽象方法 

2. 在函数接口中定义 object 类中方法 

3. 可以有使用默认或者静态方法 

4. @FunctionalInterface 表示该接口为函数接口

```java
/**
 * 函数式接口
 * @InterfaceName DefaultInterface
 * @Author 朱云飞
 * @Date 2021/7/6 10:15
 * @Version 1.0
 **/
@FunctionalInterface
public interface DefaultInterface {
    void get();
    /**
     * 接口中可以使用default static来修饰方法，则可以写方法体
     */
    default void eat(){
        System.out.println("eat");
    }

    static void run(){
        System.out.println("run");
    }

    //重写Object类的方法
    String toString();
}
```

## 3.Lambda表达式

**使用Lambda表达式时，如果只有一行代码，可以不写大括号和return**

无参的调用：

```java
@FunctionalInterface
public interface
    DefaultInterface {
    void get();
}

 //Lambda无参的调用方法
        DefaultInterface defaultInterface=()->{
            System.out.println("无参Lambda");
        };
        defaultInterface.get();
//精简版
 ((DefaultInterface)()-> System.out.println("无参Lambda")).get();
```

有参的调用:

```java
@FunctionalInterface
public interface DefaultInterface2 {
    int add(int i,int j);
}
     DefaultInterface2 defaultInterface2=(i,j)->{
            return i+j;
        };
        System.out.println(defaultInterface2.add(1, 1));
//精简版
System.out.println(((DefaultInterface2) (i, j) -> i + j).add(1, 1));
```

## 4.Steam流

实体类：

```java
  User user1=new User("小明",12);
        User user2=new User("小红",13);
        User user3=new User("小张",14);
        ArrayList<User> list=new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
```



### <1>Stream流的创建

 ```java
      //Stream流的创建
        Stream<User> stream = list.stream();//单线程版
        Stream<User> userStream = list.parallelStream();//多线程版
 ```

### <2>集合转换

1.list转Set

```java
//list转Set
        Set<User> collect = stream.collect(Collectors.toSet());
        collect.forEach((User)-> System.out.println(User.toString()));
```

2.list转Map

```java
 //list转map
        stream.collect(Collectors.toMap(
                new Function<User, String>() {
                                            @Override
                                            public String apply(User user) {
                                                return user.getName();
                                            }
                                        },
                new Function<User, User>() {
                    @Override
                    public User apply(User user) {
                        return user;
                    }
                }));

//Lambda表达式
   //list转map
        //key:user.getName  value:user
        stream.collect(Collectors.toMap(
                user -> user.getName(),
                user -> user)).forEach((k,v)->{
            System.out.println("key:"+k+"value"+v);
        });
```

### <3>Stream流的其他用法

1.Stream流的求和

```java
    //Stream流求和
        Optional<User> sum = stream.reduce(new BinaryOperator<User>() {
            @Override
            public User apply(User user, User user2) {
                User sum = new User("sum", user.getAge() + user2.getAge());
                return sum;
            }
        });
        System.out.println(sum.get().getAge());

//Lambda表达式
   Optional<User> sum = stream.reduce((user, user21) -> {
            User sum1 = new User("sum", user.getAge() + user21.getAge());
            return sum1;
        });
```

2.Stream流求最大最小

```java
 //Stream求最大最小，实现Comparator接口
        Optional<User> max = stream.max((o1, o2) -> o1.getAge() - o2.getAge());
        Optional<User> min = stream.min((o1, o2) -> o1.getAge() - o2.getAge());
```

3.Stream流匹配集合中的数据

```java
  //Stream匹配集合中的数据，返回boolean
        boolean b = stream.anyMatch((User) -> {
            return "小红".equals(User.getName());
            //return 13==User.getAge();
        });
```

4.Stream过滤器的使用

```java
  //Stream流过滤器的使用
        stream.filter((User)->{
            return "小红".equals(User.getName());
        }).forEach(System.out::println);
```

5.Stream的Limit

```java
 //Stream的Limit
        stream.limit(2).forEach(System.out::println);
```

6.Stream流实现排序

```java
    //Stream流实现排序
        stream.sorted((o1, o2) -> {
            return o1.getAge()-o2.getAge();
        }).forEach(System.out::println);
```

# 六.操作系统

## 1.进程和线程

### 1.1进程和线程的区别

线程具有许多传统进程所具有的特征，故又称为轻型进程(Light—Weight Process)或进程元；而把传统的进程称为重型进程(Heavy—Weight Process)，它相当于只有一个线程的任务。在引入了线程的操作系统中，通常一个进程都有若干个线程，至少包含一个线程。

* 根本区别：进程是操作系统资源分配的基本单位，而线程是处理器任务调度和执行的基本单位
* 资源开销：每个进程都有独立的代码和数据空间（程序上下文），程序之间的切换会有较大的开销；线程可以看做轻量级的进程，同一类线程共享代码和数据空间，每个线程都有自己独立的运行栈和程序计数器（PC），线程之间切换的开销小。
* 包含关系：如果一个进程内有多个线程，则执行过程不是一条线的，而是多条线（线程）共同完成的；线程是进程的一部分，所以线程也被称为轻权进程或者轻量级进程。
* 内存分配：同一进程的线程共享本进程的地址空间和资源，而进程之间的地址空间和资源是相互独立的
* 影响关系：一个进程崩溃后，在保护模式下不会对其他进程产生影响，但是一个线程崩溃整个进程都死掉。所以多进程要比多线程健壮。
* 执行过程：每个独立的进程有程序运行的入口、顺序执行序列和程序出口。但是线程不能独立执行，必须依存在应用程序中，由应用程序提供多个线程执行控制，两者均可并发执行

### 1.2进程的状态转换

三种基本状态：

* 运行态：占用CPU，并在CPU上运行
* 就绪态：已经具备了运行条件，但由于没有空闲的CPU，而暂时不能运行
* 阻塞态：因等待某一事件而暂时不能运行

另外两种状态：

* 创建态：进程正在被创建，操作系统为进程分配资源，初始化PCB
* 进程正在从系统中撤销，操作系统会回收进程拥有的资源，撤销PCB

### 1.3进程间的通信

1. >**对于同步和互斥的理解：**

区别：

互斥：是指三部在不同进程之间的若干程序片断，当某个进程运行其中一个程序片段时，其它进程就不能运行它们之中的任一程序片段，只能等到该进程运行完这个程序片段后才可以运行。

同步：是指散步在不同进程之间的若干程序片断，它们的运行必须严格按照规定的 某种先后次序来运行，这种先后次序依赖于要完成的特定的任务。　　

联系：

同步是一种更为复杂的互斥，而互斥是一种特殊的同步。也就是说互斥是两个线程之间不可以同时运行，他们会相互排斥，必须等待一个线程运行完毕，另一个才能运行，而同步也是不能同时运行，但他是必须要安照某种次序来运行相应的线程（也是一种互斥）。

2. >进程间为什么需要通信

在操作系统中，协作的进程可能共享一些彼此都能共同读写的一些有限资源。而这些资源是有限的，或者如一些共享内存，进程随意读写可能会造成数据的顺序，内容等发生错乱，进程不能对其随意的使用，读写等。从而会发生竞争。**我们把对共享内存进行访问的程序片称为临界资源或临界区**，对同一共享内存，任何时候两个进程不能同时处于临界区.

进程间通信的目的：

- 数据传输：一个进程需要将它的数据发送给另一个进程。
- 通知事件：一个进程需要向另一个或一组进程发送消息，通知它（它们）发生了某种事件（如进程终止时要通知父进程）。
- 资源共享：多个进程之间共享同样的资源。为了做到这一点，需要内核提供互斥和同步机制。
- 进程控制：有些进程希望完全控制另一个进程的执行（如 Debug 进程），此时控制进程希望能够拦截另一个进程的所有陷入和异常，并能够及时知道它的状态改变



3. >进程间通信的方式

**1.管道通信：**

![image-20210806143250987](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806143250987.png)

* 管道只能采取半双工通信，某一时间段内只能实现单向的传输。如果要实现双向同时通信，则需要设置两个管道
* 各个进程要互斥的访问管道
* 数据以字节流的形式写入管道，当管道写满时，写进程的write()系统调用将会被阻塞，等待读进程将数据取走。当读进程将数据全部取走后，管道变空，此时读进程的read()系统调用将会被阻塞

**注意：匿名管道只能用于有亲缘关系间的进程，而有名管道允许无亲缘关系的进程间通信**



**2.消息队列MessageQueue：**

消息队列是由消息的链表，存放在内核中并由消息队列标识符标识。消息队列克服了信号传递信息少、管道只能承载无格式字节流以及缓冲区大小受限等缺点。

**3.信号**

信号是进程之间唯一的异步通信机制，信号的主要来源主要有硬件来源（入键盘操作ctrl + C） 和软件来源（如kill命令），信号传递的信息比较少，主要用于通知进程某个时间已经发生。比如利用kill pid，可以让系统优雅停机。

**4.信号量**

信号量是一个计数器，可以用来控制多个进程对资源的访问，通常作为一种锁机制，防止某个进程正在访问共享资源，其他进程也访问资源

**5.共享内存**

共享内存就是映射一段能被进程之间共享的内存，这段内存由一个进程创建，但是多个进程都可以共享访问，是最快的一种进程间通信的方式（不需要从用户态到内核态的切换），它是针对其他进程间通信方式运行效率低而专门设计的。它往往与其他通信机制，如信号量，配合使用，来实现进程间的同步和通信。

**6.Socket**

socket套接字，不仅仅可以用于本地进程通信，还可以用于不通主机进程之间的通信。

### 1.4进程的调度和处理机调度

进程调度(低级调度)，就是按照某种算法，从就绪队列中选择一个进程为其分配处理机

1. >进程调度的时机

   * 进程主动放弃处理机：进程正常终止，发生异常终止，进程主动请求阻塞(如等待I/O)等
   * 进程被动放弃处理机：分配的时间片用完，IO中断，有更高的优先级进程进入就绪队列等

2. >调度算法

   * 先来先服务
   * 最短作业优先
   * 最高响应比优先      响应比：(等待时间+服务时间)/要求服务的时间
   * 时间片轮转调度
   * 优先级调度
   * 多级反馈队列
     * 设置多级就绪队列，各级的队列优先级从高到低，时间片从小到大
     * 新进程到达时先进入第一级队列，按照先来先服务排队等待被分配时间片，若用完时间片进程还未结束，则进程进入下一级队列的队尾，如果此时已经在最下级队列，则从新放回最后一级队列的队尾
     * 只有当第K级的队列为空时，才会为K+1级的队列队头的进程分配时间片

## 2.  内存管理

### 1.1内存管理的功能

* 内存空间的分配与回收：由操作系统完成主存储器空间的分配和管理，使程序员摆脱存储分配的麻烦，提高编程效率。
* 地址转换：在多道程序环境下， 程序中的逻辑地址与内存中的物理地址不可能一致， 因此存储管理必须提供地址变换功能，把逻辑地址转换成相应的物理地址。
* 内存空间的扩充：利用虚拟存储技术或自动覆盖技术，从逻辑上扩充内存 。
* 存储保护：保证各道作业在各自的存储空间内运行，互不干扰。

### 1.2内存分配方式

#### 1.2.1连续分配管理方式

>连续分配方式，是指为一个用户程序分配一个连续的内存空间，比如说某用户需要1GB的内存空间，它就在内存空间中分配一块连续的 1GB的空间给用户。

* **单一连续分配：**内存在此方式下分为系统区和用户区，系统区仅提供给操作系统使用，通常在低地址部分；用户区是为用户提供的、除系统区之外的内存空间。 这种方式无需进行内存保护。
* **固定分区分配：**固定分区分配是最简单的一种多道程序存储管理方式，它将用户内存空间划分为若干个固定大小的区域，每个分区只装入一道作业。当有空闲分区时，便可以再从外存的后备作业队列中， 选择适当大小的作业装入该分区，如此循环。
* **动态分区分配：**动态分区分配又称为可变分区分配，是一种动态划分内存的分区方法。这种分区方法不预先将内存划分，而是在进程装入内存时，根据进程的大小动态地建立分区 ，并使分区的大小正好适合进程的需要。因此系统中分区的大小和数目是可变的。

#### 1.2.2分配策略算法

* 首次适应 (First Fit) 算法：空闲分区以地址递增的次序链接。分配内存时顺序查找，找到大小能满足要求的第一个空闲分区。
* 最佳适应 ( Best Fit )算法：空闲分区按容量递增形成分区链，找到第一个能满足要求的空闲分区。
* 最坏适应 ( Worst Fit )算法：又称最大适应 （ Largest Fit )算法，空闲分区以容量递减的次序链接。找到第一个能满足要求的空闲分区，也就是挑选出最大的分区。
* 邻近适应 ( Next Fit )算法：又称循环首次适应算法，由首次适应算法演变而成。不同之处是分配内存时从上次查找结束的位置开始继续查找。

#### 1.2.3非连续分配管理方式

>非连续分配允许一个程序分散地装入到不相邻的内存分区中

1. 分页存储管理方式

   * 将内存空间分为一个个大小相等的分区(比如：每个分区4KB)，每个分区就是一个页框(页帧，内存块，物理块)，每个页框都有一个编号，即页框号(页帧号，内存块号，物理块号)，页框号从0开始
   * 将用户进程的地址空间也分为与页框大小相等的一个个区域，称为 "页"或 "页面"，每个页面也有一个编号，即页号，页号也是从0开始(**注意：**进程最后一个页面可能没有页框那么大，因此页框不能太大，否则会产生过大的内部碎片)
   * 操作系统以页框为单位为各个进程分配内存空间。进程的每个页面分别放入一个页框中，则进程的页面和内存的页框产生了一一对应的关系

   ![image-20210806154802085](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806154802085.png)

2. 分段存储管理方式

   * 进程的地址空间：按照程序自身的逻辑关系划分为若干个段，每个段都有一个段名(在低级语言中，程序员使用段名来编程)，每段从0开始编址
   * 内存分配规则：以段位单位进行分配，每个段在内存中占据连续空间，但是各个段之间可以不相邻
   * 优点：由于是按逻辑功能划分，用户编程更加方便，程序的可读性更高

   ![image-20210806155803151](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806155803151.png)

3. 分页和分段存储管理的区别

   * 页是信息的物理单位，分页是为实现离散分配方式，提高内存利用率。分页仅仅是由于系统管理的需要而并不是用户的需要。而段则是信息的逻辑单位，是为了更好地满足用户的需要。
   * 分段比分页更容易实现信息的保护与共享，分段可以在某个段编写逻辑，实现对另外一个段的保护，而分页不行
   * 页的大小固定且由系统决定，而段的长度取决于用户所编写的程序。

#### 1.2.4 页面置换算法(追求最少的缺页率)

1. 最佳置换算法OPT(无法实现，作为一个标准):每次选择淘汰的页面将是以后永不使用，或者在最长的时间内不被使用，由于无法预知将会访问哪些页面，所以这种算法无法实现，只能作为一个标准

例如：需要访问7 0 1 2 0 3 0 4 2 3 0 3 2 1 2 0 1 7 0 1，则访问顺序：

![image-20210806161112588](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806161112588.png)

2. 先进先出置换算法FIFO:每次选择淘汰的页面是最早进入内存的页面

例如：需要访问 3 2 1 0 3 2 4 3 2 1 0 4 ，则访问顺序

![image-20210806161305682](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806161305682.png)

3. 最近最久未使用置换算法(LRU)：每次淘汰的页面是最近最久未使用的页面

例如：需要访问 1 8 1 7 8 2 7 2 1 8 3 8 2 1 3 1 7 1 3 7 则访问顺序

![image-20210806161811346](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806161811346.png)

4. 最近未用置换算法NRU(Clock算法):为每个页面设置一个访问位，再将内存中的页面都通过链接指针链接成一个循环队列。当某页被访问时，其访问位置为1.当需要淘汰某个页面时，只需要检查页的访问位。如果是0，就将该页面换出，如果是1，则将他置为0，暂不换出。继续检查下一个页面，如果第一轮扫描之后全是1，则扫描完成，这些都置为0.再进行第二轮扫描，因此简单的Clock算法选择一个页面淘汰最多两轮

![image-20210806163007655](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806163007655.png)

## 3.文件管理

### 3.1文件的分配方式(物理结构)

1. >文件块和磁盘块 ： 类似于内存的分页

   * 磁盘块:磁盘中的存储单元会被分为一个个"块/磁盘块/物理块"，在很多的操作系统中，磁盘块的大小与内存块，页面的大小相同

   ![image-20210806165222699](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806165222699.png)

   * 文件块：在外存管理中，为了方便对文件数据的管理，文件的逻辑地址空间被分为一个一个的文件块，文件的逻辑地址可以表示为(逻辑块号，块内地址)的形式。用户通过逻辑地址来操作自己的文件，操作系统负责实现从逻辑地址到物理地址的映射。

   ![image-20210806165713887](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806165713887.png)



2. >文件的分配方式

   1.连续分配：要求每个文件在磁盘上占有一组连续的块

   * 优点：支持顺序访问和直接访问(类似数组)，连续分配的文件在顺序访问时速度最快
   * 缺点：不方便文件的扩展，存储空间利用率低,会产生磁盘碎片

   ![image-20210806170013779](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806170013779.png)

   2. 链接分配：采取离散分配的方式，为文件分配离散的磁盘块。(类似链表数据结构)

      * 隐式链接：目录中记录的文件的起始块号和结束块号。除了文件最后一个磁盘块之外，每个磁盘块中都会保存指向下一个盘块的指针，这些指针对用户是透明的,每次访问某个磁盘块都需从头访问
        * 优点：方便文件的扩展，不会产生碎片问题，外存的利用率高
        * 缺点：只支持顺序访问，不支持随机访问，查找时效率低

      ![image-20210806170821393](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806170821393.png)

      * 显示链接：把用于链接文件各物理块的指针显示的存放在一张表中，即文件分配表。文件目录只需要记录起始块号。一个磁盘只需要设置一张分配表，开机时，将分配表读入内存，并常驻内存

        * 优点：支持顺序访问，也支持随机访问，方便文件的扩展，不会产生碎片问题，地址转换不需要访问磁盘，因此文件的访问效率更高
        * 缺点：文件分配表需要占据一定的存储空间

        ![image-20210806171343617](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806171343617.png)

   

   3. 索引分配：索引分配允许文件离散的分配在各个磁盘块中，系统会为每个文件建立一张索引表，索引表中记录了文件的各个逻辑块对应的物理块(索引表的功能类似于内存管理的页表--建立逻辑页面到物理页面之间的映射关系)。索引表存放的磁盘块称为索引块，文件数据存放的磁盘块称为数据块

### 3.2文件存储空间管理

1. >存储空间的划分和初始化

   * 存储空间的划分：将物理磁盘划分为一个个文件卷(逻辑卷，逻辑盘，如Windows系统下的C，D，E盘等)

     有的系统支持超大型文件，可由多个物理磁盘组成一个文件卷

   * 存储空间的初始化：将各个文件卷划分为目录区，文件区

     * 目录区：目录区主要存放文件的目录信息(FCB)，用于磁盘存储空间的管理的信息
     * 文件区：文件区用于存放文件数据

     ![image-20210806233041820](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806233041820.png)

2. >存储空间的管理方法

   1. 空闲表法：与内存管理中的动态分区分配很类似，为一个文件分配连续的存储空间。同样可以采用首次适应，最佳适应，最坏适应等算法来决定要为文件分配哪个区间

      ![image-20210806233919741](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806233919741.png)

   2. 空闲链表法：分为--->

      * 空闲盘块链：以盘块为单位组成一条空闲链
      * 空闲盘区链：以盘区为单位组成一条空闲链

      ![image-20210806234322091](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806234322091.png)

   

   3. 位示图法：每个二进制位代表一个盘块。例如可以用"0"来代表盘块空闲 ，"1"代表盘块已经分配

      ![image-20210806234718530](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806234718530.png)

   

   4. 成组链接法：UNIX采用的策略，适合大型的文件系统。

## 4.IO管理

### 4.1磁盘调度算法

一次磁盘读/写操作需要的时间：寻找时间+延迟时间+传输时间

* 寻找时间：在读/写前，将磁头移动到指定磁道所画的时间(启动磁头臂和移动磁头臂)
* 延迟时间：通过旋转磁盘，使磁头定位到目标扇区所需要的时间
* 传输时间：从磁盘中读出或写入数据所经历的时间

磁盘调度算法：

1. 先来先服务算法(FIFO)：根据进程请求访问磁盘的先后顺序进行调度
2. 最短寻找时间优先算法(SSTF)：优先处理的磁道是与当前磁道最近的磁道，可以保证每次的寻道时间最短，但是不能保证总的寻道时间最短(贪心算法)
3. 扫描算法(SCAN，电梯调度算法)：SSTF算法可能会产生饥饿，磁头有可能在一个小区域内来回移动，因此扫描算法规定，只有磁头移动到最外侧磁道的时候才能往内移动，移动到最内侧磁道才能往外移动，在这个基础上使用SSTF算法
4. 循环扫描算法(C-SCAN)：SCAN算法对于各个位置磁道的响应频率不平均，C-SCAN算法在SCAN算法的基础上规定：只有磁头朝着某个特定的方向移动时才能处理磁道的访问请求，而返回时直接快速移动到起始端而不处理任何请求

## 5.死锁

#### 5.1 对死锁的理解

如果一组进程中的每个进程都在等待一个事件，而这个事件是有这组中的某一个进程触发，这种情况则会导致死锁

**资源死锁的条件：**发生死锁时，以下四个条件必须全部具备

* 互斥条件：进程要求对所分配的资源进行排它性控制，即在一段时间内某资源仅为一进程所占用。
* 保持和等待条件：当进程因请求资源而阻塞时，对已获得的资源保持不放。
* 不可抢占条件：进程已获得的资源在未使用完之前，不能剥夺，只能在使用完时由自己释放。
* 循环等待条件：在发生死锁时，必然存在一个进程--资源的环形链。

#### 5.2死锁的避免->银行家算法

当一个进程申请使用资源的时候，银行家算法通过先 **试探** 分配给该进程资源，然后通过安全性算法判断分配后的系统是否处于安全状态，若不安全则试探分配作废，让该进程继续等待。

安全序列的判断：

![img](https://img-blog.csdn.net/20180508204335770?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzMzNDE0Mjcx/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

#### 5.3死锁的解除

* 资源剥夺法：挂起(暂时放到外存上)某些死锁的进程，并抢占他的资源，将这些资源分配给其他的死锁进程。但是应防止被挂起的进程长时间得不到资源而饥饿
* 撤销进程法：强制撤销部分，甚至全部的死锁进程，并剥夺这些进程的资源。虽然实现简单，但是代价可能较大
* 进程回退法：让一个或多个死锁进程回退到足以避免死锁的地步

简单来说，死锁的破坏就是对死锁产生的四个条件进行破坏，让其中任意一个不满足即可。



































# 七.网络

## 1.对网络的基础认识 

### <1>.组网方式

1.网络互联:使用集线器将少量主机连在一起

![image-20210708160908597](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708160908597.png)

2. 局域网(LAN):使用交换机和路由器将主机连接，可以自由组合三种方式

   组网方式：

   <1>.交换机

   <2>.路由器

   <3>.交换机+路由器

   ![image-20210708161122775](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708161122775.png)

3.广域网(WAN):广域网和局域网知识相对的概念

例如：一个学校之间的网络就可以成为局域网，而一个国家，多个国家之间可以称为广域网，覆盖的区域不同

![image-20210708161211963](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708161211963.png)

组网方式：公网上，网络结点组成，每一个结点可以是：

![image-20210708161517796](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708161517796.png)

### <2>.OSI七层模型

1.对协议的简单理解：本质上是数据格式的定义。而知名的数据格式，大家普遍遵循的规定，就属于协议

2.OSI七层模型：一种网络分层的设计方法论，比较复杂且不实用，落地几乎都是TCP/IP四层，五层模型

![image-20210708161757205](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708161757205.png)

### <3>.TCP/IP五层（四层模型）

五层模型：除去OSI的表示层和会话层

四层模型：除去OSI的表示层，会话层和物理层

![image-20210708162025473](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708162025473.png)

**注意：**

应用程序实现对应用层的封装分用

对于一台主机, 它的操作系统内核实现了从传输层到物理层的内容;（四.层封装分用）对于一台路由器, 它实现了从网络层到物理层;（下三层封装分用）
对于一台交换机, 它实现了从数据链路层到物理层;（下两层分装分用）
对于集线器, 它只实现了物理层;

### <4>.对封装分用的理解

**1.封装：发送数据时，从高到低的顺序，按照对应的网络分层协议对数据进行包装**

![image-20210708162644146](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708162644146.png)

例如：

![image-20210708162703467](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708162703467.png)

**2.分用：封装的逆过程：接收数据时，从低到高的顺序，按照对应的网络分层协议，解析数据**

![image-20210708162751807](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708162751807.png)

例如：

![image-20210708162806938](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708162806938.png)

## 2.网络数据传输

### <1>局域网

#### (1)认识IP和MAC

**IP:**

IP的格式：xxx.xxx.xxx.xxx
IP由四个部分组成，每个部分都是0-255.
网络号：前三个部分组成（用来标识网段），前三个部分相同，标识在一个网段
主机号：最后一个部分用来标识主机号
IP分为A-E五大类，部分范围是局域网IP，部分是广域网IP，可以根据规范，知道某个IP是局域网IP还是公网IP
注意： 局域网内（局域网IP）：网段唯一，同一个网段，主机号唯一
公网(公网IP）：公网IP是唯一的



**MAC:**

和网卡硬件绑定的，全球唯一
作用：网络数据传输定位网卡硬件的位置，一个主机可能有多个网卡（例如蓝牙连接，无线连接，有线连接的网卡），电脑硬件定位数据发送的目的位置只能使用MAC

**总结：**
IP地址描述的是路途总体的起点和终点。（给人用的，网络主机的逻辑地址）
MAC地址描述的是路途上的每一个区间的起点和终点（给电脑硬件用的，网络主机的物理地址）

#### (2)网络数据传输的特性

1.IP，MAC起的作用
2.封装分用——发送数据从高到低封装，接收数据从低到高分用
3.结合IP，MAC，理解网络数据传输，本质上是一跳一跳的传输数据

![image-20210708164037908](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164037908.png)

首先根据目的主机发送http请求，从源IP发送数据到目的IP
从源MAC（1）发送数据到目的MAC（2），然后MAC（2）对数据进行封装和分用，再以MAC（2）为源MAC，目的MAC为MAC（3），以此，发送数据到最终目的MAC。
注意： 接收数据报的主机：可能在一些情况下（广播或者转发），出现目的MAC不是我，我也能收到的情况（后面会提到）。



**五元组：**
源IP，目的IP，源端口，目的端口，协议号

IP：标识主机，给人用
源IP：发送数据的主机
目的IP：接收数据的主机

端口号：
源端口：标识发送数据的进程
目的端口，标识接收数据的进程
协议号：进程需要封装，解析数据报的数据格式



**DNS协议：**
作用：域名转IP

![image-20210708164225295](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164225295.png)

主机/路由器：都存在DNS缓存
域名查询的方式：上图树形结构从下往上查找（缓存，域名服务器）。
先在主机/路由器的DNS缓存中找，如果找不到，依次向上

特殊的IP，域名：本机IP为127.0.0.1，本机域名为localhost

#### (3)网络数据传输流程

**ARP/RARP协议：**
主机中有ARP缓存表
ARP协议：IP转MAC
RARP协议：MAC转IP

注意：交换机和集线器自己是没有MAC地址的，都是通过转发(不会修改源MAC和目的MAC）
交换机有MAC地址转换表，可以根据MAC找到对应的端口，而集线器没有这个功能

##### 1)网络互联的方式

![image-20210708164518022](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164518022.png)

首先介绍集线器：如上图，网络数据传输时，直接转发到其他所有端口（工作在物理层）

网络数据传输的过程：

**1.ARP缓存表找到了**

![image-20210708164605173](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164605173.png)

1.主机1发送数据到主机3（http：//主机3：80）
2.主机1查找本机的ARP缓存表，根据ARP协议，找到目的MAC
3.数据报由主机1，发送到集线器（数据报中的源MAC（主机1），目的MAC（主机3）真实的数据报
4.集线器转发数据报到除主机1的其他所有相连的主机（主机2，主机3）
5.主机2接收：数据报中，目的MAC不是我，丢弃
主机3接收，数据报中，目的MAC是我，接收
目的IP是我，交给对应端口处理，如果不是我，执行上述网络传输（一跳一跳的过程）

**2.ARP缓存表没找到**

![image-20210708164738262](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164738262.png)

1.主机1发送数据到主机3，http：//主机3：80
2.主机1查找本机的ARP缓存表，发现找不到
3.主机1发送广播数据报（非真实数据，只是要求对应主机返回MAC：我要IP为主机3的MAC，谁是主机3，快告诉我）

**注意：**

![image-20210708164801958](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708164801958.png)

4.集线器转发到主机2，主机3
5.主机2接收：要求的IP不是我，丢弃
主机3接收：要求的IP是我，返回我的MAC
6.主机1收到主机3的返回数据（IP，MAC）更新自己的ARP缓存表
7.主机1发送真实的数据到主机3



**注意：使用集线器的缺陷**
网络冲突，这样构成的网络区域叫冲突域/碰撞域（例如，房间里有多个人说话，那么其中某一个人说话就听不清楚了）



##### 2).局域网交换机组网的方式

![image-20210708165021423](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165021423.png)

首先介绍交换机，交换机的作用：
MAC地址转换表：保存连接的主机MAC和端口的映射，目的MAC是谁，直接转发到对应的端口（不像集线器，发送到所有端口），不会产生冲突域。

![image-20210708165108538](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165108538.png)

1.主机1发送数据到主机3 ，http：//主机3：80
2.主机1查找本机的ARP缓存表，如果找到，主机1发送数据到主机3。如果找不到，发送广播数据报，让IP为主机3的告诉我，你的MAC
3.交换机转发到其他所有端口（广播）
4.主机2丢弃，主机3返回自己的MAC
5.交换机知道主机3的MAC，主机1知道主机3的MAC（更新ARP缓存表）
注意：上述五个步骤，都是根据IP找MAC，和集线器的流程相似，下面的步骤时根据MAC找端口
6.主机1发送真实数据给交换机
7.交换机查找自己的MAC地址转换表，通过MAC找端口，发送数据到对应的端口
8.主机3接收，目的MAC是我，目的IP也[java项目——CRM客户管理系统（SpringBoot+MyBatis）](https://blog.csdn.net/qq_45704528/article/details/117451506)是我
这种网络数据传输的方式就像：先问张三的手机号，再打电话给张三，对别人没有影响

##### 3)局域网交换机+路由器组网的方式

**注意：单独由路由器组网的方式，和上述由交换机单独组网的方式相同**
首先介绍路由器，这里介绍两种：
<1>LAN口连接局域网，为主机分配局域网IP，分配的局域网IP都是一个网段（路由器下连接多个主机的类型）
路由器还有个网卡：绑定局域网的IP，和下面连接的主机进行信息交互用的

![image-20210708165324334](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165324334.png)

<2>LAN口是网卡。每个LAN口都可以连接类似交换机组网的方式

![image-20210708165337759](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165337759.png)

主机上的网络信息：

![image-20210708165347585](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165347585.png)

第二种路由器组网方式：

![image-20210708165408374](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165408374.png)

1.主机1发送数据到主机2:http://192.168.2.y:8080/xxx
2.通过目的IP+子网掩码，计算出目的主机和本机是否在一个网段
3.如果是，不需要使用路由器，和上述使用交换机组网方式一样
4.如果不是，表示我主机1和交换机处理不了，要发送给网关转发（网关就类似于IP的管理者，能查询其他主机的IP）
5.数据报发送给网关设备
![image-20210708165423671](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165423671.png)

目的MC：通过路由器网关的IP在主机1的ARP缓存表中，获取网关的MAC
6.路由器接收到数据报，分用：物理层到网络层，网络层分用，所有可以获取到目的IP
7.路由器查找自己的ARP缓存表（IP找MAC）
8.找不到，路由器发广播，主机2在哪，告诉我你的MAC
9.有了MAC，直接发到主机3

### <2>广域网传输流程

1.NAT和NAPT

NAT协议：局域网IP映射公网IP
NAPT协议：局域网IP+局域网端口映射----->公网IP+公网端口

2.传输流程

![image-20210708165547933](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165547933.png)

**结合上图，理解广域网传输流程**
首先：主机1发送http://www.baidu.com网络流程



**传输流程**
**1**首先主机1发送http请求，使用DNS协议：进行域名转IP
域名转IP：首先在本机DNS缓存表找，如果找不到---->向上查找------>如果根域名服务器也找不到，表示公网上没有该域名的主机

**2.** 找到IP，数据报IP部分，PORT部分都有了：

![image-20210708165620602](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165620602.png)

3. 根据目的IP计算是否和主机在同一个网段
主机1的IP+子网掩码 计算出------>主机1的网段
目的IP+子网掩码 计算出------->目的主机的网段
通过上述计算，判断目的IP和主机是否在同一个网段

4. 如果是同一个网段，和局域网传输一样
如果不是同一个网段：发送数据到网关
找网关的MAC：

![image-20210708165637238](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165637238.png)

5. 找到网关的MAC之后，将http数据重新封装，交由交换机转发
交换机转发：在MAC地址转换表（MAC映射端口），通过目的MAC找端口（交换机的屁股口）
注意：这个过程没有封装和分用

注意：前五个步骤，和路由器组成的局域网传输流程一样 参考：局域网传输

6. 路由器接收，分用数据报

注意：路由器会根据最短路径算法，计算出下一个发送数据的设备，会离目的IP更近一步


![image-20210708165715981](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165715981.png)

**7.** 上述步骤之后，数据报由局域网到广域网进行传输
路途中的设备：

![image-20210708165735017](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165735017.png)

**8.** 数据报到达百度服务器之后

![image-20210708165751126](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165751126.png)

**9.**数据由百度服务器返回，路途上经过的设备传输流程和步骤七相同（但是不一定是原路返回)
**10.**路由器1接收响应数据（对接收的数据进行分用，修改，封装)

![image-20210708165803694](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708165803694.png)

**11.** 之后的步骤，和局域网传输相同
主机接收数据报，分用

## 3.UDP和TCP

### <1>UDP协议

UDP协议端格式：
![image-20210708233755692](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708233755692.png)

16位UDP校验和作用：类似于藏头诗，双方约定好的校验数据，进行数据校验



UDP的特性：
1.无连接：没有建立连接就发数据
2.不可靠：没有类似TCP保证数据传输的安全机制，（连接管理机制，确认应答机制，超时机制
，）效率更高。
3.面向数据报：只能一次接收（系统级别的操作：调用系统函数）
4.没有发送缓冲区（发了消息就不管），有接收缓冲区
5.数据最大为64k
![image-20210708233819994](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708233819994.png)

发送缓冲区：主机1发送完数据，发出之后就不管了
接收缓冲区：
如果发送端调用一次sendto, 发送100个字节, 那么接收端也必须调用对应的一次recvfrom, 接收100个字节; 而不能循环调用10次recvfrom, 每次接收10个字节;
所以，接收数据的时候，发送100个字节，系统读取只调用一次，但是可以读取多次发来的其他100字节。
但是这个接收缓冲区不能保证收到的UDP报的顺序和发送UDP报的顺序一致; 如果缓冲区满了, 再到达的UDP数据就会被丢弃;

### <2>TCP协议(可靠的传输协议)

#### (1)TCP相关概念

TCP协议：可靠的传输协议，安全，效率(有连接的可靠传输协议)
设计TCP协议的理念：非100%安全，保证可承受范围内的安全，尽可能的提高网络传输数据的效率
TCP协议端格式：

![image-20210708233949845](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708233949845.png)

六位标志位：
URG: 紧急指针是否有效
ACK: 确认号是否有效
PSH: 提示接收端应用程序立刻从TCP缓冲区把数据读走
RST: 对方要求重新建立连接; 我们把携带RST标识的称为复位报文段
SYN: 请求建立连接; 我们把携带SYN标识的称为同步报文段
FIN: 通知对方, 本端要关闭了, 我们称携带FIN标识的为结束报文段
重点掌握ACK,SYN,FIN

#### (2)确认应答机制

![image-20210708234019755](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234019755.png)

主机A发送数据给主机B，每个数据都带了数据序号,主机B返回ACK应答
每一个ACK都带有对应的确认序列号, 意思是告诉发送者, 我已经收到了哪些数据; 下一次你从哪里开始发

作用：
1.保证安全：保证‘我’发送的消息，对方必须确认并恢复
2.保证多条数据确认信息的安全（告诉发送者，这次回应是对哪些数据，下次数据发送应该从什么时候开始）

#### (3)超时重传机制(安全机制)

超时重传机制触发：主机A发送数据给主机B，如果主机A在一个特定的时间间隔内没有收到来自主机B的确认应答，就会进行数据重发。

没有收到确认应答的情况：1.主机A的数据报在发送的过程中丢了。2.主机B的ACK应答丢了

超时时间的确定：TCP会根据当时的网络状态，动态的计算数据发送的速度，得到单次数据报发送的最大生存时间（MSL），超时时间即为（2MSL）

了解：如果一直接收不到ACK，超时时间会如何处理？
Linux中(BSD Unix和Windows也是如此), 超时以500ms为一个单位进行控制, 每次判定超时重发的超时时间都是500ms的整数倍.
如果重发一次之后, 仍然得不到应答, 等待 2500ms 后再进行重传.
如果仍然得不到应答, 等待 4500ms 进行重传. 依次类推, 以指数形式递增（2的指数倍）.
累计到一定的重传次数, TCP认为网络或者对端主机出现异常, 强制关闭连接.

#### (4)连接管理机制（安全机制）

流程图：

![image-20210708234104704](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234104704.png)

**1.建立连接------>TCP三次握手:**

TCP------>三次握手的流程

1.主机A发送syn到主机B，要求建立a到b的连接。此时主机A的状态为syn_sent
2.主机B回复ack+syn（这里的ack和syn数据报本来是两个，但是仅标志位不同，所以可以合并,为什么不是四次的原因），要求建立b到a的连接，主机B的状态为syn_rcvd
3.主机A回复第2步syn的ack。主机A的状态为established，建立A到B的连接
主机B接收到第3步的数据报，建立B到A 的连接，主机B的状态置为established

TCP------>三次握手中的问题：
1.syn为什么有两个？
双方的连接状态会持续，且连接是有方向的

2.第二步中，为什么是ack+syn？
本质上是一个发ack应答，一个发syn请求，而且是方向一致的两个数据报，可以合并

3.第三步中，ack确认应答哪个？
应答第二步的syn

**2.断开连接------>TCP四次挥手:**

TCP------>四次挥手的流程
1.主机A发送fin到主机B，请求关闭a到b的连接
2.主机B回复ack，主机B的状态置为close_wait
3.主机B发送fin到主机A，请求关闭b到a的连接
4.值即A回复ack（第三步的fin），状态置为time_wait
主机B接收到第四步的数据报，状态置为closed
主机A经过2MSL（超时等待时间）之后，状态置为closed

TCP------>4次挥手中的问题
1.第2步和第3步为什么不能和3次握手流程一样，进行合并
原因：第2步是TCP协议在系统内核中实现时，自动响应的ack
第3步时应用程序手动调用close来关闭连接的
程序在关闭连接之前，可能需要执行释放资源等前置操作，所以不能合并（TCP协议实现时，没有这样进行设计）

2.第3步中，主机A为什么不能直接设置为closed状态
原因： 第4个数据报可能丢包，如果直接置为closed，丢包后无法重新发送数据。
主机B达到超时时间之后，会重发第三个数据报，然后要求主机A再次回复ack

3.服务器出现大量的close_wait状态，是为什么？
服务端没有正确的关闭连接（程序没有调用close，或者没有正确使用）

#### (5)滑动窗口（效率）

如果没有滑动窗口，网路数据传输就是串行的方式（发送一次之后，等待应答，这个时间内，主机A无事可做，主机B也一样），效率比较差。

使用滑动窗口可以解决效率的问题：类似于多线程的方式，并发的，同时发送多个数据报。
如下图：

![image-20210708234420496](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234420496.png)

1.窗口大小指的是无需等待确认应答而可以继续发送数据的最大值. 上图的窗口大小就是4000个字节(四个段).
2.发送前四个段的时候, 不需要等待任何ACK, 直接发送;
3.收到第一个ACK后, 滑动窗口向后移动, 继续发送第五个段的数据; 依次类推;
4.操作系统内核为了维护这个滑动窗口, 需要开辟 发送缓冲区 来记录当前还有哪些数据没有应答;只有确认应答过的数据, 才能从缓冲区删掉;
5.窗口越大, 则网络的吞吐率就越高;

丢包问题：
1.数据报丢包

![image-20210708234436585](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234436585.png)

如上图：如果主机A发送的数据报丢包，主机B的ack应答，会根据主机A已经收到的连续数据报的最大值+1返回ack应答，当主机A收到三个同样的ack应答之后，会将丢掉的数据报进行重发（具有接收缓冲区，来记录已经接收的数据报的序号）

2.ACK应答丢包：这种情况下, 部分ACK丢了并不要紧, 因为可以通过后续的ACK进行确认
![image-20210708234459309](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234459309.png)

如果是滑动窗口的第一个包丢了，根据上述数据报丢包的情况，收到了第6个报的ACK应答，是从6001开始，说明第一个报主机B已经收到，所以ack丢包可以根据后序ack确定数据报主机B是否收到

关于滑动窗口的几个问题：
<1>.滑动窗口的大小：无需等待确认应答而可以继续发送数据的最大值
<2>.如何确定窗口的大小：由拥塞窗口和流量控制窗口决定（滑动窗口大小=（拥塞窗口大小，流量控制大小））（后序会讲到）
<3>.如何滑动：依赖于ACK的确认序号（ack确认序号前的数据报都已经接收到了），在该ACK确认序号前，当次并行收到了多少个数据报，就可以滑动多少
<4.>为什么要有接收缓冲区和发送缓冲区：
发送端的发送缓冲区：记录已经发送的数据——搜到对应的ACK应答，才可以清理该数据
接收端的接收缓冲区：记录已经接收的数据——如果发送数据报丢包，才知道让对方重发

#### (6)流量控制机制（安全机制）

接收端处理数据的速度是有限的. 如果发送端发的太快, 导致接收端的缓冲区被打满, 这个时候如果发送端继续发送, 就
会造成丢包, 继而引起丢包重传等等一系列连锁反应.

接收端将自己可以接收的缓冲区大小放入 TCP 首部中的 “窗口大小” 字段, 通过ACK端通知发送端;
窗口大小字段越大, 说明网络的吞吐量越高;
接收端一旦发现自己的缓冲区快满了, 就会将窗口大小设置成一个更小的值通知给发送端;
发送端接受到这个窗口之后, 就会减慢自己的发送速度;
如果接收端缓冲区满了, 就会将窗口置为0; 这时发送方不再发送数据, 但是需要定期发送一个窗口探测数据
段, 使接收端把窗口大小告诉发送端.

当接收端使用流量控制窗口时，如何保证接受端的数据安全？
告诉发送端，影响发送端滑动窗口的大小

#### (7)拥塞控制机制（安全机制）

少量的丢包, 我们仅仅是触发超时重传; 大量的丢包, 我们就认为网络拥塞;

发送端在网络状态不明的情况下，贸然发送大量的数据，会造成网络拥堵，需要先发送少量数据探路，设置拥塞窗口的大小

![image-20210708234605470](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234605470.png)

如上图：如何确定拥塞窗口的大小
此处引入一个概念程为拥塞窗口
发送开始的时候, 定义拥塞窗口大小为1;
每次收到一个ACK应答, 拥塞窗口加1;
每次发送数据包的时候, 将拥塞窗口和接收端主机反馈的窗口大小做比较, 取较小的值作为实际发送的窗口;
为了不增长的那么快, 因此不能使拥塞窗口单纯的加倍.
此处引入一个叫做慢启动的阈值
当拥塞窗口超过这个阈值的时候, 不再按照指数方式增长, 而是按照线性方式增长

#### (8)延迟应答机制(效率)

举个例子：
假设接收端缓冲区为1M. 一次收到了500K的数据; 如果立刻应答, 返回的窗口就是500K;
但实际上可能处理端处理的速度很快, 10ms之内就把500K数据从缓冲区消费掉了;
在这种情况下, 接收端处理还远没有达到自己的极限, 即使窗口再放大一些, 也能处理过来;
如果接收端稍微等一会再应答, 比如等待200ms再应答, 那么这个时候返回的窗口大小就是1M;

延迟应答类型：
数量限制: 每隔N个包就应答一次;
时间限制: 超过最大延迟时间就应答一次;

#### (9)捎带机制（效率）

在延迟应答的基础上, 我们发现, 很多情况下, 客户端服务器在应用层也是 “一发一收” 的，意味着当客户端给服务端发送请求时，服务端会给客户端响应数据，此时ACK就像可以搭请求数据的顺风车，一起发送。

接收端响应的ACK，和主动发送的数据，可以合并返回。



### <3>TCP的总结

#### (1)TCP特性

TCP是有连接的可靠协议

![image-20210708234808269](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708234808269.png)

#### (2)面向字节流

TCP既有发送缓冲区，也有接收缓冲区，数据没有大小限制

调用write时, 数据会先写入发送缓冲区中;
如果发送的字节数太长, 会被拆分成多个TCP的数据包发出;
如果发送的字节数太短, 就会先在缓冲区里等待, 等到缓冲区长度差不多了, 或者其他合适的时机发送出去;
接收数据的时候, 数据也是从网卡驱动程序到达内核的接收缓冲区;
然后应用程序可以调用read从接收缓冲区拿数据;
另一方面, TCP的一个连接, 既有发送缓冲区, 也有接收缓冲区, 那么对于这一个连接, 既可以读数据, 也可以写数据. 这个概念叫做 全双工

#### (3)粘包问题

在TCP的协议头中, 没有如同UDP一样的 “报文长度” 这样的字段, 但是有一个序号这样的字段
站在传输层角度看，报文是一个一个按照顺序排序好放在缓冲区，但是站在应用层角度看，都是一个个数字，不知道哪个数字是一段保文的开头，也不知道哪一个数字是结尾。这就是粘包
所以得明确一个报文的开头和结尾

但是对应UDP来说：
对于UDP, 如果还没有上层交付数据, UDP的报文长度仍然在. 同时, UDP是一个一个把数据交付给应用层.就有很明确的数据边界.
站在应用层的站在应用层的角度, 使用UDP的时候, 要么收到完整的UDP报文, 要么不收. 不会出现"半个"的情况


### <4>UDP VS TCP

#### (1)UDP和TCP的特性

TCP用于可靠传输的情况, 应用于文件传输, 重要状态更新等场景;
UDP用于对高速传输和实时性要求较高的通信领域, 例如, 早期的QQ, 视频传输等. 另外UDP可以用于广播

#### (2)如何使用UDP进行可靠传输

引入序列号, 保证数据顺序;
引入确认应答, 确保对端收到了数据;
引入超时重传, 如果隔一段时间没有应答, 就重发数据;



## 4.MTU和IP协议

### <1>MTU协议

MTU相当于发快递时对包裹尺寸的限制. 这个限制是不同的数据链路对应的物理层, 产生的限制.
以太网帧中的数据长度规定最小46字节,最大1500字节,ARP数据包的长度不够46字节,要在后面补填充位;最大值1500称为以太网的最大传输单元(MTU),不同的网络类型有不同的MTU;
如果一个数据包从以太网路由到拨号链路上,数据包长度大于拨号链路的MTU了,则需要对数据包进行分片
不同的数据链路层标准的MTU是不同的;

### <2>IP协议

1.协议头格式

![image-20210708235124225](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235124225.png)

简单了解：
4位版本号(version): 指定IP协议的版本, 对于IPv4来说, 就是4.

8位服务类型(Type Of Service): 3位优先权字段(已经弃用), 4位TOS字段, 和1位保留字段(必须置为0). 4位 TOS分别表示: 最小延时, 最大吞吐量, 最高可靠性, 最小成本. 这四者相互冲突, 只能选择一个. 对于ssh/telnet这样的应用程序, 最小延时比较重要; 对于ftp这样的程序, 最大吞吐量比较重要（应用层协议需要不同安全/效率需求，此时可以设置服务类型来满足）

下面三个字段都与数据链路层MTU相关：
![image-20210708235148872](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235148872.png)

## 5.HTTP和HTTPS

### <1>HTTP

#### (1)Http的前置知识

##### 1)网络数据传输

网络数据传输，都需要使用相同的协议，双方约定好的统一规范（封装和解析的数据格式规范）
协议：数据格式的约定
目标：对于http协议来说，就是学习里边的协议格式。结合理论实操，进行程序的调试，http协议格式本身，http数据中，包括自己的数据格式

![image-20210708235337491](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235337491.png)

##### 2)认识URL

![image-20210708235416096](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235416096.png)

![image-20210708235428101](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235428101.png)

#### (2)HTTP

##### 1)域名

域名：基于DNS解析为IP
IP：网络中定位主机的地址（逻辑地址）
PORT：端口号，定位某个主机中唯一的进程（应用程序）
url：url是全路径（绝对路径）
uri：uri包含了url这种全路径，还包含相对路径
特殊注意事项：输入域名直接访问，其实是访问/这个资源的路径。
浏览器中，不输入端口号，是因为http协议的默认端口是80
url中的请求数据：请求路径？key1=value1&key2=value2…
问号前代表绝对路径，问号后代表请求的资源，数据

##### 2)http协议格式

![image-20210708235543971](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235543971.png)

##### 3)http请求方法

![image-20210708235607035](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235607035.png)

**重点了解get和post方法**

**get和post方法的区别：**

![image-20210708235619687](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235619687.png)

>1.get的请求数据只能放在url中，post的数据，可以放在url和请求体
>
>2.url长度有限制，所有get方法请求数据不能太多，冰球url只能传输ascli字符
>
>3.安全性将，post可以存放请求数据在请求体，相对更加安全
>
>其他区别：了解即可

##### 4)http状态码

服务端返回（服务端设置），站在服务端的角色上，状态码都是对应的含义，站在客户端的角色上就不一定

![image-20210708235909490](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235909490.png)

注：x表示0到9的数字
**重点掌握：**

![image-20210708235927051](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235927051.png)

##### 5)http头信息

![image-20210708235948464](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210708235948464.png)

### <2>HTTPS

HTTP是明文传输的，不安全
HTTPS是基于HTTP+SSL/TSL来实现的，发送的数据需要加密，接收到的数据需要解密，比HTTP安全，但是传输效率比HTTP低

<1>.前置知识：为什么需要HTTPS

![image-20210709000210827](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000210827.png)

如上图：HTTP是不安全的，在传输的过程中，当客户端发送数据时，可能被钓鱼网站“欺骗”，将钓鱼网站当作服务端，或者直接被钓鱼网站窃取到数据，然后更改，造成不安全的影响

此时需要解决：
a：如何保证服务器是真实的，不是钓鱼网站？
b：解决网络数据传输，使用明文，所有路途中的设备，如果获取到，存在信息泄露

所以，就要使用到证书来解决安全问题
a：权威的证书机构颁发的证书（安装浏览器时，初始化就内置权威证书）（解决上述a问题）
b：https服务器证书（解决上述b问题）

私钥，公钥，密钥：
![image-20210709000233298](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000233298.png)

密钥：客户端，服务端用来加解密
对称加密：使用同一个钥匙，来加解密
公钥的生成（SSL握手阶段）： 见SLL握手

https中涉及的细节：
1.使用公钥和私钥来生成密钥（这里是非对称加密生成密钥）
2.密钥加解密真正的数据（这里进行的对称加密，效率比非对称加密高）

如何获取并验证服务器证书：
![image-20210709000257139](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000257139.png)

具体流程：[流程](http://www.ruanyifeng.com/blog/2014/09/illustration-ssl.html)
1.用密钥进行加解密：
<1>.客户端向服务端索要并验证公钥
<2>.双放协商生成"对话密钥"
<3>.双方采用“对话密钥”进行加解密通信

2.HTTPS握手阶段（根据公钥私钥生成对话密钥）（以上1，2步需要保证对话密钥不被钓鱼）

![image-20210709000329956](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000329956.png)

<1>.首先客户端给出协议版本号，一个客户端生成的随机数，以及支持的加密方式

<2>.服务端确认双方使用的加密方式，给出数字证书，以及一个服务器生成的随机数

<3>.客户端确认数字证书有效，然后生成一个新的随机数，并使用数字证书中的公钥，加密这个随机数，发个服务端

<4>.服务端使用自己的私钥，获取客户端发来的随机数

<5>.客户端和服务端根据约定的加密方式，使用前面的三个随机数，生成密钥

3.发送数据的阶段：
客户端使用对话密钥加解密真正的数据
服务端使用对话密钥加解密真正的数据
注意：此部分被钓鱼也没有关系（对话密钥无法解密）

## 6.正向代理和反向代理

### <1>.正向代理服务器
<1>概念

正向代理服务器：抓包工具
正向代理是一个位于客户端和目标服务器之间的代理服务器(中间服务器)。为了从原始服务器取得内容，客户端向代理服务器发送一个请求，并且指定目标服务器，之后代理向目标服务器转交并且将获得的内容返回给客户端。正向代理的情况下客户端必须要进行一些特别的设置才能使用。

<2>原理图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210129210209729.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

<3>使用场景和特点

1.特点：要访问的服务器只知道代理服务器来访问它，并不知道真实的客户端是谁
2.使用场景：正向代理的典型用途是为在防火墙内的局域网客户端提供访问Internet的途径。正向代理还可以使用缓冲特性减少网络使用率。

### <2>.反向代理服务器
<1>概念

反向代理服务器：nginx等
反向代理正好相反。对于客户端来说，反向代理就好像目标服务器。并且客户端不需要进行任何设置。客户端向反向代理发送请求，接着反向代理判断请求走向何处，并将请求转交给客户端，使得这些内容就好似他自己一样，一次客户端并不会感知到反向代理后面的服务，也因此不需要客户端做任何设置，只需要把反向代理服务器当成真正的服务器就好了

<2>原理图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210129210221594.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

<3>使用场景和特点

1.特点:特点:反向代理服务器隐藏了真实服务器的信息，例如淘宝，京东，天猫等
2.使用场景:
反向代理的典型用途是将 防火墙后面的服务器提供给Internet用户访问。反向代理还可以为后端的多台服务器提供负载平衡，或为后端较慢的服务器提供缓冲服务。

## 7.网络面试题

### <1>.说一说TCP/IP模型，以及都做了哪些事情

TCP/IP模型分为五层，分别是应用层，传输层，网络层，数据链路层，物理层

TCP/IP协议群主要是报文的拆分，增加协议头，数据的传输，路由和寻址以及数据的重组

### <2>.说一说TCP的三次握手四次挥手

**1.建立连接------>TCP三次握手:**

TCP------>三次握手的流程

1.主机A发送syn到主机B，要求建立a到b的连接。此时主机A的状态为syn_sent
2.主机B回复ack+syn（这里的ack和syn数据报本来是两个，但是仅标志位不同，所以可以合并,为什么不是四次的原因），要求建立b到a的连接，主机B的状态为syn_rcvd
3.主机A回复第2步syn的ack。主机A的状态为established，建立A到B的连接
主机B接收到第3步的数据报，建立B到A 的连接，主机B的状态置为established

TCP------>三次握手中的问题：
1.syn为什么有两个？
双方的连接状态会持续，且连接是有方向的

2.第二步中，为什么是ack+syn？
本质上是一个发ack应答，一个发syn请求，而且是方向一致的两个数据报，可以合并

3.第三步中，ack确认应答哪个？
应答第二步的syn

**2.断开连接------>TCP四次挥手:**

TCP------>四次挥手的流程
1.主机A发送fin到主机B，请求关闭a到b的连接
2.主机B回复ack，主机B的状态置为close_wait
3.主机B发送fin到主机A，请求关闭b到a的连接
4.值即A回复ack（第三步的fin），状态置为time_wait
主机B接收到第四步的数据报，状态置为closed
主机A经过2MSL（超时等待时间）之后，状态置为closed

TCP------>4次挥手中的问题
1.第2步和第3步为什么不能和3次握手流程一样，进行合并
原因：第2步是TCP协议在系统内核中实现时，自动响应的ack
第3步时应用程序手动调用close来关闭连接的
程序在关闭连接之前，可能需要执行释放资源等前置操作，所以不能合并（TCP协议实现时，没有这样进行设计）

2.第3步中，主机A为什么不能直接设置为closed状态
原因： 第4个数据报可能丢包，如果直接置为closed，丢包后无法重新发送数据。
主机B达到超时时间之后，会重发第三个数据报，然后要求主机A再次回复ack

3.服务器出现大量的close_wait状态，是为什么？
服务端没有正确的关闭连接（程序没有调用close，或者没有正确使用）

### <3>IPV4和IPV6的区别

1.地址不同（IPV4 32位，IPV6 128位），所有地址的空间，数目不同

2.地址分配不同（IPV4资源不够，分配的话需要竞争，而IPV6可以给每个人都分配很多的地址）

3.寻址的方式不同：IPV4 通过子网掩码计算网络地址，而IPV6有固定的计算方式划分网络

### <4>TCP和UDP的区别

1.TCP是有连接的可靠传输协议，而UDP是无连接的
2.UDP传时数据是有大小限制的，而TCP没有
3.UPD是面向数据报的，而TCP是面向数据流的。
4.TCP保证数据正确性，顺序性，而UDP不能保证.

5.UPD的传输速率高于TCP

### <5>如何用UDP进行可靠传输

引入序列号, 保证数据顺序;
引入确认应答, 确保对端收到了数据;
引入超时重传, 如果隔一段时间没有应答, 就重发数据;

### <6>正向代理和反向代理的区别

正向代理：要访问的服务器只知道代理服务器来访问它，并不知道真实的客户端是谁

反向代理：反向代理正好相反。对于客户端来说，反向代理就好像目标服务器，客户端向反向代理发送请求，接着反向代理判断请求走向何处，隐藏了真实的服务器。

### <7>说说HTTP和HTTPS

HTTP是超文本传输协议，是目前应用最广泛的网络通信协议，也是客户端和服务端交互的一系列行为的标准

http header包含三大部分，有General。Response Headers（响应头）。 Request Headers（请求头）。

http是无连接，无状态的（每次连接只处理一个请求，发送完数据后，不会记录）

而https简单讲是HTTP的安全版，即HTTP下加入SSL层，主要是来确认网站的真实性和数据传输的安全。

区别：

1.http的数据是明文传输，而https是加密传输，需要用到ca证书

2.http使用80端口，而https是443端口

3.http的速度比https要快



### <8>https中SSL握手的过程

<1>.首先客户端给出协议版本号，一个客户端生成的随机数，以及支持的加密方式

<2>.服务端确认双方使用的加密方式，给出数字证书，以及一个服务器生成的随机数

<3>.客户端确认数字证书有效，然后生成一个新的随机数，并使用数字证书中的公钥，加密这个随机数，发个服务端

<4>.服务端使用自己的私钥，获取客户端发来的随机数

<5>.客户端和服务端根据约定的加密方式，使用前面的三个随机数，生成密钥

### <9>DNS解析（DNS找IP）

1.当浏览器中输入www.bai.com域名时，操作系统会检查自己本地的hosts文件查看是否有这个网址的映射关系，如果有，直接调用

2.如果没有，则查找本地的DNS解析器缓存，如果有，则直接返回IP

3.如果没有，再找TCP/IP参数中设置的本地的DNS服务器，如果该域名包含再本地配置区域的资源中，则返回解析结果。

4.也可能查找的域名，本地的DNS服务器已经缓存在网址的映射关系，那么直接调用这个IP

5.如果本地DNS服务器也无法解析，会根据本地的DNS服务器是否设置转发器进行查询

如果是未转发模式，本地DNS会把请求发给13台根DNS，由对应的根服务器（例如.com）向下找，最后完成解析

如果是转发模式，那么DNS服务器会把请求一级一级向上传，往上找，直到传到根DNS。

### <10>GET和POST的区别

1.get的请求数据只能放在url中，post的数据，可以放在url和请求体

2.url长度有限制，所有get方法请求数据不能太多，并且url只能传输ascli字符

3.安全性将，post可以存放请求数据在请求体，相对更加安全

4.GET主要是从服务端获取数据，而POST请求主要是将数据发送到服务端

5.POST请求刷新会被重新提交，但Get请求不会

### <11>常见的状态码

400:客户端请求语法错误，服务端无法理解

405：映射找到了，但是客户端请求方法和服务端提供的请求方法不匹配

500:服务端内部报错

403：无权限

### <12>输入一个URL到浏览器中，会发生什么

1.域名解析(DNS解析)

2.发起TCP的三次握手

3.建立TCP连接后发起HTTP请求（如果浏览器存储了该域名下的Cookies，那么会把Cookies放入HTTP请求头里发给服务器。）

4.服务器端响应http请求，浏览器得到html代码

5.浏览器解析html代码，并请求html代码中的资源

6.浏览器对页面进行渲染呈现给用户



### 8.记录上了csdn热榜1

![image-20210711144232935](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210711144232935.png)



![image-20210711114606883](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210711114606883.png)

# 八.多线程

## 1.认识线程及线程的创建

### <1>.线程的概念

线程和进程的区别：
进程是系统分配资源的最小单位，线程是系统调度的最小单位。
一个进程内的线程之间是可以共享资源的。
每个进程至少有一个线程存在，即主线程。

注：
每个进程至少有一个线程存在，即主线程（系统级别的，C语言的主线程）
java级别的主线程（自己写的入口函数main方法（可以没有这个线程）
对java进程来说，至少有一个非守护线程还没终止，进程就不会结束

### <2>.线程的特性

在后面线程的安全性会详细介绍
1.原子性：即一个操作或者多个操作 要么全部执行并且执行的过程不会被任何因素打断，要么就都不执行。
2.可见性：当多个线程访问同一个变量时，一个线程修改了这个变量的值，其他线程能够立即看得到修改的值。
3.有序性：程序执行的顺序按照代码的先后顺序执行。

### <3>.线程的创建方式

#### (1)继承Thread类

```java
class MyThread extends Thread{
    @Override
    public void run() {
        System.out.println("继承Thread类创建线程");
    }
}
 public static void main(String[] args) {
        //1.继承Thread类创建线程
        MyThread t=new MyThread();
        t.start();
        }
```

#### (2)实现Runnable接口

1. 将MyRunnable对象作为任务传入Thread中

```java
class MyRunnable implements Runnable{
    @Override
    public void run() {
        System.out.println("继承Runnable接口，创建描述任务对象，实现多线程");
    }
}
  public static void main(String[] args) {
     
        //2.实现Runnable接口
        Thread t1=new Thread(new MyRunnable());
        t1.start();
        }
```

2.使用匿名内部类实现

```java
 Thread t2=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("使用Runnable接口，创建匿名内部类实现");
            }
        });
        t2.start();
```

#### (3)实现Callable接口

实现Callable重现call方法，允许抛出异常，允许带有返回值，返回数据类型为接口上的泛型

```java
class MyCallable implements Callable<String> {
    //允许抛出异常，允许带有返回值，返回数据类型为接口上的泛型
    @Override
    public String call() throws Exception {
        System.out.println("实现了Callable接口");
        return "这不是一个线程类，而是一个任务类";
    }
}
public static void main(String[] args) throws ExecutionException, InterruptedException {
        //方法三：实现Callable接口,是一个任务类
        //FutureTask底层也实现了Runnable接口
        FutureTask<String> task=new FutureTask<>(new MyCallable());
        new Thread(task).start();
        System.out.println(task.get());
    }
```

## 2.线程的常用方法

### <1>构造方法和属性的获取方法

构造方法：

![image-20210709000934947](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000934947.png)

属性的获取方法：

![image-20210709000947076](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709000947076.png)

### <2>常用方法

#### (1)run()和 start()

start();方法:启动线程
run();方法:覆写 run 方法是提供给线程要做的事情的指令清单

**start()和run()的区别：见代码**

```java
public class Thread_Run_VS_Start {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){

                }
            }
        }).run();
        /**
         * main线程直接调用Thread对象的run方法会直接在main线程
         * 运行Thread对象的run（）方法---->传入的runnable对象.run（）
         * 结果，main线程直接运行while（true）
         *
         * start()是启动一个线程，调用新线程的while（true）方法
         * 对比通过start（）调用的结果区别
         */

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){

                }
            }
        }).start();
    }
}
```

#### (2)interrupt()方法

![image-20210709001057538](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001057538.png)

通过interrupt()方法,通知线程中的中断标志位,由false变为true,但是线程什么时候中断,需要线程自己的代码实现
通过线程中的中断标志位实现,比起自己手动设置中断标志位,可以避免线程处于阻塞状态下,无法中断的情况

对interrupt，isInterrupt，interrupted的理解：
实例方法：
（1）interrupt：置线程的中断状态
如果调用该方法的线程处于阻塞状态（休眠等），会抛出InterruptedException异常
并且会重置Thread.interrupted;返回当前标志位，并重置
（2）isInterrupt：线程是否中断,返回boolean
静态方法：
（3）interrupted：返回线程的上次的中断状态，并清除中断状

```java
public class Interrupt {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {

                //...执行任务，执行时间可能比较长
               //运行到这里，在t的构造方法中不能引用t使用Thread.currentThread()方法，获取当前代码行所在线程的引用
                for (int i = 0; i <10000&&!Thread.currentThread().isInterrupted() ; i++) {
                    System.out.println(i);
                    //模拟中断线程
                    try {
                        Thread.sleep(1000);
                        //通过标志位自行实现，无法解决线程阻塞导致无法中断
                        //Thread,sleep(100000)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        t.start();//线程启动，中断标志位=false
        System.out.println("t start");
        //模拟，t执行了5秒，进程没有结束，要中断，停止t线程
        Thread.sleep(5000);

        //未设置时，isInterrupt为false

        //如果t线程处于阻塞状态（休眠等），会抛出InterruptedException异常
        //并且会重置isInterrupt中断标志位位false
        t.interrupt();//告诉t线程，要中断（设置t线程的中断标志位为true），由t的代码自行决定是否要中断
        //isInterrupt设置为true
        //t.isInterrupted();  Interrupted是线程中的标志位
        System.out.println("t stop");


        //注：Thread.interrupted(); 返回当前线程的中断标志位，然后重置中断标志位
         
    }
}
```

#### (3)join方法

注意: join方法是实例方法
等待一个线程执行完毕,才执行下一个线程（调用该方法的线程等待

![image-20210709001143643](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001143643.png)

无参：t.join：当前线程无条件等待，直到t线程运行完毕

![image-20210709001153976](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001153976.png)

有参：t.join(1000)等待1秒，或者t线程结束，哪个条件满足，当前线程继续往下执行

```java
//join方法：实例方法：
// 1.无参：t.join：当前线程无条件等待，直到t线程运行完毕
//  2.有参：t.join(1000)等待1秒，或者t线程结束，哪个条件满足，当前线程继续往下执行
public class Join {
    public static void main(String[] args) throws InterruptedException {
        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("1");
            }
        });
        t.start();

        t.join();//当前线程main线程无条件等待，直到t线程执行完毕，当前线程再往后执行
       // t.join(1000);当前线程等到1秒，或者等t线程执行完毕
        System.out.println("ok");

    }
}
```

#### (4)获取当前线程的引用currentThread();方法

静态方法：

![image-20210709001304951](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001304951.png)

```java
    public class ThreadDemo { 
    public static void main(String[] args) { 
    Thread thread = Thread.currentThread(); 
    System.out.println(thread.getName()); 
     } 
    }
```

#### (5)休眠当前线程sleep();方法

让线程等待一定时间后,继续运行

![image-20210709001410719](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001410719.png)

```java
Thread.sleep(1000);
```

#### (6)线程让步yield();方法

让yield();所在代码行的线程让步,当其他线程先执行

```java
public class Yield {
    public static void main(String[] args) {
        for(int i=0;i<20;i++){
            final int n=i;
            Thread t=new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println(n);
                }
            });
            t.start();
        }
        //判断：如果活跃的线程数量大于1，main线程让步
        while (Thread.activeCount()>1){//记录活跃线程的数量
            Thread.yield();
        }//注意：要用debug方式，因为run方式，idea后台还会启动一个线程
        //实现ok在1到二十之后打印
        System.out.println("ok");
    }
}
```

## 3.线程的生命周期和状态转换

Java 语言中线程共有六种状态，分别是：

NEW(初始化状态)

RUNNABLE(可运行 / 运行状态)

BLOCKED(阻塞状态)

WAITING(无时限等待)

TIMED_WAITING(有时限等待)

TERMINATED(终止状态)

生命周期和状态转换图：

![image-20210709001556385](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001556385.png)

常见的API导致的状态转换：
1.线程的阻塞：
Thread.sleep(long);当前线程休眠
t.join/t.join(long);t线程加入当前线程，当前线程等待阻塞
synchronized:竞争对象锁失败的线程，进入阻塞态
2.线程的启动：
start() ----->注意：run（）只是任务的定义，start（）才是启动

3.线程的中断：interrupt让某个线程中断，不是直接停止线程，而是一个“建议”，是否中断，由线程代码自己决定

## 4.线程间的通信

wait(0方法:线程等待
notify();方法:随机唤醒一个线程
notifyAll():方法:唤醒所有等待的线程
注意:这三个方法都需要被Synchronized包裹x

![image-20210709001637849](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001637849.png)

线程间通信的案例:
有三个线程，每个线程只能打印A，B或C
要求：同时执行三个线程，按ABC顺序打印，依次打印十次
ABC换行 ABC换行。。。。

```java
public class SequencePrintHomeWork {
    //有三个线程，每个线程只能打印A，B或C
    //要求：同时执行三个线程，按ABC顺序打印，依次打印十次
    //ABC换行 ABC换行。。。。
    //考察知识点：代码设计，多线程通信

    public static void main(String[] args) {
        Thread a = new Thread(new Task("A"));
        Thread b = new Thread(new Task("B"));
        Thread c = new Thread(new Task("C"));
        c.start();
        b.start();
        a.start();
    }

    private static class Task implements Runnable{

        private String content;
        //顺序打印的内容：可以循环打印
        private static String[] ARR = {"A", "B", "C"};
        private static int INDEX;//从数组哪个索引打印

        public Task(String content) {
            this.content = content;
        }

        @Override
        public void run() {
            try {
                for(int i=0; i<10; i++){
                    synchronized (ARR){//三个线程使用同一把锁
                        //从数组索引位置打印，如果当前线程要打印的内容不一致，释放对象锁等待
                        while(!content.equals(ARR[INDEX])){
                            ARR.wait();
                        }
                        //如果数组要打印的内容和当前线程要打印的一致，
                        // 就打印，并把数组索引切换到一个位置，通知其他线程
                        System.out.print(content);
                        if(INDEX==ARR.length-1){
                            System.out.println();
                        }
                        INDEX = (INDEX+1)%ARR.length;
                        ARR.notifyAll();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
```

补充： wait（）和sleep（）的区别：

wait 之前需要请求锁，而wait执行时会先释放锁，等被唤醒时再重新请求锁。这个锁是 wait 对象上的 monitor
lock
sleep 是无视锁的存在的，即之前请求的锁不会释放，没有锁也不会请求。
wait 是 Object 的方法
sleep 是 Thread 的静态方法

## 5.多线程的安全及解决

### <1>原子性

**对原子性的理解：** 我们把一段代码想象成一个房间，每个线程就是要进入这个房间的人。如果没有任何机制保证，A进入房间之后，还没有出来；B 是不是也可以进入房间，打断 A 在房间里的隐私。这个就是不具备原子性的。
**注意：** 一条 java 语句不一定是原子的，也不一定只是一条指令
例如：

![image-20210709001831336](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001831336.png)

如果一个线程正在对一个变量操作，中途其他线程插入进来了，如果这个操作被打断了，结果就可能是错

### <2>可见性

为了提高效率，JVM在执行过程中，会尽可能的将数据在工作内存中执行，但这样会造成一个问题，共享变量在多线程之间不能及时看到改变，这个就是可见性问题。

![image-20210709001856181](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001856181.png)

**可见性：**
系统调度CPU执行线程内，某个方法，产生CPU视角的主存，工作内存
主存：线程共享
工作内存：线程私有内存+CPU高速缓存/寄存器
对主存中共享数据的操作，存在主存到工作内存<====>从主存读取,工作内存修改,写回主存(拷贝)

### <3>代码的顺序性

**代码的重排序:**
一段代码:
**1**.去前台取下 U 盘    
**2**. 去教室写 10 分钟作业  
  **3**. 去前台取下快递
  如果是在单线程情况下，JVM、CPU指令集会对其进行优化，比如，按 1->3->2的方式执行，也是没问题，可以少跑一次前台。这种叫做指令重排序

  **代码重排序会给多线程带来什么问题:**
刚才那个例子中，单线程情况是没问题的，优化是正确的，但在多线程场景下就有问题了，什么问题呢。可能快递是在你写作业的10分钟内被另一个线程放过来的，或者被人变过了，如果指令重排序了，代码就会是错误的。

![image-20210709001927107](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709001927107.png)



### <4>线程不安全问题的解决

#### (1)synchronized 关键字

这里会在下面锁体系中详细说

#### (2)volatile 关键字

volatile 关键字的作用：
(1)保证可见性
(2)禁止指令重排序，建立内存屏障——单例模式说明
(3)不保证原子性
常见的使用场景：一般是读写分离的操作，提高性能
(1)写操作不依赖共享变量，赋值是一个常量（依赖共享变量的赋值不是原子性操作）
(2)作用在读，写依赖其他手段（加锁）

一个volatile的简单例子：

```java
public class Test {
    private static boolean flag = true;
    public static void main(String[] args) {
        //创建一个线程并启动
        new Thread(new Runnable() {
            int i=0;
            @Override
            public void run() {
                while(flag){
                    //这个语句底层使用了synchronized，保证了可见性
                    //System.out.println("=============");

                    i++;
                }
            }
        }).start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //即使改了，上面的线程flag也不会改，会一直循环
        flag = false;
    }
}
```

## 6.锁体系

**多线程中锁的作用：保证线程的同步**

### <1>Synchronized加锁方式

#### (1)<1>Synchronized的加锁方式及语法基础

**如何解决上述原子性例子的问题:**
是不是只要给房间加一把锁，A 进去就把门锁上，其他人是不是就进不来了。这样就保证了这段代码的原子性了。有时也把这个现象叫做同步互斥，表示操作是互相排斥的。
synchronized 关键字:
(1)作用:对一段代码进行加锁操作,让某一段代码满足三个特性:原子性,可见性,有序性
(2)原理:多个线程间同步互斥(一段代码在任意一个时间点,只有一个线程执行:加锁,释放锁)
**注意:** 加锁/释放锁是基于对象来进行加锁和释放锁,不是把代码锁了

只有对同一个对象加锁,才会让线程产生同步互斥的效果:
那么怎样才叫对同一个对象加锁呢？
这里t代表类名，t1，t2是  new了两个t    increment是t中的一个方法（是静态还是实例具体看）

![image-20210709002131110](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709002131110.png)

synchronized处加锁，抛出异常或代码块结束释放锁

![image-20210709002145581](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709002145581.png)

具体过程：

![image-20210709002237498](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210709002237498.png)

synchronized 多个线程n同步互斥:
(1):一个时间只有一个线程执行(同步互斥)
(2):竞争失败的线程,不停的在阻塞态和运行态切换(用户态和内核态切换)
(3)同步线程数量越多,性能越低

一个简单的小例子：

```java
public class SafeThread {
    //有一个遍历COUNT=0；同时启动20个线程，每个线程循环1000次，每次循环把COUNT++
    //等待二十个子线程执行完毕之后，再main中打印COUNT的值
    //（预期）count=20000
    private static int COUNT=0;

    //对当前类对象进行加锁，线程间同步互斥
//    public synchronized static void increment(){
//        COUNT++;
//    }


    //使用不同的对象加锁，没有同步互斥的效果，并发并行
//    public static void increment(){
//        synchronized (new SafeThread()){
//            COUNT++;
//        }
//    }
    public static void main(String[] args) throws InterruptedException {
        //尽量同时启动，不让new线程操作影响
        Class clazz=SafeThread.class;
      Thread[]threads=new Thread[20];
        for (int i = 0; i <20 ; i++) {
            threads[i]=new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j <1000 ; j++) {
                        //给SafeThread对象加一把锁
                        synchronized (clazz){
                            COUNT++;
                        }
                    }
                }
            });
        }
        for (int i = 0; i <20 ; i++) {
            threads[i].start();
        }

        //让main线程等待20个子线程运行完毕
        for (int i = 0; i <20 ; i++) {
            threads[i].join();
        }

        System.out.println(COUNT);
    }
}

```

synchronized加锁的缺点：
a)如果获取锁的线程由于要等待IO或其他原因（如调用sleep方法）被阻塞了，但又没有释放锁，其他线程只能干巴巴地等待，此时会影响程序执行效率。
b)只要获取了synchronized锁，不管是读操作还是写操作，都要上锁，都会独占。如果希望多个读操作可以同时运行，但是一个写操作运行，无法实现。

#### (2)Synchronized的原理及实现

1.Monitor机制：
（1）基于monitor对象的监视器：使用对象头的锁状态来加锁
（2）编译为字节码指令为：1个monitoren+2个monitorexit
**多出来的一个monitorexit：如果出现异常，第一个monitorexit无法正确释放锁，这个monitorexit进行锁释放**

例如下列代码：

```java
public class Test1 {
    public Test1() {
    }

    public static void main(String[] args) {
        Class var1 = Test1.class;
        synchronized(Test1.class) {
            System.out.println("hello");
        }
    }
}
```
反编译：



![在这里插入图片描述](https://img-blog.csdnimg.cn/20210528175837692.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

（3）monitor存在计数器实现synchronized的可重入性：进入+1，退出-1；

#### (3)JVM对Synchronized的优化

##### 1)对锁的优化

Synchronized是基于对象头的锁状态来实现的，从低到高：（锁只能升级不能降级）
（1）无锁
（2）偏向锁：对同一个对象多次加锁（重入）
（3）轻量级锁：基于CAS实现，同一个时间点，经常只有一个线程竞争锁
（4）重量级锁：基于系统的mutex锁，同一个时间点，经常有多个线程竞争
   特点：mutex是系统级别的加锁，线程会由用户态切换到内核态，切换的成本比较高（一个线程总是竞争失败，就会不停的在用户态和内核态之间切换，比较耗费资源，进一步，如果很多个竞争失败的线程，性能就会有很大的影响）

##### 2)锁粗话

多个synchronized连续执行加锁，释放锁，可以合并为一个
示例：StringBuffer静态变量，在一个线程中多次append（静态变量属于方法区，jdk 1.8后是在堆里面，线程共享）

```java
public class Test {
    private static StringBuffer sb;
    public static void main(String[] args) {
        sb.append("1").append("2").append("3");
    }
}
```

##### 3)锁消除

对不会逃逸到其他线程的变量，执行加锁的操作，可以删除加锁
示例：StringBuffer局部变量，在一个线程中多次append（局部变量属于虚拟机栈，是线程私有的）

```java
public class Test {
    public static void main(String[] args) {
        StringBuffer sb=new StringBuffer();
        sb.append("1");
        sb.append("2");
        sb.append("3");
    }
}
```

### <2>常见的锁策略及CAS

多线程中锁类型的划分：
API层面：synchronized加锁              Lock加锁
锁的类型：偏向锁，轻量级锁，重量级锁，自旋锁，独占锁，共享锁，公平锁，非公平锁等等

#### (1)乐观锁和悲观锁

乐观锁和悲观锁的设计思想（和语言是无关的，不是java多线程独有的）
根据使用常见来阐述：
乐观锁：同一个时间点，经常只有一个线程来操作共享变量，适合使用乐观锁
悲观锁：同一个时间点，经常有多个线程来操作共享变量，适合使用悲观锁

乐观锁的实现原理：
通过直接操作共享变变量（不会阻塞），通过调用的api的返回值，来知道操作是成功还是失败的
java多线程的实现：基于CAS的方式实现（Compare and Swap）
令：主存中需要操作的变量为V，线程A的工作内存中，读入A，修改为N
有另一个线程可能对主存中的V进行操作
此时：新的主存中操作的变量令为O，比较线程A中的V和此时主存中的O是否相等，如果相等，说明可以将N写回主存，如果不相等，任务主存中的变量被B线程操作过，此时A中的N不写入主存，线程A不做任何事情。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210528180144506.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)
悲观锁的实现原理：类似于synchronized加锁方式  

**CAS中可能存在的问题（ABA问题） **
肯主存中原来的V值，被线程B加一，再减一，依然满足上述线程A可以写入N的条件
解决办法：为主存中的变量加上一个版本好，在上诉A线程可写入的基础上，再比较一次版本好。即可解决。

CAS在java中是使用unsafe类来完成的，本质上是基于CPU提供的对变量原子性线程安全的修改操作

#### (2)自旋锁

按照普通加锁的方式处理，当线程在抢锁失败之后会进入阻塞状态，放弃CPU，需要经过很久才能被再次调度，所以，引入读写锁，当锁竞争失败之后，只需要很短时间，锁就能再次被释放，此时，让竞争失败的线程，进入自旋，不在用户态和内核态之间切换。只要没抢到锁，就死等。
类似以下代码：

<1>.无条件的自选：
```java
while(抢锁(lock)==失败{}
```
自旋锁的缺陷：如果之前的假设（锁很快就能被释放）没有满足，那么进入自旋的线程就一直在消耗CPU的资源，长期在做无用功

<2>.有条件的自旋：
如可中断的自旋：自旋时线程判断中断标志位后再执行，或者限制自旋的次数，限制自旋的时间




**自旋锁，悲观乐观锁，CAS的总结：**
<1>.悲观锁是线程先加锁，之后再修改变量的操作
<2>.乐观锁是线程直接尝试修改变量（不会阻塞）。在java多线程中是基于CAS 实现的。
<3>.CAS
概念：Compare and Swap比较并交换
实现/原理：基于unsafe来实现，本质上是基于CPU提供的接口保证线程安全修改变量。
使用（V,O,N）：V为内存地址中存放的实际值，O为预期的值（旧值），N为更新的值（新值）
可能出现的问题：ABA问题（引入版本号解决）
<4>.自旋+CAS
适用的场景：同一个时间点，常常只有一个线程进行操作
不适应的场景：1.同一个时间点，常常有多个线程进行操作
                          2.CAS的操作时间时间太长，给了其他线程操作共享变量的机会，那么CAS的成功率会很低，经常做无用功

自旋的缺陷：线程一直处于运行态，会很耗费CPU的资源

#### (3)可重入锁

允许同一个线程多次获取同一把锁
java中只要以Reentrant开头命名的锁都是可重入的锁，现有的jdk提供的lock的实现类和synchronized加锁,都是可重入锁
例如：

```java
public class Test2 {
    public static synchronized void t1(){
        t2();
    }
    public static synchronized void t2(){

    }
    public static void main(String[] args) {
        t1();
    }
}
```

### <3>Lock体系



![在这里插入图片描述](https://img-blog.csdnimg.cn/20210528182408621.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

#### (1)Lock接口

##### 1)使用Lock锁实现线程同步

上代码！

```java
public class AccountRunnable implements  Runnable {
    private Account account = new Account();
    //买一把锁
    Lock lock = new ReentrantLock(); //Re-entrant-Lock  可重入锁
    @Override
    public void run() {
        //此处省略300句
        try{
//上锁
            lock.lock();
            //判断余额是否足够，够，取之；不够，不取之；
            if(account.getBalance()>=400){
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                method1();
                //取之
                account.withDraw(400);
                //输出信息
                System.out.println(Thread.currentThread().getName()+
                   "取款成功，现在的余额是"+account.getBalance());
            }else{
                 System.out.println("余额不足，"+Thread.currentThread().getName()
                 +"取款失败，现在的余额是"   +account.getBalance());
            }
        }finally {
            //解锁
            lock.unlock();
        }
        //此处省略100句
    }
}
```
**这里要注意：释放锁时，要考虑是否出现异常，和上面synchronized加锁相同，要进行两次锁释放，这里将锁放在finally代码块中**

##### 2)Lock加锁的四种方式
形象记忆：男生追女生

1.lock():一直表白，直到成功
lock()方法是平常使用得最多的一个方法，就是用来获取锁。如果锁已被其他线程获取，则进行等待。

2.tryLock():表白一次，失败就放弃
 tryLock()方法是有返回值的，它表示用来尝试获取锁，如果获取成功，则返回true，如果获取失败（即锁已被其他线程获取），则返回false，也就说这个方法无论如何都会立即返回。拿不到锁时不会一直在那等待。

3.tryLock(long time, TimeUnit unit)  在一定的时间内持续表白，如果时间到了则放弃
tryLock(long time, TimeUnit unit)方法和tryLock()方法是类似的，只不过区别在于这个方法在拿不到锁时会等待一定的时间，在时间期限之内如果还拿不到锁，就返回false。如果如果一开始拿到锁或者在等待期间内拿到了锁，则返回true。

4.lockInterruptibly()　　 一直表白，当被通知她有男朋友了，才放弃
lockInterruptibly()方法比较特殊，当通过这个方法去获取锁时，如果线程正在等待获取锁，则这个线程能够响应中断，即中断线程的等待状态。也就使说，当这个线程使用lockInterruptibly()获取锁，当被interrupt中断时，才会停止竞争锁

#### (2)AQS简单认识
**AQS：** AbstractQuenedSynchronizer抽象的队列式同步器。是除了java自带的synchronized关键字之外的锁机制。这个类在java.util.concurrent.locks包.

**AQS的核心思想是:** 如果被请求的共享资源空闲，则将当前请求资源的线程设置为有效的工作线程，并将共享资源设置为锁定状态，如果被请求的共享资源被占用，那么就需要一套线程阻塞等待以及被唤醒时锁分配的机制，这个机制AQS是用CLH队列锁实现的，即将暂时获取不到锁的线程加入到队列中。

**AQS的实现方式：**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210529180038903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)
如图示，AQS维护了一个volatile int state和一个FIFO线程等待队列，多线程争用资源被阻塞的时候就会进入这个队列。state就是共享资源

AQS 定义了两种资源共享方式：
1.Exclusive：独占，只有一个线程能执行，如ReentrantLock
2.Share：共享，多个线程可以同时执行，如Semaphore、CountDownLatch、ReadWriteLock，CyclicBarrier

#### (3)ReentrantLock
##### 1)ReentrantLock基本概念
ReentrantLock，意思是“可重入锁”。ReentrantLock是唯一实现了Lock接口的非内部类，并且ReentrantLock提供了更多的方法。 
ReentrantLock锁在同一个时间点只能被一个线程锁持有。
ReentraantLock是通过一个FIFO的等待队列来管理获取该锁所有线程的。在“公平锁”的机制下，线程依次排队获取锁；而“非公平锁”在锁是可获取状态时，不管自己是不是在队列的开头都会获取锁。 

当单个线程或线程交替执行时，他与队列无关，只会在jdk级别解决，性能高

##### 2）自己实现一个简单的ReentrantLock
原理：自旋+park--unpark+CAS
```java
public class Test2 {
    volatile int status=0;
    Queue parkQueue;//集合 数组  list
    void lock(){
        while(!compareAndSet(0,1)){
            //这里不能用sleep或yield实现
            //sleep无法确定睡眠的时间
            //yield只能用于两个线程竞争，当有多个线程之后，t1抢不到锁，yield会让出cpu，但是可能下一次cpu还是调t1
            park();
        }
        unlock();
    }
    void unlock(){
        lock_notify();
    }
    void park(){
        //将当期线程加入到等待队列
        parkQueue.add(currentThread);
        //将当期线程释放cpu  阻塞   睡眠
        releaseCpu();
    }
    void lock_notify(){
        //status=0
        //得到要唤醒的线程头部线程
        Thread t=parkQueue.header();
        //唤醒等待线程
        unpark(t);
    }
}
```

##### 3）ReentrantLock部分源码分析
ReentrantLock锁分为公平锁和非公平锁（创建不加参数时默认非公平锁）

**ReentrantLock提供了两个构造器：**

```java
//非公平锁
 public ReentrantLock() {
        sync = new NonfairSync();
    }
//公平锁
 public ReentrantLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
    }
```

**ReentrantLock的lock方式：**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210529155119603.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)


**非公平锁：**
调用lock方法：

```java
final void lock() {
    if (compareAndSetState(0, 1))//首先用一个CAS操作，判断state是否是0（表示当前锁未被占用）
        setExclusiveOwnerThread(Thread.currentThread());//设置当前占有锁的线程为该线程
    else
        acquire(1);
}
```
  首先用一个CAS操作，判断state是否是0（表示当前锁未被占用），如果是0则把它置为1，并且设置当前线程为该锁的独占线程，表示获取锁成功。当多个线程同时尝试占用同一个锁时，CAS操作只能保证一个线程操作成功，剩下的只能乖乖的去排队。
  “非公平”即体现在这里，如果占用锁的线程刚释放锁，state置为0，而排队等待锁的线程还未唤醒时，新来的线程就直接抢占了该锁，那么就“插队”了。

下面说说acquire的过程

```java
public final void acquire(int arg) {
    //首先看看自己要不要排队，如果不用排队，获取锁，要排队，加入AQS队列 
    if (!tryAcquire(arg) &&
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
        selfInterrupt();
}
```

(1)尝试去获取锁（看看自己要不要排队）
非公平锁tryAcquire的流程是：检查state字段，若为0，表示锁未被占用，那么尝试占用，若不为0，检查当前锁是否被自己占用，若被自己占用，则更新state字段，表示重入锁的次数。如果以上两点都没有成功，则获取锁失败，返回false。

```java
tryAcquire(arg)
final boolean nonfairTryAcquire(int acquires) {
    //获取当前线程
    final Thread current = Thread.currentThread();
    //获取state变量值
    int c = getState();
    if (c == 0) { //没有线程占用锁
        if (compareAndSetState(0, acquires)) {
            //占用锁成功,设置独占线程为当前线程
            setExclusiveOwnerThread(current);
            return true;
        }
    } else if (current == getExclusiveOwnerThread()) { //当前线程已经占用该锁 重入锁
        int nextc = c + acquires;
        if (nextc < 0) // overflow
            throw new Error("Maximum lock count exceeded");
        // 更新state值为新的重入次数
        setState(nextc);
        return true;
    }
    //获取锁失败
    return false;
}
```
(2)入队
根据java运算符短路，如果不需要排队，方法直接返回，如果需要排队，进入addWaiter方法

**公平锁：**
    公平锁和非公平锁不同之处在于，公平锁在获取锁的时候，不会先去检查state状态，而是直接执行aqcuire(1）

##### 4)ReadWriteLock锁
ReadWriteLock也是一个接口，在它里面只定义了两个方法：

```java
public   interface   ReadWriteLock { 
      Lock readLock();   
      Lock writeLock(); 
} 
```

一个用来获取读锁，一个用来获取写锁。也就是说将文件的读写操作分开，分成2个锁来分配给线程，从而使得多个线程可以同时进行读操作。 
ReadWriteLock是一个接口，ReentrantReadWriteLock是它的实现类，该类中包括两个内部类ReadLock和WriteLock，这两个内部类实现了Lock接口。

**认识ReadWriteLock锁**

```java
public class TestLock {
    public static void main(String[] args) {
//默认也是非公平锁  也是可重入锁
        ReadWriteLock rwl = new ReentrantReadWriteLock();
        //多次返回的都是同一把读锁 同一把写锁
        Lock readLock = rwl.readLock();
        Lock readLock2 = rwl.readLock();
        Lock writeLock = rwl.writeLock();
        readLock.lock();
        readLock.unlock();
        System.out.println(readLock==readLock2);
    }
}
```
注意：从结果中看到，从一个ReadWriteLock中多次获取的ReadLock、WriteLock是同一把读锁，同一把写锁。

###  <4>.Lock锁和同步锁（synchronized）的区别与对比

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210529181219368.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

**对比：**

1.概念：都是用于加锁和保证线程安全的手段，知识Lock提供锁对象，而synchronized是基于对象头来实现的加锁

2.语法：locak显示的加锁，释放锁；而synchronized是内建锁（JVM内部构建的锁），隐式的加锁和释放锁，lock使用上来说更加灵活

3.功能：lock提供了多种获取锁的方式（获取锁，非阻塞式的获取锁，可中断的获取锁，超时获取锁等），而synchronized只有一种方式：竞争锁（竞争失败，阻塞）

4.性能：同一个时间点，竞争锁的线程数量越多，synchronized性能下降的越快（竞争失败的线程，不停的再阻塞态与唤醒态之间切换，用户态与内核态之间的切换），使用lock效果好。

### <5>死锁

先上代码：

```java
package threadadvanced.lesson1;

class Pen {
	private String pen = "笔" ; 
	public String getPen() {
		return pen;
	}
}
class Book {
	private String book = "本" ; 
	public String getBook() {
		return book;
	}
}
public class DeadLock {
	private static Pen pen = new Pen() ; 
	private static Book book = new Book() ; 
	public static void main(String[] args) {
		new DeadLock().deadLock();
	}
	public void deadLock() {
		Thread thread1 = new Thread(new Runnable() { // 笔线程
			@Override
			public void run() {
				synchronized (pen) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println(Thread.currentThread()+" :我有笔，我就不给你");
					synchronized (book) {
						System.out.println(Thread.currentThread()+" :把你的本给我！");
					}
				}
			}
		},"Pen") ; 
		
		Thread thread2 = new Thread(new Runnable() { // 本子线程
			@Override
			public void run() {
				synchronized (book) {
					System.out.println(Thread.currentThread()+" :我有本子，我就不给你！");
					synchronized (pen) {
						System.out.println(Thread.currentThread()+" :把你的笔给我！");
					}
				}
				
			}
		},"Book") ; 
		thread1.start();
		thread2.start();
	}
}
```
出现死锁：
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021052918150734.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

jconsole检查死锁：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210529181528648.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)
1.死锁出现的原因：
至少两个线程，互相持有对方需要的资源没有释放，再次申请对方以及持有的资源

2.出现死锁的后果：
线程互相阻塞等待地方的资源，会一直处于阻塞等待的状态

3.如何检测死锁：
使用jdk工具：jconsole（查看线程）---->jstack

4.解决死锁的方法：
（1）资源一次性分配（破坏请求与保持条件）
（2）在满足一定条件的时候，主动释放资源
（3）资源的有序分配：系统为每一类资源赋予一个编号，每个线程按照编号递请求资源，释放则相反

## 7.多线程案例

### <1>生产者消费者问题

示例：
  面包店
  10个生产者，每个每次生产3个
  20个消费者，每个每次消费一个

 *进阶版需求
 *面包师傅每个最多生产30次，面包店每天生产10*30*3=900个面包
  消费者也不是一直消费。把900个面包消费完结束

  隐藏信息：面包店每天生产面包的最大数量为900个

消费者把900个面包消费完结束
代码示例：

```java
/**
 * 面包店
 * 10个生产者，每个每次生产3个
 * 20个消费者，每个每次消费一个
 *
 * 进阶版需求
 * 面包师傅每个最多生产30次，面包店每天生产10*30*3=900个面包
 * 消费者也不是一直消费。把900个面包消费完结束
 *
 * 隐藏信息：面包店每天生产面包的最大数量为900个
 *            消费者把900个面包消费完结束
 */


public class AdvancedBreadShop {
    //面包店库存数
    private static int COUNT;

    //面包店生产面包的总数,不会消费的
    private static int PRODUCE_NUMBER;


    public static class Consumer implements Runnable{
        private String name;


        public Consumer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                while (true){
                    synchronized (AdvancedBreadShop.class){
                        if(PRODUCE_NUMBER==900&&COUNT==0){
                            System.out.println("今天面包已经卖完了");
                            break;
                        }else {
                            if(COUNT==0){
                                AdvancedBreadShop.class.wait();
                            }else {
                                System.out.printf("%s消费了一个面包\n",this.name);
                                COUNT--;
                                AdvancedBreadShop.class.notifyAll();
                                Thread.sleep(100);
                            }
                        }
                    }
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private static class Producer implements Runnable{
        private String name;


        public Producer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                //生产者生产30次，结束循环
                for(int i=0;i<=30;i++) {
                    synchronized (AdvancedBreadShop.class){
                        if(i==30){
                            System.out.println("今天面包生产完了");
                            break;
                        }else {
                            if(COUNT>97){
                                AdvancedBreadShop.class.wait();
                            }else {
                                COUNT=COUNT+3;
                                PRODUCE_NUMBER=PRODUCE_NUMBER+3;
                                System.out.printf("%s生产了三个面包\n",this.name);
                                AdvancedBreadShop.class.notifyAll();
                                Thread.sleep(100);
                            }
                        }
                    }
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        Thread[] Consumers=new Thread[20];
        Thread[] Producers=new Thread[10];
        for (int i = 0; i <20 ; i++) {
            Consumers[i]=new Thread(new Consumer(String.valueOf(i)));
        }
        for (int i = 0; i <10 ; i++) {
            Producers[i]=new Thread(new Producer(String.valueOf(i)));
        }
        for (int i = 0; i <20 ; i++) {
            Consumers[i].start();
        }
        for (int i = 0; i <10 ; i++) {
            Producers[i].start();
        }
    }
}
```

### <2>单例模式

1.饿汉式（线程安全）

```java
public class Singleton {
    private static Singleton instance=new Singleton();//对象的创建是类加载的时候进行
    private Singleton() {}
    public static Singleton getInstance(){
        return instance;
    }
}
```

缺点：

<1>类加载时就创建了对象，但是在很多场景下，获取对象时，才需要实例化对象，这样才能节省内存空间，节省new对象的操作时间

<2>new对象操作，没有类加载，需要先执行类加载，在执行对象初始化的工作（成员变量，示例代码快，构造方法），如果执行时，这些抛异常了，那么以后都不能再使用

2.懒汉式（单线程版）

```java
public class Singleton {
    private static Singleton instance=null;
    private Singleton(){}
    public static Singleton getInstance(){
        if(instance == null){
            instance=new Singleton();
        }
        return instance;
    }
}
```

存在共享变量instanc，线程不安全

<3>懒汉式（多线程版，性能较低）

```java
public class Singleton {
    private static Singleton instance=null;
    private Singleton(){}
    public synchronized static Singleton getInstance(){
        if(instance == null){
            instance=new Singleton();
        }
        return instance;
    }
}
```

<4>

基于单例模式下的懒汉模式（双重校验锁实现）（多线程版，二次判断，效率高）
代码示例：

```java
public class Singleton {
    //volatile关键字修饰，保证的可见性和代码的顺序性
    private static volatile Singleton instance = null;

    private Singleton() {
    }

    public static Singleton getInstance() {
        //判断instance是否为空，竞争锁的条件
        if (instance == null) {
            //保证线程安全，为Singleton.class加锁
            synchronized (Singleton.class) {
                //再次判断instance是否为空，防止多个线程进入第一个if后
                //对synchronized锁竞争失败进入阻塞状态后，再次进入运行态时
                //new了多个Singleton，不符合单例模式
                //保证线程安全
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
        }
        }
```

### <3>阻塞式队列

生产者消费者模式就是通过一个容器来解决生产者和消费者的强耦合问题。生产者和消费者彼此之间不直接通讯，而通过阻塞队列来进行通讯，所以生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。这个阻塞队列就是用来给生产者和消费者解耦的。

阻塞式队列代码实现：

```java
/**
 * 实现阻塞队列
 * 1.线程安全问题：在多线程情况下，put，take不具有原子性，4个属性，不具有可见性
 * 2.put操作：如果存满了，需要阻塞等待。take操作：如果是空，阻塞等待
 * @param <T>
 */
public class MyBlockingQueue <T>{
    //使用数组实现循环队列
    private Object[] queue;

    //存放元素的索引
    private int putIndex ;

    //取元素的索引
    private int takeIndex;

    //当前存放元素的数量
    private int size;
    public MyBlockingQueue(int len){
        queue=new Object[len];
    }

    //存放元素，需要考虑：
    //1.putIndex超过数组长度
    //2.size达到数组最大长度
    public synchronized void put(T e) throws InterruptedException {
        //不满足执行条件时，一直阻塞等待
        //当阻塞等待都被唤醒并再次竞争成功对象锁，回复往下执行时，条件可能被其他线程修改
        while (size==queue.length){
            this.wait();
        }
        //存放到数组中放元素的索引位置
        queue[putIndex]=e;
        putIndex=(putIndex+1)%queue.length;
        size++;
        notifyAll();
    }

    //取元素
    public synchronized T take() throws InterruptedException {
       while (size==0){
            this.wait();
        }
        T t= (T) queue[takeIndex];
        queue[takeIndex]=null;
        takeIndex=(takeIndex+1)%queue.length;
        size--;
        notifyAll();
        return t;
    }

    public int size(){
        return size;
    }

    public static void main(String[] args) {
        MyBlockingQueue<Integer>queue=new MyBlockingQueue<>(10);
        //多线程的调试方式：1.写打印语句 2.jconsole
        for (int i = 0; i <3 ; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        for (int j = 0; j <100 ; j++) {
                            queue.put(j);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        for (int i = 0; i <3 ; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                       while (true){
                          int t= queue.take();
                           System.out.println(Thread.currentThread().getName()+":"+t);
                       }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
```

### <4>线程池

线程池最大的好处就是减少每次启动、销毁线程的损耗

```java
import java.util.concurrent.*;

public class ThreadPoolExecutorTest {
    public static void main(String[] args) {
        //以快递公司，快递员，快递业务为模型
        ThreadPoolExecutor pool=new ThreadPoolExecutor(
                5,//核心线程数---->正式员工数
                10,//最大线程数-->正式员工+临时员工
                60,//临时工的最大等待时间(数量)
                TimeUnit.SECONDS,//idle线程的空闲时间(时间单位)-->临时工最大的存活时间，超过就解雇
                new LinkedBlockingQueue<>(),//阻塞队列，任务存放的地方--->快递仓库
                new ThreadFactory() {
                    @Override
                    public Thread newThread(Runnable r) {
                        return new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //r对象是线程池内部封装过的工作任务类（Worker），会一直循环等待的方式从阻塞队列中拿取任务并执行
                                //所以不能调用r.run();方法
                                System.out.println(Thread.currentThread().getName()+"开始执行了");
                            }
                        });
                    }
                },//创建线程的工厂类  线程池创建线程时，调用该工厂类的方法创建线程（满足该工厂创建线程的要求）
                   //---->对应招聘员工的标准

                /**
                 * 拒绝策略：达到最大线程数且阻塞队列已满，采取拒绝策略
                 * AbortPolicy:直接抛出RejectedExecutionException(不提供handler时的默认策略）
                 * CallerRunsPolicy：谁（某个线程）交给我（线程池）的任务，我拒绝执行，由谁自己去执行
                 * DiscardPolicy：交给我的任务直接丢弃掉
                 * DiscardOldestPolicy：阻塞队列中最旧的任务丢弃
                 */
                new ThreadPoolExecutor.AbortPolicy()//拒绝策略-->达到最大线程数，且阻塞队列已满，采取的拒绝策略
        );//线程池创建以后，只要有任务们就会自动执行

        for (int i = 0; i <20 ; i++) {
            //线程池执行任务：execute方法，submit方法--->提交执行一个任务
            //区别：返回值不同
            pool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        //线程池有4个快捷的创建方式（实际工作不使用，作为面试了解）
        //实际工作需要使用ThreadPoolExecutor，构造参数是我们自己指定，比较灵活
        ExecutorService pool2=Executors.newSingleThreadExecutor();//创建单线程池
        ExecutorService pool3=Executors.newCachedThreadPool();//缓存的线程池
        ExecutorService pool5=Executors.newFixedThreadPool(4);//固定大小线程池
        ScheduledExecutorService pool4=Executors.newScheduledThreadPool(4);//计划任务线程池

        //两秒中之后执行这个任务
        pool4.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        }, 2, TimeUnit.SECONDS);

        //一直执行任务
        pool4.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello");
            }
        }, 2, 1,TimeUnit.SECONDS);//比如一个脑子，两秒后开始叫我，然后每隔一秒叫我一次
    }
}
```

## 8.JUC包

**共享锁：**

### <1>Semaphore(信号量 )

两个重要的API

```java
  Semaphore semaphore=new Semaphore(0);
        semaphore.acquire();//需要/占用一个信号量
        semaphore.acquire(10);// 需要/占用10个型号量
        semaphore.release();//释放一个信号量
        semaphore.release(10);//释放是个信号量
```

两种使用场景：

1.当一定量线程执行完毕后，主线程才能执行

```java
  public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(0);//初始时没有线程释放，这里设置为0
        for (int i = 0; i <10 ; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName()+"线程已经执行完毕，释放资源");
                semaphore.release();//释放一个信号量
            }).start();
        }
        try {
            semaphore.acquire(10);//需要信号量达到10，才执行
            System.out.println("main线程执行完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
```

2.有限资源的争夺

十个线程抢五个有限资源

```java
public class SemaphoreDemo2 {
    //有限资源的争夺  这里定义5个有限资源
    public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(5);//5个有限资源
        for (int i = 0; i < 10; i++) {//10个线程抢5个资源
            new Thread(()->{
                try {
                    semaphore.acquire();//占用一个资源
                    System.out.println(Thread.currentThread().getName()+"占用了一个资源");
                    Thread.sleep(1000);
                    semaphore.release();//释放了一个资源
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
```

### <2>CountDownLatch

两个重要API

```java
CountDownLatch countDownLatch=new CountDownLatch(10);//10个线程
countDownLatch.countDown();//latch初始为10，调用该方法减一
countDownLatch.await();//一直等待，latch减到0时释放
```

使用场景：

和上面信号量第一个一样，能用CountDownLatch解决的，信号量也可以

等待10个线程执行完毕，再执行主线程

```java
public class CountDownLatchDemo {
    //案例：main线程在10个子线程执行完毕后才能执行
    public static void main(String[] args) {
        CountDownLatch countDownLatch=new CountDownLatch(10);//10个线程
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"已经执行");
                countDownLatch.countDown();//Latch减一
            }).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程执行完毕");
    }
}
```

### <3>CyclicBarrier(循环栅栏)

![image-20210712193844656](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210712193844656.png)

```java
CyclicBarrier cyclicBarrier=new CyclicBarrier(4);//初始化parties数
cyclicBarrier.await();//当await的线程小于4时，这些线程全部等待，达到4时，一起释放
cyclicBarrier.reset();// 将parties回归初始状态
```

示例：

```java
public class CyclicBarrierDemo {
    //循环栅栏
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier=new CyclicBarrier(4);
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                try {
                    cyclicBarrier.await();
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"已经执行");
            }).start();
        }
    }
}
```

### <4>ConcurrentHashMap

#### (1)ConcurrentHashMap加锁方式

![image-20210702005523421](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210702005523421.png)

结点结构：

```java
 static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;
        final K key;
        volatile V val;
        volatile Node<K,V> next;
        }
```

1.底层的数据结构和HashMap jdk1.8版本一样，都是基于数组+链表+红黑树
2.支持多线程的并发操作，实现原理：CAS+Synchronized保证并发更新
3.put方法存储元素时，通过key的hashCode计算出相应数组的索引，如果没有Node，CAS自旋直到插入成功；如果存在Node，则使用synchronized 锁住该Node元素（链表或者红黑树）
4.键，值迭代器为弱一致性迭代器，创建迭代器之后，可以对元素进行更新
5.读操作没有加锁，value是volatile修饰的，保证了可见性，所以是安全的
6.读写分离可以提高效率：多线程多不同的Node/Segment的插入/删除操作是可以并发，并行的，多同一个Node/Segment的作用是互斥的。读操作都是无锁操作，可以并发，并行执行

7.1.7对一段加锁,使用ReentrantLock加锁，1.8使用CAS和Synchronized加锁

#### (2)fail-fast和fail-safe

HashMap的迭代器时fali-fast快速失败的迭代器（强一致性的），

ConcurrentHashMap等线程安全的数据结构，使用的都是fail-safe安全失败的迭代器（若一致性）

再fail-fast迭代器遍历的时候，如果发生了修改（插入，删除，修改），那么再次遍历下一个元素时，就会报错。

而fail-safe迭代器，当一个位置进行了修改（插入，删除，修改）操作时，另外一个位置也可以进行，不会报错.

## 9.ThreadLocal

### <1>基础API

常用API：

![image-20210713114650656](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713114650656.png)

案例：

```java
public class ThreadLocalDemo {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static void main(String[] args) {
        ThreadLocalDemo demo=new ThreadLocalDemo();
        for (int i = 0; i <10 ; i++) {
            Thread thread=new Thread(()->{
                demo.setContent(Thread.currentThread().getName()+"的数据");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"---->"+demo.getContent());
            });
            thread.setName("线程"+i);
            thread.start();
        }
    }
}
```

这里的content是这十个线程的共享变量，可能会出现以下结果：

![image-20210713111745945](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713111745945.png)

而使用ThreadLocal时，就能实现content多个线程隔离

```java
public class ThreadLocalDemo {
    private String content;

    ThreadLocal<String> threadLocal=new ThreadLocal<>();
    public String getContent() {
        return threadLocal.get();
    }

    public void setContent(String content) {
        threadLocal.set(content);
    }

    public static void main(String[] args) {
        ThreadLocalDemo demo=new ThreadLocalDemo();
        for (int i = 0; i <10 ; i++) {
            Thread thread=new Thread(()->{
                demo.setContent(Thread.currentThread().getName()+"的数据");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"---->"+demo.getContent());
            });
            thread.setName("线程"+i);
            thread.start();
        }
    }
}
```

![image-20210713112040403](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713112040403.png)

### <2>ThreadLocal和synchronized的区别

![image-20210713112506475](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713112506475.png)

### <3>ThreadLocal的内部结构

![image-20210713113844431](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713113844431.png)

JDK早期的设计是ThreadLocal维护Thread，只有一个ThreadLocalMap，将Thread作为Map的key，存储的值作为Value

而JDK8的设计是：每个线程都有一个ThreadLocalMap，Thread维护，ThreadLocal对象本身作为Key，值作为Value存储

JDK8这样设计的好处：

1.早期的设计时，是ThreadLocal维护ThreadLocalMap，有多少个线程就有多少个Entry，较多

而JDK8这样设计，减少了Entry个数

2.当Thread销毁时，ThreadLocalMap也会随之销毁，减少了内存的使用



**ThreadLocalMap是ThreadLocal的内部类，没有实现Map接口，用独立的方式实现了Map的功能，其内部的Entry也是独立实现的**

### <4>ThrealLocal内存泄漏

弱引用造成内存泄漏：

![image-20210713135409317](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713135409317.png)

如图：

**1.弱引用原因:** 假设ThreadLocal Ref用完被回收了，而ThreadLoca被弱引用Entry中的Key指向，也会被回收，此时Key就变为了null，但是如果没有手动删除Entry以及CurrentThread依然运行的前提下，也存在这强引用连threadLocal ->currentThread->threadLocalMap->Entry->value,而value不会被回收，但key的值为null，所以value永远都不会被访问到，导致内存泄漏



**2.解决内存泄漏：**

<1> 没有手动删除这个Entry：只要在使用完ThreadLocal，调用其remove方法删除对应的Entry，就能避免内存泄漏

<2>CurrentThread依然运行：由于ThreadLocalMap是Thread的一个属性，被当前线程所引用，所以它的生命周期和Thread一样长，那么在使用完ThreadLocal情况下，如果当前Thread也随之指向结束，那么ThreadLocalMap自然也会被GC所回收，从根源上避免了内存泄漏



**综上：**ThreadLocal内存泄漏的根源是：由于ThreadLocalMap的生命周期和Thread一样长，如果没有手动删除对应的key就会导致内存泄漏



## 10.Java多线程相关面试题



### <1>、volatile、ThreadLocal的使用场景和原理；

**volatile原理**     

volatile变量进行写操作时，JVM 会向处理器发送一条 Lock 前缀的指令，将这个变量所在缓 存行的数据写会到系统内存。     

Lock 前缀指令实际上相当于一个内存屏障（也成内存栅栏），它确保指令重排序时不会把其 后面的指令排到内存屏障之前的位置，也不会把前面的指令排到内存屏障的后面；即在执行到内 存屏障这句指令时，在它前面的操作已经全部完成。

![volatile](D:/马士兵/面试题/700道面试题/700道面试题/02-BAT面试题汇总及详解(进大厂必看)/BAT面试题汇总及详解(进大厂必看).assets/volatile.png)

volatile的适用场景 

1）状态标志,如：初始化或请求停机 

2）一次性安全发布，如：单列模式 

3）独立观察，如：定期更新某个值 

4）“volatile bean” 模式 

5) 开销较低的“读－写锁”策略，如：计数器 

> [volatile的使用场景](BAT面试题汇总及详解(进大厂必看)_子文档\volatile的使用场景.md)

> [正确使用 Volatile 变量](BAT面试题汇总及详解(进大厂必看)_子文档\正确使用 Volatile 变量.md)

ThreadLocal原理     

ThreadLocal是用来维护本线程的变量的，并不能解决共享变量的并发问题。ThreadLocal是 各线程将值存入该线程的map中，以ThreadLocal自身作为key，需要用时获得的是该线程之前 存入的值。如果存入的是共享变量，那取出的也是共享变量，并发问题还是存在的。 

> [对ThreadLocal实现原理的一点思考](BAT面试题汇总及详解(进大厂必看)_子文档\对ThreadLocal实现原理的一点思考.md)

> [volatile的使用场景](BAT面试题汇总及详解(进大厂必看)_子文档\volatile的使用场景.md)

![ThreadLocal原理](D:/马士兵/面试题/700道面试题/700道面试题/02-BAT面试题汇总及详解(进大厂必看)/BAT面试题汇总及详解(进大厂必看).assets/ThreadLocal原理.png)

ThreadLocal的适用场景 

场景：数据库连接、Session管理

### <2>、ThreadLocal什么时候会出现OOM的情况？为什么？   

ThreadLocal变量是维护在Thread内部的，这样的话只要我们的线程不退出，对象的引用就会 一直存在。当线程退出时，Thread类会进行一些清理工作，其中就包含ThreadLocalMap， Thread调用exit方法如下： 

![Thread调用exit方法 ](D:/马士兵/面试题/700道面试题/700道面试题/02-BAT面试题汇总及详解(进大厂必看)/BAT面试题汇总及详解(进大厂必看).assets/Thread调用exit方法 .png)

ThreadLocal在没有线程池使用的情况下，正常情况下不会存在内存泄露，但是如果使用了线程 池的话，就依赖于线程池的实现，如果线程池不销毁线程的话，那么就会存在内存泄露。 

> [深入分析ThreadLocal原理](BAT面试题汇总及详解(进大厂必看)_子文档\深入分析ThreadLocal原理.md)

### <3>、synchronized、volatile区别 

1) volatile主要应用在多个线程对实例变量更改的场合，刷新主内存共享变量的值从而使得各个 线程可以获得最新的值，线程读取变量的值需要从主存中读取；synchronized则是锁定当前变 量，只有当前线程可以访问该变量，其他线程被阻塞住。另外，synchronized还会创建一个内 存屏障，内存屏障指令保证了所有CPU操作结果都会直接刷到主存中（即释放锁前），从而保证 了操作的内存可见性，同时也使得先获得这个锁的线程的所有操作

2) volatile仅能使用在变量级别；synchronized则可以使用在变量、方法、和类级别的。 volatile不会造成线程的阻塞；synchronized可能会造成线程的阻塞，比如多个线程争抢 synchronized锁对象时，会出现阻塞。

3) volatile仅能实现变量的修改可见性，不能保证原子性；而synchronized则可以保证变量的 修改可见性和原子性，因为线程获得锁才能进入临界区，从而保证临界区中的所有语句全部得到 执行。

4) volatile标记的变量不会被编译器优化，可以禁止进行指令重排；synchronized标记的变量 可以被编译器优化。

> [synchronized和volatile区别](BAT面试题汇总及详解(进大厂必看)_子文档\synchronized和volatile区别.md)

8、synchronized锁粒度、模拟死锁场景； 

synchronized：具有原子性，有序性和可见性 

> [三大性质总结：原子性，有序性，可见性](BAT面试题汇总及详解(进大厂必看)_子文档\三大性质总结：原子性，有序性，可见性.md)

粒度：对象锁、类锁

> [基于synchronized的对象锁，类锁以及死锁模拟](

### <4>ThreadLocal的作用以及为什么发生内存泄漏和解决方法

作用：存放线程的本地变量，实现线程之间的隔离，它内部有静态内部类ThreadlocalMap，Entry，实现了和Map类似的功能，单没用实现Map接口，每个线程都有一个ThreadLocalMap的副本。Threadlocal继承了弱引用

内存泄漏：

弱引用造成内存泄漏：

![image-20210713135409317](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210713135409317.png)

如图：

**1.弱引用原因:** 假设ThreadLocal Ref用完被回收了，而ThreadLoca被弱引用Entry中的Key指向，也会被回收，此时Key就变为了null，但是如果没有手动删除Entry以及CurrentThread依然运行的前提下，也存在这强引用连threadLocal ->currentThread->threadLocalMap->Entry->value,而value不会被回收，但key的值为null，所以value永远都不会被访问到，导致内存泄漏



**2.解决内存泄漏：**

<1> 没有手动删除这个Entry：只要在使用完ThreadLocal，调用其remove方法删除对应的Entry，就能避免内存泄漏

<2>CurrentThread依然运行：由于ThreadLocalMap是Thread的一个属性，被当前线程所引用，所以它的生命周期和Thread一样长，那么在使用完ThreadLocal情况下，如果当前Thread也随之指向结束，那么ThreadLocalMap自然也会被GC所回收，从根源上避免了内存泄漏



**综上：**ThreadLocal内存泄漏的根源是：由于ThreadLocalMap的生命周期和Thread一样长，如果没有手动删除对应的key就会导致内存泄漏

### <5>.如何中断一个线程

1.使用interrupt方法，修改中断标志位

2.可以在外部线程指定一个条件变量（比如主线程），使用volatile修饰，然后在子线程中循环检查这个变量

# 九.IO

## 1.IO的种类

1. BIO同步阻塞IO

同步并阻塞(传统阻塞型)，服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器
端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销 【简单示意图

![image-20210806124140110](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806124140110.png)

1. NIO同步非阻塞IO

Java NIO ： 同步非阻塞，服务器实现模式为一个线程处理多个请求(连接)，即客户端发送的连接请求都会注
册到多路复用器上，多路复用器轮询到连接有 I/O 请求就进行处理 【简单示意图】

![image-20210806124205991](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210806124205991.png)

2. AIO异步非阻塞IO

Java AIO(NIO.2) ： 异步 异步非阻塞，服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理，一般适用于连接数较
多且连接时间较长的应用

## 2.IO的读写方式

![img](https://img-blog.csdnimg.cn/2021032314473499.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

## 3.对文件的相关操作

![img](https://img-blog.csdnimg.cn/20210323151137586.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

>文件操作示例：遍历某个目录下，所有的子文件/子文件夹

```java
  public static void main(String[] args) {
        File file=new File("C:\\Users\\26905\\Desktop\\javaCode");
        List<File> list=listDir(file);
//        for (int i = 0; i <list.size() ; i++) {
//            System.out.println(list.get(i));
//        }
        //jdk1.8集合框架使用Stream操作，可以使用lambda表达式打印
        list.stream().forEach(System.out::println);
    }

```

1. 方法一：效率不高

```java
public static List<File> listDir(File dir){
        List<File> list=new ArrayList<>();
        if(dir.isFile()){
            list.add(dir);//效率不太好的地方，如果是文件，返回的list只存放一个元素
        }else {
            File[] children=dir.listFiles();
            for(File file:children){
                List<File> files=listDir(file);
                list.addAll(files);
            }
        }
        return list;
    }

```

2.方法二：效率较高

```java
 public static List<File>  listDir2(File dir){
        List<File> list=new ArrayList<>();
        if(dir.isDirectory()){
            //获取目录下一级的子文件和子文件夹
            File[] children=dir.listFiles();
            if(children!=null){
                for(File file:children){
                    if(file.isDirectory()){
                        //如果子文件还是文件夹，递归调用
                        list.addAll(listDir2(file));
                    }else{
                        list.add(file);
                    }
                }
            }
        }
        return list;
    }

```

## 4.BIO

### <1>流的分类

![img](https://img-blog.csdnimg.cn/20210323151926522.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20210323151540579.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

**IO流的分类：**

1. 文件操作流：File
2. 字节流：Stream
3. 字符流：Reader，Writer         处理字节数据----->write/read操作byte[],或一个byte
4. 输入流：Input/Reader             处理字符数据----->   一般用在文本操作    操作byte[]，byte，char []，char
5. 输出流：OutPut/Writer
6. 缓冲流：Buffered
7. 字节字符转换流    InputStreamReader    OutputStreamWriter     要把字节转换为字符流，需要在中间套上字节字符转换流
8. 打印输出流：PrintWriter
9. 字符串输出流：StringWriter
10. 对象流：带Object

### <2>流的使用

1. >输入流

```java
import java.io.*;

public class  FileInput {
    /**
     * 字节文件：C:\Users\26905\Desktop\画图笔记\JavaWeb\JSON和ajax简单介绍.png
     * 字符文件:C:\Users\26905\Desktop\论文，ppt，作业，笔记，图片等\java笔记.txt
     * @param args
     */
    public static void main(String[] args) throws IOException {
        //图片文件，以二进制的方式操作
        File file1=new File("C:\\Users\\26905\\Desktop\\画图笔记\\JavaWeb\\JSON和ajax简单介绍");
        File file =new File("C:\\Users\\26905\\Desktop\\论文，ppt，作业，笔记，图片等\\比特草稿本.txt");


        //1.文件输入字节流
        FileInputStream fis=new FileInputStream(file);
        //不知道文件有多 大，先定义一个一定大小的字节数组，然后不停读入，每次刷新以下就好
        byte[] bytes=new byte[1024*8];
        //输入流读取的固定写法：读取到一个字节/字符数组，定义read的返回值变量，while
        int len=0;
        //len表示读入字节的长度
        //如果不等于-1，说明读取未结束
        while ((len=fis.read(bytes)) !=-1){
            //读取到的长度，数组可能读满，也可能没有读满
            //当次读取，一般使用数组[0,len]表示读取内容

            //字节数组转字符串
            String str=new String(bytes,0,len);
           // System.out.println(str);
        }
        //一般来说，输入输出流使用完，一定要关闭，关闭的关系是反向关系
        //例如：创建的时候是从里往外创建，则关闭的时候就是从外往内关闭
        fis.close();


        //2.文件的字符输入流
        FileReader fr=new FileReader(file);
        char []chars=new char[1024];
        int lenFr=0;
        while ((lenFr=fr.read(chars))!=-1){
             String strFr=new String(chars,0,lenFr);
            //System.out.println(strFr);
        }
        fr.close();


        //3.缓冲流：缓冲字节输入，缓冲字符输入
        FileInputStream FIS=new FileInputStream(file);//文件字节输入流
        //字节流转字符流一定要经过字节字符转换流来转换,并且还可以指定编码
        InputStreamReader isr=new InputStreamReader(FIS);
        //缓冲流
        BufferedReader br=new BufferedReader(isr);
        String str;
        //读取，当字符串为空时，结束
        while ((str=br.readLine()) !=null){
            System.out.println(str);
        }
        br.close();
        isr.close();
        FIS.close();

    }
}
```

2. >输出流

```java
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class FileOutput {
    public static void main(String[] args) throws IOException {
        /**
         * D:\java代码\1.txt
         */
        //路径上没有该文件，new File不会报错，但是在操作输出流会抛出FileNotFoundException
        File file2=new File("D:\\java代码\\1.txt");

        //把a-z换行输出到某个文件，需要考虑文件是否存在的问题
        if(!file2.exists()){
            file2.createNewFile();
        }

        //new FileWriter()  文件的不带缓冲的字符输出
        //new FileWriter()  文件的不带缓冲的字节输出
        //new BufferedWriter() 带缓冲的字符输出
        //new PrintWriter()    打印输出流


        //缓冲的字符输出
//        BufferedWriter bw=new BufferedWriter(new FileWriter(file2));
//        bw.write("\n");


        //打印输出流
        //PrintWriter pw=new PrintWriter(new FileWriter(file));
        PrintWriter pw =new PrintWriter(new FileOutputStream(file2));
        //快速打印a-z
        for (int i = 'a'; i <='z' ; i++) {
            pw.println((char)i);
        }
        pw.flush();
    }
}

```

3. >文件的复制

```java
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    public static void main(String[] args) throws IOException {
        //文件复制
        File input=new File("D:\\java代码\\1.txt");
        File output=new File("D:\\java代码\\2.txt");
        if(!output.exists()){
            output.createNewFile();
        }
        //定义输入输出流
        //输入流
        FileInputStream fis=new FileInputStream(input);

        // 输出流
        FileOutputStream fos=new FileOutputStream(output);

        byte[]bytes=new byte[1024*8];
        int len;
        while ((len=fis.read(bytes))!=-1){//每次从输入流读取到byte[]的内容，直接输出到某个文件，就是复制
            //bytes可能没存满，需要指定长度
            fos.write(bytes,0,len);
        }
        fis.close();
        fos.close();
    }
}

```

## 5.NIO

### <1> NIO 和 BIO 的比较

* BIO 以流的方式处理数据,而 NIO 以块的方式处理数据,块 I/O 的效率比流 I/O 高很多
* BIO 是阻塞的，NIO 则是非阻塞的
* BIO 基于字节流和字符流进行操作，而 NIO 基于 Channel(通道)和 Buffer(缓冲区)进行操作，数据总是从通道
  读取到缓冲区中，或者从缓冲区写入到通道中。Selector(选择器)用于监听多个通道的事件（比如：连接请求，数据到达等），因此使用单个线程就可以监听多个客户端通道

| NIO                       | BIO                 |
| ------------------------- | ------------------- |
| 面向缓冲区（Buffer）      | 面向流（Stream）    |
| 非阻塞（Non Blocking IO） | 阻塞IO(Blocking IO) |
| 选择器（Selectors）       |                     |

### <2> NIO 三大核心原理示意图

NIO 有三大核心部分：**Channel( 通道) ，Buffer( 缓冲区), Selector( 选择器)**

#### Buffer缓冲区

缓冲区本质上是一块可以写入数据，然后可以从中读取数据的内存。这块内存被包装成NIO Buffer对象，并提供了一组方法，用来方便的访问该块内存。相比较直接对数组的操作，Buffer API更加容易操作和管理。

#### **Channel（通道）**

Java NIO的通道类似流，但又有些不同：既可以从通道中读取数据，又可以写数据到通道。但流的（input或output)读写通常是单向的。 通道可以非阻塞读取和写入通道，通道可以支持读取或写入缓冲区，也支持异步地读写。

#### Selector选择器

Selector是 一个Java NIO组件，可以能够检查一个或多个 NIO 通道，并确定哪些通道已经准备好进行读取或写入。这样，一个单独的线程可以管理多个channel，从而管理多个网络连接，提高效率

![image-20200619153658139](C:/Users/26905/Desktop/讲义/BIO、NIO、AIO.assets/image-20200619153658139.png)

* 每个 channel 都会对应一个 Buffer
* 一个线程对应Selector ， 一个Selector对应多个 channel(连接)
* 程序切换到哪个 channel 是由事件决定的
* Selector 会根据不同的事件，在各个通道上切换
* Buffer 就是一个内存块 ， 底层是一个数组
* 数据的读取写入是通过 Buffer完成的 , BIO 中要么是输入流，或者是输出流, 不能双向，但是 NIO 的 Buffer 是可以读也可以写。
* Java NIO系统的核心在于：通道(Channel)和缓冲区 (Buffer)。通道表示打开到 IO 设备(例如：文件、 套接字)的连接。若需要使用 NIO 系统，需要获取 用于连接 IO 设备的通道以及用于容纳数据的缓冲 区。然后操作缓冲区，对数据进行处理。简而言之，Channel 负责传输， Buffer 负责存取数据

### <3> NIO核心一：缓冲区(Buffer)

#### 缓冲区（Buffer）

一个用于特定基本数据类 型的容器。由 java.nio 包定义的，所有缓冲区 都是 Buffer 抽象类的子类.。Java NIO 中的 Buffer 主要用于与 NIO 通道进行 交互，数据是从通道读入缓冲区，从缓冲区写入通道中的

![image-20200619163952309](C:/Users/26905/Desktop/讲义/BIO、NIO、AIO.assets/image-20200619163952309.png)

#### **Buffer 类及其子类**

**Buffer** 就像一个数组，可以保存多个相同类型的数据。根 据数据类型不同 ，有以下 Buffer 常用子类： 

* ByteBuffer 
* CharBuffer 
* ShortBuffer 
* IntBuffer 
* LongBuffer 
* FloatBuffer 
* DoubleBuffer 

上述 Buffer 类 他们都采用相似的方法进行管理数据，只是各自 管理的数据类型不同而已。都是通过如下方法获取一个 Buffer 对象：

```java
static XxxBuffer allocate(int capacity) : 创建一个容量为capacity 的 XxxBuffer 对象
```

#### 缓冲区的基本属性

Buffer 中的重要概念： 

* **容量 (capacity)** ：作为一个内存块，Buffer具有一定的固定大小，也称为"容量"，缓冲区容量不能为负，并且创建后不能更改。 
* **限制 (limit)**：表示缓冲区中可以操作数据的大小（limit 后数据不能进行读写）。缓冲区的限制不能为负，并且不能大于其容量。 **写入模式，限制等于buffer的容量。读取模式下，limit等于写入的数据量**。
* **位置 (position)**：下一个要读取或写入的数据的索引。缓冲区的位置不能为 负，并且不能大于其限制 
* **标记 (mark)与重置 (reset)**：标记是一个索引，通过 Buffer 中的 mark() 方法 指定 Buffer 中一个特定的 position，之后可以通过调用 reset() 方法恢复到这 个 position.
  **标记、位置、限制、容量遵守以下不变式： 0 <= mark <= position <= limit <= capacity**
* **图示:**
* ![image-20200619172434538](C:/Users/26905/Desktop/讲义/BIO、NIO、AIO.assets/image-20200619172434538.png)

#### Buffer常见方法

```java
Buffer clear() 清空缓冲区并返回对缓冲区的引用
Buffer flip() 为 将缓冲区的界限设置为当前位置，并将当前位置充值为 0
int capacity() 返回 Buffer 的 capacity 大小
boolean hasRemaining() 判断缓冲区中是否还有元素
int limit() 返回 Buffer 的界限(limit) 的位置
Buffer limit(int n) 将设置缓冲区界限为 n, 并返回一个具有新 limit 的缓冲区对象
Buffer mark() 对缓冲区设置标记
int position() 返回缓冲区的当前位置 position
Buffer position(int n) 将设置缓冲区的当前位置为 n , 并返回修改后的 Buffer 对象
int remaining() 返回 position 和 limit 之间的元素个数
Buffer reset() 将位置 position 转到以前设置的 mark 所在的位置
Buffer rewind() 将位置设为为 0， 取消设置的 mark
```

#### 缓冲区的数据操作

```java
Buffer 所有子类提供了两个用于数据操作的方法：get()put() 方法
取获取 Buffer中的数据
get() ：读取单个字节
get(byte[] dst)：批量读取多个字节到 dst 中
get(int index)：读取指定索引位置的字节(不会移动 position)
    
放到 入数据到 Buffer 中 中
put(byte b)：将给定单个字节写入缓冲区的当前位置
put(byte[] src)：将 src 中的字节写入缓冲区的当前位置
put(int index, byte b)：将指定字节写入缓冲区的索引位置(不会移动 position)
```

**使用Buffer读写数据一般遵循以下四个步骤：**

* 1.写入数据到Buffer
* 2.调用flip()方法，转换为读取模式
* 3.从Buffer中读取数据
* 4.调用buffer.clear()方法或者buffer.compact()方法清除缓冲区

#### 案例演示

```java
public class TestBuffer {
   @Test
   public void test3(){
      //分配直接缓冲区
      ByteBuffer buf = ByteBuffer.allocateDirect(1024);
      System.out.println(buf.isDirect());
   }
   
   @Test
   public void test2(){
      String str = "itheima";
      
      ByteBuffer buf = ByteBuffer.allocate(1024);
      
      buf.put(str.getBytes());
      
      buf.flip();
      
      byte[] dst = new byte[buf.limit()];
      buf.get(dst, 0, 2);
      System.out.println(new String(dst, 0, 2));
      System.out.println(buf.position());
      
      //mark() : 标记
      buf.mark();
      
      buf.get(dst, 2, 2);
      System.out.println(new String(dst, 2, 2));
      System.out.println(buf.position());
      
      //reset() : 恢复到 mark 的位置
      buf.reset();
      System.out.println(buf.position());
      
      //判断缓冲区中是否还有剩余数据
      if(buf.hasRemaining()){
         //获取缓冲区中可以操作的数量
         System.out.println(buf.remaining());
      }
   }
    
   @Test
   public void test1(){
      String str = "itheima";
      //1. 分配一个指定大小的缓冲区
      ByteBuffer buf = ByteBuffer.allocate(1024);
      System.out.println("-----------------allocate()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      
      //2. 利用 put() 存入数据到缓冲区中
      buf.put(str.getBytes());
      System.out.println("-----------------put()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      
      //3. 切换读取数据模式
      buf.flip();
      System.out.println("-----------------flip()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      
      //4. 利用 get() 读取缓冲区中的数据
      byte[] dst = new byte[buf.limit()];
      buf.get(dst);
      System.out.println(new String(dst, 0, dst.length));

      System.out.println("-----------------get()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      //5. rewind() : 可重复读
      buf.rewind();
      System.out.println("-----------------rewind()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      
      //6. clear() : 清空缓冲区. 但是缓冲区中的数据依然存在，但是处于“被遗忘”状态
      buf.clear();
      System.out.println("-----------------clear()----------------");
      System.out.println(buf.position());
      System.out.println(buf.limit());
      System.out.println(buf.capacity());
      System.out.println((char)buf.get());
      
   }

}
```

#### 直接与非直接缓冲区

什么是直接内存与非直接内存

根据官方文档的描述：

`byte byffer`可以是两种类型，一种是基于直接内存（也就是非堆内存）；另一种是非直接内存（也就是堆内存）。对于直接内存来说，JVM将会在IO操作上具有更高的性能，因为它直接作用于本地系统的IO操作。而非直接内存，也就是堆内存中的数据，如果要作IO操作，会先从本进程内存复制到直接内存，再利用本地IO处理。

从数据流的角度，非直接内存是下面这样的作用链：

```
本地IO-->直接内存-->非直接内存-->直接内存-->本地IO
```

而直接内存是：

```
本地IO-->直接内存-->本地IO
```

很明显，在做IO处理时，比如网络发送大量数据时，直接内存会具有更高的效率。直接内存使用allocateDirect创建，但是它比申请普通的堆内存需要耗费更高的性能。不过，这部分的数据是在JVM之外的，因此它不会占用应用的内存。所以呢，当你有很大的数据要缓存，并且它的生命周期又很长，那么就比较适合使用直接内存。只是一般来说，如果不是能带来很明显的性能提升，还是推荐直接使用堆内存。字节缓冲区是直接缓冲区还是非直接缓冲区可通过调用其 isDirect()  方法来确定。

**使用场景**

- 1 有很大的数据需要存储，它的生命周期又很长
- 2 适合频繁的IO操作，比如网络并发场景



### <4> NIO核心二：通道(Channel)

#### 通道Channe概述

通道（Channel）：由 java.nio.channels 包定义 的。Channel 表示 IO 源与目标打开的连接。 Channel 类似于传统的“流”。只不过 Channel 本身不能直接访问数据，Channel 只能与 Buffer 进行交互。

1、 NIO 的通道类似于流，但有些区别如下：

* 通道可以同时进行读写，而流只能读或者只能写

* 通道可以实现异步读写数据

* 通道可以从缓冲读数据，也可以写数据到缓冲:

2、BIO 中的 stream 是单向的，例如 FileInputStream 对象只能进行读取数据的操作，而 NIO 中的通道(Channel)
  是双向的，可以读操作，也可以写操作。

3、Channel 在 NIO 中是一个接口

```java
public interface Channel extends Closeable{}
```

#### 常用的Channel实现类

* FileChannel：用于读取、写入、映射和操作文件的通道。
* DatagramChannel：通过 UDP 读写网络中的数据通道。
* SocketChannel：通过 TCP 读写网络中的数据。
* ServerSocketChannel：可以监听新进来的 TCP 连接，对每一个新进来的连接都会创建一个 SocketChannel。 【ServerSocketChanne 类似 ServerSocket , SocketChannel 类似 Socket】

#### FileChannel 类

获取通道的一种方式是对支持通道的对象调用getChannel() 方法。支持通道的类如下：

* FileInputStream
* FileOutputStream
* RandomAccessFile
* DatagramSocket
* Socket
* ServerSocket
  获取通道的其他方式是使用 Files 类的静态方法 newByteChannel() 获取字节通道。或者通过通道的静态方法 open() 打开并返回指定通道

#### FileChannel的常用方法

```java
int read(ByteBuffer dst) 从 从  Channel 到 中读取数据到  ByteBuffer
long  read(ByteBuffer[] dsts) 将 将  Channel 到 中的数据“分散”到  ByteBuffer[]
int  write(ByteBuffer src) 将 将  ByteBuffer 到 中的数据写入到  Channel
long write(ByteBuffer[] srcs) 将 将  ByteBuffer[] 到 中的数据“聚集”到  Channel
long position() 返回此通道的文件位置
FileChannel position(long p) 设置此通道的文件位置
long size() 返回此通道的文件的当前大小
FileChannel truncate(long s) 将此通道的文件截取为给定大小
void force(boolean metaData) 强制将所有对此通道的文件更新写入到存储设备中
```

#### 案例1-本地文件写数据

需求：使用前面学习后的 ByteBuffer(缓冲) 和 FileChannel(通道)， 将 "hello,黑马Java程序员！" 写入到 data.txt 中.

```java
package com.itheima;


import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ChannelTest {
    @Test
    public void write(){
        try {
            // 1、字节输出流通向目标文件
            FileOutputStream fos = new FileOutputStream("data01.txt");
            // 2、得到字节输出流对应的通道Channel
            FileChannel channel = fos.getChannel();
            // 3、分配缓冲区
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            buffer.put("hello,黑马Java程序员！".getBytes());
            // 4、把缓冲区切换成写出模式
            buffer.flip();
            channel.write(buffer);
            channel.close();
            System.out.println("写数据到文件中！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

#### 案例2-本地文件读数据

需求：使用前面学习后的 ByteBuffer(缓冲) 和 FileChannel(通道)， 将 data01.txt 中的数据读入到程序，并显示在控制台屏幕

```java
public class ChannelTest {

    @Test
    public void read() throws Exception {
        // 1、定义一个文件字节输入流与源文件接通
        FileInputStream is = new FileInputStream("data01.txt");
        // 2、需要得到文件字节输入流的文件通道
        FileChannel channel = is.getChannel();
        // 3、定义一个缓冲区
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        // 4、读取数据到缓冲区
        channel.read(buffer);
        buffer.flip();
        // 5、读取出缓冲区中的数据并输出即可
        String rs = new String(buffer.array(),0,buffer.remaining());
        System.out.println(rs);

    }
```

#### 案例3-使用Buffer完成文件复制

使用 FileChannel(通道) ，完成文件的拷贝。

```java
@Test
public void copy() throws Exception {
    // 源文件
    File srcFile = new File("C:\\Users\\dlei\\Desktop\\BIO,NIO,AIO\\文件\\壁纸.jpg");
    File destFile = new File("C:\\Users\\dlei\\Desktop\\BIO,NIO,AIO\\文件\\壁纸new.jpg");
    // 得到一个字节字节输入流
    FileInputStream fis = new FileInputStream(srcFile);
    // 得到一个字节输出流
    FileOutputStream fos = new FileOutputStream(destFile);
    // 得到的是文件通道
    FileChannel isChannel = fis.getChannel();
    FileChannel osChannel = fos.getChannel();
    // 分配缓冲区
    ByteBuffer buffer = ByteBuffer.allocate(1024);
    while(true){
        // 必须先清空缓冲然后再写入数据到缓冲区
        buffer.clear();
        // 开始读取一次数据
        int flag = isChannel.read(buffer);
        if(flag == -1){
            break;
        }
        // 已经读取了数据 ，把缓冲区的模式切换成可读模式
        buffer.flip();
        // 把数据写出到
        osChannel.write(buffer);
    }
    isChannel.close();
    osChannel.close();
    System.out.println("复制完成！");
}
```

#### 案例4-分散 (Scatter) 和聚集 (Gather)

分散读取（Scatter ）:是指把Channel通道的数据读入到多个缓冲区中去 

聚集写入（Gathering ）是指将多个 Buffer 中的数据“聚集”到 Channel。

```java
//分散和聚集
@Test
public void test() throws IOException{
		RandomAccessFile raf1 = new RandomAccessFile("1.txt", "rw");
	//1. 获取通道
	FileChannel channel1 = raf1.getChannel();
	
	//2. 分配指定大小的缓冲区
	ByteBuffer buf1 = ByteBuffer.allocate(100);
	ByteBuffer buf2 = ByteBuffer.allocate(1024);
	
	//3. 分散读取
	ByteBuffer[] bufs = {buf1, buf2};
	channel1.read(bufs);
	
	for (ByteBuffer byteBuffer : bufs) {
		byteBuffer.flip();
	}
	
	System.out.println(new String(bufs[0].array(), 0, bufs[0].limit()));
	System.out.println("-----------------");
	System.out.println(new String(bufs[1].array(), 0, bufs[1].limit()));
	
	//4. 聚集写入
	RandomAccessFile raf2 = new RandomAccessFile("2.txt", "rw");
	FileChannel channel2 = raf2.getChannel();
	
	channel2.write(bufs);
}
```

#### 案例5-transferFrom()

从目标通道中去复制原通道数据

```java
@Test
public void test02() throws Exception {
    // 1、字节输入管道
    FileInputStream is = new FileInputStream("data01.txt");
    FileChannel isChannel = is.getChannel();
    // 2、字节输出流管道
    FileOutputStream fos = new FileOutputStream("data03.txt");
    FileChannel osChannel = fos.getChannel();
    // 3、复制
    osChannel.transferFrom(isChannel,isChannel.position(),isChannel.size());
    isChannel.close();
    osChannel.close();
}
```

#### 案例6-transferTo()

把原通道数据复制到目标通道

```java
@Test
public void test02() throws Exception {
    // 1、字节输入管道
    FileInputStream is = new FileInputStream("data01.txt");
    FileChannel isChannel = is.getChannel();
    // 2、字节输出流管道
    FileOutputStream fos = new FileOutputStream("data04.txt");
    FileChannel osChannel = fos.getChannel();
    // 3、复制
    isChannel.transferTo(isChannel.position() , isChannel.size() , osChannel);
    isChannel.close();
    osChannel.close();
}
```

### <5> NIO核心三：选择器(Selector)

#### 选择器(Selector)概述

选择器（Selector） 是 SelectableChannle 对象的多路复用器，Selector 可以同时监控多个 SelectableChannel 的 IO 状况，也就是说，利用 Selector可使一个单独的线程管理多个 Channel。Selector 是非阻塞 IO 的核心

![image-20200619230246145](C:/Users/26905/Desktop/讲义/BIO、NIO、AIO.assets/image-20200619230246145.png)

* Java 的 NIO，用非阻塞的 IO 方式。可以用一个线程，处理多个的客户端连接，就会使用到 Selector(选择器)
* Selector 能够检测多个注册的通道上是否有事件发生(注意:多个 Channel 以事件的方式可以注册到同一个
  Selector)，如果有事件发生，便获取事件然后针对每个事件进行相应的处理。这样就可以只用一个单线程去管
  理多个通道，也就是管理多个连接和请求。
* 只有在 连接/通道 真正有读写事件发生时，才会进行读写，就大大地减少了系统开销，并且不必为每个连接都
  创建一个线程，不用去维护多个线程
* 避免了多线程之间的上下文切换导致的开销

#### 选择 器（Selector）的应用

创建 Selector ：通过调用 Selector.open() 方法创建一个 Selector。

```java
Selector selector = Selector.open();
```

向选择器注册通道：SelectableChannel.register(Selector sel, int ops)

```java
//1. 获取通道
ServerSocketChannel ssChannel = ServerSocketChannel.open();
//2. 切换非阻塞模式
ssChannel.configureBlocking(false);
//3. 绑定连接
ssChannel.bind(new InetSocketAddress(9898));
//4. 获取选择器
Selector selector = Selector.open();
//5. 将通道注册到选择器上, 并且指定“监听接收事件”
ssChannel.register(selector, SelectionKey.OP_ACCEPT);
```

 当调用 register(Selector sel, int ops) 将通道注册选择器时，选择器对通道的监听事件，需要通过第二个参数 ops 指定。可以监听的事件类型（用 可使用 SelectionKey  的四个常量 表示）：

* 读 : SelectionKey.OP_READ （1）
* 写 : SelectionKey.OP_WRITE （4）
* 连接 : SelectionKey.OP_CONNECT （8）
* 接收 : SelectionKey.OP_ACCEPT （16）
* 若注册时不止监听一个事件，则可以使用“位或”操作符连接。

```java
int interestSet = SelectionKey.OP_READ|SelectionKey.OP_WRITE 
```

## 6. AIO

* Java AIO(NIO.2) ： 异步非阻塞，服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。

```java
AIO
异步非阻塞，基于NIO的，可以称之为NIO2.0
    BIO                   NIO                              AIO        
Socket                SocketChannel                    AsynchronousSocketChannel
ServerSocket          ServerSocketChannel	       AsynchronousServerSocketChannel
```


与NIO不同，当进行读写操作时，只须直接调用API的read或write方法即可, 这两种方法均为异步的，对于读操作而言，当有流可读取时，操作系统会将可读的流传入read方法的缓冲区,对于写操作而言，当操作系统将write方法传递的流写入完毕时，操作系统主动通知应用程序

即可以理解为，read/write方法都是异步的，完成后会主动调用回调函数。在JDK1.7中，这部分内容被称作NIO.2，主要在Java.nio.channels包下增加了下面四个异步通道：

```java
	AsynchronousSocketChannel
	AsynchronousServerSocketChannel
	AsynchronousFileChannel
	AsynchronousDatagramChannel
```

## 7. BIO,NIO,AIO总结

**BIO、NIO、AIO：**

- Java BIO ： 同步并阻塞，服务器实现模式为一个连接一个线程，即客户端有连接请求时服务器端就需要启动一个线程进行处理，如果这个连接不做任何事情会造成不必要的线程开销，当然可以通过线程池机制改善。
- Java NIO ： 同步非阻塞，服务器实现模式为一个请求一个线程，即客户端发送的连接请求都会注册到多路复用器上，多路复用器轮询到连接有I/O请求时才启动一个线程进行处理。
- Java AIO(NIO.2) ： 异步非阻塞，服务器实现模式为一个有效请求一个线程，客户端的I/O请求都是由OS先完成了再通知服务器应用去启动线程进行处理。

**BIO、NIO、AIO适用场景分析:**

- BIO方式适用于连接数目比较小且固定的架构，这种方式对服务器资源要求比较高，并发局限于应用中，JDK1.4以前的唯一选择，但程序直观简单易理解。
- NIO方式适用于连接数目多且连接比较短（轻操作）的架构，比如聊天服务器，并发局限于应用中，编程比较复杂，JDK1.4开始支持。
- AIO方式使用于连接数目多且连接比较长（重操作）的架构，比如相册服务器，充分调用OS参与并发操作，编程比较复杂，JDK7开始支持。Netty!





# 十.JVM

## 1.类加载

### <1>.父子类执行的顺序

1.父类的静态变量和静态代码块(书写顺序)

2.子类的静态变量和静态代码块(书写顺序)

3.父类的实例代码块(书写顺序) 	

4.父类的成员变量和构造方法

5.子类的实例代码块

6.子类的成员变量和构造方法

### <2>类加载的时机

如果类没有进行初始化，则需要先进行初始化，虚拟机规范则是严格规定有且只有5种情况必须先对类进行初始化(而加载，验证，准备要在这个之前开始)

1.创建类的实例(new的方式)，访问某个类的静态变量，或者对该静态变量赋值，调用类的静态方法

2.反射的方式

3.初始化某个类的子类，则其父类也会被初始化

4.java虚拟机启动时被标记为启动类的类，直接使用java.exe来运行的某个主类(如main类)

5.使用jdk1.7的动态语言支持时

### <3>类的生命周期

七个阶段：加载，验证，准备，解析，初始化，使用和卸载。其中验证，准备和解析三个部分被称为连接

![img](file:///C:/Users/26905/Desktop/%E6%AF%94%E7%89%B9%E8%AF%BE%E4%BB%B6/%E7%B1%BB%E5%8A%A0%E8%BD%BD_files/Image.png)

**解析阶段在某些情况下可以在初始化阶段之后再进行，这是为了支持java语言的运行时绑定(动态绑定)**

### <4>类加载的过程

接下来我们详细讲解一下Java虚拟机中类加载的全过程，也就是加载、验证、准备、解析和初始化这5个阶段所执行的具体动作。

1.加载

<1>通过一个类的全限定名来获取定义此类的二进制字节流。

<2>将这个字节流所代表的静态存储结构转化为方法区的运行时数据结构。

<3>在内存中生成一个代表这个类的java.lang.Class对象，作为方法区这个类的各种数据的访问入口。



2.验证

这一阶段的目的是为了确保Class文件的字节流中包含的信息符合当前虚拟机的要求，并且不会危害虚拟机自身的安全。



3.准备

准备阶段是正式为**类变量**分配内存并设置类变量**初始值**的阶段，这些变量所使用的内存都将在方法区中进行分配。

假设一个类变量的定义为：

```java
public static int value=123;
```

那变量value在准备阶段过后的初始值为0而不是123，因为这时候尚未开始执行任何Java方法，而把value赋值为123的putstatic指令是程序被编译后，存放于类构造器<clinit>()方法之中，所以把value赋值为123的动作将在初始化阶段才会执行。

4.解析

虚拟机将常量池内的符号引用替换为直接引用的过程。

**符号引用**：符号引用与虚拟机实现的内存布局无关，引用的目标并不一定已经加载到内存中。

**直接引用**：直接引用是和虚拟机实现的内存布局相关的。如果有了直接引用，那引用的目标必定已经在内存中存在。

5.初始化

在准备阶段，变量已经赋过一次系统要求的初始值，而在初始化阶段，则根据程序员通过程序制定的主观计划去初始化类变量和其他资源，或者可以从另外一个角度来表达：初始化阶段是执行类构造器<clinit>()方法的过程。

**了解：**

<clinit>()方法是由编译器自动收集类中的所有类变量的赋值动作和静态语句块（static{}块）中的语句合并产生的，编译器收集的顺序是由语句在源文件中出现的顺序所决定的，静态语句块中只能访问到定义在静态语句块之前的变量，定义在它之后的变量，在前面的静态语句块可以赋值，但是不能访问：

```java
public class Test{
    static{
        i=0; //给变量赋值可以正常编译通过
        System.out.print(i); //这句编译器会提示"非法向前引用"
    }
    static int i=1；
}
```

1.<clinit>()方法(**Class类的构造方法**)与类的构造函数（或者说实例构造器<init>()方法）不同，它不需要显式地调用父类构造器，虚拟机会保证在子类的<init>()方法执行之前，父类的<clinit>()方法已经执行完毕。因此在虚拟机中第一个被执行的<clinit>()方法的类肯定是java.lang.Object。

2.<clinit>()方法对于类或接口来说并不是必需的，如果一个类中没有静态语句块，也没有对变量的赋值操作，那么编译器可以不为这个类生成<clinit>()方法。

3.接口中定义的变量使用时，接口才会初始化：接口中不能使用静态语句块，但仍然有变量初始化的赋值操作，因此接口与类一样都会生<clinit>()方法。但接口与类不同的是，执行接口的<clinit>()方法不需要先执行父接口的<clinit>()方法。只有当父接口中定义的变量使用时，父接口才会初始化。另外，接口的实现类在初始化时也一样不会执行接口的<clinit>()方法。

4.虚拟机会保证一个类的<clinit>()方法在多线程环境中被正确地加锁、同步，如果多个线程同时去初始化一个类，那么只会有一个线程去执行这个类的<clinit>()方法，其他线程都需要阻塞等待，直到活动线程执行<clinit>()方法完毕。如果在一个类的<clinit>()方法中有耗时很长的操作，就可能造成多个进程阻塞，在实际应用中这种阻塞往往是很隐蔽的。

### <5>类加载器

类加载器可以分为：启动类加载器、扩展类加载器、应用程序类加载器、自定义类加载器。他们的关系一般如下：

![image-20210728134416563](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728134416563.png)

1.启动类加载器（BootstrapClassLoader）
这个类由C++语言实现，是虚拟机自身的一部分，并不继承ClassLoader，不能操作它。用来加载Java的核心类。

2.扩展类加载器（ExtClassLoader）
这个类加载器是在类sun.misc.Launcher$ExtClassLoader中以Java代码的形式实现的。它负责加载<JAVA_HOME>\lib\ext目录中，或者被java.ext.dirs系统变量所指定的路径中所有的类库。

3.应用程序类加载器（AppClassLoader）
它负责在 JVM 启动时加载来自 Java 命令的 -classpath 或者 -cp 选项、java.class.path 系统属性指定的 jar 包和类路径。在应用程序代码里可以通过 ClassLoader 的静态方法 getSystemClassLoader() 来获取应用类加载器。如果没有特别指定，则在没有使用自定义类加载器情况下，用户自定义的类都由此加载器加载。

4.2 自定义加载器
用户自定义了类加载器，则自定义类加载器都以应用类加载器作为父加载器。应用类加载器的父类加载器为扩展类加载器。这些类加载器是有层次关系的，启动加载器又叫根加载器，是扩展加载器的父加载器

### <6>类加载机制——双亲委派模型

双亲委派模型的过程：如果一个类加载器收到了类加载的请求，它首先不会自己尝试加载这个类，而是把这个请求委派给父类加载器去完成，每一层次的类加载器都是如此，因此所有的加载请求信息最终都会传送到最顶层的启动类加载器中，只有当父加载器反馈自己无法完成这个加载请求(即它的搜索范围没有找到所需要的类)时，子加载器才会尝试自己去完成加载

先查找，再进行加载

(1)从下往上找

(2)从上往下加载

双亲委派模型的好处：双亲委派模型对于java程序的稳定运行极为重要

劣势：无法满足灵活的类加载方式。(解决方案：自己重写loadClass破坏双亲委派模型 例如SPI机制)



## 2.Java内存模型(JMM)

### <1>线程私有的内存区域

**程序计数器：** 一块比较小的内存空间，可以看做是当前线程所执行的字节码的行号指示器。

**Java虚拟机栈：** 每个方法执行的同时都会创建一个栈帧用于存储局部变量表、操作数栈、动态链接、方法出口等信息。每一个方法从调用直至执行完成的过程，就对应一个栈帧在虚拟机栈中入栈和出栈的过程。

 此区域一共会产生以下两种异常:

<1>如果线程请求的栈深度大于虚拟机所允许的深度(-Xss设置栈容量)，将会抛出StackOverFlowError异常。     

<2>虚拟机在动态扩展时无法申请到足够的内存，会抛出OOM(OutOfMemoryError)异常

**本地方法栈:**    本地方法栈与虚拟机栈的作用完全一样，他俩的区别无非是本地方法栈为虚拟机使用的Native方法服务，而虚拟机栈为JVM执行的Java方法服务。

### <2>线程共享的内存区域

**Java堆:**  在JVM启动时创建，所有的对象实例以及数组都要在堆上分配。如果在堆中没有足够的内存完成实例分配并且堆也无法再拓展时，将会抛出OOM。

**方法区/元数据区:**

用于存储已被虚拟机加载的类信息、常量、静态变量、即时编译器编译后的代码等数据。
此区域的内存回收主要是针对常量池的回收以及对类型的卸载。当方法区无法满足内存分配需求时，将抛出OOM异常。

**运行时常量池:** 编译期及运行期间产生的常量被放在运行时常量池中。
   这里所说的常量包括：基本类型、包装类（包装类不管理浮点型，整形只会管理-128到127）和String。
   类加载时，会查询字符串常量池，以保证运行时常量池所引用的字符串与字符串常量池中是一致的。

**直接内存：**直接内存并不是虚拟机运行时数据区的一部分， 也不是《Java虚拟机规范》中定义的内存区域。但是这部分内存也被频繁地使用，而且也可能导致OutOfMemoryError异常出现。



**会发生OOM的区域：**方法区，堆，Java虚拟机栈,本地方法栈

### 	<3>内存模型

**JDK1.7:**

![image-20210728142336299](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728142336299.png)

**JDK1.8:**

![image-20210728142357288](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728142357288.png)

### <4>对常量池的说明

<1>类常量池: Class文件中除了有类的版本、字段、方法、接口等描述信息外，还有一项信息是常量池（Constant Pool Table），用于存放编译期生成的各种字面量和符号引用，这部分内容将在类加载后进入方法区的运行时常量池中存放。

<2>运行时常量池:运行时常量池相对于Class文件常量池的另外一个重要特征是具备动态性，Java语言并不要求常量一定只有编译期才能产生，也就是并非预置入Class文件中常量池的内容才能进入方法区运行时常量池，运行期间也可能将新的常量放入池中。

<3>字符串常量池:   存储字符串对象，或是字符串对象的引用。

## 3.垃圾回收前置知识

### <1>对创建对象，内存，GC的笼统理解

![image-20210728170708913](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728170708913.png)

### <2>垃圾回收策略(如何判断对象已死)

#### (1)**引用计数算法**

给对象中添加一个引用计数器，每当有一个地方引用它时，计数器值就加1；当引用失效时，计数器值

就减1；任何时刻计数器为0的对象就是不可能再被使用的。但是它很难解决对象之间相互循环引用的问题

#### (2)可达性分析算法

通过一系列的称为“**GC Roots**”的对象作为起始点，从这些节点开始向下搜索，搜索所走过的路径称为

**GC Roots**引用链（Reference Chain），当一个对象到GC Roots没有任何引用链相连（用图论的话来

说，就是从GC Roots到这个对象不可达）时，则证明此对象是不可用的。

![image-20210728162515560](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728162515560.png)

如图：Object5和Object6即不可达

**注意：**当一个引用t=null时，代表t不指向任何对象，而不是指向一个null的对象

### <3> 四种引用类型

**强引用：**就是我们最常见的普通对象的引用，只要还有强引用指向一个对象，就能表明还有对象还"活着"，垃圾回收器就不会回收这种对象

**软引用:**用来描述一些还有用但并非必需的对象。对于软引用关联着的对象，在系统将要发生内存溢出异常之

前，将会把这些对象列进回收范围之中进行第二次回收。如果这次回收还没有足够的内存，才会抛出内

存溢出异常。在JDK 1.2之后，提供了SoftReference类来实现软引用。

**弱引用：**垃圾回收器一旦发现了只具有弱引用的对象，不管当前内存空间是否足够，都会回收它的内存.

**虚引用:**也称为幽灵引用或者幻影引用，它是最弱的一种引用关系。一个对象是否有虚引用的存在，完全不会对

其生存时间构成影响，也无法通过虚引用来取得一个对象实例。为一个对象设置虚引用

（PhantomReference）关联的唯一目的就是能在这个对象被收集器回收时收到一个系统通知。

### <4>内存泄漏和内存溢出

内存溢出：内存溢出就是你要的内存空间超过了系统实际分配给你的空间，此时系统相当于没法满足你的需求，就会报内存溢出的错误

内存泄漏：内存泄漏是指程序中已经动态分配的堆内存由于某种原因程序未释放或无法释放(new了不delete)，造成了系统内存的浪费，导致程序运行速度减慢甚至崩溃等严重后果.

内存泄漏的堆积终将会导致内存溢出.但内存溢出不一定时内存泄漏导致的.



### <5>需要垃圾回收的内存

#### (1).方法区(jdk1.7)/元空间(jdk1.8)

JDK1.7的方法区在GC中一般称为**永久代（Permanent Generation）**。

JDK1.8的元空间存在于本地内存，GC也是即对元空间垃圾回收。

永久代或元空间的垃圾收集主要回收两部分内容：废弃常量和无用的类。此区域进行垃圾收集

的“性价比”一般比较低。



1.**新生代（Young Generation）**：又可以分为Eden空间、From Survivor空间、To Survivor空间。

新生代的垃圾回收又称为**Young GC**（**YGC**）、**Minor GC**。指发生在新生代的垃圾收集动作，因为Java对象大多都具备朝生夕灭的特性，所以Minor GC非常频繁，一般回收速度也比较快。

**朝生夕灭：**

```java

    public static void main(String[] args) {
        createObject();
    }
    public static void createObject(){
        Object[] temp=new Object[10];
        for (int i = 0; i <10 ; i++) {
            temp[i]=new Object();
        }
    }
```

例如上述代码：当调用createObject方法时，会创建10个Object对象，而当方法结束，局部变量temp销毁，则创建的十个对象不可达，即方法调用结束之后，对象无用.

即：方法调用，方法返回之后，方法栈帧出栈，局部变量消失，则局部变量引用的对象不可达

2.**老年代（Old Generation、Tenured Generation)**。

老年代垃圾回收又称为**Major GC**。

指发生在老年代的GC，出现了Major GC，经常会伴随至少一次的Minor GC（但非绝对的，在Parallel Scavenge收集器的收集策略里就有直接进行Major GC的策略选择过程。

Major GC的速度一般会比Minor GC慢10倍以上。

3.Full GC：在不同的语义条件下，对Full GC的定义也不同，有时候指老年代的垃圾回收，有时候指全堆（新生代+老年代）的垃圾回收，还可能指有用户线程暂停（Stop-The-World）的垃圾回收（如GC日志中）。

![image-20210728165730095](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728165730095.png)

## 4.GC

### <1>垃圾回收算法

#### (1)老年代回收算法

**1.标记清除算法(Mark-Sweep):**先将无用对象标记出来，再进行删除

![image-20210728171057942](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728171057942.png)

缺陷：

1. 效率问题，标记和清除两个过程的效率都不高。

2. 空间问题，标记清除之后会产生大量不连续的内存碎片，空间碎片太多可能会导致以后在程序运行过程中需要分配较大对象时，无法找到足够的连续内存而不得不提前触发另一次垃圾收集动作。



**2.标记整理算法(Mark-Compact):**先对无用对象进行标记，然后将有用对象向一端移动，最后清除最后一个有用对象后面的无用对象即可

![image-20210728171506087](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728171506087.png)

#### (2)新生代回收算法

**1.复制算法(Copying算法)**

将可用内存按容量划分为大小相等的两块，每次只使用其中的一块。当这一块的内存用完了，就将还存活着的对象复制到另外一块上面，然后再把已使用过的内存空间一次清理掉。这样使得每次都是对整个半区进行内存回收，内存分配时也就不用考虑内存碎片等复杂情况，只要移动堆顶指针，按顺序分配内存即可，实现简单，运行高效。

缺点：内存利用率只有50%。

![image-20210728201441534](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728201441534.png)

**2.分代收集算法(不固定，不同的内存划分，使用不同的收集算法)**

新生代中98%的对象都是"朝生夕死"的，所以并不需要按照复制算法所要求1 : 1的比例来划分内存空间，而是将内存（新生代内存）分为一块较大的Eden（伊甸园）空间和两块较小的Survivor（幸存者）空间，每次使用Eden和其中一块Survivor（两个Survivor区域一个称为From区，另一个称为To区域）。HotSpot默认Eden与Survivor的大小比例是8 : 1，也就是说Eden :Survivor From : Survivor To = 8 : 1 : 1。所以每次新生代可用内存空间为整个新生代容量的90%，只有10%的内存会被”浪费“。

### <2>垃圾回收的过程

**1.Eden空间不足，触发Minor GC:**用户线程创建的对象优先分配在Eden区，当Eden区空间不够时，会触发Minor GC：将Eden和Survivor中还存活的对象一次性复制到另一块Survivor空间上，最后清理掉Eden和刚才用过的Survivor空间。(分代收集算法)
![image-20210728202227150](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210728202227150.png)

2.垃圾回收结束后，用户线程又开始新创建对象并分配在Eden区，当Eden区空间不足时，重复上次

的步骤进行Minor GC

3.年老对象晋升到老年代

4.Survivor空间不足，存活对象通过分配担保机制进入老年代

5.老年代空间不足，触发Major GC

### <3>内存分配与回收策略

**1.对象优先在Eden分配：**大多数情况下，对象在新生代Eden区中分配。当Eden区没有足够的空间进行分配时，虚拟机将发生一次Minor GC。

**2.大对象直接进入老年代:**

所谓的大对象是指，需要大量连续空间的Java对象，最典型的大对象就是那种很长的字符串以及数组。

大对象对虚拟机的内存分配是一个坏消息，经常出现大对象容易导致内存还有不少空间时就提前触发GC以获取足够的连续空间来放置大对象。

**3.长期存在的对象进入老年代：**，虚拟机给每个对象定义了一个对象年龄(Age)计数器。如果对象在Eden出生并经过一次Minor GC后仍然存活，并且能被Survivor容纳的话，将被移动到Survivor空间中，并且把对象年龄设为1。对象在Survivor空间中每"熬过"一次Minor GC，年龄就增加1岁，当它的年龄增加到一定程度(默认为15岁)，就将晋 升到老年代中。

**4.动态对象年轻判定 ：**在Survivor空间中相同年龄所有对象大小的总和大于Survivor空间的一半，年龄大于或等于该年龄的对象就可以直接进入老年代，无须等到MaxTenuringThreshold中要求的年龄

**5.空间分配担保策略:**

Survivor区只占新生代10%空间，我们没有办法保证每次回收都只有不多于10%的对象存活，当Survivor空间不够用时，需要依赖其他内存（这里指老年代）进行分配担保（HandlePromotion）

**过程：![未命名文件 (3)](C:\Users\26905\Downloads\未命名文件 (3).png)**

### <4>垃圾收集的影响

**STW：**

执行垃圾回收其中某一步骤时，需要暂停用户线程，也就是Stop-The-World（STW）.程序执行时并非在所有地方都能停顿下来开始GC，只有在到达安全点时才能暂停。



**中断方式：**

1.抢先式中断:不需要线程的执行代码主动去配合，在GC发生时，首先把所有线程全部中断，如果发现有线程中断的地方不在安全点上，就恢复线程，让它“跑”到安全点上(现在基本不用)

2.主动式中断：是当GC需要中断线程的时候，不直接对线程操作，仅仅简单地设置一个标志，各个线程执行时主动去轮询这个标志，发现中断标志为真时就自己中断挂起



**垃圾收集器中的两个概念:**

1.并行(Parallel) : 指多条垃圾收集线程并行工作，用户线程仍处于等待状态。

2.并发(Concurrent) : 指用户线程与垃圾收集线程同时执行(不一定并行，可能会交替执行)，用户程

序继续运 行，而垃圾收集程序在另外一个CPU上。



**吞吐量和用户体验(停顿时间):**

1.吞吐量：CPU用于运行用户代码的时间与CPU总消耗时间的比值，即吞吐量=运行用户代码时间/（运行用户代码时间+垃圾收集时间）

**用户体验优先**：用户线程单次停顿时间短，即使总的停顿时间长一点也可以接受。

**吞吐量优先**：用户线程总的停顿时间短，即使单次停顿时间长一点也可以接受。



## 5.垃圾收集器

### <1>概览

![image-20210729132940839](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729132940839.png)

**重点：**

1.用户体验优先的收集器组合

![image-20210729133304311](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729133304311.png)

2.吞吐量优先的收集器组合

![image-20210729133324219](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729133324219.png)

### <2>垃圾收集器

**1.Serial收集器（新生代收集器,串行GC)**

特性：单线程,复制算法,Stop The World（STW）  单核CPU适用，现在基本不用



 **2.ParNew收集器（新生代收集器,并行GC）**

特性：单线程,复制算法,Stop The World（STW）

搭配CMS收集器，在用户体验优先的程序中使用



**3.Parallel Scavenge收集器（新生代收集器,并行GC）**

特性：多线程，复制算法，可控制的吞吐量。 

吞吐量优先收集器，适用吞吐量需求高的任务型程序



**4.Serial Old收集器（老年代收集器，串行GC）**

单线程，标记-整理算法



**5.Parallel Old收集器（老年代收集器，并行GC）**

多线程，标记整理算法



**6.CMS收集器（老年代收集器，并发GC）**

并发收集，低停顿。标记-清除算法



**整个过程分为4个步骤：**

1. **初始标记**（CMS initial mark） 初始标记仅仅只是标记一下GC Roots能直接关联到的对象，

速度很快， 需要“Stop The World”。 

2. **并发标记**（CMS concurrent mark） 并发标记阶段就是进行GC Roots Tracing的过程。（找引用链）

3. **重新标记**（CMS remark） 重新标记阶段是为了修正并发标记期间因用户程序继续运作而导

致标记产生 变动的那一部分对象的标记记录，这个阶段的停顿时间一般会比初始标记阶段稍

长一些，但远比并发标 记的时间短，仍然需要“Stop The World”。 

4. **并发清除**（CMS concurrent sweep） 并发清除阶段会清除对象。

**缺陷：**

1. CMS会抢占CPU资源。并发阶段虽然不会导致用户线程暂停，但却需要CPU分出精力去执行多条

垃圾收集线程，从而使得用户线程的执行速度下降。

2. CMS的“标记-清除”算法，会导致大量空间碎片的产生
3. CMS无法处理**浮动垃圾**，可能会出现“Concurrent Mode Failure”而导致另一次Full GC

**浮动垃圾问题：**并发清理的过程中，由于用户线程还在执行，因此就会继续产生对象和垃圾，这些新的垃圾没有被标记，CMS只能在下一次收集中处理它们。这也导致了CMS不能在老年代几乎完全被填满了再去进行收集，必须预留一部分空间提供给并发收集时程序运作使用



**7.G1垃圾收集器(用户体验优先)**

特性：

1.用在heap memory很大的情况下，把heap划分为很多很多的region块，然后并行的对其进行垃圾回收。

2.G1垃圾回收器回收region的时候基本不会STW，而是基于 most garbage优先回收（整体来看是基于"标记-整理"算法，从局部即两个region之间基于"复制"算法）的策略来对region进行垃圾回收的。

3.用户体验优先

4.无论如何，G1收集器采用的算法都意味着 一个region有可能属于Eden，Survivo或者Tenured内存区域。



**G1的年轻代垃圾收集：**  H区域表示大对象

![image-20210729135608478](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729135608478.png)



​	**G1老年代垃圾收集:**

对于老年代上的垃圾收集，G1垃圾收集器也分为4个阶段，基本跟CMS垃圾收集器一样，但略有不同：



1. 初始标记(Initial Mark)阶段 - 同CMS垃圾收集器的Initial Mark阶段一样，G1也需要暂停应用程序的执行，它会标记从根对象出发，在根对象的第一层孩子节点中标记所有可达的对象。但是G1的垃圾收集器的Initial Mark阶段是跟minor gc一同发生**的。也就是说，在G1中，你不用像在CMS那样，单独暂停应用程序的执行来运行Initial Mark阶段，而是在G1触发minor gc的时候一并将年老代上的Initial Mark给做了。

2. 并发标记(Concurrent Mark)阶段 - 在这个阶段G1做的事情跟CMS一样。但G1同时还多做了一件事情，就是如果在Concurrent Mark阶段中，发现哪些Tenured region中对象的存活率很小或者基本没有对象存活，那G1就会在这个阶段将其回收掉，而不用等到后面的clean up阶段。这也是Garbage First名字的由来。同时,在该阶段，G1会计算每个 region的对象存活率，方便后面的clean up阶段使用 。

3. 最终标记(CMS中的Remark阶段) - 在这个阶段G1做的事情跟CMS一样, 但是采用的算法不同，G1采用一种叫做SATB(snapshot-at-the-begining)的算法能够在Remark阶段更快的标记可达对象。

4. 筛选回收(Clean up/Copy)阶段 - 在G1中，没有CMS中对应的Sweep阶段。相反 它有一个Clean up/Copy阶段，在这个阶段中,G1会挑选出那些对象存活率低的region进行回收，这个阶段也是和minor gc一同发生的,如下图所示：

   ![image-20210729141132430](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729141132430.png)

## 6.垃圾回收的时机

### <1>System.gc()

显示的调用System.gc()：此方法的调用是**建议**JVM进行 FGC（Full GC），虽然只是建议而非一定，但

很多情况下它会触发 FGC，从而增加FGC的频率。一般不使用此方法，让虚拟机自己去管理它的内存。

### <2>JVM垃圾回收机制决定

创建对象时需要分配内存空间，如果空间不足，触发GC

java.lang.Object中有一个fifinalize() 方法，当JVM 确定不再有指向该对象的引用时，垃圾收集器在对象上调用该方法。fifinalize() 方法有点类似对象生命周期的临终方法，JVM 调用该方法，表示该对象即将“死亡”，之后就可以回收该对象了。注意回收还是在JVM 中处理的，所以手动调用某个对象的fifinalize() 方法，不会造成对象的“死亡”。

### <3>GC触发的时机

**Minor GC触发条件：**创建对象在Eden区，且Eden区空间不足

**Majar GC触发的条件：**对象需要存放在老年代，而老年代空间不足，都会触发

1.新生代年老对象晋升

2.大对象直接进入

3.Minor GC的分配担保机制

4.CMS无法处理浮动垃圾，可能会出现“Concurrent Mode Failure”而导致另一次Full GC

## 7.常用的JVM监控工具

![image-20210729151345216](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729151345216.png)

# 十一.Mysql

## 1.常见数据类型

1.数值：

![img](https://img-blog.csdnimg.cn/20210117093206244.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

2.字符串类型

![img](https://img-blog.csdnimg.cn/20210117093228934.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

3.日期类型

![img](https://img-blog.csdnimg.cn/20210117093351998.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

##  2.基础操作CRUD

**数据库创建：**  create database 数据库名称 character set utf8mb4

**数据库删除：**drop database 数据库名称;

**表的创建：**

```mysql
 create table stu_test (
   id int,
   name varchar(20) comment '姓名',
   password varchar(50) comment '密码',   age int,
   sex varchar(1),
   birthday timestamp,
   amout decimal(13,2),
   resume text
);
```

**查看表结构：**desc 表名;

**表的删除：**

```mysql
droptable stu_test;
```

**新增数据：**

单行数据+全列插入：

```mysql
insert into  student values (100, 10000, '唐三藏', NULL);
```

多行数据+指定列插入

```mysql
insert into student (id, sn, name) values
    (102, 20001, '曹孟德'),
    (103, 20002, '孙仲谋');
```

**查询操作：**

```mysql
select * from exam_result;   
select id, name, english from exam_result;
SELECT id, name, chinese + math + english FROM exam_result;

```

**去重：**

```mysql
select distinct math from exam_result;
```

**排序：**

-- ASC 为升序（从小到大）
-- DESC 为降序（从大到小）
-- 默认为 ASC

```mysql
SELECT name, math, english, chinese from exam_result 
order by math DESC, english, chinese;
```

**条件查询：**

```mysql
SELECT * FROM exam_result WHERE chinese > 80and english > 80;-- And和Or
SELECT name, chinese FROM exam_result WHERE chinese BETWEEN80AND90;-- Between And

-- % 匹配任意多个（包括 0 个）字符
SELECT name FROM exam_result WHERE name LIKE'孙%';-- 匹配到孙悟空、孙权-- _ 匹配严格的一个任意字符
SELECT name FROM exam_result WHERE name LIKE'孙_';-- 匹配到孙权

-- Null
select name, qq_mail from student where qq_mail is not null;

-- Limit
-- 起始下标为 0
-- 从 0 开始，筛选 n 条结果
SELECT ... FROMtable_name [WHERE ...] [ORDERBY ...] LIMIT n;
-- 从 s 开始，筛选 n 条结果
SELECT ... FROMtable_name [WHERE ...] [ORDERBY ...] LIMIT s, n;
-- 从 s 开始，筛选 n 条结果，比第二种用法更明确，建议使用
SELECT ... FROMtable_name [WHERE ...] [ORDERBY ...] LIMIT n OFFSET s;
```

**修改：**

```mysql
UPDATE exam_result SET math = 80WHERE name = '孙悟空';
UPDATE exam_result SET math = math + 30ORDERBY chinese + math + english LIMIT 3;
```

**删除：**

```mysql
1.delete(删除表中数据）
语法：DELETEFROM  table_name [WHERE ...] [ORDERBY ...] [LIMIT ...]
         
2.drop（删除表）
drop table 表名
```

**其他操作：**

```mysql
1.增加列：alter table 表名 add 列名 数据类型;
2.删除列:   alter table  表名 drop column 列名;
3.修改列的数据类型：alter table 表名 alter column 列名 新的数据类型
```



**约束：**

NOT NULL - 指示某列不能存储 NULL 值。
UNIQUE - 保证某列的每行必须有唯一的值。
DEFAULT - 规定没有给列赋值时的默认值。
PRIMARY KEY - NOT NULL 和 UNIQUE 的结合。确保某列（或两个列多个列的结合）有唯一标识，有助于更容易更快速地找到表中的一个特定的记录。
FOREIGN KEY - 保证一个表中的数据匹配另一个表中的值的参照完整性。

```mysql
CREATETABLE student (
    id INT PRIMARY KEY auto_increment,
    sn INT UNIQUE,
    name VARCHAR(20) DEFAULT 'unkown',
    qq_mail VARCHAR(20),
    classes_id int,
    FOREIGN KEY (classes_id) REFERENCES classes(id));
```

CHECK - 保证列中的值符合指定的条件。对于MySQL数据库，对CHECK子句进行分析，但是忽略CHECK子句。



**聚合函数:**

![img](https://img-blog.csdnimg.cn/20210117111552894.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

**分组：**GROUP BY

**联合查询：**

1.内连接：

```mysql
select字段from表1 别名1 [inner] join表2 别名2 on连接条件and其他条件;
select字段from表1 别名1,表2 别名2 where连接条件and其他条件;
```

2.外连接：

```mysql
-- 左外连接，表1完全显示
select字段名  from表名1 left join表名2 on 连接条件;
-- 右外连接，表2完全显示
select字段from表名1 right join表名2 on 连接条件;
```
3.自连接:

![img](https://img-blog.csdnimg.cn/2021011711402438.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

## 3.JDBC

1.创建连接：

```java
  DataSource  ds = new MysqlDataSource();
            ((MysqlDataSource) ds).setUrl
                    ("jdbc:mysql://localhost:3306/stu_information?user=root" +
                            "&password=密码&useUnicode=true&characterEncoding=UTF-8");
             connection = ((MysqlDataSource) ds).getConnection();
```

2.创建命令对象Statement，执行sql语句：

![img](https://img-blog.csdnimg.cn/20210119104907870.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)

3.处理结果集ResultSet

![img](https://img-blog.csdnimg.cn/20210119105811366.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQ1NzA0NTI4,size_16,color_FFFFFF,t_70)	4.释放资源

```java
 try {
                if(connection!=null)
                    connection.close();
                if(statement!=null)
                    statement.close();
                if(resultSet!=null)
                    resultSet.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
```

## 4.存储引擎

InnoDB和MyISAM的区别

![image-20210729170936669](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210729170936669.png)

## 5.索引

### <1>索引的理解

什么是索引：排好序的快速查找数据结构，可用高效的获取数据

索引的优势：提高了数据检索的效率，降低了数据库的IO成本；降低了数据排序的成本，降低了CPU的消耗

索引的劣势：索引也要占空间，更新表数据时，不仅要更新数据，还要更新索引，降低了更新表的速度

### <2>索引的建立

单值索引：

```mysql
-- user表的name字段建立索引
create index idx_user_name on user(name)
```

复合索引：

```mysql
-- user表的name和emial建立索引
create index idx_user_nameEmail on user(name,email)
```



```mysql
-- 创建：
create [unique] index indexname on mytable(columnname(length));

-- 如果是char，varchar类型，length可以小于字段实际长度；如果是blob和text类型，必须指定length。
alter mytable add [unique] index [indexname] on (columnname(length))

-- 删除：
drop index [indexname] on mytable;

-- 查看：
show index from table_name\G

```

### <3>Mysql索引结构

**1.B+树索引原理：**

![img](https://img-blog.csdnimg.cn/20190810135649527.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNTc5MDQ1,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20190810135841914.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNTc5MDQ1,size_16,color_FFFFFF,t_70)

2.Hash索引

3.full-text全文索引

4.R-Tree索引

### <4>性能分析

MySQL Query Optimizer  Mysql的性能分析器

- MySQL常见瓶颈
  - CPU：CPU在饱和的时候一般发生在数据装入内存或从磁盘上读取数据时候
  - IO：磁盘I/O瓶颈发生在装入数据远大于内存容量的时候
  - 服务器硬件的性能瓶颈：top，free，iostat和vmstat来查看系统的性能状态

#### (1)Explain

1. 是什么（查看执行计划）：

   使用EXPLAIN关键字可以模拟优化器执行SQL查询语句，从而知道MySQL是如何处理你的SQL语句的。分析你的查询语句或是表结构的性能瓶颈。

2. 能干嘛

   表的读取顺序
   数据读取操作的操作类型
   哪些索引可以使用
   哪些索引被实际使用
   表之间的应用
   每张表有多少行被优化器查询

3. 怎么玩

   Explain+SQL语句

   执行计划包含的信息


   ![img](https://img-blog.csdnimg.cn/20190811134519782.png)

   各字段的解释:

   1. id:表示表的读取顺序--->id如果相同，可用认为是一组，从上往下顺序执行；在所有组中，id值越大，优先级越高，越先执行。

   2. select_type:数据查询的操作类型

   3. type：显示查询使用了何种类型，从最好到最差依此是：system>const>eq_ref>ref>range>index>All

      

      ![image-20210730140729447](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210730140729447.png)

      eq_ref:用到了索引，只查出了一条记录

      ref:用到了索引，查出了多条记录

   4. possible_keys：显示可能应用在这张表中的索引，一个或多个。查询涉及到的字段上若存在索引，则该索引将被列出。但不一定被查询实际使用。----->Mysql认为可能用到的索引

   5. key：实际使用的索引。如果为NULL，则没有使用索引。查询中若使用了覆盖索引，则该索引仅出现在key列表中，不会出现在possible_keys列表中。（覆盖索引：查询的字段与建立的复合索引的个数一一吻合）

   6. key_len：表示索引中使用的字节数，可通过该列计算查询中使用的索引的长度。在不损失精确性的情况下，长度越短越好

      ![image-20210730142613997](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210730142613997.png)

   7. ref：显示索引的哪一列被使用了

      ![image-20210730143738254](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210730143738254.png)

   8. rows：根据表统计信息及索引选用情况，大致估算出找到所需的记录所需要读取的行数。

   9. Extra：包含不适合在其他列中显示但十分重要的额外信息。

      1. Using filesort：说明mysql会对数据使用一个外部的索引排序，而不是按照表内的索引顺序进行读取。MySQL中无法利用索引完成的排序操作成为“文件排序”。
      2. Using temporary：使用了临时表保存中间结果，MySQL在对查询结果排序时使用临时表。常见于排序order by和分组查询group by。
      3. Using index：表示相应的select操作中使用了覆盖索引（Covering Index），避免访问了表的数据行，效率不错！如果同时出现using where，表明索引被用来执行索引键值的查找；如果没有同时出现using where，表明索引用来读取数据而非执行查找动作。
      4. Using where：表明使用了where过滤。
      5. Using join buffer：使用了连接缓存。
      6. impossible where：where子句的值总是false，不能用来获取任何元组。（查询语句中where的条件不可能被满足，恒为False）
      7. select tables optimized away：在没有GROUPBY子句的情况下，基于索引优化MIN/MAX操作或者对于MyISAM存储引擎优化COUNT(*)操作，不必等到执行阶段再进行计算，查询执行计划生成的阶段即完成优化。(不用记)
      8. distinct：优化distinct操作，在找到第一匹配的元组后即停止找相同值的动作。
         

### <5>索引优化

![image-20210801125208819](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210801125208819.png)

总结：

![img](https://img-blog.csdnimg.cn/20190812210654615.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzIxNTc5MDQ1,size_16,color_FFFFFF,t_70)

## 6.数据库系统原理

 [数据库系统原理.pdf](比特课件\数据库系统原理.pdf) 

# 十二.Web

## 1.Servlet

1. >Servlet的工作机制

![img](https://img-blog.csdnimg.cn/2018120522281643.gif)







2. >Servlet的工作原理及工作模式

   **工作模式：**

   - 客户端发送请求至服务器
   - 服务器启动并调用Servlet，Servlet根据客户端请求生成响应内容并将其传给服务器
   - 服务器将响应返回客户端

   **工作原理：**

   * Servlet接口定义了Servlet与servlet容器之间的契约。这个契约是：Servlet容器将Servlet类载入内存，并产生Servlet实例和调用它具体的方法。但是要注意的是，在一个应用程序中，每种Servlet类型只能有一个实例。

   *  用户请求致使Servlet容器调用Servlet的Service（）方法，并传入一个ServletRequest对象和一个ServletResponse对象。ServletRequest对象和ServletResponse对象都是由Servlet容器（例如TomCat）封装好的，并不需要程序员去实现，程序员可以直接使用这两个对象。
   * ServletRequest中封装了当前的Http请求，因此，开发人员不必解析和操作原始的Http数据。ServletResponse表示当前用户的Http响应，程序员只需直接操作ServletResponse对象就能把响应轻松的发回给用户。
   * 对于每一个应用程序，Servlet容器还会创建一个ServletContext对象。这个对象中封装了上下文（应用程序）的环境详情。每个应用程序只有一个ServletContext。每个Servlet对象也都有一个封装Servlet配置的ServletConfig对象。

## 2.Cookie和Session

**背景：**http协议：无状态的协议，第一次请求和第二次请求之间没有联系。

1. >Cookie的原理

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190917204655188.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2NoZW4xMzMzMzMzNjY3Nw==,size_16,color_FFFFFF,t_70)

* 浏览器端第一次发送请求到服务器端
* 服务器端创建Cookie，该Cookie中包含用户的信息，然后将该Cookie发送到浏览器端
* 浏览器端再次访问服务器端时会携带服务器端创建的Cookie
* 服务器端通过Cookie中携带的数据区分不同的用户

2. >Session的原理

   * 浏览器端第一次发送请求到服务器端，服务器端创建一个Session，同时会创建一个特殊的Cookie（name为JSESSIONID的固定值，value为session对象的ID），然后将该Cookie发送至浏览器端

   * 浏览器端发送第N（N>1）次请求到服务器端,浏览器端访问服务器端时就会携带该name为JSESSIONID的Cookie对象

   * 服务器端根据name为JSESSIONID的Cookie的value(sessionId),去查询Session对象，从而区分不同用户。

     

     * name为JSESSIONID的Cookie不存在（关闭或更换浏览器），返回1中重新去创建Session与特殊的Cookie
     * name为JSESSIONID的Cookie存在，根据value中的SessionId去寻找session对象
     * value为SessionId不存在**（Session对象默认存活30分钟）**，返回1中重新去创建Session与特殊的Cookie
     * value为SessionId存在，返回session对象

![img](https://img-blog.csdnimg.cn/2019091720521815.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2NoZW4xMzMzMzMzNjY3Nw==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/2019091720523621.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2NoZW4xMzMzMzMzNjY3Nw==,size_16,color_FFFFFF,t_70)

3. >Cookie和Session的区别

   * cookie数据存放在客户的浏览器上，session数据放在服务器上
   * cookie不是很安全，别人可以分析存放在本地的COOKIE并进行COOKIE欺骗,如果主要考虑到安全应当使用session
   * session会在一定时间内保存在服务器上。当访问增多，会比较占用你服务器的性能，如果主要考虑到减轻服务器性能方面，应当使用COOKIE
   * 单个cookie在客户端的限制是不超过4k，就是说一个站点在客户端存放的COOKIE不超过4k。
   * 所以：将登陆信息等重要信息存放为SESSION;其他信息如果需要保留，可以放在COOKIE中

4. >Cookie和Session的联系

   * Session一般是由Cookie中带有sessionid，然后根据sessionid在本地查询对应的session
   * cookie和session都是用来跟踪浏览器用户身份的会话方式。

## 3.Filter过滤器

过滤器实际上就是过滤器链对web资源进行拦截，做一些处理后再交给下一个过滤器或servlet处理
通常都是用来拦截request进行处理的，也可以对返回的response进行拦截处理

![img](https://img-blog.csdn.net/20180730175152255?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3l1emhpcWlhbmdfMTk5Mw==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

过滤器链执行：filterChain.doFiter(request，response)方法

* 使用场景：统一的资源过滤，如用户会话统一管理，编码格式统一设置等等
* 使用方式：如果不满足条件，不执行doFilter方法，不往下执行(跳转页面或直接输出内容)；如果满足条件，执行过滤器链中的每个doFilter方法，知道执行service方法或将数据传到客户端.

## 4.Ajax和JSON

1. >Ajax

   * 什么是同步，什么是异步

     * 同步现象：客户端发送请求到服务器端，当服务器返回响应之前，客户端都处于等待 卡死状态
     * 异步现象：客户端发送请求到服务器端，无论服务器是否返回响应，客户端都可以随 意做其他事情，不会被卡死

   * Ajax的运行原理
     页面发起请求，会将请求发送给浏览器内核中的Ajax引擎，Ajax引擎会提交请求到 服务器端，在这段时间里，客户端可以任意进行任意操作，直到服务器端将数据返回 给Ajax引擎后，会触发你设置的事件，从而执行自定义的js逻辑代码完成某种页面 功能。

     * 创建Ajax引擎对象
     * 为Ajax引擎对象绑定监听（监听服务器已将数据响应给引擎）
     * 绑定提交地址
     * 发送请求
     * 接受响应数据

   * Jquery的Ajax示例：

     ```javascript
     //页面加载完成以后执行function代码
     $(function () {
         //jquery，使用$("#id")通过元素id获取某个页面元素
         $("#login_form").submit(function () {
             //ajax自己发请求
             $.ajax({
                 url: "../login",//请求的服务路径
                 type: "post",//请求方法
                 // contentType: "",//请求的数据类型：请求头Content-Type，默认表单格式，json需要指定为json
                 data: $("#login_form").serialize(),//请求数据：使用序列化表单的数据
                 dataType: "json",//响应的数据类型：使用json要指定
                 success: function (r) {//响应体json字符串，会解析为方法参数
                     if(r.success){
                         //前端页面url直接跳转某个路径
                         window.location.href = "../jsp/articleList.jsp";
                     }else{
                         alert("错误码："+r.code+"\n错误消息："+r.message)
                     }
                 }
             })
             //统一不执行默认的表单提交
             return false;
         })
     })
     ```

   * Vue的Ajax示例

     ```javascript
     methods: {
         //提交文章：需要根据type决定是新增还是修改（请求发送的url和数据是不同的）
         submitArticle: function () {
             let url = "/api/article/" + this.type;
             let query = {
                 title: this.title,
                 content: this.content,
             }
             if(this.type == "edit") query.id = this.id;
             //ajax提交文章数据
             axios.post(
                 url, query
             ).then(function (resp) {
                 let data = resp.data;
                 if(data.ok){//业务操作成功
                     alert("操作成功")
                 }else{//后端抛异常，我们还是返回200状态，通过ok字段标识
                     alert("错误码："+data.code+"\n错误信息："+data.msg);
                 }
             }).catch(function (err) {
                 alert("出错了\n"+JSON.stringify(err))
             })
         }
     }
     ```

   总结什么是ajax：自动发送请求，接受响应，数据量非常小，依然按照http协议做网络的传输，这个就是ajax技术。

2. >JSON(用于前后台数据传输)

   * 语法格式：

     - 对象用键值对表示
     - 数据由逗号分隔（最后一个数据不能加逗号）
     - 花括号保存对象
     - 方括号保存数组

   * 示例：

     ```json
     {
         "status": "0000",
         "message": "success",
         "data": {
             "title": {
                 "id": "001",
                 "name" : "白菜"
             },
             "content": [
                 {
                     "id": "001",
                     "value":"你好 白菜"
                 },
                 {
                     "id": "002",
                      "value":"你好 萝卜" 
                 }
             ]
         }
     }
     
     ```

## 5.Tomcat原理

https://blog.csdn.net/qq_36761831/article/details/89393203?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162831697316780357217210%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&amp;amp;request_id=162831697316780357217210&amp;amp;biz_id=0&amp;amp;utm_medium=distribute.pc_search_result.none-task-blog-2~all~top_positive~default-3-89393203.first_rank_v2_pc_rank_v29&amp;amp;utm_term=TOMCAT&amp;amp;spm=1018.2226.3001.4187



# 十三.HTTP项目(MyTomcat)

## 1.项目总览

流程图：

![image-20210804150841753](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210804150841753.png)

流程：

1. 初始化工作
   1. 扫描所有的Context
   2. 读取并解析各子的web配置文件
   3. 加载需要的ServletClass，表现为Class<?>
   4. 实例化需要的Servlet对象
   5. 执行Servlet对象的初始化工作
2. 处理Http请求-响应(单次的请求响应处理逻辑)
   1. 读取解析HTTP请求->Request对象，实现标准种定义的HttpServletRequest接口
      1. 解析请求行
         1. 解析方法
         2. 解析路径
            1. contextPath
            2. servletPath
            3. queryString->parameters
         3. 解析版本(这里我们不使用)
      2. 解析请求头(核心是解析cookie，根据cookie-name是session的找出session-id(也可能不存在))
      3. 理论上也需要解析请求体，但是这里我们只支持Get方法
   2. 构建Response对象
   3. 根据请求的contextPath找到，交给哪个Context处理
   4. 根据servletPath找到，交给哪个Servlet处理
   5. 调用servlet.service(请求，响应)
   6. 发送Response对象->响应
3. 销毁工作

## 2.Servlet容器

作为Servlet容器，所以满足Servlet标准，定义了满足Servlet标准的抽象类和接口

![image-20210804152535496](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210804152535496.png)

## 3.HTTP服务器

### <1>TCP连接的理解

![image-20210804152847666](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210804152847666.png)

### <2>正式项目

#### (1)初始化工作

找到所有Servlet对象，进行初始化

1. >通过webapps目录下不同的Web项目，找到他们的项目路径Context
   >
   >本质上是文件操作
   >扫描固定的目录（webapps）下有哪些子目录
   >目录名称作为context的name

```java
 public static final String WEBAPPS_BASE = "D:\\javaCode\\HTTP\\http-project\\webapps";
    //管理所有的Context对象
    public static final List<Context> contextList = new ArrayList<>();
    private static final ConfigReader configReader = new ConfigReader();
    public static final DefaultContext defaultContext=new DefaultContext(configReader);

    private static void scanContexts() {
        //扫描目录，获取context
        File webappsRoot = new File(WEBAPPS_BASE);
        File[] files = webappsRoot.listFiles();
        if (files == null) {
            throw new RuntimeException();
        }
        for (File file : files) {
            //不是目录，说明不是web应用，直接跳过
            if (!file.isDirectory()) {
                continue;
            }
            //获取各个web应用对应的应用上下文路径(context)
            String contextName = file.getName();
            Context context = new Context(configReader, contextName);
            contextList.add(context);
        }
    }
```

2. >读取并解析webapps/WEB-INF目录下的web.conf文件，获取Servlet类的名称

web.conf:例如

```
servlets:
  # ServletName = ServletClassName
  TranslateServlet = org.example.webapps.dictionary.TranslateServlet
  LoginActionServlet= org.example.webapps.dictionary.LoginActionServlet
  ProfileActionServlet = org.example.webapps.dictionary.ProfileActionServlet

servlet-mappings:
  # URLPattern = ServletName
  /translate = TranslateServlet
  /login-action = LoginActionServlet
  /profile-action = ProfileActionServlet
```

利用有限状态机已经对字符串的切割等处理，得到两个map，找到了url和需要处理的Servlet类对象之间的映射

```java
//ServletName和ServletClassName对应关系：上文servlets：后的对应关系
Map<String,String > servletNameToServletClassNameMap=new HashMap<>();
//URI和Servlet类名称的对应关系：上文servlet-mappings：后的对应关系
LinkedHashMap<String,String> urlToServletNameMap=new LinkedHashMap<>();
```

代码：

```java
 public Config read(String name) throws IOException {
         Map<String,String > servletNameToServletClassNameMap=new HashMap<>();
         LinkedHashMap<String,String> urlToServletNameMap=new LinkedHashMap<>();
        //进行web.conf文件的读取+解析
        //规范：web.conf放哪里，必须符合规范，否则就会读不到
        String fileName=String.format("%s/%s/WEB-INF/web.conf", HttpServer.WEBAPPS_BASE,name);//两个参数分别表示webapps的绝对路径即Context项目地址
        String stage="start";//"servlets"/"mappings" 下面switch的三种状态，分别表示三个解析的步骤

        //进行文本文件的读取
        try (InputStream is=new FileInputStream(fileName)){
            Scanner scanner=new Scanner(is,"UTF-8");
            while (scanner.hasNextLine()){
                String line=scanner.nextLine().trim();
                if(line.isEmpty() || line.startsWith("#")){
                    //如果是空行或者是以#开头的注释不做处理
                    continue;
                }
                switch (stage){
                    case "start":
                        if(line.equals("servlets:")){
                            stage="servlets";
                        }
                        break;
                    case "servlets":
                        if(line.equals("servlet-mappings:")){
                            stage="mappings";
                        }else {
                            // 进行Servlet解析 ServletName=>ServletClassName的解析
                            String []parts=line.split("=");
                            String servletName=parts[0].trim();
                            String servletClassName=parts[1].trim();
                            servletNameToServletClassNameMap.put(servletName,servletClassName);
                        }
                        break;
                    case "mappings":
                        //进行URL => ServletName的解析
                        String []parts=line.split("=");
                        String url=parts[0].trim();
                        String servletName=parts[1].trim();
                        urlToServletNameMap.put(url,servletName);
                        break;
                }
            }
        }
        return new Config(servletNameToServletClassNameMap,urlToServletNameMap);
    }
```

3. >加载需要的ServletClass，表现为Class<?>

通过步骤2，得到了URI对应需要处理的Servlet的全类名，通过反射，可以得到需要的类

进行Servlet类加载

```java
 List<Class<?>> servletClassList=new ArrayList<>();
    public void loadServletClasses() throws ClassNotFoundException {
       Set<String> servletClassNames = new HashSet<>(config.servletNameToServletClassNameMap.values());
       for(String servletClassName : servletClassNames){
           Class<?> clazz=webappsClassLoader.loadClass(servletClassName);
           servletClassList.add(clazz);
       }
    }
```

这里我们每个Context项目使用各自的类加载器classLoader，将项目之间进行隔离，防止数据库等版本的不同，而进行类加载时发生错误

4. >实例化需要的Servlet对象

```java
List<Servlet> servletList = new ArrayList<>();
    public void instantiateServletObjects() throws IllegalAccessException, InstantiationException {
        for(Class<?> servletClass : servletClassList){
            //利用反射，默认调用该类的无参构造方法，进行实例化对象
            Servlet servlet = (Servlet) servletClass.newInstance();
            servletList.add(servlet);
        }
    }
```

利用反射，实例化Servlet对象

5. >调用Servlet的init()，执行类的初始化工作
   >
   >涉及到“Servlet生命周期”的概念
   >调用每个Servlet对象的init（）方法，这样子类可以通过重写自己的init（）方法，进行不同的初始化

```java
private static void initializeServletObjects() throws ServletException {
        for (Context context : contextList) {
            context.initServletObjects();
        }
        defaultServlet.init();
        notFoundServlet.init();
    }
```

#### (2)处理HTTP请求-响应（单次的请求响应处理逻辑）

服务器逻辑：使用简单的线程池，使用多线程对每次的响应进行处理，每次响应间不存在共享变量，所以无序考虑线程安全问题，将单次响应任务放入RequestResponseTask任务中处理

```java
private static void startServer() throws IOException {
    ExecutorService threadPool = Executors.newFixedThreadPool(10);
    ServerSocket serverSocket = new ServerSocket(8080);
    //2.每次循环处理一个请求
    while (true) {
        Socket socket = serverSocket.accept();
        Runnable task = new RequestResponseTask(socket);
        threadPool.execute(task);
    }
}
```



**RequestResponseTask任务处理逻辑:**

1. >读取解析HTTP请求->Request对象，实现标准中定义的HttpServletRequest接口

我们定义一个专门解析请求Request的类，对HttpRequest的请求行和请求头进行解析(简易版HTTP服务器，只支持GET方法，所以我们不解析请求体).保存请求中的Cookie信息

```java
public class HttpRequestParser {

    public Request parse(InputStream socketInputStream) throws IOException, ClassNotFoundException {
        //1.读取请求行
        Scanner scanner=new Scanner(socketInputStream);
        String method=scanner.next().toUpperCase();//读取请求方法
        String path=scanner.next();//读取请求的全路径
        
        //解析parameters,请求行传来的参数
        Map<String,String> parameters=new HashMap<>();
        String requestURI=path;
        int i=requestURI.indexOf("?");
        if(i != -1){
            requestURI=path.substring(0,i);
            String queryString = path.substring(i+1);
            for(String kv : queryString.split("&")){
                String[] partsKV = kv.split("=");
                String name= URLDecoder.decode(partsKV[0],"UTF-8");
                String value= URLDecoder.decode(partsKV[1],"UTF-8");
                parameters.put(name,value);
            }
        }

        //解析contextPath和servletPath
        int j=requestURI.indexOf('/',1);//找到第二个"/"
        String contextPath="/";
        String servletPath=requestURI;
        if(j != -1){
            //例如：requestURI=/blog/add
             contextPath=requestURI.substring(1,j);//   blog(好比较)
             servletPath=requestURI.substring(j);  //   /add
        }



        String version=scanner.nextLine();//读取版本信息，没用

        //2.读取请求头,将请求头种的Cookie信息保存
        String headerLine;
        Map<String,String> headers=new HashMap<>();
        List<Cookie> cookieList=new ArrayList<>();
        while (scanner.hasNextLine() && !(headerLine=scanner.nextLine().trim()).isEmpty()){
            String[] parts=headerLine.split(":");
            String name=parts[0].toLowerCase();
            String value=parts[1];
            headers.put(name,value);
            //判断是否是cookie
            if(name.equals("cookie")){
                String [] kvcookies=value.split(";");
                for(String kvcookie : kvcookies){
                    if(kvcookie.trim().isEmpty()){
                        continue;
                    }
                    String[] split = kvcookie.split("=");
                    String cookieName=split[0].trim();
                    String cookieValue=split[1].trim();
                    Cookie cookie=new Cookie(cookieName,cookieValue);
                    cookieList.add(cookie);
                }

            }
        }
        return new Request(method,requestURI,contextPath,servletPath,parameters,headers,cookieList);
    }
}
```

将请求方法，请求的全路径，项目路径ContextPath，Servlet路径servletPath，请求的参数，请求头以及Cookie信息放入到Request对象中

对于Request对象，我们需要遍历Cookie信息，当Cookie存在Session-id时，需要构建Session对象，这里我们专门建一个session文件夹，将Session数据按文件形式保存本地，持久化

```java
 for(Cookie cookie : cookieList){
            if(cookie.getName().equals("session-id")){
                String sessionId=cookie.getValue();
                session = new HttpSessionImpl(sessionId);
                break;
            }
        }
```

**Session对象:**

通过loadSessionData()方法，将Session数据保存在本地文件

```java
public class HttpSessionImpl implements HttpSession {
    public final Map<String,Object> sessionData;

    public final String sessionId;
    //没有从cookie中拿到sessionId时使用
    public HttpSessionImpl(){
        sessionId= UUID.randomUUID().toString();//没有传入，随机生成一个
        sessionData=new HashMap<>();
    }
    //从cookie中拿到了sessionId时使用
    public HttpSessionImpl(String sessionId) throws IOException, ClassNotFoundException {
        this.sessionId=sessionId;
        sessionData=loadSessionData(sessionId);//加载Session数据
    }


    private static final String SESSION_BASE="D:\\javaCode\\HTTP\\http-project\\sessions";

    //加载Session里面的数据
    //文件名 ： <session-id>.session
    private Map<String, Object> loadSessionData(String sessionId) throws IOException, ClassNotFoundException {
        String sessionFileName=String.format("%s\\%s.session",SESSION_BASE,sessionId);
        File sessionFile=new File(sessionFileName);
        if(!(sessionFile.exists())){
            return new HashMap<>();//session不存在，返回一个空的map
        }
        try(InputStream is=new FileInputStream(sessionFile) {
        }){
            //使用ObjectInputStream进行对象读取
            ObjectInputStream objectInputStream=new ObjectInputStream(is);
            return (Map<String, Object>) objectInputStream.readObject();
        }
    }

    //保存Session里面的数据
    public void saveSessionData() throws IOException {
        if(sessionData.isEmpty()){
            return;
        }
        String sessionDataFile=String.format("%s\\%s.session",SESSION_BASE,sessionId);
        try(OutputStream os=new FileOutputStream(sessionDataFile)){
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(os);
            objectOutputStream.writeObject(sessionData);
            objectOutputStream.flush();
        }
    }
    @Override
    public Object getAttribute(String name) {
        return sessionData.get(name);
    }

    @Override
    public void removeAttribute(String name) {
        sessionData.remove(name);
    }

    @Override
    public void setAttribute(String name, Object value) {
        sessionData.put(name, value);
    }
```

2. >构建Response响应对象

将响应的状态码，响应体及Cookie等信息放入Response对象中，并使用IO输出到页面

部分Response对象代码：

```java
public class Response implements HttpServletResponse {
    public int status = 200;
    public final List<Cookie> cookieList;
    public final Map<String, String> headers;
    public final ByteArrayOutputStream bodyOutputStream;
    public final PrintWriter bodyPrintWriter;

    public Response() throws UnsupportedEncodingException {
        cookieList = new ArrayList<>();
        headers = new HashMap<>();
        bodyOutputStream = new ByteArrayOutputStream(1024);
        Writer writer = new OutputStreamWriter(bodyOutputStream, "UTF-8");
        bodyPrintWriter = new PrintWriter(writer);
    }
}
```

3. >根据请求的contextPath找到，交给哪个Context处理

如果没找到Context，即交给默认Context处理(404)

```java
Context handleContext = HttpServer.defaultContext;
for (Context context : HttpServer.contextList) {
    if (context.getName().equals(request.getContextPath())) {
        handleContext = context;
        break;
    }
}
```

4. >根据servletPath找到，交给哪个Servlet处理

如果没找到ServletPath，交给默认的Servlet处理

```java
Servlet servlet = handleContext.getServlet(request.getServletPath());
if (servlet == null) {
    servlet = HttpServer.defaultServlet;
}
```

这里默认的Servlet：

首先先寻找这个ServletPath是否是静态资源，如果是静态资源，按照静态资源的Servlet->DefaultServlet处理

如果也不是静态资源，交给NotFoundServlet处理->404处理

```java
public class DefaultServlet extends HttpServlet {
    private final String welcomeFile="index.html";
    private final Map<String,String> mime=new HashMap<>();
    private final String defaultContentType="text/plain";

    //静态资源后缀名所对应的输出格式
    @Override
    public void init() throws ServletException {
        mime.put("htm","text/html");
        mime.put("html","text/html");
        mime.put("jpg","image/jepg");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String contextPath=req.getContextPath();
        String servletPath=req.getServletPath();
        //静态资源处理
        if(servletPath.equals("/")){
            servletPath=welcomeFile;//欢迎页面
        }
        String fileName=String.format("%s\\%s\\%s", HttpServer.WEBAPPS_BASE,contextPath,servletPath);
        System.out.println(fileName);
        File file=new File(fileName);
        if(!file.exists()){
            //按照404的方式进行处理
            HttpServer.notFoundServlet.service(req,resp);
            System.out.println("not");
            return;
        }
        String contentType=getContentType(servletPath);
        System.out.println(contentType);
        resp.setContentType(contentType);

        OutputStream outputStream=resp.getOutputStream();
        try(InputStream is=new FileInputStream(file)){
            byte[] buffer=new byte[1024];
            int len=-1;
            while ((len = is.read(buffer)) != -1){
                outputStream.write(buffer,0,len);
            }
            outputStream.flush();
        }

    }

    private String getContentType(String servletPath) {
        String contentType=defaultContentType;
        int i=servletPath.lastIndexOf(".");
        if(i != -1){
            String extension=servletPath.substring(i+1);
            contentType=mime.getOrDefault(extension,defaultContentType);
        }
        return contentType;
    }
}
```

5. >调用servlet.service()方法，交给业务处理

自己实现的Servlet方法继承Servlet类，重写service()方法，实现自己的业务

```java
servlet.service(request, response);
```

6. >业务处理完之后，根据Response对象中的数据，发送HTTP响应

在发送HTTP响应时，需要将Cookie的数据放入响应体中，并将Session数据保存在本地文件中

```java
private void sendResponse(OutputStream outputStream, Request request, Response response) throws IOException {
    // 保存 session
    // 1. 种 cookie
    // 2. 保存成文件
    if (request.session != null) {
        Cookie cookie = new Cookie("session-id", request.session.sessionId);
        response.addCookie(cookie);
        request.session.saveSessionData();
    }

    Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
    PrintWriter printWriter = new PrintWriter(writer);
    for (Cookie cookie : response.cookieList) {
        response.setHeader("Set-Cookie", String.format("%s=%s", cookie.getName(), cookie.getValue()));
    }
}
```

#### (3)销毁所有的Servlet对象，结束生命周期

```java
private static void destroyAllServletClass () {
    for (Context context : contextList) {
        context.destroy();
    }
    defaultServlet.destroy();
    notFoundServlet.destroy();
}
```

### <3>自定义登录逻辑进行测试

```java
public class LoginActionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        if(username.equals("zyf") && password.equals("123")){
            User user=new User(username,password);
            HttpSession session = req.getSession();
            session.setAttribute("user",user);
            resp.sendRedirect("profile-action");
        }else {
            resp.sendRedirect("login.html");
        }
    }
}
```

```java
public class ProfileActionServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        User user=(User)session.getAttribute("user");
        if(user == null){
            resp.sendRedirect("login.html");
        }else {
            resp.setContentType("text/plain");
            resp.getWriter().println(user.toString());
        }
    }
}
```

## 4.项目总结

**HTTP服务器部分的总结：**

![image-20210804163828181](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210804163828181.png)

仿Tomcat的设计：

管理多个Web项目的Servlet容器，通过TCP建立连接，解析Http的请求和响应。对Servlet的生命周期进行管理(通过不同项目的类加载器，通过反射，每个项目对各自的Servlet进行管理)

## 5.项目的不足

1. 只支持了HTTP1.0协议(短链接)，一条TCP只能处理一次的请求-响应周期
2. 只支持了GET方法
3. 字符集的编码，固定成了"UTF-8"

对于TCP Server

4. 项目使用的时BIO的形式，真实的Tomcat使用的NIO
5. 线程池只是使用的最简单的，不高效
6. 没有涉及日志功能，调试只能使用System.out.printIn()打印观察



# 十四.Spring

## 1.初步认识和理解Spring(IOC,DI,AOP)

Spring 是一个开源框架，为简化企业级应用开发而生。Spring 可以是使简单的 JavaBean 实现以前只有 EJB 才能

实现的功能。Spring 是一个 IOC 和 AOP 容器框架。

Spring 容器的主要核心是：IOC,DI,AOP

1. >IOC:控制反转

控制反转（IOC），传统的 java 开发模式中，当需要一个对象时，我们会自己使用 new 或者 getInstance 等直接或者间接调用构造方法创建一个对象。而在 spring 开发模式中，spring 容器使用了工厂模式为我们创建了所需要的对象，不需要我们自己创建了，直接调用 spring 提供的对象就可以了，这是控制反转的思想

2. >DI:依赖注入

依赖注入（DI），spring 使用 javaBean 对象的 set 方法或者带参数的构造方法为我们在创建所需对象时将其属性自动设置所需要的值的过程，就是依赖注入的思想。

3. >AOP:面向切面编程

面向切面编程（AOP），在面向对象编程（oop）思想中，我们将事物纵向抽成一个个的对象。而在面向切面编程中，我们将一个个的对象某些类似的方面横向抽成一个切面，对这个切面进行一些如权限控制、事物管理，记录日志等公用操作处理的过程就是面向切面编程的思想。AOP 底层是动态代理，如果是接口采用 JDK 动态代理，如果是类采用CGLIB 方式实现动态代理。

## 2.Spring的使用过程

![image-20210807170524794](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210807170524794.png)

## 3.Bean的初始化和注册

1. >类注解

   * @controller 标注控制层
   * @service 标注服务层，进行业务的逻辑处理
   * @repository 标注数据访问层，也可以说用于标注数据访问组件，即DAO组件
   * @component 把普通pojo实例化到spring容器中

2. >方法上使用 @Bean 注解

```java
@Bean
public User user1(){
    User user = new User();
    user.setUsername("abc");
    user.setPassword("123");
    return user; }
@Bean
public User user2(){
    User user = new User();
    user.setUsername("我不是大佬");
    user.setPassword("lao");
    return user; }
```

注册 类型为User的对象。名为user1 ，user2

3. >@Configuration

使用 @Configuration 注解，可以注册一个配置类到容器中。配置类一般用来自定义配置某些资源。

## 4.依赖注入

1. >属性注入@Autowired 注解

   * ```java
     @Service
     public class LoginService {
         @Autowired
         private LoginRepository loginRepository; }
     ```

   * 放在setter方法上

     ```java
      @Autowired
         public void setLoginRepository(LoginRepository loginRepository) {
             System.out.printf("LoginServiceBySetter: loginRepository=%s%n", 
     loginRepository);
             this.loginRepository = loginRepository;
        }
     ```

   * 放在构造方法上

     ```java
       @Autowired
         public LoginServiceByConstructor(LoginRepository loginRepository){
             System.out.printf("LoginServiceByConstructor: %s%n", loginRepository);
             this.loginRepository = loginRepository;
        }
     ```

2. >注入指定的@Qualifier

同类型的Bean有多个时，注入该类型Bean需要指定Bean的名称：

属性名或方法参数名设置为Bean的名称

属性名或方法参数设置 @Qualifier("名称") 注解，注解内的字符串是Bean对象的名称

## 5.Bean的生命周期

* 实例化Bean：通过反射调用构造方法实例化对象。

* 依赖注入：装配Bean的属性

* 实现了Aware接口的Bean，执行接口方法：如顺序执行BeanNameAware、BeanFactoryAware、

  ApplicationContextAware的接口方法。

* Bean对象初始化前，循环调用实现了BeanPostProcessor接口的预初始化方法

  (postProcessBeforeInitialization)

* 执行Bean对象初始化方法 

* Bean对象初始化后，循环调用实现了BeanPostProcessor接口的后初始化方法

  （postProcessAfterInitialization）

* 容器关闭时，执行Bean对象的销毁方法



# 十五.CRM客户管理系统

## 1.模块划分及数据库设计

![image-20210809132246014](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809132246014.png)



1. 基础模块：包含系统基本的用户登录，退出，记住我，密码修改等基本操作

2. 客户营销管理

   * 营销机会管理 ：企业客户的质询需求所建立的信息录入功能，方便销售人员进行后续的客户需求跟踪。

   * 营销开发计划 ：开发计划是根据营销机会而来，对于企业质询的客户，会有相应的销售人员对于该客户

     进行具体的沟通交流

   ![image-20210809135008377](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809135008377.png)

3. 客户管理：

   * 客户信息管理 ：Crm 系统中完整记录客户信息来源的数据、企业与客户交往、客户订单查询等信息录入功能 ————>客户，客户对应的联系人，与客户的接触信息，客户的订单，客户的一些细节等五张表

     ![image-20210809135219731](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809135219731.png)

   * 客户流失管理 ：Crm 通过一定规则机制所定义的流失客户（无效客户），通过该规则可以有效管理客

     户信息资源——————>分为客户，流失客户及客户取消等3张表

     ![image-20210809135750244](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809135750244.png)

4. 服务管理：服务管理是针对客户而开发的功能，针对客户要求，Crm 提供客户相应的信息质询，反馈与投诉功能，提高企业对于客户的服务质量。

   ![image-20210809135617222](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809135617222.png)

5. 系统管理：系统管理包含常量字典维护工作，以及权限管理模块，Crm 权限管理是基于角色的一种权限控制，基于RBAC 实现基于角色的权限控制，通过不同角色的用户登录该系统后展示系统不同的操作功能，从而达到对不同角色完成不同操作功能。

   ![image-20210809135917684](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809135917684.png)

6. 数据报表：Crm 提供的数据报表功能能够帮助企业了解客户整体分布，了解客户开发结果整体信息。数据报表根据客户订单生成，使用Echarts进行图形化展示

## 2.配置类的导入

1. >数据库逆向工程创建mapper，pojo类

需要修改数据库的驱动，账号密码以及所生成数据对应的表名

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE generatorConfiguration
        PUBLIC "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd">

<generatorConfiguration>

    <!--
        数据库驱动
            在左侧project边栏的External Libraries中找到mysql的驱动，右键选择copy path
    -->
    <classPathEntry  location="D:\Repository\Maven\mysql\mysql-connector-java\5.1.49\mysql-connector-java-5.1.49.jar"/>
    <context id="DB2Tables" targetRuntime="MyBatis3">

        <commentGenerator>
            <!-- 是否去除日期那行注释 -->
            <property name="suppressDate" value="false"/>
            <!-- 是否去除自动生成的注释 true：是 ： false:否 -->
            <property name="suppressAllComments" value="false"/>
        </commentGenerator>

        <!-- 数据库链接地址账号密码 -->
        <jdbcConnection
                driverClass="com.mysql.jdbc.Driver"
                connectionURL="jdbc:mysql://localhost:3306/crm?serverTimezone=GMT%2B8"
                userId="root"
                password="sn20000904">
        </jdbcConnection>

        <!--
             java类型处理器
                用于处理DB中的类型到Java中的类型，默认使用JavaTypeResolverDefaultImpl；
                注意一点，默认会先尝试使用Integer，Long，Short等来对应DECIMAL和NUMERIC数据类型；
                true：使用 BigDecimal对应DECIMAL和NUMERIC数据类型
                false：默认，把JDBC DECIMAL和NUMERIC类型解析为Integer
        -->
        <javaTypeResolver>
            <property name="forceBigDecimals" value="false"/>
        </javaTypeResolver>



        <!-- 生成Model类存放位置 -->
        <javaModelGenerator targetPackage="org.example.crm.pojo" targetProject="src/main/java">
            <!-- 在targetPackage的基础上，根据数据库的schema再生成一层package，最终生成的类放在这个package下，默认为false -->
            <property name="enableSubPackages" value="true"/>
            <!-- 设置是否在getter方法中，对String类型字段调用trim()方法 -->
            <property name="trimStrings" value="true"/>
        </javaModelGenerator>


        <!--生成映射文件存放位置-->
        <sqlMapGenerator targetPackage="mappers" targetProject="src/main/resources">
            <property name="enableSubPackages" value="true"/>
        </sqlMapGenerator>


        <!--生成Dao类存放位置-->
        <javaClientGenerator type="XMLMAPPER" targetPackage="org.example.crm.dao" targetProject="src/main/java">
            <property name="enableSubPackages" value="true"/>
        </javaClientGenerator>



        <table tableName="t_user" domainObjectName="User"
               enableCountByExample="false" enableUpdateByExample="false"
               enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false"></table>

    </context>
</generatorConfiguration>
```

使用mybatis-generator生成Mybatis代码。能够生成 POJO类、能生成 mapper 映射文件（其中包括基本

的增删改查功能）、能生成 mapper 接口。命令： mybatis-generator:generate -e

2. >Maven配置文件导入

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.example</groupId>
    <artifactId>crm_review</artifactId>
    <version>1.0-SNAPSHOT</version>

    <!-- FIXME change it to the project's website -->
    <url>http://www.example.com</url>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>


    </properties>


    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.2.RELEASE</version>
    </parent>


    <dependencies>
        <!-- lombok: 简化bean代码的框架 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>

        <!-- 排除内部Tomcat的影响 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- web 环境 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- aop -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
        </dependency>
        <!-- freemarker -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-freemarker</artifactId>
        </dependency>
        <!-- 测试环境 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- mybatis -->
        <dependency>
            <groupId>org.mybatis.spring.boot</groupId>
            <artifactId>mybatis-spring-boot-starter</artifactId>
            <version>2.1.1</version>
        </dependency>

        <!-- 分页插件 -->
        <dependency>
            <groupId>com.github.pagehelper</groupId>
            <artifactId>pagehelper-spring-boot-starter</artifactId>
            <version>1.2.13</version>
        </dependency>

        <!-- 打包上线时需要忽略配置文件中的mysql -->
        <!-- mysql -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- c3p0 -->
        <dependency>
            <groupId>com.mchange</groupId>
            <artifactId>c3p0</artifactId>
            <version>0.9.5.5</version>
        </dependency>

        <!-- commons-lang3 -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.5</version>
        </dependency>

        <!-- json -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.47</version>
        </dependency>

        <!-- DevTools 热部署 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <optional>true</optional>
        </dependency>


        <!-- lombok: 简化bean代码的框架 -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>

    </dependencies>

    <build>
        <!-- 设置构建的war文件的名称 -->
        <finalName>crm</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.mybatis.generator</groupId>
                <artifactId>mybatis-generator-maven-plugin</artifactId>
                <version>1.3.2</version>
                <configuration>
                    <configurationFile>src/main/resources/generatorConfig.xml</configurationFile>
                    <verbose>true</verbose>
                    <overwrite>true</overwrite>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <!-- 如果没有该配置，热部署的devtools不生效 -->
                    <fork>true</fork>
                </configuration>
            </plugin>





            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin </artifactId>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>


        </plugins>

    </build>


</project>
```

3. >yml文件导入

需要修改数据库相关配置

```yml
## 端口号  上下文路径
server:
  port: 8080
  servlet:
    context-path: /crm

## 数据源配置
spring:
  datasource:
    type: com.mchange.v2.c3p0.ComboPooledDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://127.0.0.1:3306/crm?useUnicode=true&characterEncoding=utf8&serverTimezone=GMT%2B8
    username: root
    password: sn20000904


  ## freemarker
  freemarker:
    suffix: .ftl
    content-type: text/html
    charset: UTF-8
    template-loader-path: classpath:/views/


  ## 启用热部署
  devtools:
    restart:
      enabled: true
      additional-paths: src/main/java

## mybatis 配置
mybatis:
  mapper-locations: classpath:/mappers/*.xml
  type-aliases-package: org.example.crm.vo;org.example.crm.query;org.example.crm.dto
  configuration:
    map-underscore-to-camel-case: true

## pageHelper 分页
pagehelper:
  helper-dialect: mysql

## 设置 dao 日志打印级别
logging:
  level:
    org:
      example:
        crm:
          dao: debug
```

## 3.导入Base包及前端资源文件，工具类及创建启动类

![image-20210809144430188](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809144430188.png)

## 4.基础模块的实现

### <1>登录功能

#### 1.1进行登录页面视图的转发

```java
@RequestMapping("index")
public String index(){
    return "index";
}
@RequestMapping("welcome")
public String welcome(){
    return "welcome";
}
```

#### 1.2登录功能的实现

* Controller层

  ```java
  @PostMapping("login")
  @ResponseBody
  public ResultInfo login(String userName,String userPwd){
      ResultInfo resultInfo=new ResultInfo();
      UserVo user=userService.login(userName,userPwd);
      resultInfo.setResult(user);
      return resultInfo;
  }
  ```

* Service层

  ```java
  public UserVo login(String username, String password) {
      //1.判断参数是否异常(用户名密码是否为空)
      AssertUtil.isTrue(StringUtils.isBlank(username) ,"用户名不能为空");
      AssertUtil.isTrue(StringUtils.isBlank(password) ,"密码不能为空");
      //2.用username从数据库查询数据
      User user=userMapper.queryByUsername(username);
      //3.判断用户是否存在
      AssertUtil.isTrue(user == null,"该用户不存在");
      //4.存在的话判断密码是否正确  ---->密码经过了MD5加密，先将传入的密码进行MD5加密再比较
      AssertUtil.isTrue(!Md5Util.encode(password).equals(user.getUserPwd()), "密码错误，请重试");
      //5.构建返回对象UserVo，之后存入Cookie
      return buildUserVo(user);
  }
  
  private UserVo buildUserVo(User user) {
      UserVo userVo=new UserVo();
      userVo.setUsername(user.getUserName());
      userVo.setTrueName(user.getTrueName());
      //设置cookie id
      userVo.setUserIdStr(UserIDBase64.encoderUserID(user.getId()));
      return userVo;
  }
  ```

* mapper

  ```xml
  <select id="queryByUsername" parameterType="java.lang.String" resultType="org.example.crm.pojo.User">
    select
    <include refid="Base_Column_List"/>
    from t_user
    where
    user_name=#{username}
    and is_valid=1
  </select>
  ```

#### 1.3登录成功之后，进行视图页面转发

登录成功之后，会请求main页面的转发，并将用户信息添加到Session，前端可以展示用户

```java
@RequestMapping("main")
public String main(HttpServletRequest request){
    //从cookie中获取登录的userIdStr
    Integer userId= LoginUserUtil.releaseUserIdFromCookie(request);
    User user=userService.queryByUserId(userId);
    request.getSession().setAttribute("user",user);
    return "main";
}
```

**注意：这里有个大坑**---->要把前端页面freemaker的条件注释，项目结束再打开！！！

![image-20210809155109828](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809155109828.png)



### <2>修改密码

#### 1.1进行修改页面的视图转发

```java
@RequestMapping("toPasswordPage")
public String toPasswordPage(){
    return "user/password";
}
```

#### 1.2修改密码功能实现

* Controller

  ```java
  /**
   * 修改密码
   * @param request
   * @param oldPassword
   * @param newPassword
   * @param repeatPassword
   * @return
   */
  @PostMapping("updatePwd")
  @ResponseBody
  public ResultInfo updatePwd(HttpServletRequest request,String oldPassword,String newPassword,String repeatPassword){
      ResultInfo resultInfo=new ResultInfo();
      Integer userId= LoginUserUtil.releaseUserIdFromCookie(request);
      userService.updatePassword(userId,oldPassword,newPassword,repeatPassword);
      return resultInfo;
  }
  ```

* Service

  ```java
  public void updatePassword(Integer userId, String oldPassword, String newPassword, String repeatPassword) {
      //1.使用userId去数据库查询
      User user=userMapper.selectByPrimaryKey(userId);
      //2.判断user
      AssertUtil.isTrue(user == null,"用户不存在");
      //3.进行参数校验
      AssertUtil.isTrue(StringUtils.isBlank(oldPassword),"旧密码不能为空");
      AssertUtil.isTrue(!Md5Util.encode(oldPassword).equals(user.getUserPwd()),"密码不正确");
      AssertUtil.isTrue(StringUtils.isBlank(newPassword),"新密码不能为空");
      AssertUtil.isTrue(StringUtils.isBlank(repeatPassword),"重复密码不能为空");
      AssertUtil.isTrue(!newPassword.equals(repeatPassword),"两次输入的密码不相同");
      //4.为用户设置新密码
      user.setUserPwd(Md5Util.encode(newPassword));
      //5.更新密码
      AssertUtil.isTrue(userMapper.updateByPrimaryKeySelective(user) < 1,"更新密码失败");
  }
  ```

### <3>记住我和注销功能

这两个功能由前端实现

### <4>非法请求的拦截

* 首先定义一个未登录异常NoLoginException类.

* 定义未登录拦截器,继承HandlerInterceptorAdapter类

  ```java
  /**
   * 非法访问拦截
   * 继承HandlerInterceptorAdapter适配器
   */
  
      public class NoLoginInterceptor extends HandlerInterceptorAdapter {
      @Autowired
      private UserMapper userMapper;
  
      /**
       * 拦截用户是否是登录状态
       * 在目标方法（资源）执行前执行的方法
       * 返回boolean
       * 如果为true，表示目标方法可用被执行
       * 如果为false，表示阻止目标方法执行
       *
       * @param request
       * @param response
       * @param handler
       * @return
       * @throws Exception
       */
      @Override
      public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
          //获取cookie中的用户Id
          Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
          //判断用户Id是否为空，且数据库中是否存在改userId的记录
          if (userId == null || userMapper.selectByPrimaryKey(userId) == null) {
              //抛出未登录异常
              throw new NoLoginException();
          }
          return true;
      }
  }
  ```

* 添加MVC拦截器Config类

  ```java
  @Configuration//配置类
  public class MvcConfig extends WebMvcConfigurerAdapter {
      @Bean//将方法的返回值交给IOC
      public NoLoginInterceptor noLoginInterceptor() {
          return new NoLoginInterceptor();
      }
  
      /**
       * 添加拦截器
       *
       * @param registry
       */
      @Override
      public void addInterceptors(InterceptorRegistry registry) {
          //需要实现了拦截器功能的实例对象 NoLoginInterceptor
          registry.addInterceptor(noLoginInterceptor())
                  //设置需要被拦截的资源
                  .addPathPatterns("/**")
                  // 设置不需要被拦截的资源
                  .excludePathPatterns("/css/**", "/images/**", "/js/**", "/lib/**")
                  .excludePathPatterns("/index", "/user/login");
  
      }
  }
  ```

### <5>全局异常的统一处理

定义异常类：权限不足异常，未登录异常及自定义异常

```java
/**
 * 全局异常统一处理
 */
@Component
public class GlobalExceptionResolver implements HandlerExceptionResolver {
    /**
     * 异常处理方法
     * 方法的返回值：
     * 1. 返回视图
     * 2. 返回数据（JSON数据）
     * <p>
     * 如何判断方法的返回值？
     * 通过方法上是否声明@ResponseBody注解
     * 如果未声明，则表示返回视图
     * 如果声明了，则表示返回数据
     *
     * @param request  request请求对象
     * @param response response响应对象
     * @param handler  方法对象
     * @param ex       异常对象
     * @return org.springframework.web.servlet.ModelAndView
     */
    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        /**
         * 非法请求拦截
         *  判断是否抛出未登录异常
         *      如果抛出该异常，则要求用户登录，重定向跳转到登录页面
         */
        if (ex instanceof NoLoginException) {
            // 重定向到登录页面
            ModelAndView mv = new ModelAndView("redirect:/index");
            return mv;
        }


        /**
         * 设置默认异常处理（返回视图）
         */
        ModelAndView modelAndView = new ModelAndView("error");
        // 设置异常信息
        modelAndView.addObject("code", 500);
        modelAndView.addObject("msg", "系统异常，请重试...");


        // 判断HandlerMethod
        if (handler instanceof HandlerMethod) {
            // 类型转换
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            // 获取方法上声明的@ResponseBody注解对象
            ResponseBody responseBody = handlerMethod.getMethod().getDeclaredAnnotation(ResponseBody.class);

            // 判断ResponseBody对象是否为空 （如果对象为空，则表示返回的事视图；如果不为空，则表示返回的事数据）
            if (responseBody == null) {
                /**
                 * 方法返回视图
                 */
                // 判断异常类型
                if (ex instanceof ParamsException) {
                    ParamsException p = (ParamsException) ex;
                    // 设置异常信息
                    modelAndView.addObject("code", p.getCode());
                    modelAndView.addObject("msg", p.getMsg());

                } else if (ex instanceof AuthException) { // 认证异常
                    AuthException a = (AuthException) ex;
                    // 设置异常信息
                    modelAndView.addObject("code", a.getCode());
                    modelAndView.addObject("msg", a.getMsg());
                }

                return modelAndView;

            } else {
                /**
                 * 方法返回数据
                 */
                // 设置默认的异常处理
                ResultInfo resultInfo = new ResultInfo();
                resultInfo.setCode(500);
                resultInfo.setMsg("异常异常，请重试！");

                // 判断异常类型是否是自定义异常
                if (ex instanceof ParamsException) {
                    ParamsException p = (ParamsException) ex;
                    resultInfo.setCode(p.getCode());
                    resultInfo.setMsg(p.getMsg());

                } else if (ex instanceof AuthException) { // 认证异常
                    AuthException a = (AuthException) ex;
                    resultInfo.setCode(a.getCode());
                    resultInfo.setMsg(a.getMsg());
                }

                // 设置响应类型及编码格式（响应JSON格式的数据）
                response.setContentType("application/json;charset=UTF-8");
                // 得到字符输出流
                PrintWriter out = null;
                try {
                    // 得到输出流
                    out = response.getWriter();
                    // 将需要返回的对象转换成JOSN格式的字符
                    String json = JSON.toJSONString(resultInfo);
                    // 输出数据
                    out.write(json);

                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    // 如果对象不为空，则关闭
                    if (out != null) {
                        out.close();
                    }
                }

                return null;

            }
        }
        return modelAndView;
    }
}
```

## 5.营销管理模块实现

### <1>营销机会管理功能

#### 1.1 进入营销机会管理页面

```java
@RequestMapping("index")
public String index() {
    return "saleChance/sale_chance";
}
```

#### 1.2进行营销机会管理和客户开发计划的数据查询

关注点：需要进行分页查询

* 建立查询条件对应的实体类

  ```java
  public class SaleChanceQuery extends BaseQuery {
      //父类两个分页参数
  
      //营销机会管理  条件查询
      //条件查询
  
      /**
       * 客户名
       */
      private String customerName;
  
      /**
       * 创建人
       */
      private String createMan;
  
      /**
       * 分配状态
       * 0:未分配
       * 1.已分配
       */
      private Integer state;
  
  
      //客户开发计划    条件查询
      private String devResult;//开发状态
      private Integer assignMan;//指派人
  }
  ```

  ![image-20210809215631779](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809215631779.png)

* Controller层

  

  ```java
   /**
       * 营销机会数据查询（多条件的分页查询）
       * 如果flag值不为空，且值为1，表示当前查询的是客户开发计划
       * 否则查询营销机会数据
       *
       * @param saleChanceQuery
       * @return
       */
      @RequiredPermission(code = "101001")
      @GetMapping("list")
      @ResponseBody
      public Map<String, Object> querySaleChanceByParams(SaleChanceQuery saleChanceQuery, Integer flag,
                                                         HttpServletRequest request) {
          //判断flag的值
          if (flag != null && flag == 1) {
              //查询的是客户开发计划
              //设置分配状态
              saleChanceQuery.setState(StateStatus.STATED.getType());//客户开发计划，表示已经分配了
              //设置指派人（当前用户的ID）
              //从cookie中获取当前用户登录的ID
              Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
              saleChanceQuery.setAssignMan(userId);
          }
          return saleChanceService.querySaleChanceByParams(saleChanceQuery);
      }
  ```

* Service层：使用分页

  ```java
  /**
   * 多条件分页查询
   * 返回的数据格式必须满足layUi数据表格要求的格式
   *
   * @param saleChanceQuery
   * @return
   */
  public Map<String, Object> querySaleChanceByParams(SaleChanceQuery saleChanceQuery) {
      Map<String, Object> map = new HashMap<>();
  
      //开启分页
      PageHelper.startPage(saleChanceQuery.getPage(), saleChanceQuery.getLimit());
      //得到分页对象(对SaleChance对象进行分页）
      PageInfo<SaleChance> pageInfo = new PageInfo<>(saleChanceMapper.selectByParams(saleChanceQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

* Mapper层：这里需要查询指派人，因此SaleChance POJO类中需要加上一个uname属性，sql中对应的resultMap也要稍微修改

  ```xml
  <!-- 多条件查询 -->
  <select id="selectByParams" parameterType="org.example.crm.query.SaleChanceQuery" resultMap="BaseResultMap">
    select
    s.id, s.chance_source, s.customer_name, s.cgjl, s.overview, s.link_man, s.link_phone, s.description,
    s.create_man, s.assign_man, s.assign_time, s.state, s.dev_result, s.is_valid, s.create_date, s.update_date
    ,u.user_name uname
    from
    t_sale_chance s
    left join t_user u
    on
    s.assign_man = u.id
    <where>
      s.is_valid = 1
      <if test="customerName != null and customerName != ''">
        and s.customer_name like concat('%',#{customerName},'%')
      </if>
      <if test="createMan != null and createMan != ''">
        and s.create_man = #{createMan}
      </if>
      <if test="state != null">
        and s.state = #{state}
      </if>
      <!--根据开发状态进行查询 -->
      <if test="devResult != null and devResult != ''">
        and s.dev_result = #{devResult}
      </if>
      <!-- 根据指派人进行查询-->
      <if test="assignMan != null">
        and s.assign_man = #{assignMan}
      </if>
    </where>
  </select>
  ```

#### 1.3营销机会数据添加

1. >进入营销机会添加或修改的页面

```java
/**
 * 进入添加或者修改营销机会数据页面
 */

@RequestMapping("toSaleChancePage")
public String toSaleChancePage(Integer saleChanceId, HttpServletRequest request) {
    if (saleChanceId != null) {
        //通过ID查询营销机会数据
        SaleChance saleChance = saleChanceService.selectByPrimaryKey(saleChanceId);
        //将数据设置到请求域中
        request.setAttribute("saleChance", saleChance);
    }
    return "saleChance/add_update";
}
```

2. >营销机会数据的添加

   Controller层

```java
@ResponseBody
@PostMapping("add")
@RequiredPermission(code = "101002")
public ResultInfo addSaleChance(SaleChance saleChance, HttpServletRequest request) {
    //拿到当前登录用户设置创建人,从cookie中拿
    String userName = CookieUtil.getCookieValue(request, "userName");
    //设置用户名到saleChance中
    saleChance.setCreateMan(userName);
    //调用Service方法
    saleChanceService.addSaleChance(saleChance);
    ResultInfo resultInfo = new ResultInfo();
    resultInfo.setMsg("营销机会数据添加成功");
    return resultInfo;
}
```

* Service层

  ```java
  /**
   * 添加营销机会
   * 1. 参数校验
   * customerName客户名称    非空
   * linkMan联系人           非空
   * linkPhone联系号码       非空，手机号码格式正确
   * 2. 设置相关参数的默认值
   * createMan创建人        当前登录用户名
   * assignMan指派人
   * 如果未设置指派人（默认）
   * state分配状态 （0=未分配，1=已分配）
   * 0 = 未分配
   * assignTime指派时间
   * 设置为null
   * devResult开发状态 （0=未开发，1=开发中，2=开发成功，3=开发失败）
   * 0 = 未开发 （默认）
   * 如果设置了指派人
   * state分配状态 （0=未分配，1=已分配）
   * 1 = 已分配
   * assignTime指派时间
   * 系统当前时间
   * devResult开发状态 （0=未开发，1=开发中，2=开发成功，3=开发失败）
   * 1 = 开发中
   * isValid是否有效  （0=无效，1=有效）
   * 设置为有效 1= 有效
   * createDate创建时间
   * 默认是系统当前时间
   * updateDate
   * 默认是系统当前时间
   * 3. 执行添加操作，判断受影响的行数
   */
  
  @Transactional
  public void addSaleChance(SaleChance saleChance) {
      //1.参数校验
      checkSaleChanceParms(saleChance.getCustomerName(), saleChance.getLinkMan(),
              saleChance.getLinkPhone());
      //2.设置相关属性的默认值
      //设置为有效 1 = 有效
      saleChance.setIsValid(1);
      //createDate默认是当前的系统时间
      saleChance.setCreateDate(new Date());
      //updateDate默认是系统时间
      saleChance.setUpdateDate(new Date());
      //判断是否设置了指派人
      if (StringUtils.isBlank(saleChance.getAssignMan())) {
          //如果为空,则表示未设置指派人
          //分配状态为未分配
          //state分配状态 （0=未分配，1=已分配） 0 = 未分配
          saleChance.setState(StateStatus.UNSTATE.getType());
          //没有指派人，assignTime未空
          saleChance.setAssignTime(null);
          //开发状态为未开发
          saleChance.setDevResult(DevResult.UNDEV.getStatus());
      } else {
          //如果不为空，则表示设置了指派人
          /**
           * 如果设置了指派人
           *                 state分配状态 （0=未分配，1=已分配）
           *                    1 = 已分配
           *                 assignTime指派时间
           *                    系统当前时间
           *                 devResult开发状态 （0=未开发，1=开发中，2=开发成功，3=开发失败）
           *                    1 = 开发中
           */
          saleChance.setState(StateStatus.STATED.getType());
          saleChance.setAssignTime(new Date());
          saleChance.setDevResult(DevResult.DEVING.getStatus());
  
      }
      //3. 执行添加操作，判断受影响的行数
      AssertUtil.isTrue(saleChanceMapper.insertSelective(saleChance) != 1, "添加营销机会失败");
  }
  
  /**
   * 参数校验
   *
   * @param customerName
   * @param linkMan
   * @param linkPhone
   */
  private void checkSaleChanceParms(String customerName, String linkMan, String linkPhone) {
      //customerName不能为空
      AssertUtil.isTrue(StringUtils.isBlank(customerName), "用户名称不能为空");
      //linkMan不能为空
      AssertUtil.isTrue(StringUtils.isBlank(linkMan), "联系人不能为空");
      //linkPhone不能为空
      AssertUtil.isTrue(StringUtils.isBlank(linkPhone), "联系电话不能为空");
      //判断手机号码的格式是否正确
      AssertUtil.isTrue(!PhoneUtil.isMobile(linkPhone), "联系号码格式不正确");
  }
  ```

  #### 1.4营销机会数据的修改

  * Controller层

    ```java
    @PostMapping("update")
    @ResponseBody
    @RequiredPermission(code = "101004")
    public ResultInfo updateSaleChance(SaleChance saleChance) {
        saleChanceService.updateSaleChance(saleChance);
        ResultInfo resultInfo = new ResultInfo();
        resultInfo.setMsg("营销机会数据修改成功");
        return resultInfo;
    }
    ```

  * Service层

    ```java
    /**
     * 更新营销机会
     * 1. 参数校验
     * 营销机会ID  非空，数据库中对应的记录存在
     * customerName客户名称    非空
     * linkMan联系人           非空
     * linkPhone联系号码       非空，手机号码格式正确
     * <p>
     * 2. 设置相关参数的默认值
     * updateDate更新时间  设置为系统当前时间
     * assignMan指派人
     * 原始数据未设置
     * 修改后未设置
     * 不需要操作
     * 修改后已设置
     * assignTime指派时间  设置为系统当前时间
     * 分配状态    1=已分配
     * 开发状态    1=开发中
     * 原始数据已设置
     * 修改后未设置
     * assignTime指派时间  设置为null
     * 分配状态    0=未分配
     * 开发状态    0=未开发
     * 修改后已设置
     * 判断修改前后是否是同一个指派人
     * 如果是，则不需要操作
     * 如果不是，则需要更新 assignTime指派时间  设置为系统当前时间
     * <p>
     * 3. 执行更新操作，判断受影响的行数
     */
    @Transactional
    public void updateSaleChance(SaleChance saleChance) {
        //1.参数校验
        //营销机会ID：非空，且数据库中有对应的数据
        AssertUtil.isTrue(saleChance.getId() == null, "待更新记录不存在");
        //判断数据库中是否存在该对象
        SaleChance temp = saleChanceMapper.selectByPrimaryKey(saleChance.getId());
        AssertUtil.isTrue(temp == null, "待更新记录不存在");
    
        //传入的参数校验
        checkSaleChanceParms(saleChance.getCustomerName(), saleChance.getLinkMan(),
                saleChance.getLinkPhone());
    
        /*设置相关参数的默认值*/
        //updateDate更新时间  设置为系统当前时间
        saleChance.setUpdateDate(new Date());
        //assignMan指派人
        //判断原始数据是否存在
        if (StringUtils.isBlank(temp.getAssignMan())) {//不存在
            //判断修改后的值是否存在
            //开始不存在，修改后也不存在，则不需要操作
            //开始不存在，修改后存在，设置默认值
            if (!StringUtils.isBlank(saleChance.getAssignMan())) {
                // assignTime指派时间  设置为系统当前时间
                saleChance.setAssignTime(new Date());
                //分配状态    1=已分配
                saleChance.setState(StateStatus.STATED.getType());
                //开发状态    1=开发中
                saleChance.setDevResult(DevResult.DEVING.getStatus());
            }
    
        } else {//存在
            if (StringUtils.isBlank(saleChance.getAssignMan())) {
                //修改前有值，修改有无值
                //assignTime指派时间  设置为null
                saleChance.setAssignTime(null);
                //分配状态    0=未分配
                saleChance.setState(StateStatus.UNSTATE.getType());
                //开发状态    0=未开发
                saleChance.setDevResult(DevResult.UNDEV.getStatus());
            } else {
                //修改前有值，修改后也有值
                //判断修改前后是否是同一个指派人
                if (!temp.getAssignMan().equals(saleChance.getAssignMan())) {
                    //如果不是，则需要更新 assignTime指派时间  设置为系统当前时间
                    saleChance.setAssignTime(new Date());
                } else {
                    //如果是，使用原来的指派时间
                    saleChance.setAssignTime(temp.getAssignTime());
                }
            }
        }
        //3. 执行更新操作，判断受影响的行数
        AssertUtil.isTrue(saleChanceMapper.updateByPrimaryKeySelective(saleChance) != 1, "更新营销机会失败");
    }
    ```

#### 1.4查询营销机会的指派人

![image-20210809224324535](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809224324535.png)

在UserController中查询所有的销售员

Controller和Service层

```java
@RequestMapping("queryAllSales")
@ResponseBody
public List<Map<String, Object>> queryAllSales() {
    return userService.queryAllSales();
}

 public List<Map<String, Object>> queryAllSales() {
        return userMapper.queryAllSales();
    }
```

mapper:

```xml
<!--查询所有的销售人员-->
<select id="queryAllSales" resultType="java.util.Map">
  SELECT
   u.id,u.user_name uname
  from
      t_user u
  left join
      t_user_role ur
  on
    u.id = ur.user_id
  left join
      t_role r
  on
      r.id = ur.role_id
  where
   u.is_valid = 1
  and
   r.is_valid = 1
  and
   r.role_name = '销售'
</select>
```

#### 1.5营销机会数据的批量删除

* Controller层

  ```java
  @RequestMapping("delete")
  @ResponseBody
  @RequiredPermission(code = "101003")
  public ResultInfo deleteSaleChance(Integer[] ids) {
      saleChanceService.deleteSaleChance(ids);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("删除营销机会成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  @Transactional
  public void deleteSaleChance(Integer[] array) {
      AssertUtil.isTrue(array == null || array.length == 0, "请选择需要删除的数据");
      AssertUtil.isTrue(saleChanceMapper.deleteBatch(array) < 1, "删除营销机会数据失败");
  }
  ```

* Mapper层

  ```xml
  <update id="deleteBatch">
    update t_sale_chance
    set
    is_valid=0
    where
    id in
    <foreach collection="array" item="id" open ="(" close= ")" separator=",">
      #{id}
    </foreach>
  </update>
  ```

### <2>客户开发计划

#### 2.1进入客户开发计划页面

```java
@RequestMapping("index")
public String index() {
    return "cusDevPlan/cus_dev_plan";
}
```

#### 2.2客户开发计划查询

在1.1中已做

#### 2.3.客户开发计划详情查看

![image-20210809224133773](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809224133773.png)

1. >打开客户开发计划详情

```java
/**
 * 打开计划项开发与详情页面
 *
 * @param id
 * @return
 */
@RequiredPermission(code = "102001")
@RequestMapping("toCusDevPlanPage")
public String toCusDevPlanPage(Integer id, HttpServletRequest request) {
    //通过ID查询营销机会对象
    SaleChance saleChance = saleChanceService.selectByPrimaryKey(id);
    //将对象设置到请求域中
    request.setAttribute("saleChance", saleChance);
    return "cusDevPlan/cus_dev_plan_data";
}
```

2. > 营销计划开发数据详情查看

使用了分页:

* Controller层

  ```java
  /**
   * 查询营销机会数据开发详情
   *
   * @param cusDevPlanQuery
   * @return
   */
  @RequestMapping("list")
  @ResponseBody
  public Map<String, Object> queryCusDevPlanByParams(CusDevPlanQuery cusDevPlanQuery) {
      return cusDevPlanService.queryCusDevPlanParams(cusDevPlanQuery);
  }
  ```

* Service层

  ```java
  public Map<String, Object> queryCusDevPlanParams(CusDevPlanQuery cusDevPlanQuery) {
      Map<String, Object> map = new HashMap<>();
  
      //开启分页
      PageHelper.startPage(cusDevPlanQuery.getPage(), cusDevPlanQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<CusDevPlan> pageInfo = new PageInfo<>(cusDevPlanMapper.selectByParams(cusDevPlanQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

* Mapper层

  ```xml
  <select id="selectByParams" parameterType="org.example.crm.query.CusDevPlanQuery" resultType="org.example.crm.model.CusDevPlan">
    select
    <include refid="Base_Column_List"/>
    from
    t_cus_dev_plan
    where
    is_valid=1
    and
    sale_chance_id=#{saleChanceId}
  </select>
  ```

#### 2.4打开客户开发计划添加/修改页面

![image-20210809225407116](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809225407116.png)

```java
@RequestMapping("toAddOrUpdateCusDevPlanPage")
public String addOrUpdateCusDevPlanPage(Integer sId, Integer id, HttpServletRequest request) {
    //将营销机会ID设置刀请求域中，给计划项页面获取
    request.setAttribute("sId", sId);

    request.setAttribute("cusDevPlan", cusDevPlanService.selectByPrimaryKey(id));
    return "cusDevPlan/add_update";
}
```

#### 2.5添加客户开发计划

* Controller层

  ```java
  @RequestMapping("add")
  @ResponseBody
  public ResultInfo addCusDevPlan(CusDevPlan cusDevPlan) {
      cusDevPlanService.addCusDevPlan(cusDevPlan);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("添加用户开发计划成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 添加计划项
   * 1. 参数校验
   *     营销机会ID 非空 记录必须存在
   *     计划项内容   非空
   *     计划项时间   非空
   * 2. 设置参数默认值
   *     is_valid
   *     crateDate
   *     updateDate
   * 3. 执行添加，判断结果
   */
  @Transactional
  public void addCusDevPlan(CusDevPlan cusDevPlan) {
      //参数校验
      checkaddCusDevPlanParms(cusDevPlan);
      //设置默认值
      cusDevPlan.setIsValid(1);
      cusDevPlan.setCreateDate(new Date());
      cusDevPlan.setUpdateDate(new Date());
      AssertUtil.isTrue(cusDevPlanMapper.insertSelective(cusDevPlan) != 1, "添加客户开发计划失败");
  }
  ```

#### 2.6修改客户开发计划

* Controller层

  ```java
  /**
   * 修改客户开发计划
   *
   * @param cusDevPlan
   * @return
   */
  @RequestMapping("update")
  @ResponseBody
  public ResultInfo updateCusDevPlan(CusDevPlan cusDevPlan) {
      cusDevPlanService.updateCusDevPlan(cusDevPlan);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("客户开发计划修改成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 更新计划项
   * 1.参数校验
   *     id 非空 记录存在
   *     营销机会id 非空 记录必须存在
   *     计划项内容 非空
   *     计划项时间 非空
   * 2.参数默认值设置
   *     updateDate
   * 3.执行更新 判断结果
   */
  @Transactional
  public void updateCusDevPlan(CusDevPlan cusDevPlan) {
      checkUpdateCusDevPlan(cusDevPlan);
      cusDevPlan.setUpdateDate(new Date());
      AssertUtil.isTrue(cusDevPlanMapper.updateByPrimaryKeySelective(cusDevPlan) != 1, "修改用户开发计划失败");
  }
      private void checkUpdateCusDevPlan(CusDevPlan cusDevPlan) {
          /**
           *1.参数校验
           *id 非空 记录存在
           *营销机会id 非空 记录必须存在
           *计划项内容 非空
           *计划项时间 非空
           */
          AssertUtil.isTrue(cusDevPlan.getId() == null || cusDevPlanMapper.selectByPrimaryKey(cusDevPlan.getId()) == null, "该客户开发计划不存在");
          checkaddCusDevPlanParms(cusDevPlan);
      }
  ```

#### 2.7删除客户开发计划

* Controller层

  ```java
  /**
   * 删除客户开发计划
   *
   * @param cusDevPlan
   * @return
   */
  @RequestMapping("delete")
  @ResponseBody
  public ResultInfo deleteCusDevPlan(CusDevPlan cusDevPlan) {
      cusDevPlanService.deleteCusDevPlan(cusDevPlan);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("客户开发计划删除成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 1.参数校验
   * id 非空 记录存在
   *
   * @param cusDevPlan
   */
  @Transactional
  public void deleteCusDevPlan(CusDevPlan cusDevPlan) {
      checkDeleteCusDevPlan(cusDevPlan);
      AssertUtil.isTrue(cusDevPlanMapper.deleteByPrimaryKey(cusDevPlan.getId()) < 1, "删除客户开发项计划失败");
  }
  
  private void checkDeleteCusDevPlan(CusDevPlan cusDevPlan) {
      AssertUtil.isTrue(cusDevPlan.getId() == null || cusDevPlanMapper.selectByPrimaryKey(cusDevPlan.getId()) == null, "待删除的客户开发项计划不存在");
  }
  ```

#### 2.8营销机会开发状态更新

![image-20210809232819817](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210809232819817.png)

* Controller层

  ```java
  @RequestMapping("updateSaleChanceDevResult")
  @ResponseBody
  public ResultInfo updateSaleChanceDevResult(Integer id, Integer devResult) {
      saleChanceService.updateSaleChanceDevResult(id, devResult);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("营销机会开发状态更新成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  public void updateSaleChanceDevResult(Integer id, Integer devResult) {
      //参数判断，判断营销机会ID是否有效（ID是否存在且数据库中有记录
      AssertUtil.isTrue(id == null, "待更新记录不存在");
      SaleChance temp = saleChanceMapper.selectByPrimaryKey(id);
      AssertUtil.isTrue(temp == null, "待更新记录不存在");
      temp.setDevResult(devResult);
      saleChanceMapper.updateByPrimaryKeySelective(temp);
  }
  ```

## 6.权限管理模块的实现

### <1>用户管理模块

从表结构设计可以看出：这里有三张主表(t_user，t_role，t_module)，功能实现上这里划分为三大模

块

* 用户管理
  * 用户基本信息维护(t_user)
  * 角色分配(t_user_role)
* 角色管理
  * 角色基本信息维护(t_role)
  * 角色授权与认证(t_permission)
* 资源管理
  * 资源菜单信息维护(t_module)

#### 1.1进入用户管理页面

```java
@RequiredPermission(code = "6010")
@RequestMapping("index")
public String index() {
    return "user/user";
}
```

#### 1.2查询所有用户

使用分页：

1. >定义查询类

```java
@Getter
@Setter
public class UserQuery extends BaseQuery {
    private String userName;
    private String email;
    private String phone;
}
```

2. >查询操作

   * Controller层

     ```java
     @RequestMapping("list")
     @ResponseBody
     public Map<String, Object> queryUserByParams(UserQuery userQuery) {
         return userService.queryUserByParams(userQuery);
     }
     ```

   * Service层

     ```java
     public Map<String, Object> queryUserByParams(UserQuery userQuery) {
         Map<String, Object> map = new HashMap<>();
         //开启分页
         PageHelper.startPage(userQuery.getPage(), userQuery.getLimit());
         //得到分页对象(对SaleChance对象进行分页）
         PageInfo<User> pageInfo = new PageInfo<>(userMapper.selectByParams(userQuery));
         map.put("code", 0);
         map.put("msg", "success");
         map.put("count", pageInfo.getTotal());
         //设置分页好的列表
         map.put("data", pageInfo.getList());
         return map;
     }
     ```

   * Mapper层

     ```xml
     <select id="selectByParams" parameterType="org.example.crm.query.UserQuery"  resultType="org.example.crm.model.User">
       select
       <include refid="Base_Column_List"/>
       from t_user
       <where>
         is_valid=1
         <if test="userName != null and userName !='' ">
           and user_name like concat('%',#{userName},'%')
         </if>
         <if test="phone != null and phone !='' ">
           and phone=#{phone}
         </if>
         <if test="email != null and email != ''">
           and email=#{email}
         </if>
       </where>
     </select>
     ```



#### 1.3进入用户添加或修改页面

```java
@RequestMapping("toAddOrUpdateUserPage")
public String addUserPage(Integer id, HttpServletRequest request) {
    if (id != null) {
        //若为更新，将待更新的数据添加到数据域
        request.setAttribute("userInfo", userService.selectByPrimaryKey(id));
    }
    return "user/add_update";
}
```

#### 1.4用户添加操作

* Controller层

  ```java
  @RequestMapping("add")
  @ResponseBody
  public ResultInfo addUser(User user) {
      userService.addUser(user);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("用户添加操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 添加用户操作
   * 1. 参数校验
   *     用户名 非空 唯一性
   *     邮箱   非空
   *     手机号 非空 格式合法
   * 2. 设置默认参数
   *     isValid 1
   *     creteDate   当前时间
   *     updateDate 当前时间
   *     userPwd 123456 -> md5加密
   * 3. 执行添加，判断结果
   */
  @Transactional
  public void addUser(User user) {
      //参数校验
      checkAddUserParms(user.getUserName(), user.getEmail(), user.getPhone());
      //设置默认参数
      user.setIsValid(1);
      user.setCreateDate(new Date());
      user.setUpdateDate(new Date());
      user.setUserPwd(Md5Util.encode("123456"));
      //执行添加，判断结果
      AssertUtil.isTrue(userMapper.insertSelective(user) != 1, "添加用户操作失败");
      /**
       * 添加用户对应角色关系
       */
      //userId数据库自动设置，需要通过唯一字段userName获取user，再获取userId
      Integer userId = userMapper.selectByUserName(user.getUserName()).getId();
      relationUserRole(userId, user.getRoleIds());
  }
  
  //参数校验
  private void checkAddUserParms(String userName, String email, String phone) {
      //参数校验
      //用户名 非空 唯一性
      AssertUtil.isTrue(userName == null || userName == "", "用户名不能为空");
      User temp = userMapper.selectByUserName(userName);
      AssertUtil.isTrue(temp != null, "该用户已经存在");
      //邮箱   非空
      AssertUtil.isTrue(email == null || email == "", "邮箱不能为空");
      //机号 非空 格式合法
      AssertUtil.isTrue(phone == null || !PhoneUtil.isMobile(phone), "手机号不能为空或格式不正确");
  }
  ```

#### 1.5用户修改操作

* Controller层

  ```java
  @RequestMapping("update")
  @ResponseBody
  public ResultInfo updateUser(User user) {
      userService.updateUser(user);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("修改用户操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 更新用户操作
   * 1. 参数校验
   *    id 非空 记录必须存在
   *    用户名 非空 唯一性
   *     email 非空
   *    手机号 非空 格式合法
   * 2. 设置默认参数
   *     updateDate
   * 3. 执行更新，判断结果
   *
   * @param user
   */
  @Transactional
  public void updateUser(User user) {
      //参数校验
      checkUpdateUserParms(user.getUserName(), user.getEmail(), user.getPhone(), user.getId());
      //设置默认值
      user.setUpdateDate(new Date());
      //执行修改，判断返回影响行数
      AssertUtil.isTrue(userMapper.updateByPrimaryKeySelective(user) != 1, "用户修改操作失败");
      /**
       * 修改用户对应角色关系
       */
      relationUserRole(user.getId(), user.getRoleIds());
  }
  
  //用户修改操纵参数校验
  
  /**
   * 1. 参数校验
   * id 非空 记录必须存在
   * 用户名 非空 唯一性
   * email 非空
   * 手机号 非空 格式合法
   *
   * @param userName
   * @param email
   * @param phone
   */
  private void checkUpdateUserParms(String userName, String email, String phone, Integer id) {
      AssertUtil.isTrue(id == null, "待修改记录不存在");
      User user = userMapper.selectByPrimaryKey(id);
      AssertUtil.isTrue(user == null, "待修改记录不存在");
      AssertUtil.isTrue(userName == null, "用户名不能为空");
      User temp = userMapper.selectByUserName(userName);
      AssertUtil.isTrue(temp != null && !temp.getId().equals(id), "该用户名已经存在");
      AssertUtil.isTrue(email == null, "邮箱不能为空");
      AssertUtil.isTrue(phone == null || !PhoneUtil.isMobile(phone), "手机号不能为空或格式不正确");
  }
  ```

#### 1.6批量删除用户操作

* Controller层

  ```java
  @RequestMapping("delete")
  @ResponseBody
  public ResultInfo deleteUser(Integer[] ids) {
      userService.deleteUserByIds(ids);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("删除用户操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  @Transactional
  public void deleteUserByIds(Integer[] ids) {
      AssertUtil.isTrue(ids == null || ids.length == 0, "待删除的用户不存在");
      AssertUtil.isTrue(userMapper.deleteBatch(ids) < 1, "删除用户操作失败");
      /**
       * 删除用户对应角色关系
       */
      for (Integer id : ids) {
          if (userRoleMapper.queryUserRolesCountById(id) > 0) {
              AssertUtil.isTrue(userRoleMapper.deleteAllRolesByUserId(id) < 1, "删除用户角色失败");
          }
      }
  }
  ```

* Mapper层

  ```xml
  <update id="deleteBatch">
    update t_user
    set
    is_valid=0
    where
    id in
    <foreach collection="array" item="id" open="(" close= ") " separator=",">
      #{id}
    </foreach>
  </update>
  ```

#### 1.7用户角色关联

![image-20210810132550210](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810132550210.png)

1. >用户角色查询,加载下拉框

Controller层：

```java
@RequestMapping("queryAllRoles")
@ResponseBody
public List<Map<String, Object>> queryAllRoles(Integer userId) {
    return roleService.queryAllRoles(userId);
}
```

Mapper层

```xml
<!-- 查询所有的角色列表 -->
<select id="queryAllRoles" parameterType="int" resultType="java.util.Map">
  SELECT
      r.id,
      r.role_name AS roleName,
      CASE
          WHEN IFNULL(temp.id,0) = 0 THEN ''
          ELSE 'selected'
      END
      AS 'selected'
  FROM
      t_role r
      LEFT JOIN ( SELECT r.id FROM t_role r LEFT JOIN t_user_role ur ON r.id = ur.role_id WHERE user_id = #{userId} ) temp ON r.id = temp.id
  WHERE
      r.is_valid = 1
</select>
```

### <2>角色模块管理

#### 1.1所有角色信息查询

使用分页：

1. >创建角色查询类

```java
@Getter
@Setter
public class RoleQuery extends BaseQuery {
    private String roleName;
}
```

2. >角色信息查询

   * Controller层

     ```java
     @RequestMapping("list")
     @ResponseBody
     public Map<String, Object> queryAllRoles(RoleQuery roleQuery) {
         return roleService.selectByParamsForTables(roleQuery);
     }
     ```

   * Service层

     ```java
     public Map<String, Object> selectByParamsForTables(RoleQuery roleQuery) {
         Map<String, Object> map = new HashMap<>();
         //开启分页
         PageHelper.startPage(roleQuery.getPage(), roleQuery.getLimit());
         //得到分页对象(对SaleChance对象进行分页）
         PageInfo<Role> pageInfo = new PageInfo<>(roleMapper.selectByParams(roleQuery));
         map.put("code", 0);
         map.put("msg", "success");
         map.put("count", pageInfo.getTotal());
         //设置分页好的列表
         map.put("data", pageInfo.getList());
         return map;
     }
     ```

   * Mapper层

     ```xml
     <!--查询所有的角色，加载角色管理页面的数据 -->
     <select id="selectByParams" parameterType="org.example.crm.query.RoleQuery" resultType="org.example.crm.model.Role">
       select
       <include refid="Base_Column_List"/>
       from t_role
      <where>
        is_valid=1
        <if test="roleName != null and roleName !='' ">
          and role_name=#{roleName}
        </if>
     
      </where>
     </select>
     ```

#### 1.2进入角色修改/添加页面

```java
@RequestMapping("toAddOrUpdateRolePage")
public String toAddOrUpdateRolePage(Integer roleId, HttpServletRequest request) {
    //判断如果id不为空，则为修改操作，将id对应的角色放入数据域
    if (roleId != null) {
        Role role = roleMapper.selectByPrimaryKey(roleId);
        request.setAttribute("role", role);
    }
    return "role/add_update";
}j
```

#### 1.3用户添加操作

* Controller层

  ```java
  @RequestMapping("add")
  @ResponseBody
  public ResultInfo add(Role role) {
      roleService.add(role);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("添加角色操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 添加用户角色操作
   * 1.进行roleName判断（存在且唯一)
   * 2.设置默认参数
   * 1.createDate
   * 2.updateDate
   * 3.isValid
   * 3.判断受影响的行数
   *
   * @param role
   */
  @Transactional
  public void add(Role role) {
      AssertUtil.isTrue(StringUtils.isBlank(role.getRoleName()), "角色名称不能为空");
      Role temp = roleMapper.selectByRoleName(role.getRoleName());
      AssertUtil.isTrue(temp != null, "该角色已经存在");
      //设置默认值
      role.setIsValid(1);
      role.setCreateDate(new Date());
      role.setUpdateDate(new Date());
      AssertUtil.isTrue(roleMapper.insertSelective(role) != 1, "添加角色操作失败");
  }
  ```

#### 1.4修改用户角色操作

* Controller层

  ```java
  @ResponseBody
  @RequestMapping("update")
  public ResultInfo update(Role role) {
      roleService.update(role);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("修改角色操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 修改角色操作
   * 1.进行roleName判断，存在且唯一
   * 2.设置默认参数
   * updateTime的时间
   * 3.判断受到影响的行数
   *
   * @param role
   */
  public void update(Role role) {
      //判断角色名存在
      AssertUtil.isTrue(StringUtils.isBlank(role.getRoleName()), "角色名不能为空");
      //判断角色名唯一
      Role temp = roleMapper.selectByRoleName(role.getRoleName());
      if (temp != null) {
          AssertUtil.isTrue(!temp.getId().equals(role.getId()), "该角色已经存在");
      }
      //设置默认值参数
      role.setUpdateDate(new Date());
      //判断受影响的行数
      AssertUtil.isTrue(roleMapper.updateByPrimaryKeySelective(role) != 1, "修改角色操作失败");
  }
  ```

#### 1.5角色删除操作

* Controller层

  ```java
  @RequestMapping("delete")
  @ResponseBody
  public ResultInfo delete(Integer roleId) {
      roleService.delete(roleId);
      ResultInfo resultInfo = new ResultInfo();
      resultInfo.setMsg("删除角色操作成功");
      return resultInfo;
  }
  ```

* Service层

  ```java
  /**
   * 删除角色操作
   * 1.判断参数，roleId存在且对应的记录存在
   * 2.执行删除操作，判断受到影响的行数
   *
   * @param roleId
   */
  @Transactional
  public void delete(Integer roleId) {
      AssertUtil.isTrue(roleId == null, "待删除的角色记录不存在");
      AssertUtil.isTrue(roleMapper.selectByPrimaryKey(roleId) == null, "待删除的角色记录不存在");
      AssertUtil.isTrue(roleMapper.delete(roleId) < 1, "执行删除操作失败");
  }
  ```

#### 1.6角色资源授权操作

![image-20210810134119963](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810134119963.png)

1. >打开授权页面

```java
@RequestMapping("toAddGrantPage")
public String toAddGrantPaged(Integer roleId, HttpServletRequest request) {
    request.setAttribute("roleId", roleId);
    return "role/grant";
}
```

2. >查询所有资源

   * Controller层

     ```java
     @RequestMapping("queryAllModules")
     @ResponseBody
     public List<TreeModuleVo> queryAllModules(Integer roleId) {
         return moduleService.queryAllModules(roleId);
     }
     ```

   * Service层

     ```java
     public List<TreeModuleVo> queryAllModules(Integer roleId) {
         //查询所有的资源
         List<TreeModuleVo> treeModuleVoList = moduleMapper.queryAllModules();
         //通过roleId查询该角色拥有的资源ID
         List<Integer> ids = permissionMapper.queryRoleModuleIdsByRoleId(roleId);
         //如果ids不为空，说明拥有资源，遍历treeModuleVolist,为其checked设置值
         for (int i = 0; i < treeModuleVoList.size(); i++) {
             if (ids.contains(treeModuleVoList.get(i).getId())) {
                 treeModuleVoList.get(i).setChecked(true);
             }
         }
         return treeModuleVoList;
     }
     ```

   * Mapper层

     <!--查询资源列表 -->

     ```xml
     <select id="queryModuleList" resultType="org.example.crm.model.Module">
       select
       <include refid="Base_Column_List"/>
       from t_module
       where
       is_valid=1
     </select>
     ```

3. >授权

* Controller层

  ```java
  @RequestMapping("addGrant")
  @ResponseBody
  public ResultInfo addGrant(Integer roleId, Integer[] mIds) {
      roleService.addGrant(roleId, mIds);
      return success("角色授权操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 角色授权
   * <p>
   * 将对应的角色ID与资源ID，添加到对应的权限表中
   * 直接添加权限：不合适，会出现重复的权限数据（执行修改权限操作后删除权限操作时）
   * 推荐使用：
   * 先将已有的权限记录删除，再将需要设置的权限记录添加
   * 1. 通过角色ID查询对应的权限记录
   * 2. 如果权限记录存在，则删除对应的角色拥有的权限记录
   * 3. 如果有权限记录，则添加权限记录 (批量添加)
   *
   * @param roleId
   * @param mIds
   * @return void
   */
  @Transactional
  public void addGrant(Integer roleId, Integer[] mIds) {
      //通过roleId查询对应的权限记录
      Integer roleCount = permissionMapper.selectRoleCountByPrimaryKey(roleId);
      //如果该role有权限，全部删除(通过roleId)
      if (roleCount > 0) {
          permissionMapper.deleteByRoleId(roleId);
      }
      //如果mIds不为空，添加角色权限
      if (mIds != null) {
          List<Permission> list = new ArrayList<>();
          for (Integer mId : mIds) {
              Permission permission = new Permission();
              //设置值
              permission.setRoleId(roleId);
              permission.setModuleId(mId);
              //设置权限码
              permission.setAclValue(moduleMapper.selectByPrimaryKey(mId).getOptValue());
              permission.setCreateDate(new Date());
              permission.setCreateDate(new Date());
              list.add(permission);
          }
          //执行授权操作
          AssertUtil.isTrue(permissionMapper.insertBatch(list) != list.size(), "授权操作执行失败");
      }
  }
  ```

#### 1.7登录用户权限认证

1. >菜单级别访问控制实现

系统根据登录用户扮演的不同角色来对登录用户操作的菜单进行动态控制显示操作，这里显示的控制使用freemarker指令+内建函数实现，指令与内建函数操作参考这里。

![image-20210810135239502](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810135239502.png)

2. >后端方法级别访问控制

实现了菜单级别显示控制，但最终客户端有可能会通过浏览器来输入资源地址从而越过ui界面来访问后

端资源，所以接下来加入控制方法级别资源的访问控制操作，这里使用aop+自定义注解实现

* 自定义注解@RequirePermission

  ```java
  @Target({ElementType.TYPE, ElementType.METHOD})
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  /**
   * 定义方法需要的对应资源的权限码
   */
  public @interface RequiredPermission {
      //权限码
      String code() default "";
  }
  ```

* 方法上使用注解，标识方法的权限码

  例如：

  ![image-20210810135532006](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810135532006.png)

* 定义aop切面类 拦截指定注解标注的方法

  ```java
  @Component
  @Aspect
  public class PermissionProxy {
  
      //通过session名字查找注入
      @Resource
      private HttpSession session;
  
      /**
       * 切面会拦截指定包下的指定注解
       * 拦截org.example.crm.annotation的RequiredPermission注解
       *
       * @param pjp
       * @return java.lang.Object
       */
      //环绕增强
      @Around(value = "@annotation(org.example.crm.annotation.RequiredPermission)")
      public Object around(ProceedingJoinPoint pjp) throws Throwable {
          Object result = null;
          // 得到当前登录用户拥有的权限 （session作用域）
          List<String> permissions = (List<String>) session.getAttribute("permissions");
          // 判断用户是否拥有权限
          if (null == permissions || permissions.size() < 1) {
              // 抛出认证异常
              throw new AuthException();
          }
  
          // 得到对应的目标
          MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
          // 得到方法上的注解
          RequiredPermission requiredPermission = methodSignature.getMethod().getDeclaredAnnotation(RequiredPermission.class);
          // 判断注解上对应的状态码
          if (!(permissions.contains(requiredPermission.code()))) {
              // 如果权限中不包含当前方法上注解指定的权限码，则抛出异常
              throw new AuthException();
          }
  
          result = pjp.proceed();
          return result;
      }
  
  }
  ```

* 在登录成功后转到main页面时，查出当前角色所拥有的所有权限(IndexController中)

  ```java
  @RequestMapping("main")
  public String main(HttpServletRequest request) {
      //通过获取cookie用户ID
      Integer userId = LoginUserUtil.releaseUserIdFromCookie(request);
      //查询用户对象，设置session作用域
      User user = userService.selectByPrimaryKey(userId);
      request.getSession().setAttribute("user", user);
  
      //通过当前登录用户ID，查询当前登录用户拥有的资源列表(查询对应的资源授权码)
      List<String> permissions = null;
      permissions = permissionService.queryUserHasRoleHasPermissionByUserId(userId);
      //将集合设置作用域中（Session作用域)
      request.getSession().setAttribute("permissions", permissions);
  
      return "main";
  }
  ```

### <3>资源功能管理

#### 3.1进入资源授权页面

```java
@RequestMapping("toAddGrantPage")
public String toAddGrantPaged(Integer roleId, HttpServletRequest request) {
    request.setAttribute("roleId", roleId);
    return "role/grant";
}
```

#### 3.2查询所有角色的权限资源

使用树形结构显示：

![image-20210810141010991](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810141010991.png)

* Controller层

  ```java
    @ResponseBody
      @RequestMapping("list")
      public Map<String, Object> queryModuleList() {
          return moduleService.queryModuleList();
      }
  ```

* Service层

  ```java
   public Map<String, Object> queryModuleList() {
          Map<String, Object> map = new HashMap<>();
          //查询资源列表
          List<Module> moduleList = moduleMapper.queryModuleList();
          map.put("code", 0);
          map.put("msg", "");
          map.put("count", moduleList.size());
          map.put("data", moduleList);
          return map;
      }
  ```

* Mapper层

  ```xml
   <!--查询资源列表 -->
    <select id="queryModuleList" resultType="org.example.crm.model.Module">
      select
      <include refid="Base_Column_List"/>
      from t_module
      where
      is_valid=1
    </select>
  ```

>权限资源的树形显示模型

```java
@Getter
@Setter
/**
 * 权限资源的树形显示模型
 */
public class TreeModuleVo {
    private Integer id;
    private Integer pId;
    private String name;
    private boolean checked = false;
}
```

#### 3.3进入添加资源视图页面

```java
@RequestMapping("toAddModulePage")
public String toAddModulePage(Integer grade, Integer parentId, HttpServletRequest request) {
    // 将数据设置到请求域中
    request.setAttribute("grade", grade);
    request.setAttribute("parentId", parentId);
    return "module/add";
}
```

#### 3.4添加资源操作

* Controller层

  ```java
  @RequestMapping("add")
  @ResponseBody
  public ResultInfo add(Module module) {
      moduleService.addModule(module);
      return success("添加资源操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 添加资源
   * 1. 参数校验
   * 模块名称 moduleName
   * 非空，同一层级下模块名称唯一
   * 地址 url
   * 二级菜单（grade=1），非空且同一层级下不可重复
   * 父级菜单 parentId
   * 一级菜单（目录 grade=0）    -1
   * 二级|三级菜单（菜单|按钮 grade=1或2）    非空，父级菜单必须存在
   * 层级 grade
   * 非空，0|1|2
   * 权限码 optValue
   * 非空，不可重复
   * 2. 设置参数的默认值
   * 是否有效 isValid    1
   * 创建时间createDate  系统当前时间
   * 修改时间updateDate  系统当前时间
   * 3. 执行添加操作，判断受影响的行数
   *
   * @param module
   * @return void
   */
  @Transactional
  public void addModule(Module module) {
      //1.参数校验
      Module newModule = checkAddModule(module);
      //2.设置参数的默认值
      module.setIsValid((byte) 1);
      module.setCreateDate(new Date());
      module.setUpdateDate(new Date());
      //3.执行添加操作，判断受影响的行数
      AssertUtil.isTrue(moduleMapper.insertSelective(module) != 1, "添加资源操作失败");
  }
  
  /**
   * 1. 参数校验
   * 模块名称 moduleName
   * 非空，同一层级下模块名称唯一
   * 地址 url
   * 二级菜单（grade=1），非空且同一层级下不可重复
   * 父级菜单 parentId
   * 一级菜单（目录 grade=0）    -1
   * 二级|三级菜单（菜单|按钮 grade=1或2）    非空，父级菜单必须存在
   * 层级 grade
   * 非空，0|1|2
   * 权限码 optValue
   * 非空，不可重复
   */
  private Module checkAddModule(Module module) {
      Integer grade = module.getGrade();
      //1.模块名称
      AssertUtil.isTrue(module.getModuleName() == null, "模块名称不能为空");
      AssertUtil.isTrue(moduleMapper.selectSameGradeByModuleName(module.getModuleName(), grade) != null, "该模块已在相同层级下存在");
      //2.url
      if (grade == 1) {
          AssertUtil.isTrue(module.getUrl() == null, "第二级目录下的url地址不能为空");
          AssertUtil.isTrue(moduleMapper.selectSameGradeByModuleUrl(module.getUrl(), grade) != null, "第二级目录下该Url地址已经存在");
      }
      //3.层级grade
      AssertUtil.isTrue(grade == null || !(grade == 0 || grade == 1 || grade == 2), "添加的模块的层级目录不合法");
      AssertUtil.isTrue(StringUtils.isBlank(module.getOptValue()), "权限码不能为空");
      AssertUtil.isTrue(moduleMapper.selectByOptValue(module.getOptValue()) != null, "该权限码已经存在");
  
      //4.父级菜单
      if (grade == 0) {
          module.setParentId(-1);
      } else {
          AssertUtil.isTrue(module.getParentId() == null, "父级菜单不能为空");
          Module parent = moduleMapper.selectByPrimaryKey(module.getParentId());
          AssertUtil.isTrue(parent == null, "父级菜单不存在");
          module.setParentOptValue(parent.getOptValue());
      }
      return module;
  }
  ```

#### 3.5进入资源修改页面

```java
@RequestMapping("toUpdateModulePage")
public String toUpdateModulePage(Integer id, HttpServletRequest request) {
    //将待修改的module设置到作用域中加载
    request.setAttribute("module", moduleMapper.selectByPrimaryKey(id));
    return "module/update";
}
```

#### 3.6修改资源模块

* Controller层

  ```java
  @RequestMapping("update")
  @ResponseBody
  public ResultInfo update(Module module) {
      moduleService.update(module);
      return success("修改模块操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 修改资源
   * 1. 参数校验
   * id
   * 非空，数据存在
   * 层级 grade
   * 非空 0|1|2
   * 模块名称 moduleName
   * 非空，同一层级下模块名称唯一 （不包含当前修改记录本身）
   * 地址 url
   * 二级菜单（grade=1），非空且同一层级下不可重复（不包含当前修改记录本身）
   * 权限码 optValue
   * 非空，不可重复（不包含当前修改记录本身）
   * 2. 设置参数的默认值
   * 修改时间updateDate  系统当前时间
   * 3. 执行更新操作，判断受影响的行数
   *
   * @param module
   * @return void
   */
  @Transactional
  public void update(Module module) {
      //1.参数校验
      checkUpdateModule(module);
      //2.设置参数的默认值
      module.setUpdateDate(new Date());
      //3.执行操作，判断受到影响的行数
      AssertUtil.isTrue(moduleMapper.updateByPrimaryKeySelective(module) < 1, "修改模块操作失败");
  }
  
  /**
   * 修改资源
   * 1. 参数校验
   * id
   * 非空，数据存在
   * 层级 grade
   * 非空 0|1|2
   * 模块名称 moduleName
   * 非空，同一层级下模块名称唯一 （不包含当前修改记录本身）
   * 地址 url
   * 二级菜单（grade=1），非空且同一层级下不可重复（不包含当前修改记录本身）
   * 权限码 optValue
   * 非空，不可重复（不包含当前修改记录本身）
   */
  private void checkUpdateModule(Module module) {
      Integer grade = module.getGrade();
      AssertUtil.isTrue(module.getId() == null || moduleMapper.selectByPrimaryKey(module.getId()) == null, "待修改记录不存在");
      AssertUtil.isTrue(grade == null || !(grade == 1 || grade == 2 || grade == 0), "待修改的层级不合法");
      AssertUtil.isTrue(module.getModuleName() == null ||
                      !moduleMapper.selectSameGradeByModuleName(module.getModuleName(), grade).getId().equals(module.getId()),
              "该模块名在同级目录下已经存在");
      if (grade == 1) {
          AssertUtil.isTrue(module.getUrl() == null ||
                  !moduleMapper.selectSameGradeByModuleUrl(module.getUrl(), grade).getId().equals(module.getId()), "该url地址在同级目录下已经存在");
      }
      AssertUtil.isTrue(module.getOptValue() == null ||
                      !moduleMapper.selectByOptValue(module.getOptValue()).getId().equals(module.getId())
              , "该权限码已经存在");
  }
  ```

#### 3.7删除资源模块

* Controller层

  ```java
  @ResponseBody
  @RequestMapping("delete")
  public ResultInfo delete(Integer id) {
      moduleService.delete(id);
      return success("删除模块操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 删除资源
   * 1. 判断删除的记录是否存在
   * 2. 如果当前资源存在子记录，则不可删除
   * 3. 删除资源时，将对应的权限表的记录也删除（判断权限表中是否存在关联数据，如果存在，则删除）
   * 4. 执行删除（更新）操作，判断受影响的行数
   */
  @Transactional
  public void delete(Integer id) {
      Module temp = moduleMapper.selectByPrimaryKey(id);
      //判断删除的记录是否存在
      AssertUtil.isTrue(id == null || temp == null, "待删除记录不存在");
      Integer moduleCount = moduleMapper.selectByParentId(id);
      AssertUtil.isTrue(moduleCount == null || moduleCount != 0, "当前资源存在子记录，不可删除");
  
      //删除资源时，将对应的权限表的记录也删除（判断权限表中是否存在关联数据，如果存在，则删除）
      Integer permissionCount = permissionMapper.selectByModuleId(id);
      if (permissionCount > 0) {
          AssertUtil.isTrue(permissionMapper.deleteByModuleId(id) < 1, "删除模块时，删除对应的权限资源失败");
      }
      temp.setIsValid((byte) 0);
      temp.setUpdateDate(new Date());
      AssertUtil.isTrue(moduleMapper.updateByPrimaryKeySelective(temp) < 1, "删除模块操作失败");
  }
  ```

## 7.客户管理模块的实现

### <1>客户信息管理

#### 1.1进入客户信息管理页面

```java
@RequiredPermission(code = "2010")
@RequestMapping("index")
public String index() {
    return "customer/customer";
}
```

#### 1.2查询客户数据

使用分页:

1. >查询类

```java
@Getter
@Setter
public class CustomerQuery extends BaseQuery {
    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 客户编号
     */
    private String customerNo;
    /**
     * 客户级别
     */
    private String level;
    /**
     * 订单金额
     * 金额区间  1=1-1000 2=1000-3000  3=3000-5000  4=5000以上
     */
    private String type;
    /**
     * 订单时间
     */
    private String time;
}
```

2. >客户信息查询

   * Controller层

     ```java
     @RequestMapping("list")
     @ResponseBody
     public Map<String, Object> list(CustomerQuery customerQuery) {
         return customerService.queryCustomerQuery(customerQuery);
     }
     ```

   * Service层

     ```java
     /**
      * 多条件分页查询
      * 返回的数据格式必须满足layUi数据表格要求的格式
      *
      * @param customerQuery
      * @return
      */
     public Map<String, Object> queryCustomerQuery(CustomerQuery customerQuery) {
         Map<String, Object> map = new HashMap<>();
     
         //开启分页
         PageHelper.startPage(customerQuery.getPage(), customerQuery.getLimit());
         //得到分页对象(对CusDevPlanQuery对象进行分页）
         PageInfo<Customer> pageInfo = new PageInfo<>(customerMapper.selectByParams(customerQuery));
         map.put("code", 0);
         map.put("msg", "success");
         map.put("count", pageInfo.getTotal());
         //设置分页好的列表
         map.put("data", pageInfo.getList());
         return map;
     }
     ```

   * Mapper层

     ```xml
     <select id="selectByParams" parameterType="org.example.crm.query.CustomerQuery" resultType="org.example.crm.model.Customer">
       select
       <include refid="Base_Column_List"></include>
       from
       t_customer
       <where>
       <!--有效且未流失 -->
         is_valid = 1 and state = 0
         <if test="null != customerName and customerName != ''">
           and name like concat('%',#{customerName},'%')
         </if>
         <if test="null != customerNo and customerNo != ''">
           and khno = #{customerNo}
         </if>
         <if test="null != level and level != ''">
           and level = #{level}
         </if>
       </where>
     </select>
     ```

#### 1.3打开添加/修改顾客数据页面

```java
@RequestMapping("toAddOrUpdateCustomerPage")
public String toAddOrUpdateCustomerPage(Integer id, HttpServletRequest request) {
    if (id != null) {
        //修改操作，设置Customer到request域中
        request.setAttribute("customer", customerMapper.selectByPrimaryKey(id));
    }
    return "customer/add_update";
}
```

#### 1.4添加客户数据

* Controller层

  ```java
  @RequiredPermission(code = "201001")
  @RequestMapping("add")
  @ResponseBody
  public ResultInfo add(Customer customer) {
      customerService.add(customer);
      return success("客户添加操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 添加客户
   * 1. 参数校验
   * 客户名称 name
   * 非空，名称唯一
   * 法人代表 fr
   * 非空
   * 手机号码 phone
   * 非空，格式正确
   * 2. 设置参数的默认值
   * 是否有效 isValid    1
   * 创建时间 createDate 系统当前时间
   * 修改时间 updateDate 系统当前时间
   * 流失状态 state      0
   * 0=正常客户  1=流失客户
   * 客户编号 khno
   * 系统生成，唯一 （uuid | 时间戳 | 年月日时分秒 | 雪花算法）
   * 格式：KH + 时间戳
   * 3. 执行添加操作，判断受影响的行数
   *
   * @param customer
   * @return void
   */
  @Transactional
  public void add(Customer customer) {
      //1.参数校验
      checkAddCustomerParams(customer);
      //2.设置默认值
      Customer customer1 = setDefaultValue(customer);
      //3.执行添加操作
      AssertUtil.isTrue(customerMapper.insertSelective(customer) != 1, "顾客添加操作失败");
  }
  
  /*  1. 参数校验
   *      客户名称 name
   *          非空，名称唯一
   *      法人代表 fr
   *          非空
   *      手机号码 phone
   *          非空，格式正确
   */
  private void checkAddCustomerParams(Customer customer) {
      AssertUtil.isTrue(StringUtils.isBlank(customer.getName()), "用户名称不能为空");
      AssertUtil.isTrue(customerMapper.selectByCustomerByName(customer.getName()) != null, "该顾客名称已经存在");
      AssertUtil.isTrue(StringUtils.isBlank(customer.getFr()), "法人代表不能为空");
      AssertUtil.isTrue(StringUtils.isBlank(customer.getPhone()) || !PhoneUtil.isMobile(customer.getPhone()),
              "手机号码不能为空或手机号码格式不正确");
  }
  
  /*
   *  2. 设置参数的默认值
   *      是否有效 isValid    1
   *      创建时间 createDate 系统当前时间
   *      修改时间 updateDate 系统当前时间
   *      流失状态 state      0
   *          0=正常客户  1=流失客户
   *      客户编号 khno:KH + 时间戳
   */
  private Customer setDefaultValue(Customer customer) {
      customer.setIsValid(1);
      customer.setCreateDate(new Date());
      customer.setUpdateDate(new Date());
      customer.setState(0);
      String khno = "KH" + System.currentTimeMillis();
      customer.setKhno(khno);
      return customer;
  }
  ```

#### 1.5修改客户数据

* Controller层

  ```java
  @RequiredPermission(code = "201002")
  @ResponseBody
  @RequestMapping("update")
  public ResultInfo update(Customer customer) {
      customerService.update(customer);
      return success("客户修改操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 修改客户
   * 1. 参数校验
   * 客户ID id
   * 非空，数据存在
   * 客户名称 name
   * 非空，名称唯一
   * 法人代表 fr
   * 非空
   * 手机号码 phone
   * 非空，格式正确
   * 2. 设置参数的默认值
   * 修改时间 updateDate 系统当前时间
   * 3. 执行更新操作，判断受影响的行数
   *
   * @param customer
   * @return void
   */
  @Transactional
  public void update(Customer customer) {
      //1. 参数校验
      AssertUtil.isTrue(customer.getId() == null || customerMapper.selectByPrimaryKey(customer.getId()) == null,
              "待修改的记录不存在");
      AssertUtil.isTrue(customer.getName() == null, "客户名称不能为空");
      Customer temp = customerMapper.selectByCustomerByName(customer.getName());
      if (temp != null) {
          AssertUtil.isTrue(!temp.getId().equals(customer.getId()), "该客户名称已经存在");
      }
      AssertUtil.isTrue(StringUtils.isBlank(customer.getFr()), "法人代表不能为空");
      AssertUtil.isTrue(StringUtils.isBlank(customer.getPhone()) || !PhoneUtil.isMobile(customer.getPhone()),
              "手机号码不能为空或手机号码格式不正确");
      //2.设置默认值
      customer.setUpdateDate(new Date());
      //3.执行修改操作，判断返回的参数
      AssertUtil.isTrue(customerMapper.updateByPrimaryKeySelective(customer) < 1, "客户修改操作失败");
  }
  ```

#### 1.6删除客户数据

* Controller层

  ```java
  @RequiredPermission(code = "201003")
  @RequestMapping("delete")
  @ResponseBody
  public ResultInfo delete(Integer id) {
      customerService.delete(id);
      return success("客户删除操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 删除客户
   * 1. 参数校验
   * id
   * 非空，数据存在
   * 2. 设置参数默认值
   * isValid     0
   * updateDate  系统当前时间
   * 3. 执行删除（更新）操作，判断受影响的行数
   *
   * @param id
   * @return void
   */
  @Transactional
  public void delete(Integer id) {
      //1.参数校验
      AssertUtil.isTrue(id == null, "待删除数据不存在");
      Customer customer = customerMapper.selectByPrimaryKey(id);
      AssertUtil.isTrue(customer == null, "待删除数据不存在");
      //2.设置参数默认值
      customer.setIsValid(0);
      customer.setUpdateDate(new Date());
      //3.执行删除
      AssertUtil.isTrue(customerMapper.updateByPrimaryKeySelective(customer) < 1, "删除顾客操作失败");
  }
  ```

#### 1.7客户订单列表查看

* Controller层

  ```java
    @RequestMapping("list")
      @ResponseBody
      public Map<String, Object> list(CustomerOrderQuery customerOrderQuery) {
          return customerOrderService.queryCustomerOrderByParams(customerOrderQuery);
      }
  ```

* Service层

  ```java
  public Map<String, Object> queryCustomerOrderByParams(CustomerOrderQuery customerOrderQuery) {
      Map<String, Object> map = new HashMap<>();
      //开启分页
      PageHelper.startPage(customerOrderQuery.getPage(), customerOrderQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<CustomerOrder> pageInfo = new PageInfo<>(customerOrderMapper.selectByParams(customerOrderQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

#### 1.8进入客户订单详情页面

```java
@RequestMapping("toOrderDetailPage")
public String toOrderDetailPage(Integer orderId, HttpServletRequest request) {

    if (orderId != null) {
        Map<String, Object> map = customerOrderService.queryByOrderId(orderId);
        request.setAttribute("order", map);
    }
    return "customer/customer_order_detail";
}
```

#### 1.9查看客户订单详情

* Controller层

  ```java
  @RequestMapping("list")
  @ResponseBody
  public Map<String, Object> list(OrderDetailQuery orderDetailQuery) {
      return orderDetailsService.list(orderDetailQuery);
  }
  ```

* Service层

  ```java
  public Map<String, Object> list(OrderDetailQuery orderDetailQuery) {
      Map<String, Object> map = new HashMap<>();
  
      //开启分页
      PageHelper.startPage(orderDetailQuery.getPage(), orderDetailQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<OrderDetails> pageInfo = new PageInfo<>(orderDetailsMapper.selectByParams(orderDetailQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

### <2>客户流失管理

客户流失规则定义：客户自创建超过六个月以来未与企业产生任何订单或者客户最后下单日期距离现在超过六个月的客户定义为流失客户

#### 2.1进入客户流失管理页面

```java
@RequiredPermission(code = "2020")
@RequestMapping("index")
public String index() {
    return "customerLoss/customer_loss";
}
```

#### 2.2客户流失数据查询

* Controller层

  ```java
  @RequestMapping("list")
  @ResponseBody
  public Map<String, Object> list(CustomerLossQuery customerLossQuery) {
      return customerLossService.list(customerLossQuery);
  }
  ```

* Service层

  ```java
  public Map<String, Object> list(CustomerLossQuery customerLossQuery) {
      Map<String, Object> map = new HashMap<>();
      //开启分页
      PageHelper.startPage(customerLossQuery.getPage(), customerLossQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<CustomerLoss> pageInfo = new PageInfo<>(customerLossMapper.selectByParams(customerLossQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

#### 2.3查询客户暂缓操作列表

* Controller层

  ```java
  @RequestMapping("list")
  public Map<String, Object> list(CustomerReprieveQuery customerReprieveQuery) {
      return customerReprieveService.list(customerReprieveQuery);
  }
  ```

* Service层

  ```java
  public Map<String, Object> list(CustomerReprieveQuery customerReprieveQuery) {
      Map<String, Object> map = new HashMap<>();
      //开启分页
      PageHelper.startPage(customerReprieveQuery.getPage(), customerReprieveQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<CustomerReprieve> pageInfo = new PageInfo<>(customerReprieveMapper.selectByParams(customerReprieveQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

#### 2.4进入客户暂缓添加修改页面

```java
@RequestMapping("toAddOrUpdateCustomerReprPage")
public String toAddOrUpdateCustomerReprPage(Integer lossId, HttpServletRequest request, Integer id) {
    //客户流失Id存到作用域中
    request.setAttribute("lossId", lossId);
    if (id != null) {
        CustomerReprieve customerReprieve = customerReprieveMapper.selectByPrimaryKey(id);
        request.setAttribute("customerRep", customerReprieve);
    }

    return "customerLoss/customer_rep_add_update";
}
```

#### 2.5添加，修改，删除客户暂缓操作

单表简单的增删查改，具体看源码

#### 2.6客户流失定时任务

1. >创建定时任务类

```java
@Component
public class JobTask {
    @Autowired
    private CustomerService customerService;

    //cron表达式
    //每两秒执行一次
    //@Scheduled(cron = "0/2 * * * * ?")

    //从六月开始，每个月执行一次
    @Scheduled(cron = "* * * * 6/1 ? ")
    public void job() {
        //调用需要被执行的方法
        //开始执行定时任务
        System.out.println("开始执行定时器任务");
        customerService.updateCustomerState();
        System.out.println("定时器任务执行完成");
    }
}
```

2. >定时任务

```java
/**
 * 定时任务，将流失的客户转移到流失客户记录表中
 * <p>
 * 更新客户的流失状态
 * 1. 查询待流失的客户数据
 * 2. 将流失客户数据批量添加到客户流失表中
 * 3. 批量更新客户的流失状态  0=正常客户  1=流失客户
 */
@Transactional
public void updateCustomerState() {
    //查询待流失的客户数据
    List<Customer> customers = customerMapper.selectAllLossCustomer();
    //如果有数据
    if (customers != null && customers.size() > 0) {
        // 接收流失客户的ID
        List<Integer> LossCustomerIds = new ArrayList<>();
        //接收流失客户
        List<CustomerLoss> LossCustomers = new ArrayList<>();
        //遍历流失客户，加入到流失客户表中
        for (int i = 0; i < customers.size(); i++) {
            //定义流失客户对象
            CustomerLoss customerLoss = new CustomerLoss();
            //设置值
            customerLoss.setCreateDate(new Date());
            //客户经理
            customerLoss.setCusManager(customers.get(i).getCusManager());
            //客户名称
            customerLoss.setCusName(customers.get(i).getName());
            //客户编号
            customerLoss.setCusNo(customers.get(i).getKhno());
            //设置有效
            customerLoss.setIsValid(1);
            customerLoss.setUpdateDate(new Date());
            //流失状态 1 = 确认流失 0 =暂缓流失
            customerLoss.setState(0);

            //设置最后一单的订单时间
            //先查询最后一单时间
            CustomerOrder customerOrder = customerOrderMapper.queryCustomerLastOrderByCustomerId(customers.get(i).getId());
            if (customerOrder != null) {
                customerLoss.setLastOrderTime(customerOrder.getOrderDate());
            }
            LossCustomers.add(customerLoss);
            LossCustomerIds.add(customers.get(i).getId());
        }
        // 批量添加流失客户记录
        AssertUtil.isTrue(customerLossMapper.insertBatch(LossCustomers) != LossCustomers.size(), "客户流失数据转移失败！");

        /* 3. 批量更新客户的流失状态 */
        AssertUtil.isTrue(customerMapper.updateCustomerStateByIds(LossCustomerIds) != LossCustomerIds.size(), "客户流失数据转移失败！");
    }
}
```

3. >查询流失客户

```xml
 <!--
 流失客户规则定义：
     客户自创建起，超过六个月未与企业产生任何订单或者客户最后下单日期距离现在超过六个月的客户定义为流失客户。

 规则分析：
     1. 客户数据录入到系统的时间距离现在的时间超过6个月 （前提条件）
     2. 客户与公司没有产生（已支付）订单记录 || 客户最后下单时间距离现在时间超过6个月
         2.1 查询没有订单记录的客户
         2.2 查询最后的订单时间距离现在时间超过6个月的客户
         实现思路：
             查询2.1 与 2.2的客户，并去除重复数据
         具体实现：
             等价于查询2.1 与 2.2的记录   反向查询 （推荐使用）
             查询产生过订单记录并且最后的下单时间距离现在时间不超过6个月的客户，然后再从客户表中排除这些客户
     注：在（1）前提条件的基础上，满足（2）下面的两种情况

 SQL查询分析：
     DATE_ADD()函数
         作用：向日期添加指定的时间间隔。
         格式：DATE_ADD(date, INTERVAL expr type)
         属性：
             date参数是合法的日期表达式。
             expr参数是添加的时间的间隔。
             type参数表示类型。可以是YEAR（年）、MONTH（月）、DAY（日）、时分秒等。
         示例：向当前时间添加6个月。
             DATE_ADD(NOW(),INTERVAL 6 MONTH)
-->
 <select id="selectAllLossCustomer" resultType="org.example.crm.model.Customer">
   SELECT *
   FROM t_customer c
   WHERE is_valid = 1 and state = 0 AND DATE_ADD(c.create_date,INTERVAL 6 MONTH) &lt; NOW()
   AND c.id NOT IN (
     SELECT DISTINCT cus_id
     FROM t_customer_order o
     WHERE is_valid = 1 AND o.state = 1 AND DATE_ADD(o.order_date,INTERVAL 6 MONTH) &gt; NOW()
   )
 </select>
```

4. >开启定时任务

![image-20210810153711886](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210810153711886.png)

## 8.服务管理模块的实现

服务管理模块涉及：创建，分配，处理，反馈，归档

### <1>服务管理页面的转发

1. >创建客户服务状态的枚举类

```java
public enum CustomerServeStatus {
    // 创建
    CREATED("fw_001"),
    // 分配
    ASSIGNED("fw_002"),
    // 处理
    PROCED("fw_003"),
    // 反馈
    FEED_BACK("fw_004"),
    // 归档
    ARCHIVED("fw_005");

    private String state;

    CustomerServeStatus(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
```

2. >不同服务页面的转发

```java
/**
 * 通过不同的类型进入不同的服务页面
 *
 * @param type
 * @return java.lang.String
 */
@RequestMapping("index/{type}")
public String index(@PathVariable Integer type) {
    // 判断类型是否为空
    if (type != null) {
        if (type == 1) {

            // 服务创建
            return "customerServe/customer_serve";

        } else if (type == 2) {

            // 服务分配
            return "customerServe/customer_serve_assign";

        } else if (type == 3) {

            // 服务处理
            return "customerServe/customer_serve_proce";

        } else if (type == 4) {

            // 服务反馈
            return "customerServe/customer_serve_feed_back";

        } else if (type == 5) {

            // 服务归档
            return "customerServe/customer_serve_archive";

        } else {
            return "";
        }

    } else {
        return "";
    }
}
```

### <2>查询服务数据列表

* Controller层

  ```java
  @RequestMapping("list")
  @ResponseBody
  public Map<String, Object> queryCustomerServeByParams(CustomerServeQuery customerServeQuery,
                                                        Integer flag, HttpServletRequest request) {
  
      // 判断是否执行服务处理，如果是则查询分配给当前登录用户的服务记录
      if (flag != null && flag == 1) {
          // 设置查询条件：分配人
          customerServeQuery.setAssigner(LoginUserUtil.releaseUserIdFromCookie(request));
      }
  
      return customerServeService.queryCustomerServeByParams(customerServeQuery);
  }
  ```

* Service层

  ```java
  public Map<String, Object> queryCustomerServeByParams(CustomerServeQuery customerServeQuery) {
      Map<String, Object> map = new HashMap<>();
  
      //开启分页
      PageHelper.startPage(customerServeQuery.getPage(), customerServeQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<CustomerServe> pageInfo = new PageInfo<>(customerServeMapper.selectByParams(customerServeQuery));
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

### <3>进入服务添加视图页面

```java
@RequestMapping("toAddCustomerServePage")
public String toAddCustomerServePage() {
    return "customerServe/customer_serve_add";
}
```

### <4>服务添加操作

* Controller层

  ```java
  @ResponseBody
  @RequestMapping("add")
  public ResultInfo add(CustomerServe customerServe) {
      customerServeService.add(customerServe);
      return success("服务添加操作成功");
  }
  ```

* Service层

  ```java
  /**
   * 添加服务操作
   * 1. 参数校验
   * 客户名 customer
   * 非空，客户表中存在客户记录
   * 服务类型 serveType
   * 非空
   * 服务请求内容  serviceRequest
   * 非空
   * 2. 设置参数的默认值
   * 服务状态
   * 服务创建状态  fw_001
   * 是否有效
   * 创建时间
   * 更新时间
   * 创建人 createPeople
   * （前端页面中通过cookie获取，传递到后台）
   * 2. 执行添加操作，判断受影响的行数
   *
   * @param customerServe
   * @return void
   */
  @Transactional
  public void add(CustomerServe customerServe) {
      checkParams(customerServe);
      //设置参数的默认值
      customerServe.setServeType(CustomerServeStatus.CREATED.getState());
      customerServe.setIsValid(1);
      customerServe.setCreateDate(new Date());
      customerServe.setUpdateDate(new Date());
      //执行添加操作
      AssertUtil.isTrue(customerServeMapper.insertSelective(customerServe) < 1, "服务添加操作失败");
  }
  ```

### <5>打开服务分配页面

```java
@RequestMapping("toCustomerServeAssignPage")
public String toCustomerServeAssignPage(Integer id, HttpServletRequest request) {
    if (id != null) {
        request.setAttribute("customerServe", customerServeMapper.selectByPrimaryKey(id));
    }
    return "customerServe/customer_serve_assign_add";
}
```

### <6>客户服务分配操作

* Controller层

  ```java
  @ResponseBody
  @RequestMapping("update")
  public ResultInfo update(CustomerServe customerServe) {
      customerServeService.update(customerServe);
      return success("客户服务更新成功");
  }
  ```

* Service层

  ```java
  /**
   * 服务分配/服务处理/服务反馈
   * 1. 参数校验与设置参数的默认值
   * 客户服务ID
   * 非空，记录必须存在
   * 客户服务状态
   * 如果服务状态为 服务分配状态 fw_002
   * 分配人
   * 非空，分配用户记录存在
   * 分配时间
   * 系统当前时间
   * 更新时间
   * 系统当前时间
   * <p>
   * 如果服务状态为 服务处理状态 fw_003
   * 服务处理内容
   * 非空
   * 服务处理时间
   * 系统当前时间
   * 更新时间
   * 系统当前时间
   * <p>
   * 如果服务状态是 服务反馈状态  fw_004
   * 服务反馈内容
   * 非空
   * 服务满意度
   * 非空
   * 更新时间
   * 系统当前时间
   * 服务状态
   * 设置为 服务归档状态 fw_005
   * <p>
   * 2. 执行更新操作，判断受影响的行数
   *
   * @param customerServe
   * @return void
   */
  @Transactional
  public void update(CustomerServe customerServe) {
      // 客户服务ID  非空且记录存在
      AssertUtil.isTrue(customerServe.getId() == null
              || customerServeMapper.selectByPrimaryKey(customerServe.getId()) == null, "待更新的服务记录不存在！");
  
      // 判断客户服务的服务状态
      if (CustomerServeStatus.ASSIGNED.getState().equals(customerServe.getState())) {
          // 服务分配操作
          // 分配人       非空，分配用户记录存在
          AssertUtil.isTrue(StringUtils.isBlank(customerServe.getAssigner()), "待分配用户不能为空！");
          AssertUtil.isTrue(userMapper.selectByPrimaryKey(Integer.parseInt(customerServe.getAssigner())) == null, "待分配用户不存在！");
          // 分配时间     系统当前时间
          customerServe.setAssignTime(new Date());
  
  
      } else if (CustomerServeStatus.PROCED.getState().equals(customerServe.getState())) {
          // 服务处理操作
          // 服务处理内容   非空
          AssertUtil.isTrue(StringUtils.isBlank(customerServe.getServiceProce()), "服务处理内容不能为空！");
          // 服务处理时间   系统当前时间
          customerServe.setServiceProceTime(new Date());
  
      } else if (CustomerServeStatus.FEED_BACK.getState().equals(customerServe.getState())) {
          // 服务反馈操作
          // 服务反馈内容   非空
          AssertUtil.isTrue(StringUtils.isBlank(customerServe.getServiceProceResult()), "服务反馈内容不能为空！");
          // 服务满意度     非空
          AssertUtil.isTrue(StringUtils.isBlank(customerServe.getMyd()), "请选择服务反馈满意度！");
          // 服务状态      设置为 服务归档状态 fw_005
          customerServe.setState(CustomerServeStatus.ARCHIVED.getState());
      }
  
      // 更新时间     系统当前时间
      customerServe.setUpdateDate(new Date());
  
      // 执行更新操作，判断受影响的行数
      AssertUtil.isTrue(customerServeMapper.updateByPrimaryKeySelective(customerServe) < 1, "服务更新失败！");
  
  }
  ```

## 9.统计报表模块实现

### <1>客户贡献分析

* Controller层

  ```java
  @RequiredPermission(code = "4010")
  @RequestMapping("queryCustomerContributionByParams")
  @ResponseBody
  public Map<String, Object> queryCustomerContributionByParams(CustomerQuery customerQuery) {
      return customerService.queryCustomerContributionByParams(customerQuery);
  }
  ```

* Service层

  ```java
  /**
   * 查询客户贡献
   *
   * @param customerQuery
   * @return
   */
  public Map<String, Object> queryCustomerContributionByParams(CustomerQuery customerQuery) {
      Map<String, Object> map = new HashMap<>();
  
      //开启分页
      PageHelper.startPage(customerQuery.getPage(), customerQuery.getLimit());
      //得到分页对象(对CusDevPlanQuery对象进行分页）
      PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(customerMapper.queryCustomerContributionByParams(customerQuery));
  
      map.put("code", 0);
      map.put("msg", "success");
      map.put("count", pageInfo.getTotal());
      //设置分页好的列表
      map.put("data", pageInfo.getList());
      return map;
  }
  ```

* Mapper层

  ```java
  <!-- 查询客户贡献数据 -->
  <select id="queryCustomerContributionByParams" parameterType="org.example.crm.query.CustomerQuery" resultType="java.util.Map">
    SELECT
    c.`name`,sum(od.sum) total
    FROM
    t_customer c
    LEFT JOIN
    t_customer_order o
    ON
    c.id = o.cus_id
    LEFT JOIN
    t_order_details od
    ON
    o.id = od.order_id
    <where>
      c.is_valid = 1 AND c.state = 0 AND o.is_valid = 1 AND o.state = 1 AND od.is_valid = 1
      <if test="null != customerName and customerName != ''">
        and c.name like concat('%',#{customerName},'%')
      </if>
      <if test="null != time and time != ''">
        and o.order_date &gt; #{time}
      </if>
    </where>
    GROUP BY
    c.`name`
    <if test="null != type">
      <if test="type==1">
        HAVING total &gt;= 0 and total &lt;= 1000
      </if>
      <if test="type==2">
        HAVING total &gt; 1000 and total &lt;= 3000
      </if>
      <if test="type==3">
        HAVING total &gt; 3000 and total &lt;= 5000
      </if>
      <if test="type==4">
        HAVING total &gt; 5000
      </if>
    </if>
    ORDER BY
    total desc
  
  </select>
  ```

### <2>查询客户构成

* Controller层

  ```java
  /**
   * 查询客户构成 （折线图）
   *
   * @param
   * @return java.util.Map<java.lang.String, java.lang.Object>
   */
  @RequestMapping("countCustomerMake")
  @ResponseBody
  public Map<String, Object> countCustomerMake() {
      return customerService.countCustomerMake();
  }
  
  /**
   * 查询客户构成 （饼状图）
   *
   * @param
   * @return java.util.Map<java.lang.String, java.lang.Object>
   */
  @RequestMapping("countCustomerMake02")
  @ResponseBody
  public Map<String, Object> countCustomerMake02() {
      return customerService.countCustomerMake02();
  }
  ```

* Service层

  ```java
  /**
   * 查询客户构成 （折线图）
   * @return
   */
  public Map<String, Object> countCustomerMake() {
      Map<String, Object> map = new HashMap<>();
      // 查询客户构成数据的列表
      List<Map<String, Object>> dataList = customerMapper.countCustomerMake();
      // 折线图X轴数据  数组
      List<String> data1 = new ArrayList<>();
      // 折线图Y轴数据  数组
      List<Integer> data2 = new ArrayList<>();
  
      // 判断数据列表 循环设置数据
      if (dataList != null && dataList.size() > 0) {
          for (int i = 0; i < dataList.size(); i++) {
              data1.add(dataList.get(i).get("level").toString());
              data2.add(Integer.parseInt(dataList.get(i).get("total").toString()));
          }
      }
      // 将X轴的数据集合与Y轴的数据集合，设置到map中
      map.put("data1", data1);
      map.put("data2", data2);
  
      return map;
  }
  
  /**
   * 饼状图
   * @return
   */
  public Map<String, Object> countCustomerMake02() {
      Map<String, Object> map = new HashMap<>();
      // 查询客户构成数据的列表
      List<Map<String, Object>> dataList = customerMapper.countCustomerMake();
      // 饼状图数据   数组（数组中是字符串）
      List<String> data1 = new ArrayList<>();
      // 饼状图的数据  数组（数组中是对象）
      List<Map<String, Object>> data2 = new ArrayList<>();
  
      // 判断数据列表 循环设置数据
      if (dataList != null && dataList.size() > 0) {
          // 遍历集合
          for (int i = 0; i < dataList.size(); i++) {
              //饼状图数据， 数组（数组中是字符串
              data1.add(dataList.get(i).get("level").toString());
              //饼状图数据 数组（数组中是对象）
              Map<String, Object> dataMap = new HashMap<>();
              dataMap.put("name", dataList.get(i).get("level"));
              dataMap.put("value", dataList.get(i).get("total"));
              data2.add(dataMap);
          }
      }
  
      // 将X轴的数据集合与Y轴的数据集合，设置到map中
      map.put("data1", data1);
      map.put("data2", data2);
  
      return map;
  }
  ```

* Mapper层

  ```java
   <!-- 查询客户构成 -->
   <select id="countCustomerMake" resultType="java.util.Map">
     SELECT
         `level`,count(1) total
     FROM
         t_customer
     WHERE
         is_valid = 1
     AND
         state = 0
     GROUP BY
  `level`
   </select>
  ```

# 十六.Linux

## 1.服务器关机相关命令

```cmake
关机指令为：shutdown ；

sync # 将数据由内存同步到硬盘中。

shutdown # 关机指令，你可以man shutdown 来看一下帮助文档。例如你可以运行如下命令关机：

shutdown –h 10 # 这个命令告诉大家，计算机将在10分钟后关机

shutdown –h now # 立马关机

shutdown –h 20:25 # 系统会在今天20:25关机

shutdown –h +10 # 十分钟后关机

shutdown –r now # 系统立马重启

shutdown –r +10 # 系统十分钟后重启

reboot # 就是重启，等同于 shutdown –r now

halt # 关闭系统，等同于shutdown –h now 和 poweroff
最后总结一下，不管是重启系统还是关闭系统，首先要运行 sync 命令，把内存中的数据写到磁盘中。
```

## 2.系统目录结构介绍

![image-20210811145714170](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210811145714170.png)

![图片](https://mmbiz.qpic.cn/mmbiz_png/uJDAUKrGC7LDkhrDl4H9TqZhwyeNSeaNibQYW2xbQIL38lrCCSPEzFKJhCiau0FvQMFSa37NQxTTbbo3PrpjJic5g/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

>对于目录的解释

**以下是对这些目录的解释：**

- **/bin**：bin是Binary的缩写, 这个目录存放着最经常使用的命令。
- **/boot：** 这里存放的是启动Linux时使用的一些核心文件，包括一些连接文件以及镜像文件。
- **/dev ：** dev是Device(设备)的缩写, 存放的是Linux的外部设备，在Linux中访问设备的方式和访问文件的方式是相同的。
- ==**/etc：** 这个目录用来存放所有的系统管理所需要的配置文件和子目录。==
- ==**/home**：用户的主目录，在Linux中，每个用户都有一个自己的目录，一般该目录名是以用户的账号命名的。==
- **/lib**：这个目录里存放着系统最基本的动态连接共享库，其作用类似于Windows里的DLL文件。
- **/lost+found**：这个目录一般情况下是空的，当系统非法关机后，这里就存放了一些文件。
- **/media**：linux系统会自动识别一些设备，例如U盘、光驱等等，当识别后，linux会把识别的设备挂载到这个目录下。
- **/mnt**：系统提供该目录是为了让用户临时挂载别的文件系统的，我们可以将光驱挂载在/mnt/上，然后进入该目录就可以查看光驱里的内容了。
- ==**/opt**：这是给主机额外安装软件所摆放的目录。比如你安装一个ORACLE数据库则就可以放到这个目录下。默认是空的。==
- **/proc**：这个目录是一个虚拟的目录，它是系统内存的映射，我们可以通过直接访问这个目录来获取系统信息。
- ==**/root**：该目录为系统管理员，也称作超级权限者的用户主目录。==
- **/sbin**：s就是Super User的意思，这里存放的是系统管理员使用的系统管理程序。
- **/srv**：该目录存放一些服务启动之后需要提取的数据。
- **/sys**：这是linux2.6内核的一个很大的变化。该目录下安装了2.6内核中新出现的一个文件系统 sysfs 。
- ==**/tmp**：这个目录是用来存放一些临时文件的。==
- ==**/usr**：这是一个非常重要的目录，用户的很多应用程序和文件都放在这个目录下，类似于windows下的program files目录。==
- **/usr/bin：** 系统用户使用的应用程序。
- **/usr/sbin：** 超级用户使用的比较高级的管理程序和系统守护程序。
- **/usr/src：** 内核源代码默认的放置目录。
- ==**/var**：这个目录中存放着在不断扩充着的东西，我们习惯将那些经常被修改的目录放在这个目录下。包括各种日志文件。==
- **/run**：是一个临时文件系统，存储系统启动以来的信息。当系统重启时，这个目录下的文件应该被删掉或清除
- **/www**:==存放服务器网站相关的资源，环境，网站的项目等==

## 3.文件操作相关命令

**绝对路径：**

路径的写法，由根目录 / 写起，例如：/usr/share/doc 这个目录。

**相对路径：**

路径的写法，不是由 / 写起，例如由 /usr/share/doc 要到 /usr/share/man 底下时，可以写成：cd ../man 这就是相对路径的写法啦！

- ls: 列出目录
- cd：切换目录
- pwd：显示目前的目录
- mkdir：创建一个新的目录
- rmdir：删除一个空的目录
- cp: 复制文件或目录
- rm: 移除文件或目录
- mv: 移动文件与目录，或修改文件与目录的名称

你可以使用 *man [命令]* 来查看各个命令的使用文档，如 ：man cp。

>列出目录

- -a ：全部的文件，连同隐藏文件( 开头为 . 的文件) 一起列出来(常用)
- -l ：长数据串列出，包含文件的属性与权限等等数据；(常用)

>cd （切换目录）:cd ../切换到上一级目录

>pwd ( 显示目前所在的目录 )

选项与参数：**-P** ：显示出确实的路径，而非使用连接(link) 路径。

> touch 

touch命令参数可更改文档或目录的日期时间，包括存取时间和更改时间，或者新

建一个不存在的文件

```tcl
touch test.txt
```



>mkdir （创建新目录）	

- -m ：配置文件的权限,直接配置，不需要看默认权限 (umask) 的脸色
- -p ：帮助你直接将所需要的目录(包含上一级目录)递归创建起来！

>rmdir ( 删除空的目录 )

**-p ：**连同上一级『空的』目录也一起删除

>cp ( 复制文件或目录 )

- **-a：**相当於 -pdr 的意思，至於 pdr 请参考下列说明；(常用)
- **-p：**连同文件的属性一起复制过去，而非使用默认属性(备份常用)；
- **-d：**若来源档为连结档的属性(link file)，则复制连结档属性而非文件本身；
- **-r：**递归持续复制，用於目录的复制行为；(常用)
- **-f：**为强制(force)的意思，若目标文件已经存在且无法开启，则移除后再尝试一次；
- **-i：**若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
- **-l：**进行硬式连结(hard link)的连结档创建，而非复制文件本身。
- **-s：**复制成为符号连结档 (symbolic link)，亦即『捷径』文件；
- **-u：**若 destination 比 source 旧才升级 destination ！

```tcl
# 找一个有文件的目录，我这里找到 root目录
[root@kuangshen home]# cd /root
[root@kuangshen ~]# ls
install.sh
[root@kuangshen ~]# cd /home

# 复制 root目录下的install.sh 到 home目录下
[root@kuangshen home]# cp /root/install.sh /home
[root@kuangshen home]# ls
install.sh

# 再次复制，加上-i参数，增加覆盖询问？
[root@kuangshen home]# cp -i /root/install.sh /home
cp: overwrite ‘/home/install.sh’? y # n不覆盖，y为覆盖
```

>rm ( 移除文件或目录 )

- -f ：就是 force 的意思，忽略不存在的文件，不会出现警告信息；
- -i ：互动模式，在删除前会询问使用者是否动作
- -r ：递归删除,最常用在目录的删除,这是非常危险的选项！！！

```tcl
# 将刚刚在 cp 的实例中创建的 install.sh删除掉！
[root@kuangshen home]# rm -i install.sh
rm: remove regular file ‘install.sh’? y
# 如果加上 -i 的选项就会主动询问喔，避免你删除到错误的档名！

# 尽量不要在服务器上使用 rm -rf /
```

>mv  ( 移动文件与目录，或修改名称 )

- -f ：force 强制的意思，如果目标文件已经存在，不会询问而直接覆盖；
- -i ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！
- -u ：若目标文件已经存在，且 source 比较新，才会升级 (update)

```tcl
# 复制一个文件到当前目录
[root@kuangshen home]# cp /root/install.sh /home

# 创建一个文件夹 test
[root@kuangshen home]# mkdir test

# 将复制过来的文件移动到我们创建的目录，并查看
[root@kuangshen home]# mv install.sh test
[root@kuangshen home]# ls
test
[root@kuangshen home]# cd test
[root@kuangshen test]# ls
install.sh

# 将文件夹重命名，然后再次查看！
[root@kuangshen test]# cd ..
[root@kuangshen home]# mv test mvtest
[root@kuangshen home]# ls
mvtest
```

## 4.文件的基本属性

![image-20210811152356008](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210811152356008.png)

>第0位：确定文件类型

- 当为[ **d** ]则是目录
- 当为[ **-** ]则是文件；
- 若是[ **l** ]则表示为链接文档 ( link file )；
- 若是[ **b** ]则表示为装置文件里面的可供储存的接口设备 ( 可随机存取装置 )；
- 若是[ **c** ]则表示为装置文件里面的串行端口设备，例如键盘、鼠标 ( 一次性读取装置 )。

>后面9位，三个为一组，确定权限

第0位确定文件类型，第1-3位确定属主（该文件的所有者）拥有该文件的权限。第4-6位确定属组（所有者的同组用户）拥有该文件的权限，第7-9位确定其他用户拥有该文件的权限。

![image-20210811152500770](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210811152500770.png)

> 修改文件属性

**1、chgrp：更改文件属组**

```
chgrp [-R] 属组名 文件名
```

-R：递归更改文件属组，就是在更改某个目录文件的属组时，如果加上-R的参数，那么该目录下的所有文件的属组都会更改。

**2、chown：更改文件属主，也可以同时更改文件属组**

```tcl
chown [–R] 属主名 文件名
chown [-R] 属主名：属组名 文件名
```

**3、chmod：更改文件9个属性**

```tcl
chmod [-R] xyz 文件或目录
```

Linux文件属性有两种设置方法，一种是数字，一种是符号。

Linux文件的基本权限就有九个，分别是owner/group/others三种身份各有自己的read/write/execute权限。

先复习一下刚刚上面提到的数据：文件的权限字符为：『-rwxrwxrwx』， 这九个权限是三个三个一组的！其中，我们可以使用数字来代表各个权限，各权限的分数对照表如下：

```tcl
r:4     w:2         x:1
```

每种身份(owner/group/others)各自的三个权限(r/w/x)分数是需要累加的，例如当权限为：[-rwxrwx---] 分数则是：

- owner = rwx = 4+2+1 = 7
- group = rwx = 4+2+1 = 7
- others= --- = 0+0+0 = 0

```tcl
chmod 770 filename
```

## 5.文件的内容查看

- cat 由第一行开始显示文件内容
- tac 从最后一行开始显示，可以看出 tac 是 cat 的倒着写！
- nl  显示的时候，顺道输出行号！
- more 一页一页的显示文件内容
- less 与 more 类似，但是比 more 更好的是，他可以往前翻页！
- head 只看头几行
- tail 只看尾巴几行

>cat 由第一行开始显示文件内容

- -A ：相当於 -vET 的整合选项，可列出一些特殊字符而不是空白而已；
- -b ：列出行号，仅针对非空白行做行号显示，空白行不标行号！
- -E ：将结尾的断行字节 $ 显示出来；
- -n ：列印出行号，连同空白行也会有行号，与 -b 的选项不同；
- -T ：将 [tab] 按键以 ^I 显示出来；
- -v ：列出一些看不出来的特殊字符

```tcl
# 查看网络配置: 文件地址 /etc/sysconfig/network-scripts/
[root@kuangshen ~]# cat /etc/sysconfig/network-scripts/ifcfg-eth0
DEVICE=eth0
BOOTPROTO=dhcp
ONBOOT=yes
```

>tac  和cat相反，从最后一行开始显示文件内容，倒着看

>nl 显示行号

- -b ：指定行号指定的方式，主要有两种：-b a ：表示不论是否为空行，也同样列出行号(类似 cat -n)；-b t ：如果有空行，空的那一行不要列出行号(默认值)；
- -n ：列出行号表示的方法，主要有三种：-n ln ：行号在荧幕的最左方显示；-n rn ：行号在自己栏位的最右方显示，且不加 0 ；-n rz ：行号在自己栏位的最右方显示，且加 0 ；
- -w ：行号栏位的占用的位数。

```tcl
[root@kuangshen ~]# nl /etc/sysconfig/network-scripts/ifcfg-eth0
1DEVICE=eth0
2BOOTPROTO=dhcp
3ONBOOT=yes
```

>more 一页一页的翻动

在 more 这个程序的运行过程中，你有几个按键可以按的：

- 空白键 (space)：代表向下翻一页；
- Enter   ：代表向下翻『一行』；
- /字串   ：代表在这个显示的内容当中，向下搜寻『字串』这个关键字；
- :f    ：立刻显示出档名以及目前显示的行数；
- q    ：代表立刻离开 more ，不再显示该文件内容。
- b 或 [ctrl]-b ：代表往回翻页，不过这动作只对文件有用，对管线无用。

>less  一页一页翻动，以下实例输出/etc/man.config文件的内容

- 空白键  ：向下翻动一页；
- [pagedown]：向下翻动一页；
- [pageup] ：向上翻动一页；
- /字串  ：向下搜寻『字串』的功能；
- ?字串  ：向上搜寻『字串』的功能；
- n   ：重复前一个搜寻 (与 / 或 ? 有关！)
- N   ：反向的重复前一个搜寻 (与 / 或 ? 有关！)
- q   ：离开 less 这个程序；

>head  取出文件前面几行

选项与参数：**-n** 后面接数字，代表显示几行的意思！

默认的情况中，显示前面 10 行！若要显示前 20 行，就得要这样：

```tcl
# head -n 20 /etc/csh.login
```

>tail  取出文件后面几行

选项与参数：

- -n ：后面接数字，代表显示几行的意思

默认的情况中，显示最后 10 行！若要显示最后 20 行，就得要这样：

```tcl
# tail -n 20 /etc/csh.login
```

>man:Linux的命令有很多参数，我们不可能全记住，我们可以通过查看联机手册获取帮助

 **语法**: man [选项] 命令

**常用选项**

-k 根据关键字搜索联机帮助

num 只在第num章节找

man man 能够看到 man 手册中的若干个章节及其含义.

## 6.Vim

![图片](https://mmbiz.qpic.cn/mmbiz_png/uJDAUKrGC7L1I72C1HrwJ9bG6XAbVggMIxVoDyQ4LWxSbHDjAYfHuId7xfHHichtqZK2ehsXvcC9KiaibPf68s9dg/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1)

- **i** 切换到输入模式，以输入字符。
  - **字符按键以及Shift组合**，输入字符
  - **ENTER**，回车键，换行
  - **BACK SPACE**，退格键，删除光标前一个字符
  - **DEL**，删除键，删除光标后一个字符
  - **方向键**，在文本中移动光标
  - **HOME**/**END**，移动光标到行首/行尾
  - **Page Up**/**Page Down**，上/下翻页
  - **Insert**，切换光标为输入/替换模式，光标将变成竖线/下划线
  - **ESC**，退出输入模式，切换到命令模式

- **:** 切换到底线命令模式，以在最底一行输入命令。
  - q 退出程序
  - w 保存文件

## 7.进程相关命令

>基本概念

1. 在Linux中，每个程序都有一个自己的进程，每个进程都有一个id号
2. 每一个进程都有一个进程号(即创建该进程的进程)
3. 进程一般有两种存在方式，前台和后台。一般的话服务在后台运行，基本的程序在前台

>命令

1. 查看进程

netstat -anp | grep 端口号   查看端口是否被占用

ps  -xx   ----->查看当前系统中正在运行的进程的各种信息

* -A: 将所有的进程都显示出来，和-e的效果相同
* -a: 显示现行终端机下的所有程序，包括其他用户的程序
* -u: 以用户为主的格式来显示进程的详细状态
* -x: 显示没有控制终端的进程
* -r: 只显示正在运行的程序
* e: 列出程序时，显示出每个程序的环境变量
* -f : 全格式 

```bash
# ps -aux查看所有的进程
ps -aux | grep mysql 查看mysql的进程

#  | 在Linux中称为管道  A|B  将A的结果交给B再执行
#  grep查找文件中符合条件的字符串

ps -ef | grep xxx  可用查看xx进程及父进程的信息  #但是一般查看父进程可用通过目录树结构来查看

pstree -up | grep xx
-p :显示父id
-u ：显示用户组
```

2. 结束进程(杀死进程)

   kill -9 进程id  强制杀死进程

   kill 进程pid   进程推出之前可用释放并清理资源

# 十七.Redis

## 1.Redis的简单概述

1. >为什么要用NoSQL

   * 传统的数据库数据存储在硬盘中，拿取数据时要从硬盘读入，当数据量太大时，IO也会吃不消
   * 传统的关系型数据库的表结构有很强的约束，必须按照这么个格式存储，就不够灵活。所以当有新需求需要加字段的时候就需要修改表的结构，如果表的数据很多的话修改表的结构可能会长时间锁表，而导致表的不可用。
   * Nosql基于内存，无需IO，且存储结构灵活
   * 方便扩展（数据之间没有关系，很好扩展！）
   * 大数据量高性能（Redis一秒可以写8万次，读11万次，NoSQL的缓存记录级，是一种细粒度的缓存，性能会比较高！）
   * 数据类型是多样型的！（不需要事先设计数据库，随取随用）

2. >Redis的简单介绍

redis是Nosql数据库中使用较为广泛的非关系型内存数据库，redis内部是一个key-value存储系统。它支持存储的value类型相对更多，包括string(字符串)、list(链表)、set(集合)、zset(sorted set –有序集合)和hash（哈希类型，类似于java中的map）。Redis基于内存运行并支持持久化的NoSQL数据库，是当前最热门的NoSql数据库之一，也被人们称为数据结构服务器。

3. >CPA理论

   * C：强一致性 Consistency

   * A：可用性 Availablility

   * P：分区容错性 Partition tolerancy

     

   * CAP理论的核心是：一个分布式系统不可能同时很好的满足一致性，可用性和分区容错性这三个需求，
     最多只能同时较好的满足两个。
     因此，根据 CAP 原理将 NoSQL 数据库分成了满足 CA 原则、满足 CP 原则和满足 AP 原则三 大类

     * CA - 单点集群，满足一致性，可用性的系统，通常在可扩展性上不太强大。
     * CP - 满足一致性，分区容忍必的系统，通常性能不是特别高。
     * AP - 满足可用性，分区容忍性的系统，通常可能对一致性要求低一些

   * 分区容忍性是我们必须需要实现的。所以我们只能在一致性和可用性之间进行权衡，没有NoSQL系统能同时保证这三点。

     *  CA 传统Oracle数据库

     * AP 大多数网站架构的选择

     *  CP Redis、Mongodb

        注意：分布式架构的时候必须做出取舍。一致性和可用性之间取一个平衡。多余大多数web应用，其实并不需要强一致性。

   >BASE

   BASE就是为了解决关系数据库强一致性引起的问题而引起的可用性降低而提出的解决方案。

   BASE其实是下面三个术语的缩写：
       基本可用（Basically Available）
       软状态（Soft state）
       最终一致（Eventually consistent）

## 2.Redis的常用命令

Redis命令参考：http://redisdoc.com/persistence/index.html

### <1>对Redis库操作的命令

Redis默认有16个库，类似数组下标从0开始，初始默认使用0号库

* Select  数据库号     切换数据库
* dbsize 查看当前数据库key的个数
* flushdb 清空当前库
* flushall 清空所有库

### <2>对key的常用操作命令

* keys *    查看所有key
* exists  key的名字  判断是否存在这个key
* move key  db         将key移动到第db个数据库
* expire key 秒钟     为key设置过期时间
* ttl kye   查看还有多少秒过期   -1表示永不过期   -2表示已经过期

### <3>Redis的五大数据类型

* string是redis最基本的类型，你可以理解成与Memcached一模一样的类型，一个key对应一个value。

  string类型是二进制安全的。意思是redis的string可以包含任何数据。比如jpg图片或者序列化的对象 。

  string类型是Redis最基本的数据类型，一个redis中字符串value最多可以是512M

* Hash（哈希）
  Redis hash 是一个键值对集合。
  Redis hash是一个string类型的field和value的映射表，hash特别适合用于存储对象。

  类似Java里面的Map<String,Object>

* List（列表）
  Redis 列表是简单的字符串列表，按照插入顺序排序。你可以添加一个元素导列表的头部（左边）或者尾部（右边）。它的底层实际是个链表

* Set（集合）
  Redis的Set是string类型的无序集合。它是通过HashTable实现实现的，不允许重复

* zset(sorted set：有序集合)
  Redis zset 和 set 一样也是string类型元素的集合,且不允许重复的成员。
  不同的是每个元素都会关联一个double类型的分数。
  redis正是通过分数来为集合中的成员进行从小到大的排序。zset的成员是唯一的,但分数(score)却可以重复。

### <4>五大数据类型的常用操作命令

#### 3.1 String类型

* set/get/del/append/strlen   key    分别表示/设置/获取/删除/拼接/获取长度

* 如果值(value)是数字

  incr/decr  key   将key对应的value的值加/减一

  incrby/decrby  key  increment  将key对应的value的值加/减  increment

* getrange key start end 返回key对应的value中的子字符串  从start开始到end结束

* setrange key offset value  用value覆盖掉key所存储的字符串值，从偏移量offset开始

* setex key seconds value 设置key value，并且过期时间为seconds(秒 )

* setnx key value 当key不存在时设置

* mget key1  key2  ..... 同时获取多个key对应的value

* mset key value [key value ....] 同时设置多个key value

* getset key value 先设置key的值为value，在返回旧的value

#### 3.2List类型

![image-20210814152413346](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210814152413346.png)

17.rpushx key value 为已经存在的列表添加值

#### 3.3 set类型

![image-20210814152512958](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210814152512958.png)

#### 3.4Hash类型

![image-20210814152534876](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210814152534876.png)



#### 3.5Zset类型

![image-20210814152610101](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210814152610101.png)

![image-20210814152618982](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210814152618982.png)

## 3.Redis的内存分配

假如你的Redis内存满了怎么办？ 长期的把Redis作为缓存使用，总有一天会存满的时候对吧。

这个面试题不慌呀，在Redis中有配置参数maxmemory可以设置Redis内存的大小。

在Redis的配置文件redis.conf文件中，配置maxmemory的大小参数如下所示：

![img](https://img-blog.csdnimg.cn/20200519220333869.png)

实际生产中肯定不是100mb的大小哈，不要给误导了，这里我只是让大家认识这个参数，一般小的公司都是设置为3G左右的大小。

除了在配置文件中配置生效外，还可以通过命令行参数的形式，进行配置，具体的配置命令行如下所示：

```java
//获取maxmemory配置参数的大小
127.0.0.1:6379> config get maxmemory
//设置maxmemory参数为100mb
127.0.0.1:6379> config set maxmemory 100mb
```

## 4.Redis的淘汰策略

Redis提供了6种的淘汰策略，其中默认的是noeviction，这6中淘汰策略如下：

* noeviction(默认策略)：若是内存的大小达到阀值的时候，所有申请内存的指令都会报错。
* allkeys-lru：所有key都是使用LRU算法(最近最少使用)进行淘汰。
* volatile-lru：所有设置了过期时间的key使用LRU算法进行淘汰。
* allkeys-random：所有的key使用随机淘汰的方式进行淘汰。
* volatile-random：所有设置了过期时间的key使用随机淘汰的方式进行淘汰。
* volatile-ttl：所有设置了过期时间的key根据过期时间进行淘汰，越早过期就越快被淘汰。

可以在配置文件中设置，也可以使用命令

```java
// 获取maxmemory-policy配置
127.0.0.1:6379> config get maxmemory-policy
// 设置maxmemory-policy配置为allkeys-lru
127.0.0.1:6379> config set maxmemory-policy allkeys-lru
```



>设置过期键的淘汰策略

1. 定时删除：创建一个定时器，定时的执行对key的删除操作。
2. 惰性删除：每次只有再访问key的时候，才会检查key的过期时间，若是已经过期了就执行删除。
3. 定期删除：每隔一段时间，就会检查删除掉过期的key。



## 5.Redis的持久化方式

### <1>为什么需要持久化

Redis对数据的操作都是基于内存的，当遇到了进程退出、服务器宕机等意外情况，如果没有持久化机制，那么Redis中的数据将会丢失无法恢复。有了持久化机制，Redis在下次重启时可以利用之前持久化的文件进行数据恢复。Redis支持的两种持久化机制：

* RDB：把当前数据生成快照保存在硬盘上。
* AOF：记录每次对数据的操作到硬盘上。

### <2>RDB

RDB（Redis DataBase）持久化是把当前Redis中全部数据生成快照保存在硬盘上。RDB持久化可以手动触发，也可以自动触发。保存在dump.rdb文件中

1. >触发方式

save 和bgsave

* save:执行save命令会手动触发RDB持久化，但是save命令会阻塞Redis服务，直到RDB持久化完成。当Redis服务储存大量数据时，会造成较长时间的阻塞
* bgsave:Redis服务一般不会阻塞。Redis进程会执行fork操作创建子进程，RDB持久化由子进程负责，不会阻塞Redis服务进程(主进程不进行IO)。Redis服务的阻塞只发生在fork阶段，一般情况时间很短。

在配置文件中设置了save的相关配置，如`sava m n`，它表示在m秒内数据被修改过n次时，自动触发`bgsave`操作

2. >优点

适合大数据量的恢复，且恢复数据的速度较快

3. >缺点

会在一定的时间间隔内进行快照，所以当Redis意外宕机时，可能会丢失最后一次快照之后的部分数据

### <3>AOF

AOF（Append Only File）持久化是把每次写命令追加写入日志中，当需要恢复数据时重新执行AOF文件中的命令就可以了。AOF解决了数据持久化的实时性，也是目前主流的Redis持久化方式。

AOF保存的位置是appendonly.aof文件中

>Rewrite

* AOF文件会随着时间持续增长，过大时，会fork出一条新的进程来将文件重写(先写临时文件，最后再rename)
* 新进程会将内存中的数据库内容用最简命令的方式重写一个AOF文件。然后更新原来的AOF

>优点

随数据的变化实时更新，更准确的恢复数据库

>缺点

效率较RDB慢，且aof文件要远大于rdb文件

## 6.Redis的主从模式

1. >一主多从

一主多从，主数据库（master）可以读也可以写（read/write），从数据库仅读（only read）。

但是，主从模式一般实现读写分离**，**主数据库仅写（only write），减轻主数据库的压力

![img](https://img-blog.csdnimg.cn/20200819225047906.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQzMjU1MDE3,size_16,color_FFFFFF,t_70#pic_center)

步骤：

* 当slave启动后会向master发送SYNC命令，master节后到从数据库的命令后通过bgsave保存快照（RDB持久化），并且期间的执行的些命令会被缓存起来。
* 然后master会将保存的快照发送给slave，并且继续缓存期间的写命令。
* slave收到主数据库发送过来的快照就会加载到自己的数据库中。
* 最后master讲缓存的命令同步给slave，slave收到命令后执行一遍，这样master与slave数据就保持一致了。
  



命令：在从库下执行   slaveof  主库IP  主库端口  

​            info replication  查看本机的主从设置，查看自己是主机还是从机

每次与master断开连接后，都需要重新连接，除非配置进redis.conf文件

**注意：**

* 当从机接入主机之后，会将主机之前的数据也全部录入
* 当主机宕机之后，从机仍然为从机(静默)，当注解接入之后，仍是主机的从机
* 当从机宕机，重新接入之后，不再是原主机的从机，而自身称为主机



2. >薪火相传

上一个从机可以是下一个从机的主机(类似树结构),从机同样也可以接收其他从机的连接和同步请求，该从机可以作为下一个链条的主机，这样可以有效的减轻最上层主机的写压力

但是中途变更转向，会清除之前的数据，重新复制新的主机的数据

3. >反客为主    哨兵模式

从机变主机： slaveof no one

哨兵能够后台监控主机是否故障，如果故障了根据投票数自动将从库转变为主库

**步骤：**

* 新建sentinel.conf(名字不能错)文件
* 配置哨兵：sentinel moniter +被监控数据库名字(如：127.0.0.1 6379 1  后面的1表示主机宕机后从机投票，得票数最高的成为主机)
* 启动哨兵  Redis-snetinel  /sentinel.conf

注意：当之前监控的主机宕机之后，有新的从机称为主机，但当原来的主机重新工作之后，会称为新主机的从机

## 7.缓存击穿,穿透和雪崩

### <1>缓存穿透

>概念

在默认情况下，用户请求数据时，会先在缓存(Redis)中查找，若没找到即缓存未命中，再在数据库中进行查找，数量少可能问题不大，可是一旦大量的请求数据（例如秒杀场景）缓存都没有命中的话，就会全部转移到数据库上，造成数据库极大的压力，就有可能导致数据库崩溃。网络安全中也有人恶意使用这种手段进行攻击被称为洪水攻击。

>解决方案

1. >布隆过滤器

   利用布隆过滤器，以便快速确定是否存在这个值，在控制层先进行拦截校验，校验不通过直接打回，减轻了存储系统的压力。

   ![在这里插入图片描述](https://img-blog.csdnimg.cn/20200513215824722.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mzg3MzIyNw==,size_16,color_FFFFFF,t_70)

   布隆过滤器是由布隆（Burton Howard Bloom）在1970年提出的 一种紧凑型的、比较巧妙的概率型数据结

   构，特点是高效地插入和查询，可以用来告诉你 “某样东西一定不存在或者可能存在”，它是用多个哈希函

   数，将一个数据映射到位图结构中。此种方式不仅可以提升查询效率，也可以节省大量的内存空间。

   **限制：**

   1.只适合元素是否存在的问题

   2.只能保证概率的正确性

   3.对元素没有要求了

   ​	

   **布隆过滤器的内部结构:**

   ![image-20210715133048552](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210715133048552.png)

   ![image-20210715132746239](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210715132746239.png)

   把一个元素放入到该集合中：

   >1.把元素，分别经过n个哈希函数进行计算，并转化为位图的合法的bit "下标"
   >
   >2.把所有"下标"处的位图位置改为1

   判断给定的元素是否在该集合中

   >1.把元素，分别经过n个哈希函数进行计算，并转化为位图的合法位置
   >
   >2.如果所有位置中，有0，则标识元素一定不在集合中
   >
   >3.如果所有位置，都是1，则大概率，元素在集合中
   >
   >4.但也有可能，这个元素不在集合中，但是标志位因为其他元素都改变为了1(这种情况称为假阳性)

   删除元素

   >不能再布隆过滤器中删除元素，删除元素改变了位图中下标的状态，可能对其他数据产生影响
   >如果真要删除，可以使用计数方式删除

   

   **设计多个hash函数的原因：**

   >1.由于元素不限，所有，必然有着hash冲突的情况。(不同的元素，映射到了相同的下标出)
   >
   >2.在这个情况下，hash函数个数太少，假阳性概率就会特别高，所有通过添加多个hash函数，使得假阳性概率降低

2. >缓存空对象

一次请求若在缓存和数据库中都没找到，就在缓存中方一个空对象用于处理后续这个请求。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200513215836317.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mzg3MzIyNw==,size_16,color_FFFFFF,t_70)

这样做有一个缺陷：存储空对象也需要空间，大量的空对象会耗费一定的空间，存储效率并不高

### <2>缓存击穿

>概念

 相较于缓存穿透，缓存击穿的目的性更强，一个存在的key，在缓存过期的一刻，同时有大量的请求，这些请求都会击穿到DB，造成瞬时DB请求量大、压力骤增。这就是缓存被击穿，只是针对其中某个key的缓存不可用而导致击穿，但是其他的key依然可以使用缓存响应。

 比如热搜排行上，一个热点新闻被同时大量访问就可能导致缓存击穿。

>解决方案

1. 设置热点数据永不过期

这样就不会出现热点数据过期的情况，但是当Redis内存空间满的时候也会清理部分数据，而且此种方案会占用空间，一旦热点数据多了起来，就会占用部分空间。

2. 加互斥锁(分布式锁)

在访问key之前，采用SETNX（set if not exists）来设置另一个短期key来锁住当前key的访问，访问结束再删除该短期key。保证同时刻只有一个线程访问。这样对锁的要求就十分高。

### <2>缓存雪崩

>概念

大量的key设置了相同的过期时间，导致在缓存在同一时刻全部失效，造成瞬时DB请求量大、压力骤增，引起雪崩。

1. redis高可用

这个思想的含义是，既然redis有可能挂掉，那我多增设几台redis，这样一台挂掉之后其他的还可以继续工作，其实就是搭建的集群

2. 限流降级

这个解决方案的思想是，在缓存失效后，通过加锁或者队列来控制读数据库写缓存的线程数量。比如对某个key只允许一个线程查询数据和写缓存，其他线程等待。

3. 数据预热

数据预热的含义就是在正式部署之前，我先把可能的数据先预先访问一遍，这样部分可能大量访问的数据就会加载到缓存中。在即将发生大并发访问前手动触发加载缓存不同的key，设置不同的过期时间，让缓存失效的时间点尽量均匀。

## 8.事务

>1. Redis事务没有隔离级别的概念
>2. Redis单条命令是保证原子性的，但是事务不保证原子性！

### <1>事务的操作过程

- 开启事务（`multi`）
- 命令入队
- 执行事务（`exec`）
- 事务的取消(`DISCARD`)

### <2>事务的出错

1. >代码语法错误（编译时异常）所有的命令都不执行

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> error k1 # 这是一条语法错误命令
(error) ERR unknown command `error`, with args beginning with: `k1`, # 会报错但是不影响后续命令入队 
127.0.0.1:6379> get k2
QUEUED
127.0.0.1:6379> EXEC
(error) EXECABORT Transaction discarded because of previous errors. # 执行报错
127.0.0.1:6379> get k1 
(nil) # 其他命令并没有被执行

```

2. >代码逻辑错误 (运行时异常) 其他命令可以正常执行  >>> 所以不保证事务原子性

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379>INCR k1 # 这条命令逻辑错误（对字符串进行增量）
QUEUED
127.0.0.1:6379> get k2
QUEUED
127.0.0.1:6379> exec
1) OK
2) OK
3) (error) ERR value is not an integer or out of range # 运行时报错
4) "v2" # 其他命令正常执行

# 虽然中间有一条命令报错了，但是后面的指令依旧正常执行成功了。
# 所以说Redis单条指令保证原子性，但是Redis事务不能保证原子性。
```

### <3>事务的监控

**悲观锁：**

- 很悲观，认为什么时候都会出现问题，无论做什么都会加锁

**乐观锁：**

- 很乐观，认为什么时候都不会出现问题，所以不会上锁！更新数据的时候去判断一下，在此期间是否有人修改过这个数据
- 获取version
- 更新的时候比较version

>使用`watch key`监控指定数据，相当于乐观锁加锁。

watch 监控某个key，当开启事务，在事务期间，有其他线程过来将监控的key修改，则事务执行时失败回滚

`unwatch`进行解锁。

每次提交执行exec后都会自动释放锁，不管是否成功



# 十八.RabbitMQ

## 1.对MQ的介绍

1. >说明是MQ

MQ(message queue)，从字面意思上看，本质是个队列，FIFO 先入先出，只不过队列中存放的内容是

message 而已，还是一种跨进程的通信机制，用于上下游传递消息。在互联网架构中，MQ 是一种非常常

见的上下游“逻辑解耦+物理解耦”的消息通信服务。使用了 MQ 之后，消息发送上游只需要依赖 MQ，不

用依赖其他服务。

2. >MQ的好处

   * 流量消峰

     举个例子，如果订单系统最多能处理一万次订单，这个处理能力应付正常时段的下单时绰绰有余，正常时段我们下单一秒后就能返回结果。但是在高峰期，如果有两万次下单操作系统是处理不了的，只能限制订单超过一万后不允许用户下单。使用消息队列做缓冲，我们可以取消这个限制，把一秒内下的订单分散成一段时间来处理，这时有些用户可能在下单十几秒后才能收到下单成功的操作，但是比不能下单的体验要好。

   * 应用解耦

     以电商应用为例，应用中有订单系统、库存系统、物流系统、支付系统。用户创建订单后，如果耦合调用库存系统、物流系统、支付系统，任何一个子系统出了故障，都会造成下单操作异常。当转变成基于消息队列的方式后，系统间调用的问题会减少很多，比如物流系统因为发生故障，需要几分钟来修复。在这几分钟的时间里，物流系统要处理的内存被缓存在消息队列中，用户的下单操作可以正常完成。当物流系统恢复后，继续处理订单信息即可，用户感受不到物流系统的故障，提升系统的可用性。

     ![image-20210815120928515](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815120928515.png)

   * 异步处理

     有些服务间调用是异步的，例如 A 调用 B，B 需要花费很长时间执行，但是 A 需要知道 B 什么时候可以执行完，以前一般有两种方式，A 过一段时间去调用 B 的查询 api 查询。或者 A 提供一个 callback api， B 执行完之后调用 api 通知 A 服务。这两种方式都不是很优雅，使用消息总线，可以很方便解决这个问题，A 调用 B 服务后，只需要监听 B 处理完成的消息，当 B 处理完成后，会发送一条消息给 MQ，MQ 会将此消息转发给 A 服务。这样 A 服务既不用循环调用 B 的查询 api，也不用提供 callback api。同样 B 服务也不用做这些操作。A 服务还能及时的得到异步处理成功的消息。

     ![image-20210815121038878](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815121038878.png)



## 2.RabbitMQ的六种模式 及工作原理

>工作模式

依次是：hello world   ，工作模式，发布订阅模式，路由模式，主题模式，发布确认模式

![image-20210815121925952](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815121925952.png)

>工作原理

![image-20210815122311184](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815122311184.png)

**Binding**：exchange 和 queue 之间的虚拟连接，binding 中可以包含 routing key，Binding 信息被保

存到 exchange 中的查询表中，用于 message 的分发依据

>依赖

```xml
<!--rabbitmq 依赖客户端-->
<dependency>
  <groupId>com.rabbitmq</groupId>
  <artifactId>amqp-client</artifactId>
  <version>5.8.0</version>
</dependency>
```



## 3.hello world队列

![image-20210815131022113](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815131022113.png)

1. >生产者

```java
public class Producer {
    //建立队列
    private static final String QUEUE_NAME="hello";
    public static void main(String[] args)  {
        //创建连接工场
        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost("127.0.0.1");
        factory.setUsername("guest");
        factory.setUsername("guest");

        try {
            //建立连接和信道
            //channel 实现了自动 close 接口 自动关闭 不需要显示关闭
            Connection connection=factory.newConnection();
            Channel channel=connection.createChannel();

            /**
             * 生成一个队列,并将信道和队列连接
             * 1.队列名称
             * 2.队列里面的消息是否持久化 默认消息存储在内存中
             * 3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
             * 4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
             * 5.其他参数
             */
            channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            String message="hello world";
            /**
             * 发送一个消息
             * 1.发送到那个交换机
             * 2.路由的 key 是哪个
             * 3.其他的参数信息
             * 4.发送消息的消息体
             */
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
            System.out.println("消息发送成功");
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

```

2. >消费者

```java
public class Consumer {
    //定义队列名
    private static final String QUEUE_NAME="hello";

    public static void main(String[] args) {
        //建立连接和信道
        try {
            ConnectionFactory factory=new ConnectionFactory();
            factory.setHost("127.0.0.1");
            factory.setUsername("guest");
            factory.setPassword("guest");
            Connection connection=factory.newConnection();
            Channel channel=connection.createChannel();
            System.out.println("等待接收消息");


            /**
             *1.同一个会话， consumerTag 是固定的 可以做此会话的名字， deliveryTag 每次接收消息+1，可以做此消息处理通道的名字。
             *2.包含消息的字节形式的类
             */
            DeliverCallback deliverCallback=(consumerTag,delivery)->{
                String message=new String(delivery.getBody());
                System.out.println(message);
            };
            CancelCallback cancelCallback=(consumerTag)->{
                System.out.println("消息消费被取消");
            };
            /* 消费者消费消息
             * 1.消费哪个队列
             * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
             * 3.消费者未成功消费的回调
             * 4.消费者取消消费的回调
             */
            channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);



        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
```

## 4.工作队列模式

![image-20210815131031760](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815131031760.png)

>生产者

```java
public class Producer {
    public static void main(String[] args) throws IOException, InterruptedException {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("失败");
            return;
        }
        channel.queueDeclare(RabbitMQChannelUtil.QUEUE_NAME,false,false,false,null);
        int i=0;
        while (true){
            String message="消息"+i;
            i++;
            /**
             * 发送一个消息
             * 1.发送到那个交换机
             * 2.路由的 key 是哪个
             * 3.其他的参数信息
             * 4.发送消息的消息体
             */
            channel.basicPublish("",RabbitMQChannelUtil.QUEUE_NAME,null,message.getBytes());
            System.out.println(message);
            Thread.sleep(500);
        }
    }
}
```

>消费者

```java
public class Consumer {
    public static void main(String[] args) {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("消费失败");
            return;
        }
        DeliverCallback deliverCallback=(consumerTag, delivery)->{
            String message=new String(delivery.getBody());
            System.out.println(Thread.currentThread().getName()+"消费了"+message);
        };
        CancelCallback cancelCallback=(consumerTag)->{
            System.out.println("消息消费被取消");
        };
        Thread[] threads=new Thread[5];
        for (int i = 0; i <threads.length ; i++) {
            threads[i]=new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName()+"启动等待消费");
                    channel.basicConsume(RabbitMQChannelUtil.QUEUE_NAME,true,deliverCallback,cancelCallback);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        for (int i = 0; i <threads.length ; i++) {
            threads[i].start();
        }
    }
}
```

## 5.消息应答机制

>认识

消费者处理消息时，可能在处理过程中挂掉，那么消息就会丢失为了保证消息在发送过程中不丢失，rabbitmq 引入消息应答机制，消息应答就是:消费者在接收到消息并且处理该消息之后，告诉 rabbitmq 它已经处理了rabbitmq 可以把该消息删除了。

#### 自动应答

消息发送后立即被认为已经传送成功，这种模式需要在高吞吐量和数据传输安全性方面做权衡,因为这种模式如果消息在接收到之前，消费者那边出现连接或者 channel 关闭，那么消息就丢失了	

#### 手动应答

* Channel.basicAck(用于肯定确认)

RabbitMQ 已知道该消息并且成功的处理消息，可以将其丢弃了

* Channel.basicNack(用于否定确认) 
* Channel.basicReject(用于否定确认) 

与 Channel.basicNack 相比少一个参数,不处理该消息了直接拒绝，可以将其丢弃了

>Channel.basicNack参数中Multiple(批量应答) 的解释

multiple 的 true 和 false 代表不同意思

* true 代表批量应答 channel 上未应答的消息

比如说 channel 上有传送 tag 的消息 5,6,7,8 当前 tag 是 8 那么此时

5-8 的这些还未应答的消息都会被确认收到消息应答

* false 

只会应答 tag=8 的消息 5,6,7 这三个消息依然不会被确认收到消息应答

![image-20210815141019567](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815141019567.png)

>消息手动应答的代码

* 将手动应答开启

  ```java
  /* 消费者消费消息
   * 1.消费哪个队列
   * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
   * 3.当一个消息发送过来后的回调接口
   * 4.消费者取消消费的回调
   */
  boolean ack=false;
  channel.basicConsume(QUEUE_NAME,ack,deliverCallback,cancelCallback);
  ```

* 消息消费回调时，使用手动应答

  ```java
  /**
   * 消息发送过来后的回调接口
   *1.同一个会话， consumerTag 是固定的 可以做此会话的名字， deliveryTag 每次接收消息+1，可以做此消息处理通道的名字。
   *2.消息类
   */
  DeliverCallback deliverCallback=(consumerTag,delivery)->{
      String message=new String(delivery.getBody());
      System.out.println(message);
      /**
       * 参数说明
       * 1.消息的标记tag
       * 2.是否批量应答
       */
      channel.basicAck(delivery.getEnvelope().getDeliveryTag(),false);
  };
  ```



#### 消息自动进行重新入队

如果消费者由于某些原因失去连接(其通道已关闭，连接已关闭或 TCP 连接丢失)，导致消息未发送 ACK 确认，RabbitMQ 将了解到消息未完全处理，并将对其重新排队。如果此时其他消费者可以处理，它将很快将其重新分发给另一个消费者。这样，即使某个消费者偶尔死亡，也可以确保不会丢失任何消息。

![image-20210815141252860](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815141252860.png)

## 6.RabbitMQ的持久化,不公平分发及预取值

>概念

刚刚我们已经看到了如何处理任务不丢失的情况，但是如何保障当 RabbitMQ 服务停掉以后消

息生产者发送过来的消息不丢失。默认情况下 RabbitMQ 退出或由于某种原因崩溃时，它忽视队列

和消息，除非告知它不要这样做。确保消息不会丢失需要做两件事：我们需要将队列和消息都标

记为持久化。

>队列持久化

* 之前我们创建的队列都是非持久化的，rabbitmq 如果重启的化，该队列就会被删除掉，如果

要队列实现持久化 需要在声明队列的时候把 durable(第二个) 参数设置为持久化

* 但是需要注意的就是如果之前声明的队列不是持久化的，需要把原先队列先删除，或者重新

创建一个持久化的队列，不然就会出现错误

* ```java
  channel.queueDeclare(RabbitMQChannelUtil.QUEUE_NAME,true,false,false,null);
  ```

![image-20210815145617404](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815145617404.png)

这个就是持久化队列

>消息持久化

* 要想让消息实现持久化需要在消息生产者修改代码，MessageProperties.PERSISTENT_TEXT_PLAIN 添

加这个属性。

队列持久化为false时:

```java
channel.basicPublish("",RabbitMQChannelUtil.QUEUE_NAME,null,message.getBytes());
```

队列持久化为true时

```java
channel.basicPublish("",RabbitMQChannelUtil.QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN,message.getBytes());
```

* 将消息标记为持久化并不能完全保证不会丢失消息。尽管它告诉 RabbitMQ 将消息保存到磁盘，但是

  这里依然存在当消息刚准备存储在磁盘的时候 但是还没有存储完，消息还在缓存的一个间隔点。此时并没

  有真正写入磁盘。持久性保证并不强.更强的持久化后面`发布确认`会讲到

>不公平分发

在最开始的时候我们学习到 RabbitMQ 分发消息采用的轮训分发，但是在某种场景下这种策略并不是

很好，比方说有两个消费者在处理任务，其中有个消费者 1 处理任务的速度非常快，而另外一个消费者 2 

处理速度却很慢，这个时候我们还是采用轮训分发的化就会到这处理速度快的这个消费者很大一部分时间

处于空闲状态，而处理慢的那个消费者一直在干活，这种分配方式在这种情况下其实就不太好，但是

RabbitMQ 并不知道这种情况它依然很公平的进行分发。

为了避免这种情况，我们设置不公平分发：

```java
channel.basicQos(1);
```

![image-20210815150341220](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815150341220.png)

>预取值

本身消息的发送就是异步发送的，所以在任何时候，channel 上肯定不止只有一个消息另外来自消费

者的手动确认本质上也是异步的。因此这里就存在一个未确认的消息缓冲区，因此希望开发人员能限制此

缓冲区的大小，以避免缓冲区里面无限制的未确认消息问题。这个时候就可以通过使用 basic.qos 方法设

置“预取计数”值来完成的。该值定义通道上允许的未确认消息的最大数量。一旦数量达到配置的数量，

RabbitMQ 将停止在通道上传递更多消息，除非至少有一个未处理的消息被确认

prefetch就是预取值数

![image-20210815151149353](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815151149353.png)

## 7.发布确认

上文持久化中提到，当消息持久化存入RabbitMQ磁盘时，RabbitMQ突然宕机，则消息未成功存入，会发生消息丢失。所以发布确认即：在消息成功存入磁盘时，返还给生产者一个消息，确认已经存入磁盘

>具体介绍

生产者将信道设置成 confirm 模式，一旦信道进入 confirm 模式，所有在该信道上面发布的

消息都将会被指派一个唯一的 ID(从 1 开始)，一旦消息被投递到所有匹配的队列之后，broker

就会发送一个确认给生产者(包含消息的唯一 ID)，这就使得生产者知道消息已经正确到达目的队

列了，如果消息和队列是可持久化的，那么确认消息会在将消息`写入磁盘之后发出`，broker 回传

给生产者的确认消息中 delivery-tag 域包含了确认消息的序列号，此外 broker 也可以设置

basic.ack 的 multiple 域，表示到这个序列号之前的所有消息都已经得到了处理。



为了保证消息不丢失：

* 开启队列持久化
* 开启消息持久化
* 开启信道的发布确认

>开启发布确认的方法

```java
channel.confirmSelect();
```

>发布确认的模式

1. 单个确认发布

```java
public static void singleConfirm(){
    try {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("信道建立失败");
            return;
        }
        //开启发布确认
        channel.confirmSelect();
        long begin=System.currentTimeMillis();
        for (int i = 0; i <MESSAGE_COUNT ; i++) {
            String message=i+"";
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
            //可以加时间参数，当消息发送失败或超过参数时间没成功，则返回false
            boolean flag=channel.waitForConfirms();
            //如果失败可以重发
            if(flag){
                System.out.println(message+"发送成功");
            }else {
                //这里可以实现重发
                System.out.println(message+"发送失败");
            }
        }
        long end=System.currentTimeMillis();
        System.out.println("发送"+MESSAGE_COUNT+"条消息，耗时"+(end-begin)+"ms");
    }catch (Exception e){
        e.printStackTrace();
    }
}
```

发布一个消息之后只有它被确认发布，后续的消息才能继续发布,waitForConfirmsOrDie(long)这个方法只有在消息被确认的时候才返回，如果在指定时间范围内这个消息没有被确认那么它将抛出异常。

缺点：速度太慢

2. 批量发布确认模式

```java
public static void batchConfirm(){
    try {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("建立连接失败");
            return;
        }
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //当100条消息发布成功时，再确认
        int ackMessageCount=100;
        //未确认的消息个数
        int needAckMessageCount=0;
        //开启发布确认
        channel.confirmSelect();
        long begin=System.currentTimeMillis();
        for (int i = 0; i <MESSAGE_COUNT ; i++) {
            String message=i+"";
            channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
            needAckMessageCount++;
            if(needAckMessageCount == ackMessageCount){
                //确认
                channel.waitForConfirms();
                needAckMessageCount=0;
            }
        }
        //判断可能还有消息未发送，再发送依次
        if(needAckMessageCount > 0){
            channel.waitForConfirms();
        }
        long end= System.currentTimeMillis();
        System.out.println("发送"+MESSAGE_COUNT+"条消息，耗时"+(end-begin)+"ms");
    }catch (Exception e){
        e.printStackTrace();
    }
}
```

缺点:当发生故障导致发布出现问题时，不知道是哪个消息出现问题



3. 异步确认发布

>原理

![image-20210815161553956](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815161553956.png)

有单独一个队列保存确认信号



```java
public static void asyncConfirm() throws Exception {
    try (Channel channel = RabbitMQChannelUtil.getChannel()) {
        if(channel == null){
            return;
        }
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        //开启发布确认
        channel.confirmSelect();
        /**
         * 线程安全有序的一个哈希表，适用于高并发的情况
         * 1.轻松的将序号与消息进行关联
         * 2.轻松批量删除条目 只要给到序列号
         * 3.支持并发访问
         */
        ConcurrentSkipListMap<Long, String> outstandingConfirms = new
                ConcurrentSkipListMap<>();
        /**
         * 确认收到消息的一个回调
         * 1.消息序列号
         * 2.true 可以确认小于等于当前序列号的消息
         * false 确认当前序列号消息
         */
        ConfirmCallback ackCallback = (sequenceNumber, multiple) -> {
            if (multiple) {
                //返回的是小于等于当前序列号的未确认消息 是一个 map
                ConcurrentNavigableMap<Long, String> confirmed =
                        outstandingConfirms.headMap(sequenceNumber, true);
                //清除该部分未确认消息
                confirmed.clear();
            }else{
                //只清除当前序列号的消息
                outstandingConfirms.remove(sequenceNumber);
            }
        };
        ConfirmCallback nackCallback = (sequenceNumber, multiple) -> {
            String message = outstandingConfirms.get(sequenceNumber);
            System.out.println("发布的消息"+message+"未被确认，序列号"+sequenceNumber);
        };
        /**
         * 添加一个异步确认的监听器
         * 1.确认收到消息的回调
         * 2.未收到消息的回调
         */
        channel.addConfirmListener(ackCallback, null);
        long begin = System.currentTimeMillis();
        for (int i = 0; i < MESSAGE_COUNT; i++) {
            String message = "消息" + i;
            /**
             * channel.getNextPublishSeqNo()获取下一个消息的序列号
             * 通过序列号与消息体进行一个关联
             * 全部都是未确认的消息体
             */
            outstandingConfirms.put(channel.getNextPublishSeqNo(), message);
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        }
        long end = System.currentTimeMillis();
        System.out.println("发布" + MESSAGE_COUNT + "个异步确认消息,耗时" + (end - begin) +
                "ms");
    } 
}
```

## 8.交换机

### <1>交换机的认识

#### 1.1 概念

RabbitMQ 消息传递模型的核心思想是: 生产者生产的消息从不会直接发送到队列。实际上，通常生产

者甚至都不知道这些消息传递传递到了哪些队列中。

相反，生产者只能将消息发送到交换机(exchange)，交换机工作的内容非常简单，一方面它接收来

自生产者的消息，另一方面将它们推入队列。交换机必须确切知道如何处理收到的消息。是应该把这些消

息放到特定队列还是说把他们到许多队列中还是说应该丢弃它们。这就的由交换机的类型来决定。

![image-20210815161909935](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815161909935.png)



#### 1.2Exchanges 的类型

总共有以下类型：

直接(direct), 主题(topic) ,标题(headers) , 扇出(fanout)

#### 1.3无名Exchange

```java
channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
```

第一个参数是交换机的名称。空字符串表示默认或无名称交换机：消息能路由发送到队列中其实

是由 routingKey(bindingkey)绑定 key 指定的，如果它存在的话

#### 1.4临时队列

每当我们连接到 Rabbit 时，我们都需要一个全新的空队列，为此我们可以创建一个具有随机名称的队列，或者能让服务器为我们选择一个随机队列名称那就更好了。其次一旦我们断开了消费者的连接，队列将被自动删除。

```java
String queueName = channel.queueDeclare().getQueue();
```

#### 1.5队列和交换机之间的绑定

```java
//声明交换机名称及类型
channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
//把该临时队列绑定我们的 exchange 其中 routingkey(也称之为 binding key)为空字符串
 channel.queueBind(queueName, EXCHANGE_NAME, "");
```



### <2>交换机具体介绍

>Fanout 删除(广播)

将接收到的所有消息**广播**到它知道的所有队列中。

>Direct (直接)

将详细发送到对应路由键的队列上去	

![image-20210815164132041](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815164132041.png)

在上面这张图中，我们可以看到 X 绑定了两个队列，绑定类型是 direct。队列 Q1 绑定键为 orange，

队列 Q2 绑定键有两个:一个绑定键为 black，另一个绑定键为 green.

在这种绑定情况下，生产者发布消息到 exchange 上，绑定键为 orange 的消息会被发布到队列

Q1。绑定键为 blackgreen 和的消息会被发布到队列 Q2，其他消息类型的消息将被丢弃。

**绑定**

```java
//声明交换机名称及类型
channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
//把该临时队列绑定我们的 exchange 其中 routingkey(也称之为 binding key)为空字符串
 channel.queueBind(queueName, EXCHANGE_NAME, "");
```



>Topics(主题)

* 尽管使用 direct 交换机改进了我们的系统，但是它仍然存在局限性-比方说我们想接收的日志类型有

info.base 和 info.advantage，某个队列只想 info.base 的消息，那这个时候 direct 就办不到了。这个时候

就只能使用 topic 类型

* 发送到类型是 topic 交换机的消息的 routing_key 不能随意写，必须满足一定的要求，它**必须是一个单

  词列表，以点号分隔开。这些单词可以是任意单词。但这个单词列表最多不能超过 255 个字节。

  *  可以代替一个单词
  * #可以替代零个或多个单词

  ![image-20210815164652607](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815164652607.png)

## 9.死信队列

### <1>认识死信队列

>概念

* 死信，顾名思义就是无法被消费的消息，字面意思可以这样理解，一般来说，producer 将消息投递到 broker 或者直接到 queue 里了，consumer 从 queue 取出消息进行消费，但某些时候由于特定的原因导致 queue中的某些消息无法被消费，这样的消息如果没有后续的处理，就变成了死信，有死信自然就有了死信队列。
* 应用场景:为了保证订单业务的消息数据不丢失，需要使用到 RabbitMQ 的死信队列机制，当消息消费发生异常时，将消息投入死信队列中.还有比如说: 用户在商城下单成功并点击去支付后在指定时间未支付时自动失效

>来源

* 消息超出最大存活时间过期
* 队列达到最大长度(队列满了，无法再添加数据到 mq 中)
* 消息被拒绝(basic.reject 或 basic.nack)并且 requeue=false.

### <2>死信实战

#### 2.1架构图

![image-20210815170053664](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210815170053664.png)



#### 2.2TTL模拟死信队列

>生产者

```java
public class Producer {
    private static final String NORMAL_EXCHANGE="normal_exchange";
    public static void main(String[] args) {
        try {
            Channel channel= RabbitMQChannelUtil.getChannel();
            if(channel == null){
                return;
            }
            //声明交换机类型
            channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
            //设置消息TTL时间
            AMQP.BasicProperties basicProperties=new AMQP.BasicProperties().builder().expiration("1000").build();
            //用作演示消息队列的限制个数
            for (int i = 0; i <10 ; i++) {
                String message="info"+i;
                channel.basicPublish(NORMAL_EXCHANGE,"zhangsan",basicProperties,message.getBytes());
                System.out.println("生产者发送消息");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
```

>普通消费者:启动之后关闭，模拟接收不到消息

```java
public class NormalConsumer {
    //普通交换机名称
    private static final String NORMAL_EXCHANGE = "normal_exchange";
    //死信交换机名称
    private static final String DEAD_EXCHANGE = "dead_exchange";
    public static void main(String[] argv) throws Exception {
        Channel channel = RabbitMQChannelUtil.getChannel();
        if(channel == null){
            return;
        }
        //声明死信和普通交换机 类型为 direct
        channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);
        //声明死信队列
        String deadQueue = "dead-queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        //死信队列绑定死信交换机与 routingkey
        channel.queueBind(deadQueue, DEAD_EXCHANGE, "lisi");
        //正常队列绑定死信队列信息
        Map<String, Object> params = new HashMap<>();
        //正常队列设置死信交换机 参数 key 是固定值
        params.put("x-dead-letter-exchange", DEAD_EXCHANGE);
        //正常队列设置死信 routing-key 参数 key 是固定值
        params.put("x-dead-letter-routing-key", "lisi");

        String normalQueue = "normal-queue";
        channel.queueDeclare(normalQueue, false, false, false, params);
        channel.queueBind(normalQueue, NORMAL_EXCHANGE, "zhangsan");
        System.out.println("等待接收消息.....");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("NormalConsumer 接收到消息"+message);
        };
        channel.basicConsume(normalQueue, true, deliverCallback, consumerTag -> {
        });
    }
}
```

>死信队列消费者

```java
public class DeadConsumer {
 private static final String DEAD_EXCHANGE = "dead_exchange";
 public static void main(String[] argv) throws Exception {
        Channel channel = RabbitMQChannelUtil.getChannel();
        if (channel == null) {
         return;
        }
        channel.exchangeDeclare(DEAD_EXCHANGE, BuiltinExchangeType.DIRECT);
        String deadQueue = "dead-queue";
        channel.queueDeclare(deadQueue, false, false, false, null);
        channel.queueBind(deadQueue, DEAD_EXCHANGE, "lisi");
        System.out.println("等待接收死信队列消息.....");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
         String message = new String(delivery.getBody(), "UTF-8");
         System.out.println("DeadConsumer 接收死信队列的消息" + message);
        };
        channel.basicConsume(deadQueue, true, deliverCallback, consumerTag -> {
        });
 }
```

另外两种思路相同.



# 十九.在线办公系统(Yeb)

## 1.开发环境的搭建及项目介绍

本项目目的是实现中小型企业的在线办公系统，云E办在线办公系统是一个用来管理日常的办公事务的一个系统

使用SpringSecurity做安全认证及权限管理，Redis做缓存，RabbitMq做邮件的发送，使用EasyPOI实现对员工数据的导入和导出

使用验证码登录

![image-20210813234742740](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813234742740.png)

页面展示：

![image-20210813234811074](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813234811074.png)

1. >添加依赖

2. >使用MyBatis的AutoGenerator自动生成mapper，service，Controller



## 2.登录模块及配置框架搭建

### <1>Jwt工具类及对Token的处理

#### 1.1根据用户信息生成Token

1. >定义JWT负载中用户名的Key以及创建时间的Key

```java
//用户名的key
private static final String CLAIM_KEY_USERNAME="sub";
//签名的时间
private static final String CLAIM_KEY_CREATED="created";
```

2. >从配置文件中拿到Jwt的密钥和失效时间

```java
/**
 * @Value的值有两类：
 * ① ${ property : default_value }
 * ② #{ obj.property? :default_value }
 * 第一个注入的是外部配置文件对应的property，第二个则是SpEL表达式对应的内容。 那个
 * default_value，就是前面的值为空时的默认值。注意二者的不同，#{}里面那个obj代表对象。
 */
//JWT密钥
@Value("${jwt.secret}")
private  String secret;

//JWT失效时间
@Value("${jwt.expiration}")
private Long expiration;
```

3. >根据用户信息UserDetials生成Token

```java
/**
 * 根据用户信息生成Token
 * @param userDetails
 * @return
 */
public String generateToken(UserDetails userDetails){
    //荷载
    Map<String,Object> claim=new HashMap<>();
    claim.put(CLAIM_KEY_USERNAME,userDetails.getUsername());
    claim.put(CLAIM_KEY_CREATED,new Date());
    return generateToken(claim);
}

/**
 * 根据负载生成JWT Token
 * @param claims
 * @return
 */
private String generateToken(Map<String,Object> claims) {
    return Jwts.builder()
            .setClaims(claims)
            .setExpiration(generateExpirationDate())//添加失效时间
            .signWith(SignatureAlgorithm.HS512,secret)//添加密钥以及加密方式
            .compact();
}

/**
 * 生成Token失效时间  当前时间+配置的失效时间
 * @return
 */
private Date generateExpirationDate() {
    return new Date(System.currentTimeMillis()+expiration*1000);
}
```

#### 1.2根据Token生成用户名

```java
/**
 * 根据Token生成用户名
 * @param token
 * @return
 */
public String getUsernameFormToken(String token){
    String username;
    //根据Token去拿荷载
    try {
        Claims claim=getClaimFromToken(token);
        username=claim.getSubject();//获取用户名
    } catch (Exception e) {
        e.printStackTrace();
        username=null;
    }
    return username;
}

/**
 * 从Token中获取荷载
 * @param token
 * @return
 */
private Claims getClaimFromToken(String token) {
    Claims claims=null;
    try {
        claims=Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    } catch (Exception e) {
        e.printStackTrace();
    }
    return claims;
}
```

#### 1.3判断Token是否有效

```java
/**
 * 判断Token是否有效
 * Token是否过期
 * Token中的username和UserDetails中的username是否一致
 * @param token
 * @param userDetails
 * @return
 */
public boolean TokenIsValid(String token,UserDetails userDetails){
    String username = getUsernameFormToken(token);
    return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
}

/**
 * 判断Token是否过期
 * @param token
 * @return
 */
private boolean isTokenExpired(String token) {
    //获取Token的失效时间
    Date expireDate=getExpiredDateFromToken(token);
    //在当前时间之前，则失效
    return expireDate.before(new Date());
}

/**
 * 获取Token的失效时间
 * @param token
 * @return
 */
private Date getExpiredDateFromToken(String token) {
    Claims claims = getClaimFromToken(token);
    return claims.getExpiration();
}
```

#### 1.4判断Token是否可以被刷新

```java
/**
 * 判断token是否可用被刷新
 * 如果已经过期了，则可用被刷新，未过期，则不可用被刷新
 * @param token
 * @return
 */
public boolean canRefresh(String token){
    return !isTokenExpired(token);
}
```

#### 1.5刷新Token，获取新的Token

```java
/**
 * 刷新Token
 * @param token
 * @return
 */
public String refreshToken(String token){
    Claims claims=getClaimFromToken(token);
    claims.put(CLAIM_KEY_CREATED,new Date());
    return generateToken(claims);
}
```

### <2>登录功能的实现

* Controller层

  ```java
  @ApiOperation(value = "登录之后返回token")
  @PostMapping("/login")
  //AdminLoginParam 自定义登录时传入的对象，包含账号，密码，验证码 
  public RespBean login(@RequestBody AdminLoginParam adminLoginParam, HttpServletRequest request){
      return adminService.login(adminLoginParam.getUsername(),adminLoginParam.getPassword(),adminLoginParam.getCode(),request);
  }
  ```

* Service层

  ```java
  /**
   * 登录之后返回token
   * @param username
   * @param password
   * @param request
   * @return
   */
  @Override
  public RespBean login(String username, String password,String code, HttpServletRequest request) {
      String captcha = (String)request.getSession().getAttribute("captcha");//验证码功能，后面提到
      //验证码为空或匹配不上
      if((code == null || code.length()==0) || !captcha.equalsIgnoreCase(code)){
          return RespBean.error("验证码错误,请重新输入");
      }
  
      //通过username在数据库查出这个对象
      //在SecurityConfig配置文件中，重写了loadUserByUsername方法，返回了userDetailsService Bean对象，使用我们自己的登录逻辑
      UserDetails userDetails = userDetailsService.loadUserByUsername(username);
      //如果userDetails为空或userDetails中的密码和传入的密码不相同
      if (userDetails == null||!passwordEncoder.matches(password,userDetails.getPassword())){
          return RespBean.error("用户名或密码不正确");
      }
      //判断账号是否可用
      if(!userDetails.isEnabled()){
          return RespBean.error("该账号已经被禁用,请联系管理员");
      }
  
      //更新登录用户对象，放入security全局中,密码不放
      UsernamePasswordAuthenticationToken authenticationToken=new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(authenticationToken);
  
      //生成token
      String token = jwtTokenUtil.generateToken(userDetails);
      Map<String,String> tokenMap=new HashMap<>();
      tokenMap.put("token",token);
      tokenMap.put("tokenHead",tokenHead);//tokenHead，从配置文件yml中拿到的token的请求头 == Authorization
      return RespBean.success("登陆成功",tokenMap);//将Token返回
  }
  ```

### <3>退出登录

退出登录功能由前端实现，我们只需要返回一个成功信息即可

```java
@ApiOperation(value = "退出登录")
@PostMapping("/logout")
/**
 * 退出登录
 */
public RespBean logout(){
    return RespBean.success("注销成功");
}
```

### <4>获取当前登录用户信息

* Controller层

  ```java
   @ApiOperation(value = "获取当前登录用户的信息")
      @GetMapping("/admin/info")
      public Admin getAdminInfo(Principal principal){
          //可通过principal对象获取当前登录对象
          if(principal == null){
              return null;
          }
          //当前用户的用户名
          String username = principal.getName();
          Admin admin= adminService.getAdminByUsername(username);
          //不能返回前端用户密码,设置为空
          admin.setPassword(null);
          //将用户角色返回
          admin.setRoles(adminService.getRoles(admin.getId()));
          return admin;
      }
  ```

### <5>SpringSecurity的配置类SecurityConfig

#### 5.1 覆盖SpringSecurity默认生成的账号密码，并让他走我们自定义的登录逻辑

```java
//让SpringSecurity走我们自己登陆的UserDetailsService逻辑

//认证信息的管理 用户的存储 这里配置的用户信息会覆盖掉SpringSecurity默认生成的账号密码
@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService()).passwordEncoder(passwordEncoder());
}
//密码加解密
@Bean
public PasswordEncoder passwordEncoder(){
    return new BCryptPasswordEncoder();
}
@Override
@Bean  //注入到IOC中，在登录时使用到的userDetailsService就是这个Bean，loadUserByUsername方法是这里重写过的
public UserDetailsService userDetailsService(){
    return username->{
        Admin admin=adminService.getAdminByUsername(username);
        if(admin != null){
            admin.setRoles(adminService.getRoles(admin.getId()));
            return admin;
        }
        throw new UsernameNotFoundException("用户名或密码错误");
    };
}
```

登录功能中使用的userDetailsService对象由这里注入，重写loadUserByUsername方法实现自定义登录逻辑

#### 5.2进行资源的拦截，权限设置，登录过滤器设置

```java
@Override
protected void configure(HttpSecurity http) throws Exception {
    //使用Jwt不需要csrf
    http.csrf().disable()
            //基于token，不需要Session
            .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            //授权认证
            .authorizeRequests()
            .antMatchers("/doc.html").permitAll()
            //除了上面，所有的请求都要认证
            .anyRequest()
            .authenticated()
            .withObjectPostProcessor(new ObjectPostProcessor<FilterSecurityInterceptor>() {
                //动态权限配置
                @Override
                public <O extends FilterSecurityInterceptor> O postProcess(O o) {
                    o.setAccessDecisionManager(customUrlDecisionManager);
                    o.setSecurityMetadataSource(customFilter);
                    return o;
                }
            })
            .and()
            //禁用缓存
            .headers()
            .cacheControl();

    //添加jwt登录授权过滤器  判断是否登录
    http.addFilterBefore(jwtAuthencationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    //添加自定义未授权和未登录结果返回
    http.exceptionHandling()
        //权限不足
            .accessDeniedHandler(restfulAccessDeniedHandler)
        //未登录
            .authenticationEntryPoint(restAuthorizationEntryPoint);

}

//将登录过滤器注入
@Bean
public JwtAuthencationTokenFilter jwtAuthencationTokenFilter(){
    return new JwtAuthencationTokenFilter();
}

//需要放行的资源
@Override
public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers(
            "/login",
            "/logout",
            "/css/**",
            "/js/**",
            //首页
            "/index.html",
            //网页图标
            "favicon.ico",
            //Swagger2
            "/doc.html",
            "/webjars/**",
            "/swagger-resources/**",
            "/v2/api-docs/**",
            //放行图像验证码
            "/captcha",
            //WebSocket
            "/ws/**"
    );
}
```

##### 5.2.1登录过滤器的配置

```java
public class JwtAuthencationTokenFilter extends OncePerRequestFilter {
   //Jwt存储头
    @Value("${jwt.tokenHeader}")
    private String tokenHeader;

    //Jwt头部信息
    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        //token存储在Jwt的请求头中
        //通过key：tokenHeader拿到value：token

        //这里我们定义的token后期以：Bearer开头，空格分割，加上真正的jwt
        //通过tokenHeader(Authorization)拿到以Bearer开头 空格分割 加上真正的jwt的字符串
        String authHeader = httpServletRequest.getHeader(tokenHeader);

        //判断这个token的请求头是否为空且是以配置信息中要求的tokenHead开头
        if(authHeader != null && authHeader.startsWith(tokenHead)){
            //截取真正的jwt
            String authToken=authHeader.substring(tokenHead.length());
            String username=jwtTokenUtil.getUsernameFormToken(authToken);
            //token存在用户名但是未登录
            if(username != null && SecurityContextHolder.getContext().getAuthentication() == null){
                //登录
                UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                //验证token是否有效，重新设置用户对象
                if(jwtTokenUtil.TokenIsValid(authToken,userDetails)){
                    //把对象放到Security的全局中
                    UsernamePasswordAuthenticationToken authenticationToken=new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
                    //将请求中的Session等信息放入Details，再放入Security全局中
                authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                }

            }
        }
        //放行
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
```

##### 5.2.2添加未登录结果处理器

>当未登录或者Token失效时访问未放行的接口时，自定义返回的结果

```java
@Component
public class RestAuthorizationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json");
        PrintWriter out = httpServletResponse.getWriter();
        RespBean bean=RespBean.error("尚未登录，请登录");
        bean.setCode(401);
        out.write(new ObjectMapper().writeValueAsString(bean));
        out.flush();
        out.close();
    }
}
```

##### 5.2.3添加权限不足结果处理器

>当访问接口没有权限时，自定义返回结果

```java
@Component
public class RestfulAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json");
        PrintWriter out = httpServletResponse.getWriter();
        RespBean bean=RespBean.success("权限不足，请联系管理员");
        bean.setCode(401);
        out.write(new ObjectMapper().writeValueAsString(bean));
        out.flush();
        out.close();
    }
}
```

##### 5.2.4添加权限控制器，根据请求的URL确定访问该URL需要什么角色

```java
@Component
public class CustomFilter implements FilterInvocationSecurityMetadataSource {

    @Autowired
    private IMenuService menuService;

    AntPathMatcher antPathMatcher=new AntPathMatcher();

    @Override
    public Collection<ConfigAttribute> getAttributes(Object o) throws IllegalArgumentException {
        //获取请求的URL
        String requestUrl = ((FilterInvocation) o).getRequestUrl();
        List<Menu> menus = menuService.getMenuWithRole();
        //将URL所需要的角色放入Menu中
        for (Menu menu:menus) {
            //判断请求Url与菜单角色拥有的url是否匹配
            if(antPathMatcher.match(menu.getUrl(),requestUrl)){
                // 该Url所需要的角色
                String[] str = menu.getRoles().stream().map(Role::getName).toArray(String[]::new);
                //如果匹配上放入配置中,需要的角色
                return SecurityConfig.createList(str);
            }
        }
        //没匹配的url默认登录即可访问
        return SecurityConfig.createList("ROLE_LOGIN");
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
```

##### 5.2.5添加权限控制器，对角色信息进行处理，是否可用访问URL

```java
@Component
public class CustomUrlDecisionManager implements AccessDecisionManager {
    @Autowired
    private CustomFilter customFilter;
    @Override
    public void decide(Authentication authentication, Object o, Collection<ConfigAttribute> collection) throws AccessDeniedException, InsufficientAuthenticationException {
        for (ConfigAttribute configAttribute: collection) {
            // 当前url所需要的角色
            List<ConfigAttribute> list= (List<ConfigAttribute>) customFilter.getAttributes(o);
            String[] needRoles=new String[list.size()];
            for (int i = 0; i <list.size() ; i++) {
                needRoles[i]=list.get(i).getAttribute();
            }
            //判断角色是否登录即可访问的角色,此角色在CustomFilter中设置

            for (String needRole:needRoles) {
                if ("ROLE_LOGIN".equals((needRole))) {
                    //判断是否已经登录
                    if(authentication instanceof AnonymousAuthenticationToken){
                        throw new AccessDeniedException("尚未登录，请登录");
                    }else {
                        return;
                    }
                }
            }
            //判断用户角色是否为url所需要的角色
            //得到用户拥有的角色  这里在Admin类中已经将用户的角色放入了
            Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            for (String needRole:needRoles) {
                for (GrantedAuthority authority: authorities) {
                    if(authority.getAuthority().equals(needRole)){
                        return;
                    }
                }
            }
            throw new AccessDeniedException("权限不足，请联系管理员");
        }
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return false;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
}
```

### <6>Swagger2的配置

```java
@Configuration
@EnableSwagger2
public class Swagger2Config {

    @Bean
    public Docket createRestApi(){
        return new Docket(DocumentationType.SWAGGER_2)
                //基础设置
                .apiInfo(apiInfo())
                //扫描哪个包
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.example.server.controller"))
                //任何路径都可以
                .paths(PathSelectors.any())
                .build()
                .securityContexts(securityContexts())
                .securitySchemes(securitySchemes());
    }

    private ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("云E办接口文档")
                .description("云E办接口文档")
                .contact(new Contact("朱云飞", "http:localhost:8081/doc.html","2690534598@qq.com"))
                .version("1.0")
                .build();

    }

    private List<ApiKey> securitySchemes(){
        //设置请求头信息
        List<ApiKey> result=new ArrayList<>();
        ApiKey apiKey=new ApiKey("Authorization", "Authorization","Header");
        result.add(apiKey);
        return result;
    }

    private List<SecurityContext> securityContexts(){
        //设置需要登录认证的路径
        List<SecurityContext> result=new ArrayList<>();
        result.add(getContextByPath("/hello/.*"));
        return result;
    }

    private SecurityContext getContextByPath(String pathRegex) {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())//添加全局认证
                .forPaths(PathSelectors.regex(pathRegex)) //带有pathRegex字段的接口访问不带添加的Authorization全局变量
                .build();
    }

    //添加Swagger全局的Authorization  全局认证    固定的代码
    private List<SecurityReference> defaultAuth() {
        List<SecurityReference> result=new ArrayList<>();
        //设置范围为全局
        AuthorizationScope authorizationScope=new AuthorizationScope("global","accessEeverything");
        AuthorizationScope[]authorizationScopes=new AuthorizationScope[1];
        authorizationScopes[0]=authorizationScope;
        result.add((new SecurityReference("Authorization",authorizationScopes)));//这里的Authorization和上文ApiKey第二个参数一致
        return  result;
    }
}
```

**注意：**

```java
 ApiKey apiKey=new ApiKey("Authorization", "Authorization","Header");
```

![image-20210813140133666](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813140133666.png)

### <7>验证码功能(这里使用谷歌的验证码Captcha)

#### 7.1验证码的配置类

```java
@Component
public class CaptchaConfig {
    @Bean
    public DefaultKaptcha defaultKaptcha(){
        //验证码生成器
        DefaultKaptcha defaultKaptcha=new DefaultKaptcha();
        //配置
        Properties properties = new Properties();
        //是否有边框
        properties.setProperty("kaptcha.border", "yes");
        //设置边框颜色
        properties.setProperty("kaptcha.border.color", "105,179,90");
        //边框粗细度，默认为1
        // properties.setProperty("kaptcha.border.thickness","1");
        //验证码
        properties.setProperty("kaptcha.session.key","code");
        //验证码文本字符颜色 默认为黑色
        properties.setProperty("kaptcha.textproducer.font.color", "blue");
        //设置字体样式
        properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
        //字体大小，默认40
        properties.setProperty("kaptcha.textproducer.font.size", "30");
        //验证码文本字符内容范围 默认为abced2345678gfynmnpwx
        // properties.setProperty("kaptcha.textproducer.char.string", "");
        //字符长度，默认为5
        properties.setProperty("kaptcha.textproducer.char.length", "4");
        //字符间距 默认为2
        properties.setProperty("kaptcha.textproducer.char.space", "4");
        //验证码图片宽度 默认为200
        properties.setProperty("kaptcha.image.width", "100");
        //验证码图片高度 默认为40
        properties.setProperty("kaptcha.image.height", "40");
        Config config = new Config(properties);
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}
```

#### 7.2验证码的控制器

```java
@RestController
public class CaptchaController {
    @Autowired
    private DefaultKaptcha defaultKaptcha;
    @ApiOperation(value = "验证码")
    @GetMapping(value = "/captcha",produces = "image/jpeg")
    public void captcha(HttpServletRequest request, HttpServletResponse response){
        // 定义response输出类型为image/jpeg类型
        response.setDateHeader("Expires", 0);
        // Set standard HTTP/1.1 no-cache headers.
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        // Set IE extended HTTP/1.1 no-cache headers (use addHeader).
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        // Set standard HTTP/1.0 no-cache header.
        response.setHeader("Pragma", "no-cache");
        // return a jpeg
        response.setContentType("image/jpeg");
        //-------------------生成验证码 begin --------------------------
        //获取验证码文本内容
        String text=defaultKaptcha.createText();
        System.out.println("验证码内容"+text);
        //将验证码文本内容放入Session
        request.getSession().setAttribute("captcha",text);
        //根据文本验证码内容创建图形验证码
        BufferedImage image = defaultKaptcha.createImage(text);
        ServletOutputStream outputStream=null;
        try {
             outputStream = response.getOutputStream();
             //输出流输出图片,格式为jpg
            ImageIO.write(image, "jpg",outputStream);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(outputStream !=null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        //-------------------生成验证码 end --------------------------
    }
}
```

### <8>根据用户ID查询用户所拥有操控权限的菜单列表

![image-20210813141732884](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813141732884.png)

* Controller层

  ```java
  @ApiOperation(value = "通过用户ID查询菜单列表")
  @GetMapping("/menu")
  public List<Menu> getMenuByAdminId(){
      return menuService.getMenuByAdminId();
  }
  ```

* Service层

  ```java
  @Override
  public List<Menu> getMenuByAdminId() {
      //从Security全局上下文中获取当前登录用户Admin
      Admin admin= AdminUtil.getCurrentAdmin();
      Integer adminId=admin.getId();
      ValueOperations<String,Object> valueOperations = redisTemplate.opsForValue();
      //从Redis获取菜单数据
      List<Menu> menus = (List<Menu>) valueOperations.get("menu_" + adminId);
  
      //如果为空，从数据库中获取
      if(CollectionUtils.isEmpty(menus)){
          menus=menuMapper.getMenuByAdminId(adminId);
          //查询之后放入Redis
          valueOperations.set("menu_"+adminId,menus);
      }
      return menus;
  }
  ```

* Mapper层

  ```xml
  <!-- 根据用户id查询菜单列表  -->
  <select id="getMenuByAdminId" resultMap="Menus">
      SELECT DISTINCT
          m1.*,
          m2.id AS id2,
          m2.url AS url2,
          m2.path AS path2,
          m2.component AS component2,
          m2.`name` AS name2,
          m2.iconCls AS iconCls2,
          m2.keepAlive AS keepAlive2,
          m2.requireAuth AS requireAuth2,
          m2.parentId AS parentId2,
          m2.enabled AS enabled2
      FROM
          t_menu m1,
          t_menu m2,
          t_admin_role ar,
          t_menu_role mr
      WHERE
          m1.id = m2.parentId
          AND m2.id = mr.mid
          AND mr.rid = ar.rid
          AND ar.adminId = #{id}
          AND m2.enabled = TRUE
      ORDER BY
          m2.id
  </select>
  ```

### <9>使用Redis缓存根据用户ID查出来的菜单信息

#### 9.1 Redis的配置类

```java
@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<String,Object> redisTemplate=new RedisTemplate<>();
        //String类型Key序列器
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        //String类型Value序列器
        redisTemplate.setValueSerializer(new GenericJackson2JsonRedisSerializer());

        //Hash类型的key序列器
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        //Hash类型的Value序列器
        redisTemplate.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());

        redisTemplate.setConnectionFactory(redisConnectionFactory);
        return redisTemplate;
    }
}
```

### <10>全局异常的统一处理

```java
@RestControllerAdvice
public class GlobalException {
    @ExceptionHandler(SQLException.class)
    public RespBean respBeanMysqlException(SQLException e){
        if(e instanceof SQLIntegrityConstraintViolationException){
            return RespBean.error("该数据有关联数据，操作失败");
        }
        e.printStackTrace();
        return RespBean.error("数据库异常,操作失败");
    }

    @ExceptionHandler(DateException.class)
    public RespBean respBeanDateException(DateException e){
        e.printStackTrace();
        return RespBean.error(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public RespBean respBeanException(Exception e){
        e.printStackTrace();
        return RespBean.error("未知错误，请联系管理员");
    }
}
```

## 3.基础信息设置模块

职位，职称，权限组管理仅涉及单表的增删查改，这里不多写

### <1>部门管理

#### 1.1获取所有部门

Mapper层：涉及父子类，递归查找

```xml
<select id="getAllDepartments" resultMap="DepartmentWithChildren">
    select
    <include refid="Base_Column_List"/>
    from t_department
    where parentId=#{parentId}
</select>


<!-- 通用查询映射结果 -->
    <resultMap id="BaseResultMap" type="org.example.server.pojo.Department">
        <id column="id" property="id" />
        <result column="name" property="name" />
        <result column="parentId" property="parentId" />
        <result column="depPath" property="depPath" />
        <result column="enabled" property="enabled" />
        <result column="isParent" property="isParent" />
    </resultMap>

    <resultMap id="DepartmentWithChildren" type="org.example.server.pojo.Department" extends="BaseResultMap">
        <collection property="children" ofType="org.example.server.pojo.Department" select="org.example.server.mapper.DepartmentMapper.getAllDepartments"
        column="id">
        </collection>
    </resultMap>
    <!-- 通用查询结果列 -->
    <sql id="Base_Column_List">
        id, name, parentId, depPath, enabled, isParent
    </sql>
```

#### 1.2 添加部门

```xml
<!--添加部门 -->
<!--statementType="CALLABLE 调用存储过程-->
<select id="addDep" statementType="CALLABLE">
    call addDep(#{name,mode=IN,jdbcType=VARCHAR},#{parentId,mode=IN,jdbcType=INTEGER},#{enabled,mode=IN,jdbcType=BOOLEAN},#{result,mode=OUT,jdbcType=INTEGER},#{id,mode=OUT,jdbcType=INTEGER})
</select>
```

#### 1.3删除部门

```java
<!--添加部门 -->
<!--statementType="CALLABLE 调用存储过程-->
<select id="addDep" statementType="CALLABLE">
    call addDep(#{name,mode=IN,jdbcType=VARCHAR},#{parentId,mode=IN,jdbcType=INTEGER},#{enabled,mode=IN,jdbcType=BOOLEAN},#{result,mode=OUT,jdbcType=INTEGER},#{id,mode=OUT,jdbcType=INTEGER})
</select>
```

## 4.薪资模块及薪资管理模块

这里仅介绍获取全部操作员及操作员角色的更新，其他功能都是单表简单的增删查改

### <1>获取全部操作员

* Controller层

  ```java
  @ApiOperation(value = "获取所有操作员")
  @GetMapping("/")
  public List<Admin> getAllAdmins(String keywords){
      return adminService.getAllAdmins(keywords);
  }
  ```

* Service层

  ```java
  /**
   * 获取所有操作员
   * @param keywords
   */
  @Override
  public List<Admin> getAllAdmins(String keywords) {
      //要传当前登录的Id,当前操作员不用查
      return adminMapper.getAllAdmins(AdminUtil.getCurrentAdmin().getId(),keywords);
  }
  ```

* Mapper层

  ```java
  <!--获取所有操作员 -->
  <select id="getAllAdmins" resultMap="AdminWithRole">
      SELECT
      a.*,
      r.id AS rid,
      r.`name` AS rname,
      r.nameZh AS rnameZh
      FROM
      t_admin a
      LEFT JOIN t_admin_role ar ON a.id = ar.adminId
      LEFT JOIN t_role r ON r.id = ar.rid
      WHERE
      a.id != #{id}
      <if test="null!=keywords and ''!=keywords">
          AND a.`name` LIKE CONCAT( '%', #{keywords}, '%' )
      </if>
      ORDER BY
      a.id
  </select>
  ```

  涉及操作员角色的查询

### <2>操作员角色的修改

Service层：

```java
/**
 * 更新操作员角色
 * @param adminId
 * @param rids
 * @return
 */
@Override
@Transactional
public RespBean updateAdminRole(Integer adminId, Integer[] rids) {
    //先将已经拥有的角色全部删除
    adminRoleMapper.delete(new QueryWrapper<AdminRole>().eq("adminId",adminId));
    //再将传过来的所有角色添加
    Integer result = adminRoleMapper.addAdminRole(adminId, rids);
    if(result == rids.length){
        return RespBean.success("修改角色成功");
    }
    return RespBean.error("更新角色失败");
}
```

思想：先将操作员所有的角色都删除，再将前端闯入的角色全部添加

## 5.员工模块管理

### <1>分页获取全部员工信息

* Controller

  ```java
  @ApiOperation(value = "查询所有的员工(分页)")
  @GetMapping("/")
  //beginDateScope入职的日期范围
  public RespPageBean getEmployee(@RequestParam(defaultValue = "1") Integer currentPage,
                                  @RequestParam(defaultValue = "10") Integer size,
                                  Employee employee,
                                  LocalDate[] beginDateScope){
  
  
  
      return employeeService.getEmployeeByPage(currentPage,size,employee,beginDateScope);
  }
  ```

* Service层

  ```java
  @Override
  public RespPageBean getEmployeeByPage(Integer currentPage, Integer size, Employee employee, LocalDate[] beginDateScope) {
      Page<Employee> page=new Page<>(currentPage,size);
      IPage<Employee> iPage=employeeMapper.getEmployeeByPage(page,employee,beginDateScope);
      RespPageBean respPageBean=new RespPageBean();
      respPageBean.setTotal(iPage.getTotal());
      respPageBean.setData(iPage.getRecords());
      return respPageBean;
  }
  ```

* Mapper层

  ```xml
      <resultMap id="EmployeeInfo" type="org.example.server.pojo.Employee" extends="BaseResultMap">
          <association property="nation" javaType="org.example.server.pojo.Nation">
              <id column="nid" property="id" />
              <result column="nname" property="name" />
          </association>
          <association property="politicsStatus" javaType="org.example.server.pojo.PoliticsStatus">
              <id column="pid" property="id" />
              <result column="pname" property="name" />
          </association>
          <association property="department" javaType="org.example.server.pojo.Department">
              <id column="did" property="id" />
              <result column="dname" property="name" />
          </association>
          <association property="joblevel" javaType="org.example.server.pojo.Joblevel">
              <id column="jid" property="id" />
              <result column="jname" property="name" />
          </association>
          <association property="position" javaType="org.example.server.pojo.Position">
              <id column="posid" property="id" />
              <result column="posname" property="name" />
          </association>
      </resultMap>
  
  <!-- 将员工的政治面貌，职称，民族，职位，部门等信息填充进去 -->
  <!-- 获取所有员工(分页) -->
  <select id="getEmployeeByPage" resultMap="EmployeeInfo">
      SELECT
      e.*,
      n.id AS nid,
      n.`name` AS nname,
      p.id AS pid,
      p.`name` AS pname,
      d.id AS did,
      d.`name` AS dname,
      j.id AS jid,
      j.`name` AS jname,
      pos.id AS posid,
      pos.`name` AS posname
      FROM
      t_employee e,
      t_nation n,
      t_politics_status p,
      t_department d,
      t_joblevel j,
      t_position pos
      WHERE
      e.nationId = n.id
      AND e.politicId = p.id
      AND e.departmentId = d.id
      AND e.jobLevelId = j.id
      AND e.posId = pos.id
      <if test="null!=employee.name and ''!=employee.name">
          AND e.`name` LIKE CONCAT( '%', #{employee.name}, '%' )
      </if>
      <if test="null!=employee.politicId">
          AND e.politicId = #{employee.politicId}
      </if>
      <if test="null!=employee.nationId">
          AND e.nationId = #{employee.nationId}
      </if>
      <if test="null!=employee.jobLevelId">
          AND e.jobLevelId = #{employee.jobLevelId}
      </if>
      <if test="null!=employee.posId">
          AND e.posId = #{employee.posId}
      </if>
      <if test="null!=employee.engageForm and ''!=employee.engageForm">
          AND e.engageForm = #{employee.engageForm}
      </if>
      <if test="null!=employee.departmentId">
          AND e.departmentId = #{employee.departmentId}
      </if>
      <if test="null!=beginDateScope and 2==beginDateScope.length">
          AND e.beginDate BETWEEN #{beginDateScope[0]} AND #{beginDateScope[1]}
      </if>
      ORDER BY
      e.id
  </select>
  ```

### <2>使用EasyPOI对员工信息进行导入和导出

#### 2.1EasyPOI注解的使用

![image-20210813154835838](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813154835838.png)

用于员工数据导入：Excel表中的部门，职称等字段在数据库员工表中找不到字段，数据库中是以id外键字段存储

![image-20210813154927878](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813154927878.png)

![image-20210813155023785](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813155023785.png)

#### 2.2 员工数据的导出

```java
@ApiOperation(value = "导出员工数据")
@GetMapping(value = "/export",produces = "application/octet-stream")
public void exportEmployee(HttpServletResponse response){
    List<Employee> list = employeeService.getEmployee(null);
    //参数：文件名，表名，导出的Excel的类型(03版本)
    ExportParams params=new ExportParams("员工表","员工表", ExcelType.HSSF);
    Workbook workbook = ExcelExportUtil.exportExcel(params, Employee.class, list);
    //输入workbook
    ServletOutputStream out=null;
    try{
        //流形式
        response.setHeader("content-type","application/octet-stream");
        //防止中文乱码
        response.setHeader("content-disposition","attachment;filename="+ URLEncoder.encode("员工表.xls","UTF-8"));
        out = response.getOutputStream();
        workbook.write(out);
    }catch (IOException e){
        e.printStackTrace();
    }finally {
        if(out != null){
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
```

#### 2.3 员工数据的导入

```java
@ApiOperation(value = "导入员工数据")
@PostMapping("/import")
public RespBean importEmployee(MultipartFile file){
    //准备导入的数据表
    ImportParams params=new ImportParams();
    //去掉第一行:标题行
    params.setTitleRows(1);
    List<Nation> nationList = nationService.list();
    List<PoliticsStatus> politicsStatusList=politicsStatusService.list();
    List<Department> departmentList=departmentService.list();
    List<Joblevel> joblevelList=joblevelService.list();
    List<Position> positionList=positionService.list();
    try {
        //将Excel表变为List
        List<Employee> list = ExcelImportUtil.importExcel(file.getInputStream(), Employee.class, params);
        list.forEach(employee -> {
            //获取民族ID
            Integer nationId = nationList.get(nationList.indexOf(new Nation(employee.getNation().getName()))).getId();
            employee.setNationId(nationId);

            //获取政治面貌Id
            Integer politicsStatusId=politicsStatusList.get(politicsStatusList.indexOf(new PoliticsStatus(employee.getPoliticsStatus().getName()))).getId();
            employee.setPoliticId(politicsStatusId);

            //获取部门Id
            Integer departmentId=departmentList.get(departmentList.indexOf(new Department(employee.getDepartment().getName()))).getId();
            employee.setDepartmentId(departmentId);

            //获取职称Id
            Integer joblevelId=joblevelList.get(joblevelList.indexOf(new Joblevel(employee.getJoblevel().getName()))).getId();
            employee.setJobLevelId(joblevelId);

            //获取职位Id
            Integer positionId=positionList.get(positionList.indexOf(new Position(employee.getPosition().getName()))).getId();
            employee.setPosId(positionId);
        });

        if(employeeService.saveBatch(list)){
            return RespBean.success("导入成功");
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return RespBean.error("导入失败");
}
```

### <3>使用RabbitMQ对新入职的员工发送欢迎邮件

**这里使用SMTP**：需要先去邮箱开通SMTP服务

#### 3.1 RabbitMQ消息发送的可靠性

1. >消息落库，对消息状态进行标记

![image-20210813165231388](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813165231388.png)

**步骤：**

* 发送消息时，将当前消息数据存入数据库，投递状态为消息投递中

* 开启消息确认回调机制。确认成功，更新投递状态为消息投递成功

* 开启定时任务，重新投递失败的消息。重试超过3次，更新投递状态为投递失败

  2. >消息延迟投递，做二次确认，回调检查

![image-20210813165547247](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813165547247.png)

**步骤：**

* 发送消息时，将当前消息存入数据库，消息状态为消息投递
* 过一段时间进行第二次的消息发送
* 开启消息回调机制，当第一次发送的消息被成功消费时，消费端的确认会被MQ Broker监听，成功则将消息队列中的状态变为投递成功
* 如果消息投递没有成功，则过一段时间第二次发送的消息也会被MQ Broker监听到，会根据这条消息的ID去消息数据库查找，如果发现消息数据库中的状态为投递中而不是投递成功，则会通知消息放松端重新进行步骤一

#### 3.2消息功能的实现

>在进行新员工插入成功后，对新员工发出邮件,并将发送的邮件保存到数据库中

```java
    //获取合同开始和结束的时间
    LocalDate beginContact=employee.getBeginContract();
    LocalDate endContact=employee.getEndContract();
    long days = beginContact.until(endContact, ChronoUnit.DAYS);
    //保留两位小数
    DecimalFormat decimalFormat=new DecimalFormat("##.00");
    employee.setContractTerm(Double.parseDouble(decimalFormat.format(days/365.00)));
    if(employeeMapper.insert(employee) == 1){
        //获取新插入的员工对象
        Employee emp=employeeMapper.getEmployee(employee.getId()).get(0);
        //数据库记录发送的消息
        String msgId = UUID.randomUUID().toString();
        MailLog mailLog=new MailLog();
        mailLog.setMsgId(msgId);
        mailLog.setEid(employee.getId());
        mailLog.setStatus(0);
        //消息的状态保存在Model中
        mailLog.setRouteKey(MailConstants.MAIL_ROUTING_KEY_NAME);
        mailLog.setExchange(MailConstants.MAIL_EXCHANGE_NAME);
        mailLog.setCount(MailConstants.MAX_TRY_COUNT);
        mailLog.setTryTime(LocalDateTime.now().plusMinutes(MailConstants.MAX_TRY_COUNT));
        mailLog.setCreateTime(LocalDateTime.now());
        mailLog.setUpdateTime(LocalDateTime.now());
        mailLogMapper.insert(mailLog);

        //发送信息
        //发送交换机，路由键，用户对象和消息ID
        rabbitTemplate.convertAndSend(MailConstants.MAIL_EXCHANGE_NAME,
                MailConstants.MAIL_ROUTING_KEY_NAME,
                emp,
                new CorrelationData(msgId));
        return RespBean.success("添加成功");
    }
    return RespBean.error("添加失败");
}
```

>消费端的处理,这里我们使用上述第一种方式，--->消息落库，对消息状态进行标记. 为保证消费者不重复消费同一消息，采取 消息序号+我们传入的消息msgId来识别每一个消息

```java
@Component
public class MailReceiver {

    //日志
    private static final Logger LOGGER = LoggerFactory.getLogger(MailReceiver.class);

    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private MailProperties mailProperties;
    @Autowired
    private TemplateEngine templateEngine;
    @Autowired
    private RedisTemplate redisTemplate;

    @RabbitListener(queues = MailConstants.MAIL_QUEUE_NAME)
    //拿取Message 和 channel 可以拿到 消息序号鉴别消息是否统一个消息多收    通过消息序号+msgId两个来鉴别
    public void handler(Message message, Channel channel) {
        Employee employee = (Employee) message.getPayload();
        MessageHeaders headers = message.getHeaders();
        //消息序号
        long tag = (long) headers.get(AmqpHeaders.DELIVERY_TAG);
        //拿到存取的UUID
        String msgId = (String) headers.get("spring_returned_message_correlation");//这个key固定
        HashOperations hashOperations = redisTemplate.opsForHash();
        try {
            //从Redis中拿取，如果存在，说明消息已经发送成功了，这里直接确认返回
            if (hashOperations.entries("mail_log").containsKey(msgId)){
                LOGGER.error("消息已经被消费=============>{}",msgId);
                /**
                 * 手动确认消息
                 * tag:消息序号
                 * multiple:是否确认多条
                 */
                channel.basicAck(tag,false);
                return;
            }
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg);
            //发件人
            helper.setFrom(mailProperties.getUsername());
            //收件人
            helper.setTo(employee.getEmail());
            //主题
            helper.setSubject("入职欢迎邮件");
            //发送日期
            helper.setSentDate(new Date());
            //邮件内容
            Context context = new Context();
            //用于theymeleaf获取
            context.setVariable("name", employee.getName());
            context.setVariable("posName", employee.getPosition().getName());
            context.setVariable("joblevelName", employee.getJoblevel().getName());
            context.setVariable("departmentName", employee.getDepartment().getName());
            //将准备好的theymeleaf模板中的信息转为String
            String mail = templateEngine.process("mail", context);
            helper.setText(mail, true);
            //发送邮件
            javaMailSender.send(msg);
            LOGGER.info("邮件发送成功");
            //将消息id存入redis
            //mail_log是Redis  hash的key   msgId是真正的key  "OK"是Value，主要是拿到msgId，"OK"没啥用
            hashOperations.put("mail_log", msgId, "OK");
            //手动确认消息
            channel.basicAck(tag, false);
        } catch (Exception e) {
            /**
             * 手动确认消息
             * tag：消息序号
             * multiple：是否确认多条
             * requeue：是否退回到队列
             */
            try {
                channel.basicNack(tag,false,true);
            } catch (IOException ex) {
                LOGGER.error("邮件发送失败=========>{}", e.getMessage());
            }
            LOGGER.error("邮件发送失败=========>{}", e.getMessage());
        }
    }
}
```

>消息的配置类，确认应答等

```java
@Configuration
public class RabbitMQConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQConfig.class);
    @Autowired
    private CachingConnectionFactory cachingConnectionFactory;

    @Autowired
    private IMailLogService mailLogService;

    @Bean
    public RabbitTemplate rabbitTemplate(){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(cachingConnectionFactory);

        /**
         * 消息确认回调,确认消息是否到达broker
         * data:消息的唯一标识
         * ack:确认结果
         * cause:失败原因
         */
        rabbitTemplate.setConfirmCallback((data,ack,cause)->{
            String msgId = data.getId();
            if(ack){
                LOGGER.info("{}======>消息发送成功",msgId);
                mailLogService.update(new UpdateWrapper<MailLog>().set("status",1 ).eq("msgId",msgId));
            }else {
                LOGGER.error("{}=====>消息发送失败",msgId);
            }
        });

        /**
         * 消息失败回调,比如router不到queue时回调
         * msg:消息的主题
         * repCode:响应码
         * repText:响应描述
         * exchange:交换机
         * routingkey:路由键
         */
        rabbitTemplate.setReturnCallback((msg,repCode,repText,exchange,routingkey)->{
            LOGGER.error("{}=====>消息发送queue时失败",msg.getBody());
        });
        return rabbitTemplate;
    }


    @Bean
    public Queue queue(){
        return new Queue(MailConstants.MAIL_QUEUE_NAME);
    }

    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(MailConstants.MAIL_EXCHANGE_NAME);
    }

    @Bean
    public Binding binding(){
        return BindingBuilder.bind(queue()).to(directExchange()).with(MailConstants.MAIL_ROUTING_KEY_NAME);
    }
```

## 6.在线聊天功能的实现

>这里使用WebSocket

WebSocket 是 HTML5 开始提供的一种在单个 TCP 连接上进行全双工通讯的协议。

WebSocket 使得客户端和服务器之间的数据交换变得更加简单，允许服务端主动向客户端推送数据。

在 WebSocket API 中，浏览器和服务器只需要完成一次握手，两者之间就直接可以创建持久性的连

接，并进行双向数据传输。

它的最大特点就是，服务器可以主动向客户端推送信息，客户端也可以主动向服务器发送信息，是真正

的双向平等对话，属于服务器推送技术的一种。

![image-20210813230427839](C:\Users\26905\AppData\Roaming\Typora\typora-user-images\image-20210813230427839.png)

>WebSocket的配置

这里主要是前端实现，后端只是增加一些配置

```java
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserDetailsService userDetailsService;


    /**
     * 添加这个Endpoint，这样在网页可以通过websocket连接上服务
     * 也就是我们配置websocket的服务地址，并且可以指定是否使用socketJS
     * @param registry
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        /**
         * 1.将ws/ep路径注册为stomp的端点，用户连接了这个端点就可以进行websocket通讯，支持socketJS
         * 2.setAllowedOrigins("*")：允许跨域
         * 3.withSockJS():支持socketJS访问
         */
        registry.addEndpoint("/ws/ep").setAllowedOrigins("*").withSockJS();
    }


    /**
     * 输入通道参数配置  JWT配置
     * @param registration
     */
    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.interceptors(new ChannelInterceptor() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                //判断是否为连接，如果是，需要获取token，并且设置用户对象
                if (StompCommand.CONNECT.equals(accessor.getCommand())){
                    //拿取Token
                    String token = accessor.getFirstNativeHeader("Auth-Token");//参数前端已经固定
                    if (!StringUtils.isEmpty(token)){
                        String authToken = token.substring(tokenHead.length());
                        String username = jwtTokenUtil.getUsernameFormToken(authToken);
                        //token中存在用户名
                        if (!StringUtils.isEmpty(username)){
                            //登录
                            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
                            //验证token是否有效，重新设置用户对象
                            if (jwtTokenUtil.TokenIsValid(authToken,userDetails)){
                                UsernamePasswordAuthenticationToken authenticationToken =
                                        new UsernamePasswordAuthenticationToken(userDetails, null,
                                                userDetails.getAuthorities());
                                SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                                accessor.setUser(authenticationToken);
                            }
                        }
                    }
                }
                return message;
            }
        });
    }

    /**
     * 配置消息代理
     * @param registry
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        //配置代理域，可以配置多个，配置代理目的地前缀为/queue,可以在配置域上向客户端推送消息
        registry.enableSimpleBroker("/queue");
    }
}
```

