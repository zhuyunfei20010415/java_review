package org.example.network.http3.serializable;

import java.io.Serializable;

/**
 * @ClassName User
 * @Author 朱云飞
 * @Date 2021/8/1 17:12
 * @Version 1.0
 **/
public class User implements Serializable {
    private int uid;
    private String username;
    private String gender;

    @Override
    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public User(int uid, String username, String gender) {
        this.uid = uid;
        this.username = username;
        this.gender = gender;
    }
}
