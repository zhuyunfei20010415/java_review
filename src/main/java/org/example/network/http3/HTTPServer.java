package org.example.network.http3;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @ClassName HTTPServer
 * @Author 朱云飞
 * @Date 2021/7/31 21:47
 * @Version 1.0
 **/
public class HTTPServer {

    //线程池版本
    public static void main(String[] args) throws IOException {
        ExecutorService threadPool= Executors.newFixedThreadPool(10);
        ServerSocket serverSocket=new ServerSocket(8080);
        while (true){
            Socket socket=serverSocket.accept();
            Runnable task=new RequestResponseTask(socket);
            threadPool.execute(task);
        }
    }
}
