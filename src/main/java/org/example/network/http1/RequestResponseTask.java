package org.example.network.http1;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.Socket;

/**
 * @ClassName RequestResponseTask
 * @Author 朱云飞
 * @Date 2021/8/1 13:27
 * @Version 1.0
 **/
public class RequestResponseTask implements Runnable{
    private final Socket socket;

    public RequestResponseTask(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
       try {
           // 写回响应
           OutputStream outputStream = socket.getOutputStream();
           Writer writer = new OutputStreamWriter(outputStream, "UTF-8");
           PrintWriter printWriter = new PrintWriter(writer);
           Thread.sleep(1_000);
           //写响应
           printWriter.printf("HTTP/1.0 200 OK\r\n");
           //响应头
           printWriter.printf("Content-Type: text/html; charset=utf-8\r\n");
           //写入空行，代表响应头结束
           printWriter.printf("\n\r");

           //写响应体,html内容
           printWriter.printf("<h1>正常工作了</h1>");
           //刷新，把数据写入TCP发送缓冲区
           printWriter.flush();
           socket.close();
       }catch (Exception e){
           //单次的请求响应周期错误，不影响其他响应，进程不用结束
           e.printStackTrace(System.out);
       }
    }
}
