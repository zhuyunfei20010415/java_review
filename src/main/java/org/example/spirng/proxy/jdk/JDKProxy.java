package org.example.spirng.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @ClassName JDKProxy
 * @Author 朱云飞
 * @Date 2021/12/4 16:24
 * @Version 1.0
 **/
public class JDKProxy implements InvocationHandler {
    //目标类
    private Object targetObject;

    /**
     * 反射方式动态获取代理对象
     * @param targetObject
     * @return
     */
    public Object newProxyInstance(Object targetObject){
        this.targetObject=targetObject;
        //绑定关系，让targetObject和具体的那个类实现了关联
        return Proxy.newProxyInstance(targetObject.getClass().getClassLoader(),targetObject.getClass().getInterfaces(),this);
    }

    /**
     * JDK动态代理
     * @param proxy 代理对象
     * @param method 要调用的方法
     * @param args 方法调用时需要的参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result=null;
        try {
            System.out.println("通过JDK动态代理调用"+method.getName()+",日志打印begin");
            result=method.invoke(targetObject,args);
            System.out.println("通过JDK动态代理调用"+method.getName()+",打印日志end");
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
