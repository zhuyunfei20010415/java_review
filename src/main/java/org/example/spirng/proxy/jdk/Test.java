package org.example.spirng.proxy.jdk;

/**
 * @ClassName Test
 * @Author 朱云飞
 * @Date 2021/12/4 16:33
 * @Version 1.0
 **/
public class Test {
    public static void main(String[] args) {
        JDKProxy jdkProxy=new JDKProxy();
        PayService payService=(PayService) jdkProxy.newProxyInstance(new PayServiceImpl());
        payService.save(1,1);

    }
}
