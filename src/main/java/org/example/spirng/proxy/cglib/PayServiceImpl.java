package org.example.spirng.proxy.cglib;

/**
 * @ClassName PayServiceImpl
 * @Author 朱云飞
 * @Date 2021/12/4 16:22
 * @Version 1.0
 **/
public class PayServiceImpl implements PayService {
    @Override
    public String callBack(String outTradeNo) {
        System.out.println("目标类PayServiceImpl回调了callBack方法");
        return outTradeNo;
    }

    @Override
    public int save(int userId, int productId) {
        System.out.println("目标类PayServiceImpl执行了save方法");
        return productId;
    }
}
