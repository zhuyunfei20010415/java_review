package org.example.spirng.proxy.cglib;

/**
 * @InterfaceName PayProxy
 * @Author 朱云飞
 * @Date 2021/12/4 16:19
 * @Version 1.0
 **/
public interface PayService {

    /**
     * 支付回调
     * @param outTradeNo
     * @return
     */
    public String callBack(String outTradeNo);


    /**
     * 下单
     * @param userId
     * @param productId
     * @return
     */
    public int save(int userId,int productId);
}
