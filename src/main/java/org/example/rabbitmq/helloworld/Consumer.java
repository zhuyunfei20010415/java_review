package org.example.rabbitmq.helloworld;

import com.rabbitmq.client.*;

/**
 * @ClassName Consumer
 * @Author 朱云飞
 * @Date 2021/8/15 12:38
 * @Version 1.0
 **/
public class Consumer {
    //定义队列名
    private static final String QUEUE_NAME="hello";

    public static void main(String[] args) {
        //建立连接和信道
        try {
            ConnectionFactory factory=new ConnectionFactory();
            factory.setHost("127.0.0.1");
            factory.setUsername("guest");
            factory.setPassword("guest");
            Connection connection=factory.newConnection();
            Channel channel=connection.createChannel();
            System.out.println("等待接收消息");


            /**
             * 消息发送过来后的回调接口
             *1.同一个会话， consumerTag 是固定的 可以做此会话的名字， deliveryTag 每次接收消息+1，可以做此消息处理通道的名字。
             *2.消息类
             */
            DeliverCallback deliverCallback=(consumerTag,delivery)->{
                String message=new String(delivery.getBody());
                System.out.println(message);
                /**
                 * 参数说明
                 * 1.消息的标记tag
                 * 2.是否批量应答
                 */
                channel.basicAck(delivery.getEnvelope().getDeliveryTag(),false);
            };
            CancelCallback cancelCallback=(consumerTag)->{
                System.out.println("消息消费被取消");
            };
            /* 消费者消费消息
             * 1.消费哪个队列
             * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
             * 3.当一个消息发送过来后的回调接口
             * 4.消费者取消消费的回调
             */
            boolean ack=false;
            channel.basicConsume(QUEUE_NAME,ack,deliverCallback,cancelCallback);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
