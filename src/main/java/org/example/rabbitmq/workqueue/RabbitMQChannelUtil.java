package org.example.rabbitmq.workqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @ClassName RabbitMQChannelUtil
 * @Author 朱云飞
 * @Date 2021/8/15 13:17
 * @Version 1.0
 **/
public class RabbitMQChannelUtil {
    public static final String QUEUE_NAME="work";
    public static Channel getChannel(){
        try {
            ConnectionFactory factory=new ConnectionFactory();
            factory.setHost("127.0.0.1");
            factory.setUsername("guest");
            factory.setPassword("guest");
            Connection connection=factory.newConnection();
            return connection.createChannel();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
