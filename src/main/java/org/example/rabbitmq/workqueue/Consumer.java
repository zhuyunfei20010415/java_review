package org.example.rabbitmq.workqueue;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;

/**
 * @ClassName Consumer
 * @Author 朱云飞
 * @Date 2021/8/15 13:27
 * @Version 1.0
 **/
public class Consumer {
    public static void main(String[] args) {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("消费失败");
            return;
        }
        DeliverCallback deliverCallback=(consumerTag, delivery)->{
            String message=new String(delivery.getBody());
            System.out.println(Thread.currentThread().getName()+"消费了"+message);
        };
        CancelCallback cancelCallback=(consumerTag)->{
            System.out.println("消息消费被取消");
        };
        Thread[] threads=new Thread[5];
        for (int i = 0; i <threads.length ; i++) {
            threads[i]=new Thread(()->{
                try {
                    System.out.println(Thread.currentThread().getName()+"启动等待消费");
                    channel.basicConsume(RabbitMQChannelUtil.QUEUE_NAME,true,deliverCallback,cancelCallback);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        for (int i = 0; i <threads.length ; i++) {
            threads[i].start();
        }
    }
}
