package org.example.rabbitmq.workqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;

/**
 * @ClassName Producer
 * @Author 朱云飞
 * @Date 2021/8/15 13:20
 * @Version 1.0
 **/
public class Producer {
    public static void main(String[] args) throws IOException, InterruptedException {
        Channel channel=RabbitMQChannelUtil.getChannel();
        if(channel == null){
            System.out.println("失败");
            return;
        }
        channel.queueDeclare(RabbitMQChannelUtil.QUEUE_NAME,true,false,false,null);
        channel.basicQos(1);
        int i=0;
        while (true){
            String message="消息"+i;
            i++;
            /**
             * 发送一个消息
             * 1.发送到那个交换机
             * 2.路由的 key 是哪个
             * 3.其他的参数信息
             * 4.发送消息的消息体
             */
            channel.basicPublish("",RabbitMQChannelUtil.QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN,message.getBytes());
            System.out.println(message);
            Thread.sleep(500);
        }
    }
}
