package org.example.rabbitmq.workqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;

import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * 发布确认方式
 * @ClassName PublishAndConfirmMethod
 * @Author 朱云飞
 * @Date 2021/8/15 15:30
 * @Version 1.0
 **/
public class PublishAndConfirmMethod {
    private static final Integer MESSAGE_COUNT= 1000;
    private static final String  QUEUE_NAME="publish";

    public static void main(String[] args) throws Exception {
        //singleConfirm();
        //batchConfirm();
        asyncConfirm();
    }

    //单个确认
    public static void singleConfirm(){
        try {
            Channel channel=RabbitMQChannelUtil.getChannel();
            if(channel == null){
                System.out.println("信道建立失败");
                return;
            }
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            //开启发布确认
            channel.confirmSelect();
            long begin=System.currentTimeMillis();
            for (int i = 0; i <MESSAGE_COUNT ; i++) {
                String message=i+"";
                channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
                //可以加时间参数，当消息发送失败或超过参数时间没成功，则返回false
                boolean flag=channel.waitForConfirms();
                //如果失败可以重发
                if(flag){
                    System.out.println(message+"发送成功");
                }else {
                    //这里可以实现重发
                    System.out.println(message+"发送失败");
                }
            }
            long end=System.currentTimeMillis();
            System.out.println("发送"+MESSAGE_COUNT+"条消息，耗时"+(end-begin)+"ms");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //批量确认
    public static void batchConfirm(){
        try {
            Channel channel=RabbitMQChannelUtil.getChannel();
            if(channel == null){
                System.out.println("建立连接失败");
                return;
            }
            //当100条消息发布成功时，再确认
            int ackMessageCount=100;
            //未确认的消息个数
            int needAckMessageCount=0;
            //开启发布确认
            channel.confirmSelect();
            long begin=System.currentTimeMillis();
            for (int i = 0; i <MESSAGE_COUNT ; i++) {
                String message=i+"";
                channel.basicPublish("",QUEUE_NAME,null,message.getBytes());
                needAckMessageCount++;
                if(needAckMessageCount == ackMessageCount){
                    //确认
                    channel.waitForConfirms();
                    needAckMessageCount=0;
                }
            }
            //判断可能还有消息未发送，再发送依次
            if(needAckMessageCount > 0){
                channel.waitForConfirms();
            }
            long end= System.currentTimeMillis();
            System.out.println("发送"+MESSAGE_COUNT+"条消息，耗时"+(end-begin)+"ms");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //异步确认
    public static void asyncConfirm() throws Exception {
        try (Channel channel = RabbitMQChannelUtil.getChannel()) {
            if(channel == null){
                return;
            }
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            //开启发布确认
            channel.confirmSelect();
            /**
             * 线程安全有序的一个哈希表，适用于高并发的情况
             * 1.轻松的将序号与消息进行关联
             * 2.轻松批量删除条目 只要给到序列号
             * 3.支持并发访问
             */
            ConcurrentSkipListMap<Long, String> outstandingConfirms = new
                    ConcurrentSkipListMap<>();
            /**
             * 确认收到消息的一个回调
             * 1.消息序列号
             * 2.true 可以确认小于等于当前序列号的消息
             * false 确认当前序列号消息
             */
            ConfirmCallback ackCallback = (sequenceNumber, multiple) -> {
                if (multiple) {
                    //返回的是小于等于当前序列号的未确认消息 是一个 map
                    ConcurrentNavigableMap<Long, String> confirmed =
                            outstandingConfirms.headMap(sequenceNumber, true);
                    //清除该部分未确认消息
                    confirmed.clear();
                }else{
                    //只清除当前序列号的消息
                    outstandingConfirms.remove(sequenceNumber);
                }
            };
            ConfirmCallback nackCallback = (sequenceNumber, multiple) -> {
                String message = outstandingConfirms.get(sequenceNumber);
                System.out.println("发布的消息"+message+"未被确认，序列号"+sequenceNumber);
            };
            /**
             * 添加一个异步确认的监听器
             * 1.确认收到消息的回调
             * 2.未收到消息的回调
             */
            channel.addConfirmListener(ackCallback, null);
            long begin = System.currentTimeMillis();
            for (int i = 0; i < MESSAGE_COUNT; i++) {
                String message = "消息" + i;
                /**
                 * channel.getNextPublishSeqNo()获取下一个消息的序列号
                 * 通过序列号与消息体进行一个关联
                 * 全部都是未确认的消息体
                 */
                outstandingConfirms.put(channel.getNextPublishSeqNo(), message);
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            }
            long end = System.currentTimeMillis();
            System.out.println("发布" + MESSAGE_COUNT + "个异步确认消息,耗时" + (end - begin) +
                    "ms");
        }
    }
}
