package org.example.rabbitmq.dead_message_queue;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import org.example.rabbitmq.workqueue.RabbitMQChannelUtil;

/**
 * 死信队列生产者
 *
 * @ClassName Producer
 * @Author 朱云飞
 * @Date 2021/8/16 10:36
 * @Version 1.0
 **/
    public class Producer {
        private static final String NORMAL_EXCHANGE="normal_exchange";
        public static void main(String[] args) {
            try {
                Channel channel= RabbitMQChannelUtil.getChannel();
                if(channel == null){
                    return;
                }
                //声明交换机类型
                channel.exchangeDeclare(NORMAL_EXCHANGE, BuiltinExchangeType.DIRECT);
                //设置消息TTL时间
                AMQP.BasicProperties basicProperties=new AMQP.BasicProperties().builder().expiration("1000").build();
                //用作演示消息队列的限制个数
                for (int i = 0; i <10 ; i++) {
                    String message="info"+i;
                    channel.basicPublish(NORMAL_EXCHANGE,"zhangsan",basicProperties,message.getBytes());
                    System.out.println("生产者发送消息");
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
