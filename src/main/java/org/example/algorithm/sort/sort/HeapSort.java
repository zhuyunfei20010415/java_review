package org.example.algorithm.sort.sort;

import java.util.Arrays;

/**
 * @ClassName HeapSort
 * @Author 朱云飞
 * @Date 2021/12/2 10:34
 * @Version 1.0
 **/
public class HeapSort {
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        heapSort(array);
        System.out.println(Arrays.toString(array));
    }
    private static void heapSort(int[] array){
        if(array == null || array.length < 2){
            return;
        }
        //建堆
        buildHeap(array,array.length);
        //将堆顶放到最后，对堆顶元素进行向下调整
       for(int i=0;i<array.length;i++){
           swap(array,0,array.length-1-i);
           adjustDown(array,0,array.length-1-i);
       }
    }
    private static void buildHeap(int[] array,int length){
        if(array == null || length < 2){
            return;
        }
        //找最后一个节点
        int lastIndex=length-1;
        //找最后一个结点的父亲节点
        int lastParentIndex=(lastIndex - 1)/2;
        //最后一个父亲节点到根节点，一直向下调整
        for(int i=lastParentIndex;i>=0;i--){
            adjustDown(array,i,length);
        }
    }

    private static void adjustDown(int[] array, int index, int length) {
        if(array == null || array.length <2){
            return;
        }
        while (true) {
            //完全二叉树，找左孩子
            int leftIndex=index*2+1;
            //左孩子不存在，叶子节点
            if(leftIndex >=length){
                return;
            }
            //找右孩子
            int rightIndex=leftIndex+1;
            //最大的孩子
            int maxIndex=leftIndex;
            //有孩子存在且右孩子较大
            if(rightIndex < length && array[rightIndex] > array[leftIndex]){
                maxIndex=rightIndex;
            }
            //父亲节点较小则交换，否则结束调整
           if(array[index] >= array[maxIndex]){
               return;
           }
            //将maxIndex视为index循环
            swap(array,index,maxIndex);
            index=maxIndex;
        }
    }

    private static void swap(int[]array,int i,int j){
        int temp=array[i];
        array[i]=array[j];
        array[j]=temp;
    }
}
