package org.example.algorithm.sort.sort;

import java.util.Arrays;

/**
 * @ClassName MergeSort
 * @Author 朱云飞
 * @Date 2021/12/2 11:16
 * @Version 1.0
 **/
public class MergeSort {
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
    public static void mergeSort(int[] array){
        if(array == null || array.length < 2){
            return;
        }
        _mergeSort(array,0,array.length);
    }

    private static void _mergeSort(int[] array, int start, int end) {
        if(end-start <= 1){
            return;
        }
        int mid=(end+start)/2;
        _mergeSort(array,start,mid);
        _mergeSort(array,mid,end);
        mergeTowArray(array,start,mid,end);
    }

    private static void mergeTowArray(int[] array, int start, int mid, int end) {
        int[] temp=new int[end-start];
        int index=0;//数组位置
        int firstIndex=start;//第一个数组起始位置
        int secondIndex=mid;//第二个数组起始位置
        while (firstIndex < mid && secondIndex < end){
            if(array[firstIndex] < array[secondIndex]){
                temp[index]=array[firstIndex];
                firstIndex++;
                index++;
            }else{
                temp[index]=array[secondIndex];
                secondIndex++;
                index++;
            }
        }
      if(firstIndex < mid){
          for(int i=firstIndex;i<mid;i++){
              temp[index]=array[i];
              index++;
          }
      }
        if (secondIndex < end){
            for(int i=secondIndex;i<end;i++){
                temp[index]=array[i];
                index++;
            }
        }
        //复制回原数组
        for(int i=0;i<temp.length;i++){
            array[start+i]=temp[i];
        }
    }
}
