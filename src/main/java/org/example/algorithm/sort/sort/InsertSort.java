package org.example.algorithm.sort.sort;

import java.util.Arrays;

/**
 * @ClassName InsertSort
 * @Author 朱云飞
 * @Date 2021/11/16 16:07
 * @Version 1.0
 **/
public class InsertSort {
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void insertSort(int[] array){
        //有序空间[0,i)  无序空间[i,array.length)
        if(array == null || array.length < 2){
            return;
        }
        for (int i=1;i < array.length;i++){
            int key=array[i];//待插入数据
           for(int j=i;j>=1;j--){
               if(key < array[j-1]){
                   swap(array,j,j-1);
               }else {
                   break;
               }
           }
            }
        }

    public static void selectSort(int[] array){
        if(array == null || array.length < 2){
            return;
        }
        //有序区间 (0,index) 无序区间[index,array.length)
        for(int i=0;i<array.length;i++){
            int index=i;//记录最小值的索引
            for(int j=i;j<array.length;j++){
                if(array[j] < array[index]){
                    index=j;
                }
            }
            swap(array,i,index);
        }
    }

    public static void bubblingSort(int[]array){
        if(array == null || array.length < 2){
            return;
        }
        //有序区间(index,array.length-1]
        //无序区间[0,index]
       int index=array.length-1;//记录有序区间开始的位置
        for(int i=0;i<array.length;i++){
            for(int j=0;j<index;j++){
                if(array[j] > array[j+1]){
                    swap(array,j,j+1);
                }
            }
            index--;
        }
    }

    private static void quickSort(int[]array){
        if(array == null || array.length < 2){
            return;
        }
        _quickSort(array,0,array.length-1);
    }

    private static void _quickSort(int[] array, int low, int high) {
        if(low -high < 1 ){
            return;
        }
        //第一个数为基准，小的放左边，大的放右边
        int key=array[low];
        int left=low+1;
        int right=high;
        while (left <= right){
            while (true){
                if(array[right] < key){
                    break;
                }else{
                    right--;
                }
            }
            while (left <= right){
                if(array[left] > key){
                    break;
                }else {
                    left++;
                }
            }
            swap(array,left,right);
        }
        swap(array,low,right);
        _quickSort(array,low,right-1);
        _quickSort(array,right+1,high);
    }

    public static void shellSort(int[] array){
        if(array == null || array.length < 2){
            return;
        }
        int gap=array.length/2;
        while(gap >=1){
            _shellSort(array,gap);
            gap=gap/2;
        }
    }

    private static void _shellSort(int[] array, int gap) {
        //有序[gap,index)  无序[index,array.length)
        for(int i=gap;i<array.length;i++){
            int key=array[i];//记录待插入值
            for (int j=i;j>=gap;j=j-gap){
                if(key < array[j-gap]){
                    swap(array,j,j-gap);
                }else{
                    break;
                }
            }
        }
    }

    private static void  swap(int[]array,int i,int j){
        int temp=array[i];
        array[i]=array[j];
        array[j]=temp;
    }
}
