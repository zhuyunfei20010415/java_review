package org.example.algorithm.sort.sort;

import java.util.Arrays;

/**
 * @ClassName Test
 * @Author 朱云飞
 * @Date 2021/12/31 16:15
 * @Version 1.0
 **/
public class Test {
    private static void QuickSort(int[] array){
        if(array == null || array.length == 0){
            return;
        }
        _quickSort(array,0,array.length-1);
    }

    private static void _quickSort(int[] array, int low, int high) {
        if(high-low < 1){
            return;
        }
        int left=low+1;
        int right=high;
        int key=array[low];
        while (left < right){
            while (true){
                if(array[right] < key){
                    break;
                }else {
                    right--;
                }
            }
            while (left < right){
                if(array[left] > key){
                    break;
                }else {
                    left++;
                }
            }
            swap(array,left,right);
        }
        swap(array,low,right);
        _quickSort(array,low,left-1);
        _quickSort(array,left+1,high);
    }
    private static void swap(int[] array, int leftIndex, int rightIndex) {
        int temp=array[leftIndex];
        array[leftIndex]=array[rightIndex];
        array[rightIndex]=temp;
    }

    private static void buildding(int[] array){
        if(array == null || array.length == 0){
            return;
        }
        int index=array.length-1;//无序区间结束位置
        for(int i=0;i<array.length;i++){
            for(int j=0;j<index;j++){
                if(array[j]>array[j+1]){
                    swap(array,j,j+1);
                }
            }
            index--;
        }
    }

    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        buildding(array);
        System.out.println(Arrays.toString(array));
    }
}
