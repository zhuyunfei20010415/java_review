package org.example.algorithm.sort.sort;

import java.util.Arrays;

/**
 * @ClassName QuickSort
 * @Author 朱云飞
 * @Date 2021/11/16 16:12
 * @Version 1.0
 **/
public class QuickSort {

    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void quickSort(int[]array){
        //数组合法性判断
        if(array == null || array.length == 0){
            return;
        }
        //快排
        _quick(array,0,array.length-1);
    }

    private static void _quick(int[] array, int low, int high) {
        //递归结束条件   数组长度小于2
        if(high-low+1 < 2){
            return;
        }
        //定义左右指针，选择low位置为标准，比low位置小的数放左边，大的数放右边
        int leftIndex=low+1;
        int rightIndex=high;
        int key=array[low];
        while (leftIndex<rightIndex){
            //右指针先动，遇到第一个比array[low]小的数停下
            while (true){
                if(array[rightIndex] < key){
                    break;
                }else {
                    rightIndex--;
                }
            }
            //左指针动，遇到第一个比key大的数停下
            while (leftIndex < rightIndex){
                if (array[leftIndex] > key){
                    break;
                }else {
                    leftIndex++;
                }
            }
            //交换两个索引的值
            swap(array,leftIndex,rightIndex);
        }
        //此时leftIndex==rightIndex，交换key和array[leftIndex]
        swap(array,low,leftIndex);
        //递归左右区间
        _quick(array,low,leftIndex-1);
        _quick(array,rightIndex+1,high);
    }

    private static void swap(int[] array, int leftIndex, int rightIndex) {
        int temp=array[leftIndex];
        array[leftIndex]=array[rightIndex];
        array[rightIndex]=temp;
    }
}
