package org.example.algorithm.sort;

import java.util.Arrays;

/**
 * 插入排序
 *
 * @ClassName InsertSort
 * @Author 朱云飞
 * @Date 2021/7/15 17:37
 * @Version 1.0
 **/
public class InsertSort {

    /**
     * 前面为有序区间，后面为无序区间，在无序区间中依次将每个数插入到有序区间中
     * @param array
     */
    public static void insertSort(int[] array){
        //1.数组判断
        if(array == null || array.length == 0){
            return;
        }
        //2.记录无序区间开始的位置  有序区间：[0,index)  无序区间：[index,array.length)
        int index=1;
        for (int i = 1; i < array.length ; i++) {//第一层循环，排序的个数
            index = i;
            for (int j = index; j > 0 ; j--) { //第二层循环，和有序区间做比较
                //当前j索引值小于前一个，交换
                if(array[j] < array[j - 1]){
                    int t=array[j];
                    array[j]=array[j-1];
                    array[j-1]=t;
                }else {
                    break;
                }
            }
        }
    }
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        insertSort(array);
        System.out.println(Arrays.toString(array));
    }
}
