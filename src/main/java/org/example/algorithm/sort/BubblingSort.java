package org.example.algorithm.sort;

import java.util.Arrays;

/**
 * 冒泡排序
 *
 * @ClassName BubblingSort
 * @Author 朱云飞
 * @Date 2021/7/15 18:48
 * @Version 1.0
 **/
public class BubblingSort {
    /**
     * 从索引0开始，向右依次比较，较大的数放在数组后面
     * @param array
     */
    public static void bubblingSort(int[] array){
        //1.判断数组是否合法
        if(array == null || array.length == 0){
            return;
        }
        int index=array.length - 1; // 记录无序区间结束的位置 有序区间(index,array.length),无序区间[0,index]
        for (int i = 0; i < array.length ; i++) { // 第一层循环，元素个数
            for (int j = 0; j < index; j++) {//第二层循环，将无序区间中的最大值移动到有序区间的开始的前一个位置
                //如果前一个值大于后一个，交换
                if(array[j] > array[j+1]){
                    int t=array[j];
                    array[j]=array[j+1];
                    array[j+1] = t;
                }
            }
            //有序区间位置向前移动一个
            index--;
        }
    }
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        bubblingSort(array);
        System.out.println(Arrays.toString(array));
    }
}
