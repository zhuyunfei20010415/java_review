package org.example.algorithm.sort;

import java.util.Arrays;

/**
 * 选择排序
 *
 * @ClassName SelectSort
 * @Author 朱云飞
 * @Date 2021/7/15 17:22
 * @Version 1.0
 **/
public class SelectSort {
    /**
     * 前面为有序区间，后面为无序区间
     * 再无序区间中遍历，找到最大的数，和无序区间的最后一个数进行交换
     */
    public static void selectSort(int[] array){
        //1.判断数组
        if(array == null || array.length == 0){
            return;
        }
        //2.记录无序区间的开始位置,即最小元素插入位置
        int index=0;
        //3.遍历数组，依次从无序区间找到最小的数的下标索引
        for (int i = 0; i < array.length ; i++) { //第一层循环：记录数组中数据应插入的位置
            int minIndex=index;//记录最小值的下标
            for (int j = index; j < array.length ; j++) {//第二层循环：从无序区间的开始位置index开始找，找无序区间最小数的下标
                if(array[minIndex] > array[j]){
                    minIndex=j;
                }
            }
            //4.交换index和minIndex下标的值
            int temp=array[index];
            array[index]=array[minIndex];
            array[minIndex]=temp;
            //5.无序区间往后移动一位
            index++;
        }
    }

    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        selectSort(array);
        System.out.println(Arrays.toString(array));
    }
}
