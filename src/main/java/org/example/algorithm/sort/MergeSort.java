package org.example.algorithm.sort;

import java.util.Arrays;

/**
 * 归并排序
 *
 * @ClassName MergeSort
 * @Author 朱云飞
 * @Date 2021/7/16 11:32
 * @Version 1.0
 **/
public class MergeSort {
    public static void mergeSort(int[] array){
        //1.判断数组下标合法性
        if(array == null || array.length == 0){
            return;
        }
        //2.分成左右两边两个数组，进行二路归并
        _mergeSort(array,0,array.length);
    }

    private static void _mergeSort(int[] array, int start, int end) {
        //1.递归结束条件，当数组元素个数小于2时
        if(end-start < 2){
            return;
        }
        //2.找到数组中间索引
        int middle=(end+start)/2;
        //3.递归，分为两个数组
        _mergeSort(array,start,middle);
        _mergeSort(array,middle,end);
        //4.合并两个有序数组
        mergeTwoSortedArray(array,start,middle,end);
    }

    private static void mergeTwoSortedArray(int[] array, int start, int middle, int end) {
        //1.第一个数组[start,middle)  第二个数组[middle,end)
        //2.从start到end排序后的数组
        int[] temp=new int[end-start];
        int newIndex=0;
        //3.合并两个有序数组
       int oneIndex=start;
       int twoIndex=middle;
       while (oneIndex < middle && twoIndex <end){
           if(array[oneIndex] < array[twoIndex]){
               temp[newIndex]=array[oneIndex];
               oneIndex++;
               newIndex++;
           }else {
               temp[newIndex]=array[twoIndex];
               twoIndex++;
               newIndex++;
           }
       }
       if(oneIndex < middle){
           for (int i = oneIndex; i <middle ; i++) {
               temp[newIndex]=array[i];
               newIndex++;
           }
       }
       if(twoIndex <end){
           for (int i = twoIndex; i <end ; i++) {
               temp[newIndex]=array[i];
               newIndex++;
           }
       }
        //4.将temp复制到array的相应位置
        for (int i = 0; i < temp.length ; i++) {
            array[i+start]=temp[i];
        }
    }
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
}
