package org.example.algorithm.sort;

import java.util.*;

/**
 * 快速排序
 *
 * @ClassName QuickSort
 * @Author 朱云飞
 * @Date 2021/7/16 11:04
 * @Version 1.0
 **/
public class QuickSort {

    public static int[] quickSort (int[] arr) {
        // write code here
        if(arr == null || arr.length < 2){
            return arr;
        }
        _quickSort(arr,0,arr.length-1);
        return arr;
    }
    private static void _quickSort(int[]arr,int low,int high){
        if(high -low +1< 2){
            return;
        }
        //以数组第一个数为标准，比他小的放左边，大的放右边
        int key=arr[low];
        //定义左右指针
        int left=low+1;
        int right=high;
        while(left <= right){
            //右指针先动，找到第一个比key小的值停下
            while(true){
                if(arr[right] <= key){
                    break;
                }else{
                    right--;
                }
            }
            //左指针动，找到第一个比key大的值
            while(left <= right){
                if(arr[left] > key){
                    //交换左右值
                    swap(arr,left,right);
                    break;
                }else{
                    left++;
                }
            }
        }
        //此时left==right，交换key的索引和right的值
        swap(arr,low,right);
        //递归左右区间
        _quickSort(arr,low,right-1);
        _quickSort(arr,right+1,high);
    }
    //交换数组的值
    private static void swap(int []arr,int first,int second){
        int temp=arr[first];
        arr[first]=arr[second];
        arr[second]=temp;
    }
    public static void main(String[] args) {
        int[] array={4,2,1,3};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
