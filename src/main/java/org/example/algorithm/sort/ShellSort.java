package org.example.algorithm.sort;

import java.util.Arrays;

/**
 * 希尔排序(带间隔的插入排序)
 *
 * @ClassName ShellSort
 * @Author 朱云飞
 * @Date 2021/7/15 19:38
 * @Version 1.0
 **/
public class ShellSort {
    public static void shellSort(int[] array){
        //1.数组合法性判断
        if(array == null || array.length == 0){
            return;
        }
        //2.分为gap组
        int gap=array.length/2;
        //3.每次组数减少一半，直到1
        while (true){
            //4.分组插排
            insertSortWithGap(array,gap);
            if(gap == 1){
                return;
            }
            gap=gap/2;
        }
    }

    //分组插排
    private static void insertSortWithGap(int[] array, int gap) {
        for (int i = gap; i < array.length ; i++) {//表示需要进行插排的个数
            //1.刚开始时，认为每组的第一个数是有序的，所以从gap索引开始
            int key=array[i];
            for (int j = i; j >= gap ; j=j-gap) {//从第i个数开始
                if(key < array[j-gap]){
                    swap(array,j,j-gap);
                }else {
                    break;
                }
            }
        }
    }

    private static void swap(int[] array, int j, int i) {
        int t=array[j];
        array[j]=array[i];
        array[i]=t;
    }
    public static void main(String[] args) {
        int[] array={1,9,2,7,6,91,57,63,44,95,63,-1,-9,0,11,15,7};
        shellSort(array);
        System.out.println(Arrays.toString(array));
    }
}
