package org.example.algorithm.questions;

import java.util.*;

/**
 * @ClassName LeetCodeHot100
 * @Author 朱云飞
 * @Date 2021/7/7 9:02
 * @Version 1.0
 **/
  class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 }

public class LeetCodeHot100 {
    //1.两数之和
    public int[] twoSum(int[] nums, int target) {
        //判断数组是否为空
        if(nums == null || nums.length == 0){
            return null;
        }
        int[] result=new int[2];
        //找两个目标值
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j <nums.length ; j++) {
                if(nums[i]+nums[j] == target){
                    result[0]=i;
                    result[1]=j;
                }
            }
        }
        //如果数组索引都为默认0，说明没找到
        if(result[0] == 0&&result[1] == 0){
            return null;
        }
        //找到了，返回
        return result;
    }

    //2.两数相加
    public  ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //1.判断l1和l2是否为空
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        //2.建立返回链表的傀儡结点
        ListNode node=new ListNode(0);
        ListNode result=node;
        //3.处理进位
        int t=0;
        while (l1 != null || l2 != null){
            int temp =t;
            //4.链表不为空时，加上数值
            if(l1 != null){
                temp+=l1.val;
            }
            if(l2 != null){
                temp+=l2.val;
            }
            //5.处理进位
            if(temp > 9){
                temp=temp-10;
                t=1;
            }else {
                t=0;
            }
            result.next=new ListNode(temp);
            result=result.next;
            //6.判断当前结点是否为空
            if(l1 != null){
                l1=l1.next;
            }
            if(l2 != null){
                l2=l2.next;
            }
        }
        //7.判断当前进位是否为1
        if(t == 1){
            result.next=new ListNode(1);
        }
        //8.返回傀儡结点的下一结点
        return node.next;
    }

    //3.无重复字符的最长字串
    //使用滑动窗口解决 例如：abcabcbb
    //使用双指针，当窗口区域内没有重复字符时，右指针右移，有重复时，左指针移到下一相同字符对于的索引
    public int lengthOfLongestSubstring(String s) {
        //1.为空或长度为0
        if(s == null || s.length() == 0){
            return 0;
        }
        Map<Character,Integer> map=new HashMap<>();
        int left=0;
        int right=0;
        //2.最长字串
        int result=0;
        while (left <= right && right<s.length()){
            char c=s.charAt(right);
            right++;
            //3.查看map中是否包含c,包含则移动左指针到下一索引
            if(map.containsKey(c)){
                left=Math.max(left,map.get(c));
            }
            map.put(c,right);

            //4.更新最长子串
            result=Math.max(result,right-left);
        }
        return result;
    }

    //4. 寻找两个正序数组的中位数
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        //1.如果两个数组都为空，返回0
        if((nums1 == null || nums1.length ==0) &&(nums2 == null || nums2.length ==0)){
            return 0;
        }
        //2.某一个为空，返回另外一个数组的中位数
        if(nums1 == null || nums1.length ==0){
            return getArrayMiddleCount(nums2);
        }
        if(nums2 == null || nums2.length ==0){
            return getArrayMiddleCount(nums1);
        }
        //2.合并两个有序数组
        int [] newNums=mergeTwoArrays(nums1,nums2);
        //3.返回一个数组的中位数
        return getArrayMiddleCount(newNums);
    }

    private static double getArrayMiddleCount(int[] newNums) {
        if(newNums.length%2 == 1){
            return newNums[newNums.length/2];
        }else {
            return (double)(newNums[newNums.length/2]+newNums[newNums.length/2-1])/2;
        }
    }

    private static int[] mergeTwoArrays(int[] nums1, int[] nums2) {
        //1.定义生成的数组
      int[] result=new int[nums1.length+nums2.length];
      //2.定义索引
      int s1=0;
      int s2=0;
      int index=0;
      //3.将两个数组比较，小的进新数组
      while (s1<nums1.length && s2<nums2.length){
          if(nums1[s1]<=nums2[s2]){
              result[index]=nums1[s1];
              index++;
              s1++;
          }else {
              result[index]=nums2[s2];
              index++;
              s2++;
          }
      }
      //4.把剩余的元素放进result
      while (s1<nums1.length&&index<result.length){
          result[index]=nums1[s1];
          index++;
          s1++;
      }
      while (s2<nums2.length&&index<result.length){
          result[index]=nums2[s2];
          index++;
          s2++;
      }
      return result;
    }


    //5.给你一个字符串 s，找到 s 中最长的回文子串。
    //中心扩散法,动态规划


    //8.三数之和
    public List<List<Integer>> threeSum(int[] nums) {
        if(nums == null || nums.length < 3){
            return null;
        }
        List<List<Integer>> result=new ArrayList<>();
        //1.对数组排序
         Arrays.sort(nums);
         //2.找第一个非负数
        int temp=0;
        for (int i = 0; i <nums.length ; i++) {
            if(nums[i]>=0){
                temp=i;
                break;
            }
        }
         //3.找三元组
        for (int i = 0; i <temp ; i++) {
            //起始的位置都大于0，说明后面不存在三元组，直接返回
            if(nums[i] > 0){
                return result;
            }
            //定义左右指针起始位置
            int left=i+1;
            int right=nums.length-1;
            while (left<right) {
                //去除重复元素
                while (left<right) {
                    if (nums[left] == nums[left + 1]) {
                        left++;
                    }
                    if (nums[right] == nums[right - 1]) {
                        right--;
                    }
                }

                if (nums[i] + nums[left] + nums[right] == 0) {
                    //如果相加为0，加入result
                    List<Integer> list = new ArrayList<>();
                    list.add(nums[i]);
                    list.add(nums[left]);
                    list.add(nums[right]);
                    result.add(list);
                } else if (nums[i] + nums[left] + nums[right] > 0) {
                    //如果太大，右指针左移
                    right--;
                }else if(nums[i] + nums[left] + nums[right] < 0){
                    //如果太小，左指针右移
                    left++;
                }
            }
        }
        return result;
    }

}
