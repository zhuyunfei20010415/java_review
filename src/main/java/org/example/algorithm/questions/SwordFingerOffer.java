package org.example.algorithm.questions;


import java.util.*;

/**
 * Leetcode剑指Offer第二版
 * @ClassName SwordFingerOffer
 * @Author 朱云飞
 * @Date 2021/7/8 10:20
 * @Version 1.0
 **/

 class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
public class SwordFingerOffer {

    //1.数组中重复的数字
    public int findRepeatNumber(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) {
                return nums[i];
            }
            set.add(nums[i]);
        }
        return 0;
    }

    //2.二维数组中的查找
    //<1>暴力破解法
    public static boolean findNumberIn2DArray1(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        if (matrix[0] == null || matrix[0].length == 0) {
            return false;
        }
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (matrix[i][j] == target) {
                    return true;
                }
            }
        }
        return false;
    }

    //<2>坐标轴法  以左下角为零点建立坐标系，比他大则沿x移动一格，小则沿着y移动一格，找到为止。xy就代表二维数组下标
    public static boolean findNumberIn2DArray(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0) {
            return false;
        }
        if (matrix[0] == null || matrix[0].length == 0) {
            return false;
        }
        int x = 0;//x轴起始为止
        int y = matrix.length - 1;//y轴起始位置
        while (x < matrix[0].length && y > -1) {
            if (matrix[y][x] == target) {
                return true;
            } else if (matrix[y][x] < target) {
                x++;
            } else if (matrix[y][x] > target) {
                y--;
            }
        }
        return false;
    }


    //3. 替换空格
    public String replaceSpace(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                str.append("%20");
            } else {
                str.append(s.charAt(i));
            }
        }
        return str.toString();
    }

    //4. 从尾到头打印链表
    public int[] reversePrint(ListNode head) {
        //1.如果为空，返回
        if (head == null) {
            return new int[0];
        }
        //2.链表转置
        int count = 1;//记录链表个数
        ListNode cur = head;
        ListNode pre = null;
        while (cur.next != null) {
            ListNode temp = cur.next;
            cur.next = pre;
            pre = cur;
            cur = temp;
            count++;
        }
        //将cur变为转置后链表的头节点
        cur.next = pre;
        //3.此时链表转置，cur指向转置后的头节点
        int index = 0;//数组下标
        int[] result = new int[count];
        while (cur != null) {
            result[index] = cur.val;
            cur = cur.next;
            index++;
        }
        return result;
    }

    //5.重建二叉树
    public Integer index = 0;

    public TreeNode buildTree(int[] preorder, int[] inorder) {
        //每次调用，将index设为0
        index = 0;
        //1.健壮性判断
        if (preorder == null || preorder.length == 0) {
            return null;
        }
        //2.将inorder变为list，好操作
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < inorder.length; i++) {
            list.add(inorder[i]);
        }
        return _builder(preorder, list);
    }

    private TreeNode _builder(int[] preorder, List<Integer> list) {
        //1.递归结束条件
        if (list.isEmpty()) {
            return null;
        }
        //2.建立结点
        TreeNode node = new TreeNode(preorder[index]);
        index++;
        //3.得到node.val在list中的索引
        int pos = list.indexOf(node.val);
        node.left = _builder(preorder, list.subList(0, pos));
        node.right = _builder(preorder, list.subList(pos + 1, list.size()));
        return node;
    }



    public static void main1(String[] args) {
        Deque <Integer> deque=new LinkedList<>();
        deque.pop();

    }

    //6.用两个栈实现队列
    Deque<Integer> deque1 = new LinkedList<>();//第一个栈，管理队列的入,第二个栈未空时，新加入的数据都放这里
    Deque<Integer> deque2 = new LinkedList<>();//第二个栈，管理队列的出

    public void appendTail(int value) {
        //直接将值加入第一个栈中
        deque1.add(value);
    }

    public int deleteHead() {
        //1.如果第二个栈不为空，直接弹出
        if (!deque2.isEmpty()) {
            return deque2.pop();
        } else {
            //2.如果第二个栈为空，第一个不为空，将第一个栈中所有元素加入第二个栈中，并弹出第二个栈的第一个元素
            if (!deque1.isEmpty()) {
                while (!deque1.isEmpty()) {
                    deque2.add(deque1.pop());
                }
                return deque2.pop();
            } else {
                //3.如果两个栈都为空，返回-1
                return -1;
            }
        }
    }

    //7.1 斐波那契数列 0 1 1 2 3 5 8
    public int fib(int n) {
        if (n < 2) {
            return n;
        }
        int first = 0;
        int second = 1;
        for (int i = 2; i <= n; i++) {
            second = first + second;//第三个数=第一个加第二个
            first = second - first;//第二个数=第三个-第二个
            second = second % 1000000007;//防止溢出
        }
        return second;
    }

    //7.2青蛙跳台阶 1 1 2 3 5 8
    public int numWays(int n) {
        if (n < 2) {
            return 1;
        }
        int first = 1;
        int second = 1;
        for (int i = 2; i <= n; i++) {
            second = first + second;//第三个数=第一个加第二个
            first = second - first;//第二个数=第三个-第二个
            second = second % 1000000007;//防止溢出
        }
        return second;
    }

    //8.旋转数组的最小数字
    public int minArray(int[] numbers) {
        //从后往前遍历数组，当不再递减，则递减的最后一个数即为最小值
        int pos = -1;//记录最小值的索引
        boolean temp = false;//记录是否存在不再递减
        for (int i = numbers.length - 1; i > 0; i--) {
            if (numbers[i] < numbers[i - 1]) {
                pos = i;
                temp = true;
            }
        }
        if (temp) {
            //如果存在，返回索引为temp的值
            return numbers[pos];
        } else {
            //如果不存在，说明有序，返回数组第一个值
            return numbers[0];
        }
    }

    //9.矩阵中的路径
    public boolean exist(char[][] board, String word) {
        //1.特殊情况判断
        if (board == null || board.length == 0) {
            return false;
        }
        if (word == null || word.length() == 0) {
            return true;
        }
        if (board.length * board[0].length < word.length()) {
            return false;
        }
        //2.以board[0][0]为零点建立坐标系,向左，右，下寻找

        //<1>先找到word对于的第一个字符
        int first = 0;//记录寻找到的列数
        int second = 0;//记录寻找到的行数
        int count = 0;//记录经过的字符

        while (count <= board.length * board[0].length) {
            boolean temp = false;//记录是否找到第一个字符
            while (first < board.length && second < board[0].length) {
                count++;
                if (board[first][second] == word.charAt(0)) {
                    temp = true;
                    break;
                }
                second++;
                if (second > board[0].length) {
                    second = second % board[0].length;
                    first++;
                }
            }
            if (temp) {
                //找到了第一个字符，开始向左右下寻找
                return findWord(board, first, second, word);
            }
        }
        return false;
    }

    //找word
    //int wordPos=1;//记录word找的位置索引
    private boolean findWord(char[][] board, int first, int second, String word) {
        int wordPos = 1;//记录word的索引位置
        //起始位置是board[first][second]
        while (wordPos < word.length()) {
            boolean temp = false;//记录是否找到
            //1.向左找
            if (second - 1 > -1) {
                if (board[first][second - 1] == word.charAt(wordPos)) {
                    temp = true;
                    second = second - 1;
                    wordPos++;
                    continue;
                }
            }
            //向右找
            if (second + 1 < board[0].length) {
                if (board[first][second + 1] == word.charAt(wordPos)) {
                    temp = true;
                    second = second + 1;
                    wordPos++;
                    continue;
                }
                //向下找
                if (first + 1 < board.length) {
                    if (board[first + 1][second] == word.charAt(wordPos)) {
                        temp = true;
                        first = first + 1;
                        wordPos++;
                    }
                }
                if (!temp) {
                    return false;
                }
            }
        }
        return true;
    }



    //14.二进制中1的个数
    //右移运算，如果末位为1，右移%2余数为1，否则为0


    //15.数值的整数次方  递归
    public double myPow(double x, int n) {
        if(n == 0){
            return 1;
        }else if(n < 0){
            return 1 / (x * myPow(x, - n - 1));
        }else if(n % 2 == 1){
            return x * myPow(x, n - 1);
        }else{
            return myPow(x * x, n / 2);
        }
    }

    //16.打印从1到最大的n位数
    public int[] printNumbers(int n) {
        if(n <= 0){
            return new int[0];
        }
        int length=(int) Math.pow(10, n)-1;
        int[] result=new int[length];
        for (int i =1 ; i <=length ; i++) {
            result[i-1]=i;
        }
        return result;
    }

    //17. 删除链表的节点
    public ListNode deleteNode(ListNode head, int val) {
        if(head == null){
            return null;
        }
        if(head.val == val){
            return head.next;
        }
        ListNode cur=head;
        while (cur.next != null){
            if(cur.next.val == val){
                cur.next=cur.next.next;
                return head;
            }
            cur=cur.next;
        }
        return head;
    }

    //18.正则表达式的匹配

    //19.表示数值的字符串

    //20.调整数组顺序使奇数位于偶数前面  双指针
    public static int[] exchange(int[] nums) {
        //1.特殊判断
        if(nums == null || nums.length <2){
            return nums;
        }
        int left=0;
        int right=nums.length-1;
        while (left<=right){
            //2.右指针动，遇到第一个奇数停下
            while (left<=right&&nums[right] % 2 != 1){
                right--;
            }
            //3.左指针动，遇到第一个偶数停下
            while (left<=right&&nums[left] % 2 != 0){
                left++;
            }
            //4.交换两个位置的数
            if (left<=right) {
                swap(left,right,nums);
            }
            left++;
            right--;
        }
        return nums;
    }

    private static void swap(int left, int right, int[] nums) {
        int temp=nums[left];
        nums[left]=nums[right];
        nums[right]=temp;
    }


    //21.链表中倒数第k个节点
    public ListNode getKthFromEnd(ListNode head, int k) {
        //1.特殊判断
        if (head == null || k == 0) {
            return head;
        }
        //2.先遍历一遍，看看有多少个结点
        ListNode cur=head;
        int count=0;//记录结点个数
        while (cur != null){
            cur=cur.next;
            count++;
        }
        if(count < k){
            return null;
        }
        int length=count-k+1;//记录要返回的结点
        cur=head;
        count=1;
        while (true){
            if(count == length){
                return cur;
            }
            cur=cur.next;
            count++;
        }
    }

    //22.反转链表
    public ListNode reverseList(ListNode head) {
        if(head == null){
            return null;
        }
        ListNode cur=head;
        ListNode pre=null;
        while (cur != null){
            ListNode temp=cur.next;
            cur.next=pre;
            pre=cur;
            cur=temp;
        }
        return  pre;
    }

    //23.合并两个有序链表
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        //1.特殊判断
        if(l1 == null && l2 == null){
            return null;
        }
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }

        //2.建立第三链表的傀儡结点
        ListNode head=new ListNode(0);
        ListNode cur=head;
        //3.将小的数依次连接
        while (l1 != null && l2 != null){
            if(l1.val <= l2.val){
                cur.next=l1;
                l1=l1.next;
                cur=cur.next;
            }else {
                cur.next=l2;
                l2=l2.next;
                cur=cur.next;
            }
        }
        //4.将两个链表剩余的结点拼接到第三个链表
        if(l1 != null){
            cur.next=l1;
        }
        if(l2 != null){
            cur.next=l2;
        }
        return head.next;
    }

    //24.树的子结构
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        //1.如果任意一个为空，返回false
        if(A == null || B == null){
            return false;
        }
        //2.如果A的值和B的值相等，判断A是否有B的结构
        if(A.val == B.val && _isSubStructure(A,B)){
            return true;
        }
        //3.没找到，在左子树找
        if(isSubStructure(A.left,B)){
            return true;
        }
        //4.也没找到，在右子树找
       return isSubStructure(A.right,B);
    }

    private boolean _isSubStructure(TreeNode a, TreeNode b) {
        //递归结束条件
        if(a == null && b == null){
            //a，b同时遍历完
            return true;
        }
        if(b == null){
            //b遍历完了
            return true;
        }
        if(a == null ){
            //a遍历完了，但是还有b的结点没有包含
            return false;
        }
        if(a.val != b.val){
            //a b值不相等
            return false;
        }
        return _isSubStructure(a.left,b.left) && _isSubStructure(a.right,b.right);
    }


    //25.二叉树的镜像
    public TreeNode mirrorTree(TreeNode root) {
        //1.特殊判断
        if(root == null || (root.left == null && root.right == null)){
            return root;
        }
        //2.建立镜像的根结点
        TreeNode temp=new TreeNode(root.val);
        //3.建立镜像(temp传来没用，是为了递归方便,需要两个参数)
        return _mirrorTree(root,temp);
    }

    private TreeNode _mirrorTree(TreeNode root, TreeNode temp) {
        //root遍历完，返回
        if(root == null){
            return null;
        }
        //建立新的结点，防止直接使用root发生错误
        TreeNode node=new TreeNode(root.val);
        //node的左孩子等于root的右孩子
        node.left=_mirrorTree(root.right,node.left);
        //node的右孩子等与root的左孩子
        node.right=_mirrorTree(root.left,node.right);

        //返回node才能将所有结点连起来
        return node;
    }


    //26.对称的二叉树
    public boolean isSymmetric(TreeNode root) {
        //1.特殊判断，如果root == null或者只有一个结点
        if(root == null || (root.left == null && root.right == null)){
            return true;
        }
        //有两个结点，为false
        if(root.left == null || root.right == null){
            return false;
        }
        return _isSymmetric(root.left,root.right);
    }

    private boolean _isSymmetric(TreeNode left, TreeNode right) {
        //1.递归结束条件
        //左右同时遍历完
        if(left == null && right == null){
            return  true;
        }
        //有一个没遍历完
        if(left == null || right == null){
            return false;
        }
        //值不同
        if(left.val != right.val){
            return false;
        }
        //2.递归
        return _isSymmetric(left.left,right.right) && _isSymmetric(left.right,right.left);
    }

    //27.顺时针打印矩阵
    public static int[] spiralOrder(int[][] matrix) {
        //1.特殊判断
        if(matrix.length == 0){
            return new int[0];
        }
        int heng=matrix[0].length;//横行元素个数
        int zong=matrix.length;//纵行元素个数
        int length=heng*zong;
        //2.建立结果数组
        int [] result=new int[length];
        boolean [][] temp=new boolean[zong][heng];//数组是否遍历过的标志位
        //3.以matrix[0][0]为原点，横向为x轴，纵向为y轴建立坐标系
        int arrayIndex=0;//记录result的索引
        int direction=1;//记录打印的方向1,2,3,4依次记录右下左上
        int x=0;
        int y=0;
        //最后一个元素要手动添加，循环加不了，所以减一
        while (arrayIndex < length-1){
            //1.向右打印
            if(direction == 1){
                while (true){
                    //不能再向右打印时，改变状态从1->2
                    if(x+1 == heng || temp[y][x+1]){
                        direction=2;//改为向下打印
                        break;
                    }
                    //继续打印
                    result[arrayIndex]=matrix[y][x];
                    temp[y][x]=true;
                    x++;
                    arrayIndex++;
                }
            }else if(direction == 2){
                while (true){
                    //不能再向下打印时
                    if(y+1 == zong || temp[y+1][x]){
                        direction=3;//改为向左打印
                        break;
                    }
                    //继续打印
                    result[arrayIndex]=matrix[y][x];
                    temp[y][x]=true;
                    y++;
                    arrayIndex++;
                }
            }else if(direction == 3){
                while (true){
                    //不能继续向左打印
                    if(x == 0 || temp[y][x-1]){
                        direction=4;//改为向上打印
                        break;
                    }
                    //继续打印
                    result[arrayIndex]=matrix[y][x];
                    temp[y][x]=true;
                    x--;
                    arrayIndex++;
                }
            }else {
                while (true){
                    //不能继续向上打印
                    if(y == 0 || temp[y-1][x]){
                        direction=1;//改为向右打印
                        break;
                    }
                    result[arrayIndex]=matrix[y][x];
                    temp[y][x]=true;
                    y--;
                    arrayIndex++;
                }
            }
        }
        //放入最后一个元素
        result[arrayIndex]=matrix[y][x];
        return result;
    }


    //28.包含min函数的栈
    Deque<Integer> deque3=new LinkedList<>();//存放正常的树
    Deque<Integer> deque4=new LinkedList<>();//存放最小的树
    public void push(int x) {
        if(deque4.isEmpty() || x <= deque4.peek()){
            //如果deque4为空或出现了更小的值
            deque3.push(x);
            deque4.push(x);
        }else {
            deque3.push(x);
            deque4.push(deque4.peek());
        }
    }

    public void pop() {
        deque3.pop();
        deque4.pop();
    }

    public int top() {
        if(!deque3.isEmpty()){
            return deque3.peek();
        }else {
            return -1;
        }
    }

    public int min() {
        if(!deque4.isEmpty()){
            return deque4.peek();
        }else {
            return -1;
        }
    }

    //29.栈的压入、弹出序列
    //思路：模拟入栈方法，定义一个模拟栈，按照入栈和出栈序列，一个数入栈，然后看出栈序列相对应的位置数据是否一样，如果一样弹出，不一样压入,
    //直到压到最后一个数弹出还不一样，返回false
    public static boolean validateStackSequences(int[] pushed, int[] popped) {
        if(pushed.length == 0){
            return true;
        }
        Deque<Integer> deque=new LinkedList<>();
        int pushIndex=0;
        int popIndex=0;
        //模拟入栈
        while (pushIndex < pushed.length){
            if(deque.isEmpty()){
                //先压入栈
                deque.push(pushed[pushIndex]);
                pushIndex++;
            }
         //判断是否弹出元素
            if(deque.peek() == popped[popIndex]){
                deque.pop();
                popIndex++;
            }else {
                deque.push(pushed[pushIndex]);
                pushIndex++;
            }
        }
        //还有依次判断循环内判断不了，这里再判断依次
        if(!deque.isEmpty()&&deque.peek() == popped[popIndex]){
            deque.pop();
            popIndex++;
        }
        while (!deque.isEmpty()){
            if(deque.pop() != popped[popIndex]){
                return false;
            }
            popIndex++;
        }
        return true;
    }

    //30.从上到下打印二叉树1
    public int[] levelOrder1(TreeNode root) {
        if(root == null){
            return new int[0];
        }
        Queue<TreeNode> queue=new LinkedList<>();//存放结点
        List<Integer> list=new ArrayList<>();//存放结点的值
        queue.add(root);//先将root放入队列中
        //队列不为空时，一直遍历
        while (!queue.isEmpty()){
            //取出队首，放入list中
            TreeNode node=queue.poll();
            list.add(node.val);
            //node存在左孩子，则放入
            if(node.left != null){
                queue.add(node.left);
            }
            //存在右孩子，放入
            if(node.right != null){
                queue.add(node.right);
            }
        }
        //将list转化为数组
        int[] result=new int[list.size()];
        for (int i = 0; i <list.size(); i++) {
            result[i]=list.get(i);
        }
        return result;
    }

    //31.从上到下打印二叉树2(按层打印)
    private static class NewTreeNode{
        public Integer count;//层数
        public TreeNode node;//结点

        public NewTreeNode(Integer count, TreeNode node) {
            this.count = count;
            this.node = node;
        }
    }
    public static List<List<Integer>> levelOrder2(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }
        List<List<Integer>> result=new ArrayList<>();//存放结果的list
        Queue<NewTreeNode> queue=new LinkedList<>();//存放新构造的结点，带层数
        queue.add(new NewTreeNode(1,root));
        while (!queue.isEmpty()){
            NewTreeNode node=queue.poll();
            if(result.size() == node.count){
                result.get(result.size()-1).add(node.node.val);
            }else {
                result.add(new ArrayList<>());
                result.get(result.size()-1).add(node.node.val);
            }
            int count=node.count+1;//新结点的层数（不能写node.count++;
            if(node.node.left != null){
                queue.add(new NewTreeNode(count,node.node.left));
            }
            if(node.node.right != null){
                queue.add(new NewTreeNode(count,node.node.right));
            }
        }
        return result;
    }


    //32.从上到下打印二叉树（之字型）
    public static List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }
        List<List<Integer>> result=new ArrayList<>();//记录结果list
        Queue<NewTreeNode> queue=new LinkedList<>();//使用双端队列记录NewTreeNode
        queue.add(new NewTreeNode(1,root));
        while (!queue.isEmpty()){
            NewTreeNode node=queue.poll();
            if(result.size() == node.count){
                result.get(result.size()-1).add(node.node.val);//还没有遍历到下一层
            }else {
                result.add(new ArrayList<>());//这里是下一层了，要重建list
                result.get(result.size()-1).add(node.node.val);
            }
            int count=node.count+1;//新结点的层数（不能写node.count++;
            if(node.node.left != null){
                queue.add(new NewTreeNode(count,node.node.left));//有左孩子，加入
            }
            if(node.node.right != null){
                queue.add(new NewTreeNode(count,node.node.right));//有右孩子，加入
            }
        }
        for (int i = 0; i <result.size() ; i++) {
            if(i % 2 == 1){
                //从0层开始，奇数层打印转置
                result.set(i, listReserve(result.get(i)));
            }
        }
        return result;
    }
    //list转置
    private static List<Integer> listReserve(List<Integer> integers) {
        List<Integer> newList=new ArrayList<>();
        for (int i = 0; i <integers.size() ; i++) {
            newList.add(integers.get(integers.size()-i-1));
        }
        return newList;
    }

    //33.二叉搜索树的后序遍历序列
    public boolean verifyPostorder(int[] postorder) {
        return _verifyPostorder(postorder, 0, postorder.length - 1);
    }

    boolean _verifyPostorder(int[] postorder, int left, int right) {
        //如果left==right，就一个节点不需要判断了，如果left>right说明没有节点，
        if (left >= right)
            return true;
        //因为数组中最后一个值postorder[right]是根节点，这里从左往右找出第一个比
        //根节点大的值，他后面的都是根节点的右子节点（包含当前值，不包含最后一个值，他前面的都是根节点的左子节点
        int mid = left;
        int root = postorder[right];
        while (postorder[mid] < root)
            mid++;
        int temp = mid;
        //postorder[mid]前面的值都是比根节点root小的，
        //我们还需要确定postorder[mid]后面的值都要比根节点root大，
        //如果后面有比根节点小的直接返回false
        while (temp < right) {
            if (postorder[temp++] < root)
                return false;
        }
        //然后对左右子节点进行递归调用
        return _verifyPostorder(postorder, left, mid - 1) && _verifyPostorder(postorder, mid, right - 1);
    }


    //34.二叉树中和为某一值的路径(从根到叶子结点为1条路径)
    List<List<Integer>> lists=new LinkedList<>();
    LinkedList<Integer> list=new LinkedList<>();
    public List<List<Integer>> pathSum(TreeNode root, int target) {
        _pathSum(root,target);
        return lists;
    }

    private void _pathSum(TreeNode root, int target) {
        if(root == null){
            return;
        }
        list.add(root.val);
        //一旦减为0，且为叶子结点,说明符合
        target-=root.val;
        if(target == 0 &&root.left==null && root.right==null){
            //将list中的值赋给新的list后加入
            lists.add(new LinkedList<>(list));
        }
        _pathSum(root.left, target);
        _pathSum(root.right, target);
        //释放list中的元素
        list.removeLast();
    }

    //35.复杂链表的复制
    private static class Node1 {
        int val;
        Node1 next;
        Node1 random;

        public Node1(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }
    }

    public Node1 copyRandomList(Node1 head) {
        //1.链表为空或只有一个结点时
        if(head == null){
            return head;
        }
        //2. 利用Map键值对，建立新的链表结点和原来链表结点的一对一映射关系，random对应的结点容易找到
        Map<Node1,Node1> map=new HashMap<>();
        Node1 cur=head;
        //建立一对一映射
        while (cur != null){
            map.put(cur,new Node1(cur.val));
            cur=cur.next;
        }
        //再重头遍历，设置value Node的random
        cur=head;
        while (cur != null){
            map.get(cur).next=map.get(cur.next);
            map.get(cur).random=map.get(cur.random);
            cur=cur.next;
        }
        return map.get(head);
    }


    //36.二叉搜索树与双向链表
    private static class Node {
        public int val;
        public Node left;
        public Node right;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val,Node _left,Node _right) {
            val = _val;
            left = _left;
            right = _right;
        }
    };
    List<Node> nodelist = new ArrayList<>();
    public Node treeToDoublyList(Node root) {
        if (null == root) return null;
        inOrder(root);
        //修改链表的指向
        int size = nodelist.size();
        for (int i = 0; i < size; i++) {
            //修改左边
            if (i == 0) {
                nodelist.get(i).left = nodelist.get(size - 1);
            } else {
                nodelist.get(i).left = nodelist.get(i - 1);
            }
            //修改右边
            if (i == size - 1) {
                nodelist.get(i).right = nodelist.get(0);
            } else {
                nodelist.get(i).right = nodelist.get(i + 1);
            }
        }

        return nodelist.get(0);
    }
    //中序遍历，结点加到nodeList中
    private void inOrder(Node root) {
        if (null == root) return;
        inOrder(root.left);
        nodelist.add(root);
        inOrder(root.right);
    }

    //37.序列化二叉树


    //38.字符串的排列


    //39.数组中出现次数超过一半的数字
    private static class New{
        public int value;//记录值
        public int count;//记录出现次数

        public New(int value, int count) {
            this.value = value;
            this.count = count;
        }
    }
    public int majorityElement(int[] nums) {
        //1. 数组长度为0，1时
        if(nums.length == 0){
            return 0;
        }
        if(nums.length == 1){
            return nums[0];
        }
        List<New> integerList=new ArrayList<>();
        integerList.add(new New(nums[0],1));
        for (int i = 1; i <nums.length ; i++) {
            boolean temp=false;//判断list中是否已经有了nums[i]
            int listIndex=0;//记录nums[i]在list中的位置
            for (int j = 0; j < integerList.size() ; j++) {
                if(integerList.get(j).value == nums[i]){
                    temp=true;
                    listIndex=j;
                    break;
                }
            }
            if(temp){
                //如果找到了
                integerList.get(listIndex).count=integerList.get(listIndex).count+1;
            }else {
                integerList.add(new New(nums[i],1));
            }
        }
        for (int i = 0; i <integerList.size() ; i++) {
            if(integerList.get(i).count > nums.length/2){
                return integerList.get(i).value;
            }
        }
        return 0;
    }


    //39.最小的k个数
    public int[] getLeastNumbers(int[] arr, int k) {
        if(arr == null || arr.length < k){
            return new int[0];
        }
        Arrays.sort(arr);
        int[] result=new int[k];
        for (int i = 0; i <k ; i++) {
            result[i]=arr[i];
        }
        return result;
    }


    //40.数据流中的中位数(超时，要使用堆)
     List<Integer> middle=new LinkedList<>();
    public  void addNum(int num) {
        //1.如果为空，直接添加  如果大于等于最后一个数，直接添加
        if(middle.size() == 0 || num>=middle.get(middle.size()-1)){
            middle.add(num);
            return;
        }
        middle.add(num);
        middle.sort(null);
    }

    public  double findMedian() {
        if(middle.size() < 1){
            return 0;
        }
        //1.middle长度为奇数
        if(middle.size() % 2 == 1){
            return middle.get(middle.size()/2);
        }else {
            //2.middle长度为偶数
            return (double)(middle.get(middle.size()/2-1)+middle.get(middle.size()/2))/2;
        }
    }

    //41.连续子数组的最大和


    public static void main(String[] args) {
        int[] arr={1,2,3,4,5};
        int[] result=constructArr(arr);
        System.out.println(Arrays.toString(result));
    }


    //71.不用加减乘除做加法
    public int add(int a, int b) {
        while (b != 0) {
            int temp = a ^ b;
            b = (a & b) << 1;
            a = temp;
        }
        return a;
    }

    //72.构建乘积数组
    public static int[] constructArr(int[] a) {
        if(a == null || a.length == 0){
            return new int[0];
        }
        //1.定义两个新数组，记录当前位置的a的左右乘积
        int index=0;
        int []left=new int[a.length];
        int []right=new int[a.length];
        left[0]=1;
        right[a.length-1]=1;
        for (int i = 1 ; i < a.length; i++) {
            left[i]=left[i-1] * a[i - 1];
        }
        for (int i = a.length - 2; i >= 0 ; i--) {
            right[i]=right[i+1] * a[i+1];
        }
        //2.a的左边数的乘积乘以右边数的乘积
        int [] result=new int[a.length];
        for (int i = 0; i <a.length ; i++) {
            result[i]=left[i]*right[i];
        }
        return result;
    }
    //73.把字符串转化成数字
    public static int strToInt(String str) {
        if(str == null || str.equals("")){
            return 0;
        }
        String temp=str.trim();
        if(temp.length()<=0){
            return 0;
        }
        //判断是否是负数
        boolean result=false;
        StringBuilder stringBuilder=new StringBuilder();
        if(temp.charAt(0)=='-'){
            //开头是负数
            result=true;
            for (int i = 1; i <temp.length() ; i++) {
                //如果是数字
                if(Character.isDigit(temp.charAt(i))){
                    //是数字，拼接
                    stringBuilder.append(temp.charAt(i));
                }else {
                    //不是数字，结束循环
                    break;
                }
            }
        }else if(!Character.isDigit(temp.charAt(0))){
            //开头不是数字

            //开头是+号
            if (temp.charAt(0)=='+') {
                for (int i = 1; i <temp.length() ; i++) {
                    //如果是数字
                    if(Character.isDigit(temp.charAt(i))){
                        //是数字，拼接
                        stringBuilder.append(temp.charAt(i));
                    }else {
                        //不是数字，结束循环
                        break;
                    }
                }
            }else {
                //不是加号
                return 0;
            }

        }
        else {
            //开头是数字
            for (int i = 0; i <temp.length() ; i++) {
                //如果是数字
                if(Character.isDigit(temp.charAt(i))){
                    //是数字，拼接
                    stringBuilder.append(temp.charAt(i));
                }else {
                    //不是数字，结束循环
                    break;
                }
            }
        }
        //将stringBuilder转化为数字
        long count=0;
        //字符0对应的数值是48，依次类推
        for (int i = 0; i <stringBuilder.length() ; i++) {
            count+=((int)stringBuilder.charAt(i)-'0')*Math.pow(10.0, stringBuilder.length()-i-1);
        }
        //判断正负数
        if(result){
            count=-count;
        }
        //判断是否超出了int范围
        if(count>Integer.MAX_VALUE){
            return Integer.MAX_VALUE;
        }else if(count<Integer.MIN_VALUE){
            return Integer.MIN_VALUE;
        }
        return (int)count;
    }


    //74.二叉搜索树的最近公共祖先（1）
    public TreeNode lowestCommonAncestor1(TreeNode root, TreeNode p, TreeNode q) {
        //1.root为空或root==p==q
        if(root == null ||(root == p && root == q)){
            return root;
        }
        //2.在p的子树中找q，或在q的子树中找p
        if(_findChild(p,q)){
            return p;
        }
        if(_findChild(q,p)){
            return q;
        }
        //3.当第一次p和q在一个结点的左右两边时，返回这个结点
        return  _lowestCommonAncestor(root,p,q);
    }

    private boolean _findChild(TreeNode p, TreeNode q) {
        if(p == null){
            return false;
        }
        if(p.left == q || p.right == q){
            return true;
        }
        if(_findChild(p.left,q)){
            return true;
        }
        return _findChild(p.right,q);
    }

    private TreeNode  _lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        int max=Math.max(p.val,q.val) ;
        int min=Math.min(p.val,q.val);
        //层序遍历
        Queue<TreeNode> queue=new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()){
            TreeNode node=queue.poll();
            if(node.val > min && node.val < max){
                return node;
            }
            if(node.left != null){
                queue.add(node.left);
            }
            if(node.right != null){
                queue.add(node.right);
            }
        }
        return null;
    }


    //75.二叉树的最近公共祖先(2)
    TreeNode lowest=null;//记录结果
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        //1.root为空或root==p==q
        if(root == null ||(root == p && root == q)){
            return root;
        }
        //2.在p的子树中找q，或在q的子树中找p
        if(_findOneChild(p,q)){
            return p;
        }
        if(_findOneChild(q,p)){
            return q;
        }
        //3.一个在左子树中，一个在右子树中
        if(_findChilds(root,p,q)){
            return root;
        }
        //4.在左子树中找到，记录到lowest
        lowest=lowestCommonAncestor(root.left,p,q);
        //5.左子树中没找到，去右子树找
        if (lowest == null) {
            lowest=lowestCommonAncestor(root.right,p,q);
        }
        return lowest;
    }
    //root的左右子树分别找到了p，q中的一个
    private boolean _findChilds(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null){
            return false;
        }
        return (_findOneChild(root.left, p) && _findOneChild(root.right, q)) || (_findOneChild(root.left, q) && _findOneChild(root.right, p));
    }

    //在root的子树中找p
    private boolean _findOneChild(TreeNode root, TreeNode p) {
        if(root == null){
            return false;
        }
        if(root == p){
            return true;
        }
        if(_findOneChild(root.left,p)){
            return true;
        }
        return _findOneChild(root.right,p);
    }


}