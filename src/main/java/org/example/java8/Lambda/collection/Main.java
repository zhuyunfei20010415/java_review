package org.example.java8.Lambda.collection;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName Main
 * @Author 朱云飞
 * @Date 2021/7/6 15:23
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        User user1=new User("小明",12);
        User user2=new User("小红",13);
        User user3=new User("小张",14);
        ArrayList<User> list=new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        //Stream流的创建
        Stream<User> stream = list.stream();//单线程版
        Stream<User> userStream = list.parallelStream();//多线程版
        //list转Set
        Set<User> collect = stream.collect(Collectors.toSet());
        collect.forEach((User)-> System.out.println(User.toString()));

        //list转map
        //key:user.getName  value:user
        stream.collect(Collectors.toMap(
                user -> user.getName(),
                user -> user)).forEach((k,v)->{
            System.out.println("key:"+k+"value"+v);
        });

        //Stream流求和
        Optional<User> sum = stream.reduce((user, user21) -> {
            User sum1 = new User("sum", user.getAge() + user21.getAge());
            return sum1;
        });
        System.out.println(sum.get().getAge());

        //Stream求最大最小，实现Comparator接口
        Optional<User> max = stream.max((o1, o2) -> o1.getAge() - o2.getAge());
        Optional<User> min = stream.min((o1, o2) -> o1.getAge() - o2.getAge());

        //Stream匹配集合中的数据，返回boolean
        boolean b = stream.anyMatch((User) -> {
            return "小红".equals(User.getName());
            //return 13==User.getAge();
        });

        //Stream流过滤器的使用
        stream.filter((User)->{
            return "小红".equals(User.getName());
        }).forEach(System.out::println);

        //Stream的Limit
        stream.limit(2).forEach(System.out::println);

        //Stream流实现排序
        stream.sorted((o1, o2) -> {
            return o1.getAge()-o2.getAge();
        }).forEach(System.out::println);
    }



}
