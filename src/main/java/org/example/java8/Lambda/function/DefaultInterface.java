package org.example.java8.Lambda.function;

/**
 * 函数式接口
 * @InterfaceName DefaultInterface
 * @Author 朱云飞
 * @Date 2021/7/6 10:15
 * @Version 1.0
 **/
@FunctionalInterface
public interface DefaultInterface {
    void get();
    /**
     * 接口中可以使用default static来修饰方法，则可以写方法体
     */
    default void eat(){
        System.out.println("eat");
    }

    static void run(){
        System.out.println("run");
    }

    //重写Object类的方法
    String toString();
}
