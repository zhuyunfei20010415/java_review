package org.example.java8.Lambda.function;

/**
 * @InterfaceName DefaultInterface2
 * @Author 朱云飞
 * @Date 2021/7/6 10:56
 * @Version 1.0
 **/
@FunctionalInterface
public interface DefaultInterface2 {
    int add(int i,int j);
}
