package org.example.java8.Lambda.function;

/**
 * @ClassName Main
 * @Author 朱云飞
 * @Date 2021/7/6 10:56
 * @Version 1.0
 **/
public class Main {
    public static void main(String[] args) {
        //Lambda无参的调用方法
        DefaultInterface defaultInterface=()->{
            System.out.println("无参Lambda");
        };
        defaultInterface.get();

        ((DefaultInterface)()-> System.out.println("无参Lambda")).get();

        DefaultInterface2 defaultInterface2=(i,j)->{
            return i+j;
        };
        System.out.println(defaultInterface2.add(1, 1));
        System.out.println(((DefaultInterface2) (i, j) -> i + j).add(1, 1));
    }
}
