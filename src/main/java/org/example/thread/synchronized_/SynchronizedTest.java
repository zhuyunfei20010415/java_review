package org.example.thread.synchronized_;

/**
 * @ClassName SynchronizedTest
 * @Author 朱云飞
 * @Date 2022/3/13 15:20
 * @Version 1.0
 **/
public class SynchronizedTest implements Runnable{
    public synchronized void test(){
        Main.count++;
        System.out.println(Main.count);
    }

    @Override
    public void run() {
        for(int i=0;i<100000;i++){
            test();
        }
    }
}
