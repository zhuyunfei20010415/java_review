package org.example.thread.synchronized_;

/**
 * @ClassName Main
 * @Author 朱云飞
 * @Date 2022/3/13 15:25
 * @Version 1.0
 **/
public class Main {
    public static int count=0;

    public static void main(String[] args) {
        Thread[] threads = new Thread[10];
       for(int i=0;i<threads.length;i++){
           threads[i] = new Thread(new SynchronizedTest());
       }
       for(int i=0;i<threads.length;i++){
           threads[i].start();
       }
    }
}
