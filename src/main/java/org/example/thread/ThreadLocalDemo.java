package org.example.thread;

/**
 * @ClassName ThreadLocalDemo
 * @Author 朱云飞
 * @Date 2021/7/13 10:48
 * @Version 1.0
 **/
public class ThreadLocalDemo {
    private String content;

    ThreadLocal<String> threadLocal=new ThreadLocal<>();
    public String getContent() {
        return threadLocal.get();
    }

    public void setContent(String content) {
        threadLocal.set(content);
    }

    public static void main(String[] args) {
        ThreadLocalDemo demo=new ThreadLocalDemo();
        for (int i = 0; i <10 ; i++) {
            Thread thread=new Thread(()->{
                demo.setContent(Thread.currentThread().getName()+"的数据");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"---->"+demo.getContent());
            });
            thread.setName("线程"+i);
            thread.start();
        }

    }
}
