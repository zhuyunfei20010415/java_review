package org.example.thread.practice;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName Bread3
 * @Author 朱云飞
 * @Date 2021/12/4 14:27
 * @Version 1.0
 **/
public class Bread3 {
    private static class Producer implements Runnable{
        public static Random random = new Random(7777);
        public ReentrantLock lock;
        public Condition full;
        public Condition empty;
        public Queue<Integer> queue;
        public int maxSize;
        Producer(ReentrantLock lock, Condition full,Condition empty, Queue<Integer> queue, int maxSize){
            this.lock = lock; this.queue = queue;
            this.full = full; this.empty = empty;
            this.maxSize = maxSize;
        }
        public void run() {
            try{
                while(true) {
                    lock.lock();
                    while(queue.size() ==  maxSize) full.await();
                    int number = random.nextInt(100);
                    queue.offer(number);
                    System.out.println(Thread.currentThread().toString() + "produce:   " + String.valueOf(number));
                    empty.signalAll();
                    lock.unlock();
                    Thread.sleep(3000);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static class Consumer implements Runnable{
        ReentrantLock lock;
        Condition full;
        Condition empty;
        Queue<Integer> queue;
        public Consumer(ReentrantLock lock, Condition full, Condition empty, Queue<Integer> queue) {
            this.lock = lock; this.full = full;
            this.empty = empty; this.queue = queue;
        }

        public void run() {
            try{
                while(true) {
                    lock.lock();
                    while(queue.size() == 0) empty.await();
                    int number = queue.poll();
                    System.out.println(Thread.currentThread().toString() + "Consumer: " + String.valueOf(number));
                    full.signalAll();
                    lock.unlock();
                    Thread.sleep(2000);
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

        public static void main(String []args) {
            ReentrantLock lock = new ReentrantLock();
            Condition proCondition = lock.newCondition();
            Condition conCondition = lock.newCondition();
            LinkedList<Integer> queue = new LinkedList<>();
            ExecutorService executor = Executors.newFixedThreadPool(4);

            executor.submit(new Producer(lock, conCondition, proCondition, queue, 10));
            executor.submit(new Producer(lock, conCondition, proCondition, queue, 10));
            executor.submit(new Consumer(lock, conCondition, proCondition, queue));
            executor.submit(new Consumer(lock, conCondition, proCondition, queue));
            try {
                Thread.sleep(60000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            executor.shutdown();
        }
}
