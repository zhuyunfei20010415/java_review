package org.example.thread.practice;

/**
 * 生产者消费者问题
 * @ClassName Bread
 * @Author 朱云飞
 * @Date 2021/12/4 11:12
 * @Version 1.0
 **/
public class Bread1 {
    private static int count=0;//面包数
    private static final Object producerLock=new Object();
    private static final Object consumerLock=new Object();
    private static class Producer implements Runnable{
        private String name;

        public Producer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            while (true){
                synchronized (producerLock){
                    while(count > 300){
                        try {
                            producerLock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName()+"生产了一个面包");
                    count++;
                    consumerLock.notifyAll();
                }
            }
        }
    }


    private static class Consumer implements Runnable{
        private String name;

        public Consumer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            while (true){
                synchronized (consumerLock){
                    while (count <= 0){
                        try {
                            consumerLock.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(Thread.currentThread().getName()+"消费了一个面包");
                    count--;
                    producerLock.notifyAll();
                }
            }
        }
    }

    public void start(){
        Thread[] producers=new Thread[10];
        Thread[] consumers=new Thread[10];
        for(int i=0;i<10;i++){
            producers[i]=new Thread(new Producer(String.valueOf(i)));
        }
        for(int i=0;i<10;i++){
            consumers[i]=new Thread(new Consumer(String.valueOf(i)));
        }
        for(Thread thread : producers){
            thread.start();
        }
        for(Thread thread : consumers){
            thread.start();
        }
    }

    public static void main(String[] args) {
        Bread bread=new Bread();
        bread.start();
    }
}
