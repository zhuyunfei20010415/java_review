package org.example.thread.practice;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName PrintABC3
 * @Author 朱云飞
 * @Date 2021/12/3 22:28
 * @Version 1.0
 **/
public class PrintABC3 {
    private static  int state=0;
    private static ReentrantLock lock=new ReentrantLock();

    Thread t1=new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    lock.lock();
                    while (state % 3 ==0){
                        System.out.println("A");
                        state++;
                    }
                } finally {
                    lock.unlock();
                }
            }
        }
    });

    Thread t2=new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    lock.lock();
                    while (state % 3 ==1){
                        System.out.println("B");
                        state++;
                    }
                } finally {
                    lock.unlock();
                }
            }
        }
    });

    Thread t3=new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    lock.lock();
                    while (state % 3 ==2){
                        System.out.println("C");
                        state++;
                    }
                } finally {
                    lock.unlock();
                }
            }
        }
    });

    private void start(){
        t1.start();
        t2.start();
        t3.start();
    }

    public static void main(String[] args) {
        PrintABC3 p=new PrintABC3();
        p.start();
    }
}
