package org.example.thread.practice;

/**
 * 面包店
 * 10个生产者，每个每次生产3个
 * 20个消费者，每个每次消费一个
 *
 * 进阶版需求
 * 面包师傅每个最多生产30次，面包店每天生产10*30*3=900个面包
 * 消费者也不是一直消费。把900个面包消费完结束
 *
 * 隐藏信息：面包店每天生产面包的最大数量为900个
 *            消费者把900个面包消费完结束
 */


public class AdvancedBreadShop {
    //面包店库存数
    private static int COUNT;

    //面包店生产面包的总数,不会消费的
    private static int PRODUCE_NUMBER;


    public static class Consumer implements Runnable{
        private String name;


        public Consumer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                while (true){
                    synchronized (AdvancedBreadShop.class){
                        if(PRODUCE_NUMBER==900&&COUNT==0){
                            System.out.println("今天面包已经卖完了");
                            break;
                        }else {
                            if(COUNT==0){
                                AdvancedBreadShop.class.wait();
                            }else {
                                System.out.printf("%s消费了一个面包\n",this.name);
                                COUNT--;
                                AdvancedBreadShop.class.notifyAll();
                                Thread.sleep(100);
                            }
                        }
                    }
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    private static class Producer implements Runnable{
        private String name;


        public Producer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                //生产者生产30次，结束循环
                for(int i=0;i<=30;i++) {
                    synchronized (AdvancedBreadShop.class){
                        if(i==30){
                            System.out.println("今天面包生产完了");
                            break;
                        }else {
                            if(COUNT>97){
                                AdvancedBreadShop.class.wait();
                            }else {
                                COUNT=COUNT+3;
                                PRODUCE_NUMBER=PRODUCE_NUMBER+3;
                                System.out.printf("%s生产了三个面包\n",this.name);
                                AdvancedBreadShop.class.notifyAll();
                                Thread.sleep(100);
                            }
                        }
                    }
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args) {
        Thread[] Consumers=new Thread[20];
        Thread[] Producers=new Thread[10];
        for (int i = 0; i <20 ; i++) {
            Consumers[i]=new Thread(new Consumer(String.valueOf(i)));
        }
        for (int i = 0; i <10 ; i++) {
            Producers[i]=new Thread(new Producer(String.valueOf(i)));
        }
        for (int i = 0; i <20 ; i++) {
            Consumers[i].start();
        }
        for (int i = 0; i <10 ; i++) {
            Producers[i].start();
        }
    }
}
