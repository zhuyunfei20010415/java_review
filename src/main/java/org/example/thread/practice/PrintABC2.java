package org.example.thread.practice;

/**
 * @ClassName PrintABC2
 * @Author 朱云飞
 * @Date 2021/12/3 22:26
 * @Version 1.0
 **/
public class PrintABC2 {
    public static int count = 0;
    Thread t1 = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                synchronized (PrintABC2.class) {
                    if (count % 3 == 0) {
                        System.out.println("A");
                        count++;
                        notifyAll();
                    } else
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    });
    Thread t2 = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                synchronized (PrintABC2.class) {
                    if (count % 3 == 1) {
                        System.out.println("B");
                        count++;
                        notifyAll();
                    } else
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    });
    Thread t3 = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                synchronized (PrintABC2.class) {
                    if (count % 3 == 2) {
                        System.out.println("C");
                        count++;
                        notifyAll();
                    } else
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
            }
        }
    });

    public void fun() {
        t3.start();
        t1.start();
        t2.start();
    }

    public static void main(String[] args) {
        PrintABC1 tp = new PrintABC1();
        tp.fun();
    }
}
