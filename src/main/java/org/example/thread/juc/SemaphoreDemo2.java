package org.example.thread.juc;

import java.util.concurrent.Semaphore;

/**
 * @ClassName SemaphoreDemo2
 * @Author 朱云飞
 * @Date 2021/7/12 19:08
 * @Version 1.0
 **/
public class SemaphoreDemo2 {
    //有限资源的争夺  这里定义5个有限资源
    public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(5);//5个有限资源
        for (int i = 0; i < 10; i++) {//10个线程抢5个资源
            new Thread(()->{
                try {
                    semaphore.acquire();//占用一个资源
                    System.out.println(Thread.currentThread().getName()+"占用了一个资源");
                    Thread.sleep(1000);
                    semaphore.release();//释放了一个资源
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
