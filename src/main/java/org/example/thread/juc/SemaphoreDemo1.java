package org.example.thread.juc;

/**
 * @ClassName SemaphoreDemo1
 * @Author 朱云飞
 * @Date 2021/7/12 18:56
 * @Version 1.0
 **/
public class SemaphoreDemo1 {

    //10个线程执行完毕时。主线程才能执行
    public static void main(String[] args) {
        java.util.concurrent.Semaphore semaphore=new java.util.concurrent.Semaphore(0);//初始时没有线程释放，这里设置为0
        for (int i = 0; i <10 ; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName()+"线程已经执行完毕，释放资源");
                semaphore.release();//释放一个信号量
            }).start();
        }
        try {
            semaphore.acquire(10);//需要信号量达到10，才执行
            System.out.println("main线程执行完毕");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
