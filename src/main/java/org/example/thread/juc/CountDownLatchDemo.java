package org.example.thread.juc;

import java.util.concurrent.CountDownLatch;

/**
 * @ClassName CountDownLatchDemo
 * @Author 朱云飞
 * @Date 2021/7/12 19:21
 * @Version 1.0
 **/
public class CountDownLatchDemo {
    //案例：main线程在10个子线程执行完毕后才能执行
    public static void main(String[] args) {
        CountDownLatch countDownLatch=new CountDownLatch(10);//10个线程
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"已经执行");
                countDownLatch.countDown();//countDownLatch减一
            }).start();
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("主线程执行完毕");
    }

}
