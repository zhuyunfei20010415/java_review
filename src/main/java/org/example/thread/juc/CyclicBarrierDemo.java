package org.example.thread.juc;

import java.util.concurrent.CyclicBarrier;

/**
 * @ClassName CyclicBarrierDemo
 * @Author 朱云飞
 * @Date 2021/7/12 19:39
 * @Version 1.0
 **/
public class CyclicBarrierDemo {
    //循环栅栏
    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier=new CyclicBarrier(4);
        for (int i = 0; i <10 ; i++) {
            new Thread(()->{
                try {
                    cyclicBarrier.await();
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+"已经执行");
            }).start();
        }
    }
}
