package org.example.thread.thread_case;

/**
 * 生产者和消费者模型
 *
 *10个生产者，每个每次生产3个
 *20个消费者，每个每次消费一个
 *
 * @ClassName ProducerAndCustomer
 * @Author 朱云飞
 * @Date 2021/7/11 16:19
 * @Version 1.0
 **/
public class ProducerAndCustomer {
    private static int count;//库存数

    public static class  Customer implements Runnable{
        public String name;

        public Customer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                while (true){
                    synchronized (ProducerAndCustomer.class){
                        if(count <= 0){
                            ProducerAndCustomer.class.wait();
                        }else {
                            count--;
                            System.out.println(this.name+"消费了一个面包,还剩"+count+"个面包");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static class Producer implements Runnable{
        public String name;

        public Producer(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            try {
                while (true){
                    synchronized (ProducerAndCustomer.class){
                        count++;
                        ProducerAndCustomer.class.notifyAll();
                        //Thread.sleep(1000);
                        System.out.println(this.name+"生产了一个面包，还剩"+count+"个面包");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Thread[] producers=new Thread[2];
        Thread[] customer =new Thread[5];
        for (int i = 0; i <producers.length ; i++) {
            producers[i]=new Thread(new Producer(String.valueOf(i)));
        }
        for (int i = 0; i <customer.length ; i++) {
            customer[i]=new Thread(new Customer(String.valueOf(i)));
        }
        for (int i = 0; i <customer.length ; i++) {
            customer[i].start();
        }
        for (int i = 0; i <producers.length ; i++) {
            producers[i].start();
        }
    }

}
