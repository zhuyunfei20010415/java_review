package org.example.thread.thread_case;

/**
 * 单例模式
 * @ClassName Singleton
 * @Author 朱云飞
 * @Date 2021/7/11 14:57
 * @Version 1.0
 **/
public class Singleton {
    private static Singleton instance=null;
    private Singleton(){}
    public synchronized static Singleton getInstance(){
        if(instance == null){
            instance=new Singleton();
        }
        return instance;
    }
}
