package org.example.data_source.Tree;

/**
 * @ClassName AVL
 * @Author 朱云飞
 * @Date 2021/7/15 19:58
 * @Version 1.0
 **/
public class AVL {
    //1.使用泛型,且带Value
    private static class Node1<K,V>{
        Node1<K,V> left;
        Node1<K,V> right;
        K key;
        V value;
        int bf;//平衡因子
        Node1<K,V> parent;
    }
    //2.不使用泛型，不带value
    private static class Node2{
        Node2 left;
        Node2 right;
        int value;
        int bf;//平衡因子
        Node2 parent;
    }
}
