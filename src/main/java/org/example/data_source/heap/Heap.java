package org.example.data_source.heap;

import java.util.Arrays;

/**
 * @ClassName Heap
 * @Author 朱云飞
 * @Date 2021/7/14 16:36
 * @Version 1.0
 **/
//这里以小根堆为例
public class Heap {
    /**
     * 小根堆的向下调整
     * @param arr 待调整的数组
     * @param index 待调整的数组索引
     * @param size  数组的长度
     */
    public static void adjustDown(int[] arr,int index,int size){
        //1.数组长度小于1，不需要调整
        if(size < 2){
            return;
        }
        while (true) {
            //2.判断索引为in dex的结点是否是叶子结点(左孩子的索引index * 2 + 1)
            int leftIndex=index *2 + 1;
            if(leftIndex >= size){
                return;
            }
            //3.找到最小的孩子
            int minIndex=leftIndex;
            int rightIndex=leftIndex+1;//右孩子
            if(rightIndex < size && arr[rightIndex] < arr[leftIndex]){
                minIndex=rightIndex;
            }
            //4.判断：如果index位置的值比最小的孩子还要小或相等，直接返回
            if(arr[index] <= arr[minIndex]){
                return;
            }
            //5.如果index我i之的值要大，则与最小孩子的值交换
            int t=arr[index];
            arr[index]=arr[minIndex];
            arr[minIndex]=t;
            //5.将minIndex视为index，循环回去
            index=minIndex;
        }
    }

    /**
     * 建堆
     * @param array
     * @param size
     */
    public static void buildHeap(int[] array,int size){
        //1.如果数组只有一个元素，直接返回
        if(size < 2){
            return;
        }
        //2.找到最后一个结点
        int lastIndex=size-1;
        //3.找到最后一个父结点
        int lastParentIndex=(lastIndex - 1)/2;
        //4.从最后一个父节点开始，一直到根结点，都进行向下调整 [lastParentIndex,0]
        for (int i = lastParentIndex; i >=0 ; i--) {
            adjustDown(array,i,size);
        }
    }

    /**
     * 测试
     */
    public static void main(String[] args) {
        int[] arr={35,7,52,6,19,8,3,6,9,7,4,1,3};
        buildHeap(arr,arr.length);
        System.out.println(Arrays.toString(arr));
    }
}
