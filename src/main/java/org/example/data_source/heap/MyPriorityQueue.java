package org.example.data_source.heap;

/**
 * @ClassName MyPriorityQueue
 * @Author 朱云飞
 * @Date 2021/7/14 17:47
 * @Version 1.0
 **/
//优先级队列，这里以小根堆为例
public class MyPriorityQueue {
    private Integer array[];
    private int size;

    public MyPriorityQueue() {
        array=new Integer[100];//这里为了简单期间，不考虑扩容的情况
    }

    //添加元素
    public void addElement(Integer element){
        //1.如果数组为空，直接添加
        if(size == 0){
            array[0]=element;
            size++;
            return;
        }
        //2. 添加到数组的最后一个元素，进行一次向上调整
        array[size]=element;
        size++;
        adjustUp(array,size,size-1);
    }

    /**
     * 删除队头元素，并返回
     * 1.判断数组元素个数是否小于2
     * 2.将数组的第一个元素array[0]取出
     * 3.将最后一个元素array[size-1]放到第一个数的位置
     * 4.对第一个数进行一次向下调整
     * @return
     */
    public Integer remove(){
        //1.判断队列中元素个数是否小于2
        if(size == 0){
            throw new RuntimeException("队列已经为空");
        }
        if(size == 1){
            return array[0];
        }
        //2.将数组的第一个元素array[0]取出
        int temp=array[0];
        //3.将最后一个元素array[size-1]放到第一个数的位置
        array[0]=array[size-1];
        size--;
        //4.对第一个数进行一次向下调整
        adjustDown(array,0,size);
        //5.返回temp
        return temp;
    }

    /**
     * 小根堆的向下调整
     * @param arr 待调整的数组
     * @param index 待调整的数组索引
     * @param size  数组的长度
     */
    public static void adjustDown(Integer[] arr,int index,int size){
        //1.数组长度小于1，不需要调整
        if(size < 2){
            return;
        }
        while (true) {
            //2.判断索引为in dex的结点是否是叶子结点(左孩子的索引index * 2 + 1)
            int leftIndex=index *2 + 1;
            if(leftIndex >= size){
                return;
            }
            //3.找到最小的孩子
            int minIndex=leftIndex;
            int rightIndex=leftIndex+1;//右孩子
            if(rightIndex < size && arr[rightIndex] < arr[leftIndex]){
                minIndex=rightIndex;
            }
            //4.判断：如果index位置的值比最小的孩子还要小或相等，直接返回
            if(arr[index] <= arr[minIndex]){
                return;
            }
            //5.如果index我i之的值要大，则与最小孩子的值交换
            int t=arr[index];
            arr[index]=arr[minIndex];
            arr[minIndex]=t;
            //5.将minIndex视为index，循环回去
            index=minIndex;
        }
    }

    /**
     * 小根堆的向上调整
     * @param array 待调整的数组
     * @param index 待调整的数组索引
     * @param size  数组的长度
     */
    public static void adjustUp(Integer[]array,int size,int index){
        //1.判断数组个数，小于2，直接返回
        if(size < 2){
            return;
        }
        while (true) {
            //2.判断索引位置是不是根结点，如果是，直接返回
            int parentIndex= (index-1)/2;//index父亲结点的索引
            if(parentIndex < 0){
                return;
            }
            //3.比较index和parentIndex的值，如果array[index]>=array[parentIndex],调整结束
            if(array[index] >= array[parentIndex]){
                return;
            }
            //4.否则交换
            int t=array[index];
            array[index]=array[parentIndex];
            array[parentIndex]=t;

            //5.将parentIndex视为index，继续循环
            index=parentIndex;
        }
    }

    public static void main(String[] args) {
        MyPriorityQueue queue=new MyPriorityQueue();
        queue.addElement(7);
        queue.addElement(3);
        queue.addElement(37);
        System.out.println(queue.remove());
        queue.addElement(6);
        queue.addElement(31);
        queue.addElement(1);
        queue.addElement(-3);
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());
    }
}
