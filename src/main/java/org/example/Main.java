package org.example;

import java.util.*;

public class Main {
    List<String> list = new LinkedList<>();
    Map<Character,Character[]> map = new HashMap<>();
    public List<String> letterCombinations(String digits) {
        char[] digitChar = digits.toCharArray();
        map.put('2',new Character[]{'a','b','c'});
        map.put('3',new Character[]{'d','e','f'});
        map.put('4',new Character[]{'g','h','i'});
        map.put('5',new Character[]{'j','k','l'});
        map.put('6',new Character[]{'m','n','o'});
        map.put('7',new Character[]{'p','q','r','s'});
        map.put('8',new Character[]{'t','u','v'});
        map.put('9',new Character[]{'w','x','y','z'});
        backTrack(digitChar,new StringBuilder(""),0);
        return list;
    }
    private void backTrack(char[] digitChar,StringBuilder track,int index){
        //满足条件
        if(track.length() == digitChar.length){
            list.add(track.toString());
            return;
        }
        //有哪些选择
        Character[] temp = map.get(digitChar[index]);
        //开始做选择
        for(int i=0;i<temp.length;i++){
            track.append(temp[i]);
            //下层决策树
            backTrack(digitChar,track,index+1);
            //还原
            track.deleteCharAt(index);
        }
    }
}
