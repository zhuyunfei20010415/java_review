package org.example.JVM;

/**
 * @ClassName Test
 * @Author 朱云飞
 * @Date 2021/7/28 16:51
 * @Version 1.0
 **/
public class Test {
    public static void main(String[] args) {
        createObject();
    }
    public static void createObject(){
        Object[] temp=new Object[10];
        for (int i = 0; i <10 ; i++) {
            temp[i]=new Object();
        }
    }
}
