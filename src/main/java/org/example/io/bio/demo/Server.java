package org.example.io.bio.demo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 服务端
 *
 * @ClassName Server
 * @Author 朱云飞
 * @Date 2021/8/5 15:13
 * @Version 1.0
 **/
public class Server {
    public static void main(String[] args)  {
        try {
            ServerSocket serverSocket=new ServerSocket(8080);
            Socket socket=serverSocket.accept();
            InputStream stream=socket.getInputStream();
            //包装成字符输入流
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(stream));
            String msg=null;
            if((msg = bufferedReader.readLine()) != null){
                System.out.println("服务端已经接收到了"+msg);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
