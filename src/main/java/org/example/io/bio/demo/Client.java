package org.example.io.bio.demo;

import java.io.*;
import java.net.Socket;

/**
 * 客户端
 *
 * @ClassName Client
 * @Author 朱云飞
 * @Date 2021/8/5 15:13
 * @Version 1.0
 **/
public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket=new Socket("127.0.0.1",8080);
        OutputStream os=socket.getOutputStream();
        PrintStream printStream=new PrintStream(os);
        printStream.println("客户端发送请求");
        printStream.flush();
    }
}
